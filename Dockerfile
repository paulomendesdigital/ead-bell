FROM php:7.2-apache

ENV APP_HOME /var/www/html

RUN apt-get update && apt-get install -y \
    libicu-dev \
    libpq-dev \
    libmcrypt-dev \
    default-mysql-client \
    git \
    zip \
    unzip \
    zlib1g-dev \
    libpng-dev \
    libjpeg-dev \
    libfreetype6-dev \
    && rm -r /var/lib/apt/lists/* \
    && docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd \
    && docker-php-ext-configure gd --with-jpeg-dir --with-freetype-dir \
    && pecl install mcrypt-1.0.3 \
    && docker-php-ext-enable mcrypt \
    && docker-php-ext-install \
    intl \
    mbstring \
    pcntl \
    pdo_mysql \
    pdo_pgsql \
    pgsql \
    zip \
    opcache \
    gd \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer \
    && usermod -u 1000 www-data && groupmod -g 1000 www-data \
    && sed -i -e "s/html/html\/webroot/g" /etc/apache2/sites-enabled/000-default.conf \
    && a2enmod rewrite

COPY --chown=www-data . $APP_HOME
