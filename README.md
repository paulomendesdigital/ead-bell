# ARQUIVOS DA BASE 

Repositório da API da AGÊNCIA GROW.

## PRÉ-REQUISITOS

- Docker instalado na máquina [DOCKER](https://www.docker.com/)

- TERMINAL DE SUA PREFERÊNCIA [CMADER](https://cmder.net/)


## INSTRUÇÕES DE INSTALAÇÃO
Clonagem do diretório:
```
git clone git@bitbucket.org:grupogrow/mix-manager.git
```

Com o DOCKER instalado, acessar a pasta raiz do projeto, vamos construir a imagem primeiro.
```
$ docker-compose build
```
Quando a compilação é feita,  podemos iniciar o contêiner com o comando abaixo:

```
$ docker-compose up -d
```

Para desligar o container

```
$ docker-compose down
```

### Ambiente de Produção
Para acessar o projeto
```
http://localhost:4000/
```

Acesar ao banco de dados
```
Host: 127.0.0.1
User: root
Pass: root
db: base
```


### Manager de Acl
http://localhost:4000/admin/AclManager


### Bake do manager
#@TODO Nesse momento, é necessário ajustar os controllers do prefixo Manager para usarem o AppController do prefixo, não o AppController principal

Na pasta do projeto, executar os comandos:
bin/cake bake template <nome_tabela> --prefix manager --theme GrupoGrowManager
bin/cake bake controller <nome_tabela> --prefix manager --theme GrupoGrowManager

Ou para o CRUD inteiro:
bin/cake bake all <nome_tabela> --prefix manager --theme GrupoGrowManager


### Upload
Na pasta do projeto, executar o comando:
composer require josegonzalez/cakephp-upload
=======
### Bake de models

Na pasta do projeto, executar o comando:
bin/cake bake model <nome_table>
