const { VueLoaderPlugin } = require('vue-loader'); 
 
module.exports = {
    entry: {  
        promotions_reports: './webroot/js/manager/vue/reports/app.js',
         
    },
    output: {
        path: __dirname + '/webroot/js/vue/dist',
        filename: '[name].js'
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        },
        extensions: ['*', '.js', '.vue', '.json']
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',

                }
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /.css$/,
                use: [
                  'vue-style-loader',
                  'css-loader',
                ]
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin()
    ]
};