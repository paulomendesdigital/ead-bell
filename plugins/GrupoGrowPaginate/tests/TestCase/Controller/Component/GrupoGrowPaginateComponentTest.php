<?php
namespace GrupoGrowPaginate\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use GrupoGrowPaginate\Controller\Component\GrupoGrowPaginateComponent;

/**
 * GrupoGrowPaginate\Controller\Component\GrupoGrowPaginateComponent Test Case
 */
class GrupoGrowPaginateComponentTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \GrupoGrowPaginate\Controller\Component\GrupoGrowPaginateComponent
     */
    public $GrupoGrowPaginate;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->GrupoGrowPaginate = new GrupoGrowPaginateComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->GrupoGrowPaginate);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
