# GrupoGrowPaginate plugin for CakePHP

## Installation TODO

You can install this plugin into your CakePHP application using [composer](https://getcomposer.org).

The recommended way to install composer packages is:

```
composer require your-name-here/GrupoGrowPaginate
```

Enquanto os plugins não forem publicados via composer, a instalação deverá ser feita da seguinte maneira:

- Copiar a pasta GrupoGrowPaginate para a pasta /plugins/ do projeto
- No terminal do container, executar o comando: bin/cake plugin load GrupoGrowPaginate
- No arquivo composer.json acrescentar na chave autoload.psr-4 o valor: "GrupoGrowPaginate\\": "./plugins/GrupoGrowPaginate/src/"
- No arquivo composer.json acrescentar na chave autoload-dev.psr-4 o valor: "GrupoGrowPaginate\\Test\\": "./plugins/GrupoGrowPaginate/tests/"
- No terminal, executar o comando: php composer.phar dumpautoload
