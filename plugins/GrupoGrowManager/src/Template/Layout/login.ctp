<?php

/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * Layout Manager/Login
 */
?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <title>Manager</title>
    <?php echo $this->Html->charset(); ?>
    <?php echo $this->Html->meta('icon'); ?>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <!-- <link rel="apple-touch-icon" href="/theme/Pages/pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/theme/Pages/pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/theme/Pages/pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/theme/Pages/pages/ico/152.png"> -->

    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="<?php echo $Developer['Author']; ?>" name="author" />

    <?php
    // CSS
    echo $this->Html->css('../theme/Pages/assets/plugins/pace/pace-theme-flash');
    echo $this->Html->css('../theme/Pages/assets/plugins/bootstrapv3/css/bootstrap.min');
    echo $this->Html->css('../theme/Pages/assets/plugins/font-awesome/css/font-awesome');
    echo $this->Html->css('../theme/Pages/assets/plugins/jquery-scrollbar/jquery.scrollbar');
    echo $this->Html->css('../theme/Pages/assets/plugins/select2/css/select2.min');
    echo $this->Html->css('../theme/Pages/assets/plugins/switchery/css/switchery.min');
    echo $this->Html->css('../theme/Pages/assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min');
    echo $this->Html->css('../theme/Pages/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min');
    echo $this->Html->css('../theme/Pages/assets/plugins/datatables-responsive/css/datatables.responsive');
    echo $this->Html->css('../theme/Pages/assets/plugins/bootstrap-datepicker/css/datepicker3');
    echo $this->Html->css('../theme/Pages/pages/css/pages-icons');
    echo $this->Html->css('../theme/Pages/pages/css/pages');
    echo $this->Html->css('custom.theme.Pages.css?v='.time());

    echo $this->Html->css('sweetalert/sweetalert');
    echo $this->Html->css('wysihtml5/bootstrap-wysihtml5');
    ?>
    <!--[if lte IE 9]>
      <link href="/theme/Pages/assets/plugins/codrops-dialogFx/dialog.ie.css" rel="stylesheet" type="text/css" media="screen" />
      <link href="/theme/Pages/pages/css/ie9.css" rel="stylesheet" type="text/css" />
      <![endif]-->

    <?php
    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    ?>

  
</head>

<body class="fixed-header" style="background: url(/img/manager/bg-manager.jpg); background-size: cover; background-position: center center;">

    <div class="login-wrapper ">
        <div class="bg-pic">
            <?php //echo $this->Html->image('manager/bg.jpg'); ?>            
            <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
            </div>
        </div>
        <div class="login-container bg-white">
            <?php echo $this->fetch('content'); ?>
        </div>
    </div>
    <?php echo $this->Flash->render(); ?>
    <?php echo $this->Flash->render('Auth'); ?>

    <?php
    // Scripts
    echo $this->Html->script('../theme/Pages/assets/plugins/pace/pace.min');
    if ($this->request->getParam('plugin') != 'acl') :
        //O ACL precisa de uma biblioteca JQUERY própria
        echo $this->Html->script('../theme/Pages/assets/plugins/jquery/jquery-1.11.1.min');
    endif;
    echo $this->Html->script('../theme/Pages/assets/plugins/modernizr.custom');
    echo $this->Html->script('../theme/Pages/assets/plugins/jquery-ui/jquery-ui.min');
    echo $this->Html->script('../theme/Pages/assets/plugins/bootstrapv3/js/bootstrap.min');
    echo $this->Html->script('../theme/Pages/assets/plugins/jquery/jquery-easy');
    echo $this->Html->script('../theme/Pages/assets/plugins/jquery-unveil/jquery.unveil.min');
    echo $this->Html->script('../theme/Pages/assets/plugins/jquery-bez/jquery.bez.min');
    echo $this->Html->script('../theme/Pages/assets/plugins/jquery-ios-list/jquery.ioslist.min');
    echo $this->Html->script('../theme/Pages/assets/plugins/jquery-actual/jquery.actual.min');
    echo $this->Html->script('../theme/Pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.min');
    // echo $this->Html->script('../theme/Pages/assets/plugins/select2/js/select2.full.min');
    echo $this->Html->script('../theme/Pages/assets/plugins/classie/classie');
    echo $this->Html->script('../theme/Pages/assets/plugins/switchery/js/switchery.min');
    echo $this->Html->script('../theme/Pages/assets/plugins/jquery-validation/js/jquery.validate.min');
    echo $this->Html->script('../theme/Pages/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker');
    echo $this->Html->script('../theme/Pages/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min');
    echo $this->Html->script('../theme/Pages/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min');
    echo $this->Html->script('../theme/Pages/assets/plugins/jquery-datatable/media/js/dataTables.bootstrap');
    echo $this->Html->script('../theme/Pages/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap');
    echo $this->Html->script('../theme/Pages/assets/plugins/datatables-responsive/js/datatables.responsive');
    echo $this->Html->script('../theme/Pages/assets/plugins/datatables-responsive/js/lodash.min');
    echo $this->Html->script('../theme/Pages/pages/js/pages.min');
    echo $this->Html->script('../theme/Pages/assets/js/form_layouts');
    echo $this->Html->script('../theme/Pages/assets/js/tables');
    echo $this->Html->script('../theme/Pages/assets/js/notifications');
    echo $this->Html->script('../theme/Pages/assets/js/scripts');
    echo $this->Html->script('../theme/Pages/assets/js/demo');

    echo $this->Html->script('moment');

    echo $this->Html->script('jquery.mask');

    echo $this->Html->script('sweetalert/sweetalert.min');
    echo $this->Html->script('sweetalert/jquery.sweet-alert.custom');

    echo $this->Html->script('wysihtml5/wysihtml5-0.3.0');
    echo $this->Html->script('wysihtml5/bootstrap-wysihtml5');

    echo $this->Html->script('custom');
    ?>

    <script type="text/javascript">
        //@TODO Colocar isso em um arquivo separado
        $('#UserUsername').focus();
    </script>
</body>

</html>