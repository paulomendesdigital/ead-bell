<?php

/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * Layout Manager
 */
?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
  <title>Manager</title>

  <meta content="text/html;charset=UTF-8" http-equiv="content-type" />
  <meta charset="utf-8">
  <meta content="IE=edge" http-equiv="X-UA-Compatible">
  <meta content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" name="viewport">
  <!--<link rel="apple-touch-icon" href="/theme/Pages/pages/ico/60.png">
  <link rel="apple-touch-icon" sizes="76x76" href="/theme/Pages/pages/ico/76.png">
  <link rel="apple-touch-icon" sizes="120x120" href="/theme/Pages/pages/ico/120.png">
  <link rel="apple-touch-icon" sizes="152x152" href="/theme/Pages/pages/ico/152.png">-->

  <link rel="icon" type="image/x-icon" href="/favicon.ico" />
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-touch-fullscreen" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="default">
  <meta content="" name="description" />

  <?php
  echo $this->Html->css('../theme/Pages/assets/plugins/pace/pace-theme-flash');
  echo $this->Html->css('../theme/Pages/assets/plugins/bootstrapv3/css/bootstrap.min');
  echo $this->Html->css('../theme/Pages/assets/plugins/font-awesome/css/font-awesome');
  echo $this->Html->css('../theme/Pages/assets/plugins/jquery-scrollbar/jquery.scrollbar');
  echo $this->Html->css('../theme/Pages/assets/plugins/select2/css/select2.min');
  echo $this->Html->css('../theme/Pages/assets/plugins/switchery/css/switchery.min');
  echo $this->Html->css('../theme/Pages/assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min');
  echo $this->Html->css('../theme/Pages/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min');
  echo $this->Html->css('../theme/Pages/assets/plugins/datatables-responsive/css/datatables.responsive');
  echo $this->Html->css('../theme/Pages/assets/plugins/bootstrap-datepicker/css/datepicker3');
  echo $this->Html->css('../theme/Pages/pages/css/pages-icons');
  echo $this->Html->css('../theme/Pages/pages/css/pages.css?v=' . time());
  echo $this->Html->css('bootstrap-material-datetimepicker');
  echo $this->Html->css('manager/custom.theme.Pages.css?v=' . time());
  echo $this->Html->css('sweetalert/sweetalert');
  echo $this->Html->css('wysihtml5/bootstrap-wysihtml5');

  echo $this->fetch('meta');
  echo $this->fetch('css');
  echo $this->fetch('script');

  if ($this->request->getParam('plugin') != 'acl') :
    //O ACL precisa de uma biblioteca JQUERY própria
    echo $this->Html->script('../theme/Pages/assets/plugins/jquery/jquery-1.11.1.min');
  endif;
  ?>
</head>

<body class="fixed-header">
  <?php echo $User ? $this->Element('Manager/sidebar') : ''; ?>
  <div class="page-container">
    <?php echo $User ? $this->Element('Manager/header') : ''; ?>
    <div class="page-content-wrapper">
      <div class="content">
        <?= $this->fetch('content') ?>
      </div>
      <?php echo $User ? $this->Element('Manager/footer') : ''; ?>
    </div>

  </div>
  <?php echo $this->Flash->render(); ?>
  <?php echo $this->Flash->render('auth'); ?>

  <style type="text/css">
    .help-block {
      font-style: italic;
      color: #999;
      font-size: 11px !important;
    }

    .multselect {
      width: 100%;
      border: none;
      height: 100px !important;
    }

    .input.checkbox label[for="status"]::before {
      display: none;
    }

    .input.checkbox label {
      padding-left: 0 !important;
    }

    .switchery {
      display: block;
    }
  </style>

  <?php
  echo $this->Html->script('../theme/Pages/assets/plugins/pace/pace.min');
  if ($this->request->getParam('plugin') != 'acl') :
    //O ACL precisa de uma biblioteca JQUERY própria
    echo $this->Html->script('../theme/Pages/assets/plugins/jquery/jquery-1.11.1.min');
  endif;
  echo $this->Html->script('../theme/Pages/assets/plugins/modernizr.custom');
  echo $this->Html->script('../theme/Pages/assets/plugins/jquery-ui/jquery-ui.min');
  echo $this->Html->script('../theme/Pages/assets/plugins/bootstrapv3/js/bootstrap.min');
  echo $this->Html->script('../theme/Pages/assets/plugins/jquery/jquery-easy');
  echo $this->Html->script('../theme/Pages/assets/plugins/jquery-unveil/jquery.unveil.min');
  echo $this->Html->script('../theme/Pages/assets/plugins/jquery-bez/jquery.bez.min');
  echo $this->Html->script('../theme/Pages/assets/plugins/jquery-ios-list/jquery.ioslist.min');
  echo $this->Html->script('../theme/Pages/assets/plugins/jquery-actual/jquery.actual.min');
  echo $this->Html->script('../theme/Pages/assets/plugins/jquery-scrollbar/jquery.scrollbar.min');
  //echo $this->Html->script('../theme/Pages/assets/plugins/select2/js/select2.full.min');
  echo $this->Html->script('../theme/Pages/assets/plugins/classie/classie');
  echo $this->Html->script('../theme/Pages/assets/plugins/switchery/js/switchery.min');
  echo $this->Html->script('../theme/Pages/assets/plugins/jquery-validation/js/jquery.validate.min');
  echo $this->Html->script('../theme/Pages/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker');
  echo $this->Html->script('../theme/Pages/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min');
  echo $this->Html->script('../theme/Pages/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min');
  echo $this->Html->script('../theme/Pages/assets/plugins/jquery-datatable/media/js/dataTables.bootstrap');
  echo $this->Html->script('../theme/Pages/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap');
  echo $this->Html->script('../theme/Pages/assets/plugins/datatables-responsive/js/datatables.responsive');
  echo $this->Html->script('../theme/Pages/assets/plugins/datatables-responsive/js/lodash.min');
  echo $this->Html->script('../theme/Pages/pages/js/pages.min');
  echo $this->Html->script('../theme/Pages/assets/js/form_layouts');
  echo $this->Html->script('../theme/Pages/assets/js/tables');
  echo $this->Html->script('../theme/Pages/assets/js/notifications');
  echo $this->Html->script('../theme/Pages/assets/js/scripts');
  echo $this->Html->script('../theme/Pages/assets/js/demo');

  echo $this->Html->script('moment');

  echo $this->Html->script('jquery.mask');

  echo $this->Html->script('sweetalert/sweetalert.min');
  echo $this->Html->script('sweetalert/jquery.sweet-alert.custom');

  echo $this->Html->script('wysihtml5/wysihtml5-0.3.0');
  echo $this->Html->script('wysihtml5/bootstrap-wysihtml5');

  //echo $this->Html->script('manager/ckeditor/ckeditor');
  echo $this->Html->script('3rdparty/ckeditor/ckeditor.js');
  echo $this->Html->script('custom.js?v=' . time());
  echo $this->Html->script('bootstrap-material-datetimepicker');
  echo $this->Html->script('manager/script.js?v=' . time());

  ?>

  <?php if (in_array($this->request->getParam('action'), ['add', 'edit', 'addPromotion', 'editPromotion'])) : ?>
    <script>
      $('#ckfinderDescription').each(function() {
        var editor =
          CKEDITOR.replace('ckfinderDescription', {
            toolbar: 'Full',
            width: '100%',
            height: '350',
            filebrowserBrowseUrl: '/js/manager/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl: '/js/manager/ckfinder/ckfinder.html?type=Images',
            filebrowserFlashBrowseUrl: '/js/manager/ckfinder/ckfinder.html?type=Flash',
            filebrowserUploadUrl: '/js/manager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: '/js/manager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            filebrowserFlashUploadUrl: '/js/manager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
          });
      });

      $('#ckfinderRegulation').each(function() {
        var editor =
          CKEDITOR.replace('ckfinderRegulation', {
            toolbar: 'Page',
            width: '100%',
            height: '350',
            filebrowserBrowseUrl: '/js/manager/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl: '/js/manager/ckfinder/ckfinder.html?type=Images',
            filebrowserFlashBrowseUrl: '/js/manager/ckfinder/ckfinder.html?type=Flash',
            filebrowserUploadUrl: '/js/manager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: '/js/manager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            filebrowserFlashUploadUrl: '/js/manager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
          });
      });

      $('#ckfinderInstrutionsWinners').each(function() {
        var editor =
          CKEDITOR.replace('ckfinderInstrutionsWinners', {
            toolbar: 'Page',
            width: '100%',
            height: '350',
            filebrowserBrowseUrl: '/js/manager/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl: '/js/manager/ckfinder/ckfinder.html?type=Images',
            filebrowserFlashBrowseUrl: '/js/manager/ckfinder/ckfinder.html?type=Flash',
            filebrowserUploadUrl: '/js/manager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: '/js/manager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            filebrowserFlashUploadUrl: '/js/manager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
          });
      });


      var elems = Array.prototype.slice.call(document.querySelectorAll('.switch'));

      elems.forEach(function(html) {
        var switchery = new Switchery(html);
      });
    </script>
  <?php endif; ?>
</body>


</html>