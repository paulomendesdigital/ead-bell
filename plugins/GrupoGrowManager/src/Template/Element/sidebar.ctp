<?php
/**
 * @copyright Copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 * Element GrupoGrowManager Sidebar
 *
*/
?>
<nav class="page-sidebar" data-pages="sidebar">
   <div class="sidebar-header">
      <img src="/theme/Pages/assets/img/logo_white.png?<?php echo time();?>" alt="logo" class="brand" data-src="/theme/Pages/assets/img/logo_white.png" data-src-retina="/theme/Pages/assets/img/logo_white_2x.png?<?php echo time();?>" width="45" height="38">
      <div class="sidebar-header-controls">
         <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar"><i class="fa fs-12"></i></button>
      </div>
   </div>
   <div class="sidebar-menu">
      <ul class="menu-items">
         
         <?php $class = $this->request->getParam('controller')=='pages'?'open active ':''; ?>
         <li class="m-t-30 <?php echo $class; ?>">
            <?php echo $this->Html->link('<span class="title">Páginas</span><span class="details">Cadastro de páginas</span>', ['controller' => 'pages', 'action' => 'index'], ['class' => 'detailed', 'escape' => false]);?>
            <span class="icon-thumbnail"><i class="pg-home"></i></span>
         </li>

         <?php $class = $this->request->getParam('controller')=='webdoors'?'open active ':''; ?>
         <li class="m-t-10 <?php echo $class;?>">
            <?php echo $this->Html->link('<span class="title">Webdoors</span><span class="details">Banner página inicial</span>', ['controller' => 'webdoors', 'action' => 'index'], ['class' => 'detailed', 'escape' => false]);?>
            <span class="icon-thumbnail"><i class="pg-image"></i></span>
         </li>

        <?php $class = $this->request->getParam('controller')=='users'?'open active ':''; ?>
        <li class="m-t-10 <?php echo $class;?>">
            <?php echo $this->Html->link('<span class="title">Usuários</span><span class="details">Cadastro de Usuários</span>', ['controller' => 'users', 'action' => 'index'], ['class' => 'detailed', 'escape' => false]);?>
            <span class="icon-thumbnail"><i class="fa fa-user"></i></span>
        </li>

        <?php $class = $this->request->getParam('controller')=='groups'?'open active ':''; ?>
        <li class="m-t-10 <?php echo $class;?>">
            <?php echo $this->Html->link('<span class="title">Grupos</span><span class="details">Cadastro de Grupos</span>', ['controller' => 'groups', 'action' => 'index'], ['class' => 'detailed', 'escape' => false]);?>
            <span class="icon-thumbnail"><i class="fa fa-users"></i></span>
        </li>

      </ul>
      <div class="clearfix"></div>
   </div>
</nav>