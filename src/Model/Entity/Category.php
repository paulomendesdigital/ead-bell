<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Category extends Entity
{
    protected $_accessible = [
        'id' => true,
        'name' => true,
        'product_count' => true,
        'status' => true,
        'created' => true,
        'modified' => true
    ];
}
