<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class City extends Entity
{
    protected $_accessible = [
        'id' => true,
        'state_id' => true,
        'uf' => true,
        'name' => true
    ];
}
