<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Area extends Entity
{
    protected $_accessible = [
        'id' => true,
        'name' => true,
        'slug' => true,
        'image' => true,
        'status' => true,
        'ordination' => true,
        'show_menu' => true,
        'product_count' => true,
        'created' => true,
        'modified' => true
    ];
}
