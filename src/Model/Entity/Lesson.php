<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Lesson extends Entity
{
    protected $_accessible = [
        'id' => true,
        'teacher_id' => true,
        'title' => true,
        'filename' => true,
        'ordination' => true,
        'pdf' => true,
        'question_count' => true,
        'status' => true,
        'description' => true,
        'created' => true,
        'modified' => true
    ];
}
