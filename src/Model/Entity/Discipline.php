<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Discipline extends Entity
{
    protected $_accessible = [
        'id' => true,
        'name' => true,
        'label' => true,
        'image' => true,
        'ordination' => true,
        'question_count' => true,
        'status' => true,
        'created' => true,
        'modified' => true
    ];
}
