<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Client extends Entity
{
    protected $_accessible = [
        'id' => true,
        'name' => true,
        'email' => true,
        'phone' => true,
        'cellphone' => true,
        'birth' => true,
        'cpf' => true,
        'rg' => true,
        'gender' => true,
        'status' => true,
        'ip' => true,
        'forum_post_count' => true,
        'sessionid' => true,
        'created' => true,
        'modified' => true
    ];
}
