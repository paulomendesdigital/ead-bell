<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Webdoor Entity
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property \Cake\I18n\FrozenTime|null $start
 * @property \Cake\I18n\FrozenTime|null $finish
 * @property string|null $title
 * @property string|null $button_label
 * @property string|null $link
 * @property string|null $image_name
 * @property string|null $image_path
 * @property string|null $image_background_name
 * @property string|null $image_background_path
 * @property string|null $image_mobile_name
 * @property string|null $image_mobile_path
 * @property int $status
 * @property int|null $ordination
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Company $company
 */
class Webdoor extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'company_id' => true,
        'name' => true,
        'start' => true,
        'finish' => true,
        'title' => true,
        'button_label' => true,
        'button_bg_color' => true,
        'button_text_color' => true,
        'link' => true,
        'image_name' => true,
        'image_path' => true,
        'image_background_name' => true,
        'image_background_path' => true,
        'image_mobile_name' => true,
        'image_mobile_path' => true,
        'status' => true,
        'ordination' => true,
        'target' => true,
        'created' => true,
        'modified' => true,
        'company' => true
    ];
}
