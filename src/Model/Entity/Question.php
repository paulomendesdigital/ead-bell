<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Question extends Entity
{
    protected $_accessible = [
        'id' => true,
        'discipline_id' => true,
        'question' => true,
        'topic_id' => true,
        'lesson_id' => true,
        'question_theme_id' => true,
        'video_correction' => true,
        'status' => true,
        'created' => true,
        'modified' => true
    ];
}
