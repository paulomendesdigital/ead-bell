<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class AnswerComment extends Entity
{
    protected $_accessible = [
        'comment_id' => true,
        'company_id' => true,
        'name' => true,
        'start_date' => true,
        'finish_date' => true,
        'weight' => true,
        'max_views' => true,
        'max_unique_views' => true,
        'max_clicks' => true,
        'max_unique_clicks' => true,
        'unique_views_count' => true,
        'views_count' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'comment' => true,
        'company' => true,
        'advert_views' => true,
        'advert_clicks' => true,
        'adverts' => true
    ];
}
