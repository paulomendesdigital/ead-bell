<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Topic extends Entity
{
    protected $_accessible = [
        'id' => true,
        'discipline_id' => true,
        'name' => true,
        'label' => true,
        'ordination' => true,
        'status' => true,
        'created' => true,
        'modified' => true
    ];
}
