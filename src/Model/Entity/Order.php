<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Order extends Entity
{
    protected $_accessible = [
        'id' => true,
        'client_id' => true,
        'ticket_id' => true,
        'method_id' => true,
        'transactionid' => true,
        'payment' => true,
        'shipping' => true,
        'value' => true,
        'operation_rate' => true,
        'general_tax' => true,
        'value_discount' => true,
        'value_shipping' => true,
        'value_fee' => true,
        'value_net' => true,
        'escrowEndDate' => true,
        'portal_amount' => true,
        'transfer_amount' => true,
        'status' => true,
        'approved' => true,
        'ip' => true,
        'sessionid' => true,
        'view' => true,
        'viewed' => true,
        'observation' => true,
        'payment_link_boleto' => true,
        'created' => true,
        'modified' => true
    ];
}
