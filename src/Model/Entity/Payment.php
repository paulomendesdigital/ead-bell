<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Payment extends Entity
{
    protected $_accessible = [
        'id' => true,
        'order_id' => true,
        'TransacaoID' => true,
        'Extras' => true,
        'TipoFrete' => true,
        'ValorFrete' => true,
        'Anotacao' => true,
        'DataTransacao' => true,
        'TipoPagamento' => true,
        'StatusTransacao' => true,
        'NumItens' => true,
        'Parcelas' => true,
        'postback_json' => true,
        'created' => true,
        'modified' => true
    ];
}
