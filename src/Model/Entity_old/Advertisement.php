<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Advertisement Entity
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property \Cake\I18n\FrozenDate $start
 * @property \Cake\I18n\FrozenDate $finish
 * @property string $embed
 * @property int $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Company $company
 */
class Advertisement extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'company_id' => true,
        'name' => true,
        'start' => true,
        'finish' => true,
        'embed' => true,
        'status' => true,
        'image' => true,
        'image_path' => true,
        'image_mobile' => true,
        'image_mobile_path' => true,
        'position' => true,
        'type_pub' => true,
        'created' => true,
        'modified' => true,
        'company' => true
    ];
}
