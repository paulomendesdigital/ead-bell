<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Alternative Entity
 *
 * @property int $id
 * @property int $question_id
 * @property string $alternative
 * @property int|null $is_correct
 * @property int|null $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Question $question
 * @property \App\Model\Entity\Participant[] $participants
 */
class Alternative extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'question_id' => true,
        'alternative' => true,
        'is_correct' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'question' => true,
        'participants' => true
    ];
}
