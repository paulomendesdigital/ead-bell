<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TopMusic Entity
 *
 * @property int $id
 * @property int $company_id
 * @property string $artist
 * @property string $name
 * @property int|null $ordination
 * @property string|null $image_name
 * @property string|null $image_path
 * @property int $status
 * @property string|null $created
 * @property string|null $modified
 *
 * @property \App\Model\Entity\Company $company
 */
class TopMusic extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'company_id' => true,
        'artist' => true,
        'name' => true,
        'ordination' => true,        
        'image_name' => true,
        'image_path' => true,
        'audio' => true,
        'audio_path' => true,
        'status' => true,
        'link' => true,
        'created' => true,
        'modified' => true,
        'company' => true
    ];
}
