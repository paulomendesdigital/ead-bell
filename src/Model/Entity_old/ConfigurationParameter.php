<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ConfigurationParameter Entity
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property string $code
 * @property string $description
 * @property int $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 */
class ConfigurationParameter extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'company_id' => true,
        'name' => true,
        'code' => true,
        'description' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
    ];
}
