<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LikeMusic Entity
 *
 * @property int $id
 * @property int $company_id
 * @property int|null $user_id
 * @property int|null $code
 * @property string|null $music
 * @property string|null $artist
 * @property string|null $action
 * @property string|null $program
 * @property int|null $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Company $company
 * @property \App\Model\Entity\User $user
 */
class LikeMusic extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'company_id' => true,
        'user_id' => true,
        'code' => true,
        'music' => true,
        'artist' => true,
        'action' => true,
        'program' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'company' => true,
        'user' => true
    ];
}
