<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Teacher extends Entity
{
    protected $_accessible = [
        'id' => true,
        'site_id' => true,
        'name' => true,
        'email' => true,
        'image' => true,
        'gender' => true,
        'status' => true,
        'curriculum' => true,
        'created' => true,
        'modified' => true
    ];
}
