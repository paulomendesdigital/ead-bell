<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MidiaKit Entity
 *
 * @property int $id
 * @property int $company_id
 * @property string|null $name
 * @property string|null $pdf
 * @property int|null $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Company $company
 */
class MidiaKit extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'company_id' => true,
        'name' => true,
        'pdf' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'company' => true
    ];
}
