<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Product extends Entity
{
    protected $_accessible = [
        'id' => true,
        'category_id' => true,
        'area_id' => true,
        'concourse_id' => true,
        'examination_board_id' => true,
        'streaming_server_id' => true,
        'ordination' => true,
        'title' => true,
        'subtitle' => true,
        'slug' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
    ];
}
