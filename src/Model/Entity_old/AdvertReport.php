<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AdvertReport Entity
 *
 * @property int $id
 * @property int $model_id
 * @property string $model_name
 * @property \Cake\I18n\FrozenDate $report_date
 * @property int $views_count
 * @property int $unique_views_count
 * @property int $clicks_count
 * @property int $unique_clicks_count
 * @property float $ctr
 * @property float $unique_ctr
 * @property int $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Model $model
 */
class AdvertReport extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'model_id' => true,
        'model_name' => true,
        'report_date' => true,
        'views_count' => true,
        'unique_views_count' => true,
        'clicks_count' => true,
        'unique_clicks_count' => true,
        'ctr' => true,
        'unique_ctr' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
    ];
}
