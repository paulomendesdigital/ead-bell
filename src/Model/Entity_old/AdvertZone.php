<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AdvertZone Entity
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property string $code
 * @property int|null $width
 * @property int|null $height
 * @property int $unique_views_count
 * @property int $views_count
 * @property int $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Company $company
 * @property \App\Model\Entity\AdvertView[] $advert_views
 * @property \App\Model\Entity\Advert[] $adverts
 */
class AdvertZone extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'company_id' => true,
        'name' => true,
        'code' => true,
        'width' => true,
        'height' => true,
        'unique_views_count' => true,
        'views_count' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'company' => true,
        'advert_views' => true,
        'adverts' => true
    ];
}
