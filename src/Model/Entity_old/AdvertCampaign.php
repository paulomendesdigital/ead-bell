<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AdvertCampaign Entity
 *
 * @property int $id
 * @property int $advertiser_id
 * @property int $company_id
 * @property string|null $name
 * @property \Cake\I18n\FrozenTime|null $start_date
 * @property \Cake\I18n\FrozenTime|null $finish_date
 * @property int $weight
 * @property int|null $max_views
 * @property int $unique_views_count
 * @property int $views_count
 * @property int $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Advertiser $advertiser
 * @property \App\Model\Entity\Company $company
 * @property \App\Model\Entity\AdvertView[] $advert_views
 * @property \App\Model\Entity\Advert[] $adverts
 */
class AdvertCampaign extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'advertiser_id' => true,
        'company_id' => true,
        'name' => true,
        'start_date' => true,
        'finish_date' => true,
        'weight' => true,
        'max_views' => true,
        'max_unique_views' => true,
        'max_clicks' => true,
        'max_unique_clicks' => true,
        'unique_views_count' => true,
        'views_count' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'advertiser' => true,
        'company' => true,
        'advert_views' => true,
        'advert_clicks' => true,
        'adverts' => true
    ];
}
