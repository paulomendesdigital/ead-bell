<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Participant Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $content_id
 * @property int|null $question_id
 * @property int|null $alternative_id
 * @property int|null $correct
 * @property string|null $response_in_text
 * @property int|null $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Content $content
 * @property \App\Model\Entity\Question $question
 * @property \App\Model\Entity\Alternative $alternative
 */
class Participant extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'company_id' => true,
        'access_origin_id' => true,
        'user_id' => true,
        'content_id' => true,
        'question_id' => true,
        'alternative_id' => true,
        'correct' => true,
        'response_in_text' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'content' => true,
        'question' => true,
        'company' => true,
        'alternative' => true
    ];
}
