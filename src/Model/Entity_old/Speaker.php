<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Speaker Entity
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property string|null $image
 * @property string|null $description
 * @property string|null $facebook
 * @property string|null $instagram
 * @property string|null $twitter
 * @property string|null $whatsapp
 * @property string|null $video
 * @property string|null $work_schedule
 * @property int|null $ordination
 * @property int|null $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Company $company
 */
class Speaker extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'company_id' => true,
        'name' => true,
        'image' => true,
        'description' => true,
        'facebook' => true,
        'instagram' => true,
        'twitter' => true,
        'whatsapp' => true,
        'video' => true,
        'work_schedule' => true,
        'ordination' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'company' => true
    ];
}
