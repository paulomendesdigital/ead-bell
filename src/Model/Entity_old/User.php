<?php

namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property int $company_id
 * @property int $group_id
 * @property string|null $firebase_uid
 * @property string $name
 * @property string $email
 * @property string|null $username
 * @property string|null $password
 * @property int $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Company $company
 * @property \App\Model\Entity\Group $group
 * @property \App\Model\Entity\UserProvider[] $user_providers
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'company_id' => true,
        'group_id' => true,
        'firebase_uid' => true,
        'name' => true,
        'email' => true,
        'username' => true,
        'cpf' => true,
        'rg' => true,
        'gender' => true,
        'birth' => true,
        'phone' => true,
        'cep' => true,
        'address' => true,
        'number' => true,
        'complement' => true,
        'neighborhood' => true,
        'city' => true,
        'schooling' => true,
        'news' => true,
        'state' => true,
        'password' => true,
        'emailVerified' => true,
        'photoURL' => true,
        'pass_token' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'company' => true,
        'group' => true,
        'user_providers' => true,
        'encrypted' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    public function parentNode()
    {
        if (!$this->id) {
            return null;
        }
        if (isset($this->group_id)) {
            $groupId = $this->group_id;
        } else {
            $Users = TableRegistry::get('Users');
            $user = $Users->find('all', ['fields' => ['group_id']])->where(['id' => $this->id])->first();
            $groupId = $user->group_id;
        }
        if (!$groupId) {
            return null;
        }
        return ['Groups' => ['id' => $groupId]];
    }

    protected function _setPassword($password)
    {
        $hasher = new DefaultPasswordHasher();
        return $hasher->hash($password);
    }
}
