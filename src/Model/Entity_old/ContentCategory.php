<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ContentCategory Entity
 *
 * @property int $id
 * @property int $company_id
 * @property string $code
 * @property string $name
 * @property string|null $button_label
 * @property int $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Company $company
 * @property \App\Model\Entity\Content[] $contents
 */
class ContentCategory extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        //'company_id' => true,
        'code' => true,
        'name' => true,
        'button_label' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'company' => true,
        'contents' => true
    ];
}
