<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Program Entity
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property string|null $description
 * @property string|null $image
 * @property string|null $image_path
 * @property int|null $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Company $company
 */
class Program extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'company_id' => true,
        'name' => true,
        'sutbtitle' => true,
        'description' => true,
        'image' => true,
        'image_path' => true,
        'image_home' => true,
        'info_hour' => true,
        'status' => true,
        'program_type' => true,
        'ordination' => true,
        'spotlight' => true,
        'created' => true,
        'modified' => true,
        'company' => true,
        'contents'=>true
    ];
}
