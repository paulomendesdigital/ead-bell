<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Company Entity
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property int $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property \App\Model\Entity\TopMusic[] $top_musics
 * @property \App\Model\Entity\User[] $users
 * @property \App\Model\Entity\Webdoor[] $webdoors
 */
class Company extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'code' => true,
        'name' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'top_musics' => true,
        'users' => true,
        'webdoors' => true
    ];
}
