<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AdvertView Entity
 *
 * @property int $id
 * @property int $advert_zone_id
 * @property int $advert_campaign_id
 * @property int $advert_id
 * @property int $advert_advert_zone_id
 * @property string $sessionid
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\AdvertZone $advert_zone
 * @property \App\Model\Entity\AdvertCampaign $advert_campaign
 * @property \App\Model\Entity\Advert $advert
 * @property \App\Model\Entity\AdvertsAdvertZone $adverts_advert_zone
 */
class AdvertView extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'advert_zone_id' => true,
        'advert_campaign_id' => true,
        'advert_id' => true,
        'advert_advert_zone_id' => true,
        'sessionid' => true,
        'created' => true,
        'modified' => true,
        'advert_zone' => true,
        'advert_campaign' => true,
        'advert' => true,
        'adverts_advert_zone' => true
    ];
}
