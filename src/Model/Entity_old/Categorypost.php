<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Categorypost extends Entity
{
    protected $_accessible = [
        'id' => true,
        'categorypost_id' => true,
        'title' => true,
        'slug' => true,
        'body' => true,
        'status' => true,
        'created' => true,
        'modified' => true
    ];
}
