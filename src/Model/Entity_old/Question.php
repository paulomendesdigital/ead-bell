<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Question Entity
 *
 * @property int $id
 * @property int|null $content_id
 * @property string|null $type
 * @property string $question
 * @property int|null $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Content $content
 * @property \App\Model\Entity\Alternative[] $alternatives
 * @property \App\Model\Entity\Participant[] $participants
 */
class Question extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'content_id' => true,
        'type' => true,
        'question' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'content' => true,
        'alternatives' => true,
        'participants' => true
    ];
}
