<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Client extends Entity
{
    protected $_accessible = [
        'id' => true,
        'state_id' => true,
        'uf' => true,
        'name' => true
    ];
}
