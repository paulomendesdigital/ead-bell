<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Player Entity
 *
 * @property int $id
 * @property string|null $live
 * @property string|null $podcast
 * @property \Cake\I18n\FrozenTime $start
 * @property \Cake\I18n\FrozenTime|null $finish
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 */
class Player extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'company_id' => true,
        'live' => true,
        'podcast' => true,
        'start' => true,
        'finish' => true,
        'created' => true,
        'modified' => true,
        'company' => true,
    ];
}
