<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Content Entity
 *
 * @property int $id
 * @property int $company_id
 * @property int $content_category_id
 * @property string $name
 * @property string|null $title
 * @property string|null $subtitle
 * @property string|null $button_label
 * @property string|null $description
 * @property string|null $image_small_name
 * @property string|null $image_small_path
 * @property string|null $image_medium_name
 * @property string|null $image_medium_path
 * @property string|null $image_large_name
 * @property string|null $image_large_path
 * @property string|null $tags
 * @property int|null $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Company $company
 * @property \App\Model\Entity\ContentCategory $content_category
 */
class Content extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'company_id' => true,
        'content_category_id' => true,
        'program_id' => true,
        'name' => true,
        'title' => true,
        'subtitle' => true,
        'button_label' => true,
        'description' => true,
        'image_small_name' => true,
        'image_small_path' => true,
        'image_medium_name' => true,
        'image_medium_path' => true,
        'image_large_name' => true,
        'image_large_path' => true,
        'image_internal' => true,
        'image_internal_path' => true,
        'tags' => true,
        'status' => true,
        'audio' => true,
        'start' => true,
        'finish' => true,
        'finish_promotion' => true,
        'type_promotion' => true,
        'limitwinners' => true,
        'instrutions' => true,
        'regulation' => true,
        'created' => true,
        'modified' => true,
        'auto_draw' => true,
        'destination' => true,
        'company' => true,
        'content_category' => true,
        'program' => true,
        'participants' => true,
        'winners' => true,
        'questions' => true,
        'fixed' => true,
        'author' => true
    ];
}
