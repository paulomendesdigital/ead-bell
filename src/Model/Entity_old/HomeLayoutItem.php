<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * HomeLayoutItem Entity
 *
 * @property int $id
 * @property int $home_layout_id
 * @property int $content_id
 * @property int $size
 * @property int $row
 *
 * @property \App\Model\Entity\HomeLayout $home_layout
 * @property \App\Model\Entity\Content $content
 */
class HomeLayoutItem extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'home_layout_id' => true,
        'content_id' => true,
        'size' => true,
        'row' => true,
        'ordination' => true,
        'home_layout' => true,
        'content' => true
    ];
}
