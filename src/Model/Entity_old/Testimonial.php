<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Testimonial extends Entity
{
    protected $_accessible = [
        'id' => true,
        'client_id' => true,
        'name' => true,
        'testimony' => true,
        'video' => true,
        'status' => true,
        'created' => true,
        'modified' => true
    ];
}
