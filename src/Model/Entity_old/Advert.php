<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Advert Entity
 *
 * @property int $id
 * @property string $uuid
 * @property int $advert_campaign_id
 * @property string $name
 * @property string|null $url
 * @property string|null $image
 * @property string|null $image_dir
 * @property int $unique_views_count
 * @property int $views_count
 * @property int $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\AdvertCampaign $advert_campaign
 * @property \App\Model\Entity\AdvertView[] $advert_views
 * @property \App\Model\Entity\AdvertZone[] $advert_zones
 */
class Advert extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'uuid' => true,
        'advert_campaign_id' => true,
        'name' => true,
        'url' => true,
        'image' => true,
        'image_dir' => true,
        'unique_views_count' => true,
        'views_count' => true,
        'status' => true,
        'url_html' => true,
        'created' => true,
        'modified' => true,
        'advert_campaign' => true,
        'advert_views' => true,
        'advert_zones' => true
    ];
}
