<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AdvertsAdvertZone Entity
 *
 * @property int $id
 * @property int $advert_id
 * @property int $advert_zone_id
 * @property int $unique_views_count
 * @property int $views_count
 * @property int $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Advert $advert
 * @property \App\Model\Entity\AdvertZone $advert_zone
 */
class AdvertsAdvertZone extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'advert_id' => true,
        'advert_zone_id' => true,
        'unique_views_count' => true,
        'views_count' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'advert' => true,
        'advert_zone' => true
    ];
}
