<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Notice extends Entity
{
    protected $_accessible = [
        'id' => true,
        'title' => true,
        'slug' => true,
        'message' => true,
        'date' => true,
        'source' => true,
        'created' => true,
        'modified' => true
    ];
}
