<?php
namespace App\Model\Behavior;

//use ArrayObject;
//use Cake\Datasource\EntityInterface;
//use Cake\Event\Event;
use Cake\ORM\Behavior;
//use Cake\ORM\Entity;
//use Cake\ORM\Query;
use Cake\Utility\Text;

class UtilityBehavior extends Behavior
{

	/*
     * Função responsável por tratar uma string,
     * retirando os espaços e caracteres especiais.
     */
    public function __Normalize($string){

        $a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $b = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        $string = utf8_decode($string);
        $string = strtr($string, utf8_decode($a), $b);
        //$string = str_replace("-","",$string);
        $string = str_replace("&","e",$string);
        $string = str_replace(" ","-",$string);
        $string = str_replace(".","",$string);
        $string = str_replace("!","",$string);
        $string = str_replace("?","",$string);
        $string = str_replace(":","",$string);
        $string = str_replace(";","",$string);
        $string = str_replace(";","",$string);
        $string = str_replace(",","",$string);
        $string = str_replace("'","",$string);
        $string = str_replace("\"","",$string);
        $string = str_replace("/","",$string);
        $string = str_replace("|","",$string);
        $string = str_replace("--","-",$string);
        $string = str_replace("---","-",$string);
        $string = str_replace("----","-",$string);
        $string = strtolower($string);

        return utf8_encode($string);
    }

    /**
    * verifica se um esta esta de escrito de forma correta
    * @return boolean true or false
    */
    public function formatCep($cep) {
        // retira espacos em branco
        $cep = trim($cep);
        // expressao regular para avaliar o cep
        return ereg("^[0-9]{5}-[0-9]{3}$", $cep);
    }

    public function dateFormatAfterFind($dateString, $hasHour=false) {

        if( !$hasHour )
            return date('d/m/Y', strtotime($dateString));
        else
            return date('d/m/Y H:i', strtotime($dateString));
    }

    public function dateFormatAfterFindForBlog($dateString) {
        $dia = date('d', strtotime($dateString));
        $mes = $this->__GetMesPT_BR( date('m', strtotime($dateString)) );
        $ano = date('Y', strtotime($dateString));
        return "{$dia} de {$mes} de {$ano}";
    }

    public function dateTimeFormatAfterFind($dateString) {
        return date('d/m/Y H:i', strtotime($dateString));
    }

    public function dateFormatBeforeSave($data='done') {
    	
        $hora = substr($data, 10, 20);
        $data = substr($data, 0, 10);

        $explode = explode("/", $data);
        if( count($explode) > 1 ){
            if ($hora) {
                $data = $explode[2] . "-" . $explode[1] . "-" . $explode[0] . $hora . ":00";
            } else {
                $data = $explode[2] . "-" . $explode[1] . "-" . $explode[0];
            }
        }

        return $data;
    }

    public function CalcDate($datetime, $operador, $qtde, $type, $formatReturn){
        $obj = new \DateTime($datetime." {$operador}{$qtde} {$type}");
        return $obj->format($formatReturn);
    }

    public function __Slug($string){
        return Text::slug( strtolower( $this->__Normalize($string) ), '-' );
    }

    public function __GetMesPT_BR($mes){
        $meses = [
            '01' => 'Janeiro',
            '02' => 'Fevereiro',
            '03' => 'Março',
            '04' => 'Abril',
            '05' => 'Maio',
            '06' => 'Junho',
            '07' => 'Julho',
            '08' => 'Agosto',
            '09' => 'Setembro',
            '10' => 'Outubro',
            '11' => 'Novembro',
            '12' => 'Dezembro'
        ];

        return $meses[$mes];
    }
}