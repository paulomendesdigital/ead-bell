<?php

namespace App\Model\Behavior;

use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;

/**
 * DistinctBelongsToMany behavior
 */
class DistinctBelongsToManyBehavior extends Behavior
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        $associations = $this->getConfig('associations');

        if (empty($associations) || !is_array($associations)) return true;

        if ($entity->isNew()) return true;

        for ($i = 0; $i < count($associations); $i++) {
            $associationName = $associations[$i];

            if (!$this->getTable()->hasAssociation($associationName)) {
                throw new InvalidConfigurationException($associationName . " association not found with table " . $this->getTable()->getRegistryAlias());
            }

            $association = $this->getTable()->getAssociation($associationName);

            if (!$association instanceof \Cake\Orm\Association\BelongsToMany) {
                throw new InvalidConfigurationException($associationName . " association on table " . $this->getTable()->getRegistryAlias() . " is not a belongsToMany association");
            }

            $propertyName = $association->getProperty();

            if (empty($entity->$propertyName)) continue;

            $ids = [];
            foreach ($entity->$propertyName as $associatedEntity) {
                $ids[] = $associatedEntity->id;
            }
            $ids = array_unique($ids);

            $existentAssociatedEntities = $this->getTable()->$associationName->find('list')
                ->where([$associationName . '.id IN' => $ids])
                ->matching($this->getTable()->getRegistryAlias(), function ($q) use ($entity) {
                    return $q->where([$this->getTable()->getRegistryAlias() . '.id' => $entity->id]);
                })
                ->toArray();

            if (!count($existentAssociatedEntities)) continue;

            foreach ($existentAssociatedEntities as $associatedEntityId => $associatedEntityName) {
                $entity->$propertyName = array_filter($entity->$propertyName, function ($e) use ($associatedEntityId) {
                    return $e->id != $associatedEntityId;
                });
            }
        }
    }
}
