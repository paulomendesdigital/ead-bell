<?php

namespace App\Model\Behavior;

use ArrayObject;
use Cake\Core\Configure;
use Cake\ORM\Query;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Defuse\Crypto\Crypto;
use Defuse\Crypto\Key;
use Exception;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;

/**
 * Crypt behavior
 *
 * Referências:
 * https://github.com/defuse/php-encryption
 * https://github.com/yidas/php-aes-cryptojs/blob/master/example/aes-256-ecb.php
 * https://www.mail-archive.com/cake-php%40googlegroups.com/msg122704.html
 * https://api.cakephp.org/3.9/namespace-Cake.Database.html
 * https://api.cakephp.org/3.9/class-Cake.Database.Query.html
 * https://api.cakephp.org/3.9/namespace-Cake.Database.Expression.html
 * https://api.cakephp.org/3.9/class-Cake.Database.Expression.Comparison.html
 * https://www.php.net/manual/pt_BR/internals2.opcodes.instanceof.php
 */
class CryptBehavior extends Behavior
{
    const CRYPT_METHOD = "AES-256-ECB";

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'fields' => []
    ];

    public function initialize(array $config)
    {
        parent::initialize($config);

        $key = Configure::read('CryptKey');
        if (empty($key)) {
            throw new InvalidConfigurationException("Missing configuration 'CryptKey' on bootstrap.php Generate the key by executing the command vendor/bin/generate-defuse-key");
        }

        $fields = $this->getConfig('fields');
        if (empty($fields)) {
            throw new InvalidConfigurationException("There are no 'fields' configured on OpenSslCryptBehavior");
        }
    }

    private function getCryptKey()
    {
        return Configure::read('CryptKey');
    }

    public function encrypt($data)
    {
        $chiperRaw = openssl_encrypt($data, self::CRYPT_METHOD, $this->getCryptKey(), OPENSSL_RAW_DATA);
        $cipherHex = bin2hex($chiperRaw);
        return $cipherHex;
    }

    public function decrypt($data)
    {
        if (!ctype_xdigit($data) || (strlen($data) % 2) != 0) return $data;

        $chiperRaw = hex2bin($data);
        return openssl_decrypt($chiperRaw, self::CRYPT_METHOD, $this->getCryptKey(), OPENSSL_RAW_DATA);
    }

    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        $fields = $this->getConfig('fields');
        foreach ($fields as $field) {
            if (!isset($entity->$field)) continue;
            $entity->$field = $this->encrypt($entity->$field);
        }
    }

    public function afterSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        $fields = $this->getConfig('fields');
        foreach ($fields as $field) {
            if (!isset($entity->$field)) continue;
            $entity->$field = $this->decrypt($entity->$field);
        }
    }

    /**
     * Transforms the query expressions in OR conditions passing the encrypted and plain text values
     *
     * @param \Cake\Database\Expression\Comparison $qE
     * @return \Cake\Database\Expression\Comparison
     */
    private function handleQueryExpressionCondition($qE)
    {
        $modelName = $this->getTable()->getAlias();
        $fields = $this->getConfig('fields');

        foreach ($fields as $field) {
            $fieldName = "{$modelName}.{$field}";
            if ($fieldName == $qE->getField()) {
                $formattedQE = new \Cake\Database\Expression\QueryExpression();
                $formattedQE->setConjunction('OR');

                $comparisonQE = new \Cake\Database\Expression\Comparison(
                    $qE->getField(),
                    $qE->getValue(),
                    null,
                    $qE->getOperator()
                );
                $formattedQE->add($comparisonQE);

                $comparisonQE = new \Cake\Database\Expression\Comparison(
                    $qE->getField(),
                    $this->encrypt($qE->getValue()),
                    null,
                    $qE->getOperator()
                );
                $formattedQE->add($comparisonQE);

                return $formattedQE;
            }
        }

        return $qE;
    }

    public function beforeFind(Event $event, Query $query, ArrayObject $options, $primary)
    {
        //TODO caso existam condições em modelos relacionados que utilizem criptografia, esta função precisará ser refeita
        //Criptografa os dados e trata as condições pertencentes a este modelo
        $query->clause('where')->iterateParts(function ($qE) {
            /**
             * @var \Cake\Database\Expression\Comparison|Cake\Database\Expression\QueryExpression $qE
             */
            if ($qE instanceof \Cake\Database\Expression\Comparison) {
                return $this->handleQueryExpressionCondition($qE);
            } /*else {
                $formattedQE = new \Cake\Database\Expression\QueryExpression();
                $formattedQE->setConjunction('OR');
                debug($formattedQE);
                die(debug($qE));
                die(debug($qE->getConjunction()));
                $qE->iterateParts(function ($qE) {
                    debug($qE);
                });
            }*/
            return $qE;
        });

        $query->formatResults(function (\Cake\Collection\CollectionInterface $results) {
            return $results->map(function ($row) {
                $fields = $this->getConfig('fields');
                foreach ($fields as $field) {
                    if (!isset($row[$field])) continue;
                    try {
                        $row[$field] = $this->decrypt($row[$field]);
                    } catch (Exception $e) {
                        //Podem acontecer erros caso tente descriptografar algo que ainda não foi criptografado
                        continue;
                    }
                }
                return $row;
            });
        });
    }
}
