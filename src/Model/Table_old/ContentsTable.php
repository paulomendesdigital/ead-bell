<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Utility\Text;
use Cake\Datasource\ConnectionManager;

/**
 * Contents Model
 *
 * @property \App\Model\Table\CompaniesTable&\Cake\ORM\Association\BelongsTo $Companies
 * @property \App\Model\Table\ContentCategoriesTable&\Cake\ORM\Association\BelongsTo $ContentCategories
 *
 * @method \App\Model\Entity\Content get($primaryKey, $options = [])
 * @method \App\Model\Entity\Content newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Content[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Content|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Content saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Content patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Content[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Content findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ContentsTable extends Table
{
    CONST LIMIT_TAGS = 3;

    CONST IMAGE_SMALL_SIZE = '150x150';
    CONST IMAGE_MEDIUM_SIZE = '600x600';
    CONST IMAGE_LARGE_SIZE = '800×378';
    CONST IMAGE_INTERNAL_SIZE = '800×378';

    CONST PROMOTION_TYPE_PERGUNTA = 'PERGUNTA';
    CONST PROMOTION_TYPE_SORTEIO = 'SORTEIO';
    CONST PROMOTION_TYPE_QUIZ = 'QUIZ';
    CONST PROMOTION_TYPE_NO_ACTION = 'SEM_ACAO';
    
    CONST PROMOTION_ID = 3;
    CONST PROMOTION_DESTINATION_ONLY_SITE   = 1;
    CONST PROMOTION_DESTINATION_ONLY_APP    = 2;
    CONST PROMOTION_DESTINATION_BOTH        = 3;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('contents');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Utility');

        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ContentCategories', [
            'foreignKey' => 'content_category_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Programs', [
            'foreignKey' => 'program_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Participants', [
            'foreignKey' => 'content_id',
            'joinType' => 'LEFT'            
        ]);
        $this->hasMany('Winners', [
            'foreignKey' => 'content_id',
            'joinType' => 'LEFT'            
        ]);
        $this->hasMany('Questions', [
            'foreignKey' => 'content_id',
            'joinType' => 'LEFT',
            'saveStrategy' => 'replace'   
        ]);

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'image_small_name' => [
                //'path' =>  'webroot{DS}files{DS}{model}{DS}{field}{DS}{primaryKey}',
                'fields' => [
                    'dir' => 'image_small_path',
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $data['name'] = Text::slug( date('YmdHis') . $entity->name ) . '.' . strtolower($extension);
                    return $data['name'];
                },
                'deleteCallback' => function ($path, $entity, $field, $settings) {
                    return [
                        $path . $entity->{$field},
                    ];
                },
                'keepFilesOnDelete' => false
            ],
            'image_medium_name' => [
                //'path' =>  'webroot{DS}files{DS}{model}{DS}{field}{DS}{primaryKey}',
                'fields' => [
                    'dir' => 'image_medium_path',
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $data['name'] = Text::slug( date('YmdHis') . $entity->name ) . '.' . strtolower($extension);
                    return $data['name'];
                },
                'deleteCallback' => function ($path, $entity, $field, $settings) {
                    return [
                        $path . $entity->{$field}
                    ];
                },
                'keepFilesOnDelete' => false
            ],
            'image_large_name' => [
                //'path' =>  'webroot{DS}files{DS}{model}{DS}{field}{DS}{primaryKey}',
                'fields' => [
                    'dir' => 'image_large_path',
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $data['name'] = Text::slug( date('YmdHis') . $entity->name ) . '.' . strtolower($extension);
                    return $data['name'];
                },
                'deleteCallback' => function ($path, $entity, $field, $settings) {
                    return [
                        $path . $entity->{$field},
                    ];
                },
                'keepFilesOnDelete' => false
            ],
            'image_internal' => [
                //'path' =>  'webroot{DS}files{DS}{model}{DS}{field}{DS}{primaryKey}',
                'fields' => [
                    'dir' => 'image_internal_path',
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $data['name'] = Text::slug( date('YmdHis') . $entity->name ) . '.' . strtolower($extension);
                    return $data['name'];
                },
                'deleteCallback' => function ($path, $entity, $field, $settings) {
                    return [
                        $path . $entity->{$field},
                    ];
                },
                'keepFilesOnDelete' => false
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 145)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('title')
            ->maxLength('title', 145)
            ->allowEmptyString('title');

        $validator
            ->scalar('subtitle')
            ->maxLength('subtitle', 145)
            ->allowEmptyString('subtitle');

        $validator
            ->scalar('button_label')
            ->maxLength('button_label', 45)
            ->allowEmptyString('button_label');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->scalar('tags')
            ->allowEmptyString('tags');

        $validator
            //->scalar('image_small_name')
            //->maxLength('image_small_name', 255)
            ->allowEmptyFile('image_small_name');

        $validator
            //->scalar('image_small_path')
            //->maxLength('image_small_path', 255)
            ->allowEmptyFile('image_small_path');

        $validator
            //->scalar('image_medium_name')
            //->maxLength('image_medium_name', 255)
            ->allowEmptyFile('image_medium_name');

        $validator
            //->scalar('image_medium_path')
            //->maxLength('image_medium_path', 255)
            ->allowEmptyFile('image_medium_path');

        $validator
            //->scalar('image_large_name')
            //->maxLength('image_large_name', 255)
            ->allowEmptyFile('image_large_name');

        $validator
            //->scalar('image_large_path')
            //->maxLength('image_large_path', 255)
            ->allowEmptyFile('image_large_path');

        $validator
            //->scalar('image_large_name')
            //->maxLength('image_large_name', 255)
            ->allowEmptyFile('image_internal');

        $validator
            ->allowEmptyFile('image_internal_path');

        $validator
            ->scalar('limitwinners')
            ->allowEmptyFile('limitwinners');

        $validator
            ->scalar('tags')
            ->allowEmptyString('tags');

        $validator
            ->integer('status')
            ->allowEmptyString('status');

        $validator
            ->allowEmptyString('audio');

        $validator
            ->allowEmptyString('program_id');

        $validator
            ->allowEmptyString('promotion_type');

        $validator
            ->allowEmptyString('start');

        $validator
            ->allowEmptyString('finish');
        
        $validator
            ->allowEmptyString('finish_promotion');

        $validator
            ->allowEmptyString('regulation');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['company_id'], 'Companies'));
        $rules->add($rules->existsIn(['content_category_id'], 'ContentCategories'));

        return $rules;
    }

    public function beforeSave($event, $entity, $options){
        
        //se a categoria não for podcast, setar o program_id como null
        //if( !in_array($entity->content_category_id, [$this->ContentCategories->getPodcastId()]) ){
        //    $entity->program_id = NULL;
        //}
        $entity->tags = $this->changeTags($entity->tags);

        if( $entity->start ){
            $entity->start = $this->dateFormatBeforeSave($entity->start);
        }
        if( $entity->finish ){
            $entity->finish = $this->dateFormatBeforeSave($entity->finish);
        }

        if( $entity->finish_promotion ){
            $entity->finish_promotion = $this->dateFormatBeforeSave($entity->finish_promotion);
        }
        
        return true;
    }

    private function changeTags($tags=[]){
        if( !empty($tags) ){
            $nTags = [];
            $dataTags = explode(';', $tags);
            foreach ($dataTags as $key => $value) {
                if( $key < self::LIMIT_TAGS ){
                    $nTags[] = trim($value);
                }
            }
            return join(';',$nTags);
        }
        return $tags;
    }

    /**
     * findWebsiteContents
     *
     * @param  Cake\ORM\Query $query
     * @param  array $options
     *
     * @return Cake\ORM\Query
     */
    public function findWebsiteContents(Query $query, array $options){
        
        //$orderBy['Contents.id'] = 'DESC';
        $orderBy['Contents.start'] = 'DESC';

        if(!empty($options['company_code'])){
            $company = $this->Companies->findByCode($options['company_code'])->first();
        } else {
            $company = $this->Companies->findById($options['company_id'])->first();        
        }

        if(empty($company)){
            throw new \Cake\Http\Exception\NotFoundException('company não encontrado');
        }

        if(!empty($options['content_category_code']) || !empty($options['content_category_id'])){
            
            if(!empty($options['content_category_code'])){
                $content_category = $this->ContentCategories->findByCode($options['content_category_code'])->first();
            } else {
                $content_category = $this->ContentCategories->findById($options['content_category_id'])->first();
            }

            if(empty($content_category)){
                throw new \Cake\Http\Exception\NotFoundException('content_category não encontrado');
            }

            $query = $query
                ->where([
                    'Contents.content_category_id' => $content_category->id
                ]);
            
            if( $options['content_category_code'] == 'NEWS' ){
                $orderBy['Contents.start'] = 'DESC';
            }
        }

        //definindo conteúdos exclusivas para site e/ou app
        /* 1: apenas site | 2: apenas app | 3: ambos */ 
        if( isset($options['destination']) and !empty($options['destination']) ){
            
            if( $options['destination'] == 'site' ){
                $query = $query->where([
                    'Contents.destination <>' => self::PROMOTION_DESTINATION_ONLY_APP
                ]);
            }
        }
        else{
            $query = $query->where([
                'Contents.destination IN' => [self::PROMOTION_DESTINATION_ONLY_APP, self::PROMOTION_DESTINATION_BOTH]
            ]);
        }
        
        if( !empty($options['tags']) ){
            $tags = explode(';',$options['tags']);
            for($i = 0; $i < count($tags); $i++){
                $query = $query
                    ->where([
                        'CONCAT(";",REPLACE(Contents.tags,"; ",";"),";") LIKE CONCAT("%;",TRIM("'.$tags[$i].'"),";%")'
                    ]);
            }
        }
        
        if(!empty($options['search'])){

            $query = $query
                ->where([
                    'OR' => [
                        "Contents.name LIKE '%{$options['search']}%'",
                        "Contents.subtitle LIKE '%{$options['search']}%'",
                        "Contents.description LIKE '%{$options['search']}%'",
                        "Contents.tags LIKE '%{$options['search']}%'"
                    ]
                ]);
        }
        return $query
            ->where([
                'Contents.status' => 1,
                'Contents.company_id' => $company->id,
                'Contents.start <=' => date('Y-m-d H:i:s'),
                'OR' => [
                    'Contents.finish >' => date('Y-m-d H:i:s'),
                    'Contents.finish' => '0000-00-00 00:00:00',
                    'Contents.finish IS NULL',
                ]
            ])
            ->order($orderBy);
    }

    public function patchRemoveImages($entity, $data){

        $conn = ConnectionManager::get('default');
        if( isset($data['image_medium_name_remove']) and $data['image_medium_name_remove'] == 1 ){
            $conn->execute("UPDATE contents SET image_medium_name = NULL, image_medium_path = NULL WHERE id = {$entity->id} LIMIT 1");
        }
        if( isset($data['image_large_name_remove']) and $data['image_large_name_remove'] == 1 ){
            $conn->execute("UPDATE contents SET image_large_name = NULL, image_large_path = NULL WHERE id = {$entity->id} LIMIT 1");
        }
        if( isset($data['image_internal_remove']) and $data['image_internal_remove'] == 1 ){
            $conn->execute("UPDATE contents SET image_internal = NULL, image_internal_path = NULL WHERE id = {$entity->id} LIMIT 1");
        }
    }

    public function getPromotionId(){
        return self::PROMOTION_ID;
    }

    public function getListDestinations(){
        return [
            self::PROMOTION_DESTINATION_ONLY_SITE => 'Exclusivo Site',
            self::PROMOTION_DESTINATION_ONLY_APP => 'Exclusivo App',
            self::PROMOTION_DESTINATION_BOTH => 'Site e App'
        ];
    }
}
