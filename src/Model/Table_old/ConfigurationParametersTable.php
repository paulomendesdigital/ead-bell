<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ConfigurationParameters Model
 *
 * @property &\Cake\ORM\Association\BelongsTo $Companies
 *
 * @method \App\Model\Entity\ConfigurationParameter get($primaryKey, $options = [])
 * @method \App\Model\Entity\ConfigurationParameter newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ConfigurationParameter[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ConfigurationParameter|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ConfigurationParameter saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ConfigurationParameter patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ConfigurationParameter[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ConfigurationParameter findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ConfigurationParametersTable extends Table
{

    const IS_RANDOM_WEBDOOR_ORDER_ACTIVE_CODE = "IS_RANDOM_WEBDOOR_ORDER_ACTIVE";

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('configuration_parameters');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('code')
            ->maxLength('code', 100)
            ->requirePresence('code', 'create')
            ->notEmptyString('code');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        $validator
            ->integer('status')
            ->notEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['company_id'], 'Companies'));

        return $rules;
    }

    /**
     * Undocumented function
     *
     * @param \App\Model\Entity\Company $company
     * @return boolean
     */
    public function isRandomWebdoorOrderActive($company)
    {
        return (bool) $this->find()
            ->where([
                'ConfigurationParameters.code' => self::IS_RANDOM_WEBDOOR_ORDER_ACTIVE_CODE,
                'ConfigurationParameters.status' => 1,
                'ConfigurationParameters.company_id' => $company->id
            ])
            ->count();
    }
}
