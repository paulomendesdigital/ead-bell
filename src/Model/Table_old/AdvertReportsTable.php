<?php

namespace App\Model\Table;

use Cake\Datasource\EntityInterface;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * AdvertReports Model
 *
 * @property \App\Model\Table\ModelsTable&\Cake\ORM\Association\BelongsTo $Models
 *
 * @method \App\Model\Entity\AdvertReport get($primaryKey, $options = [])
 * @method \App\Model\Entity\AdvertReport newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AdvertReport[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AdvertReport|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AdvertReport saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AdvertReport patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AdvertReport[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AdvertReport findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AdvertReportsTable extends Table
{

    const STATUS_COMPLETE_ID = 1;
    const STATUS_INCOMPLETE_ID = 2;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('advert_reports');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('model_name')
            ->maxLength('model_name', 100)
            ->requirePresence('model_name', 'create')
            ->notEmptyString('model_name');

        $validator
            ->date('report_date')
            ->requirePresence('report_date', 'create')
            ->notEmptyDate('report_date');

        $validator
            ->integer('views_count')
            ->notEmptyString('views_count');

        $validator
            ->integer('unique_views_count')
            ->notEmptyString('unique_views_count');

        $validator
            ->integer('clicks_count')
            ->notEmptyString('clicks_count');

        $validator
            ->integer('unique_clicks_count')
            ->notEmptyString('unique_clicks_count');

        $validator
            ->decimal('ctr')
            ->notEmptyString('ctr');

        $validator
            ->decimal('unique_ctr')
            ->notEmptyString('unique_ctr');

        $validator
            ->integer('status')
            ->notEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        return $rules;
    }

    public function createReportsForEntity($entityId, $modelName)
    {
        $table = TableRegistry::getTableLocator()->get($modelName);

        $this->deleteAll([
            'AdvertReports.model_name' => $modelName,
            'AdvertReports.model_id' => $entityId,
            'AdvertReports.status' => self::STATUS_INCOMPLETE_ID
        ]);

        $completedReportsDatesList = $this->find('list', [
            'valueField' => 'report_date'
        ])
            ->where([
                'AdvertReports.model_name' => $modelName,
                'AdvertReports.model_id' => $entityId,
                'AdvertReports.status' => self::STATUS_COMPLETE_ID
            ])
            ->toArray();

        $completedDates = [];
        foreach ($completedReportsDatesList as $date) {
            $completedDates[] = $date->format('Y-m-d');
        }

        $reportDate = $this->getNextReportDate($entityId, $modelName, $table, $completedDates);

        while (!empty($reportDate)) {
            $entity = $table
                ->find()
                ->contain([
                    'AdvertClicks' => function ($q) use ($reportDate) {
                        return $q->where(['DATE(AdvertClicks.created)' => $reportDate]);
                    },
                    'AdvertViews' => function ($q) use ($reportDate) {
                        return $q->where(['DATE(AdvertViews.created)' => $reportDate]);
                    }
                ])
                ->where(["$modelName.id" => $entityId])
                ->first();

            $clickStats = [];
            $viewStats = [];

            if (empty($entity->advert_views)) return true;

            foreach ($entity->advert_clicks as $advertClick) {
                $date = $advertClick->created->format('Y-m-d');
                if (isset($clickStats[$advertClick->sessionid])) {
                    $clickStats[$advertClick->sessionid] = $clickStats[$advertClick->sessionid] + 1;
                } else {
                    $clickStats[$advertClick->sessionid] = 1;
                }
            }

            foreach ($entity->advert_views as $advertView) {
                $date = $advertView->created->format('Y-m-d');
                if (isset($viewStats[$advertView->sessionid])) {
                    $viewStats[$advertView->sessionid] = $viewStats[$advertView->sessionid] + 1;
                } else {
                    $viewStats[$advertView->sessionid] = 1;
                }
            }

            $views = 0;
            $uniqueViews = count($viewStats);
            $clicks = 0;
            foreach ($viewStats as $sessionid => $viewsCount) {
                $views = $views + $viewsCount;
            }

            if (!empty($clickStats)) {
                $uniqueClicks = count($clickStats);
                foreach ($clickStats as $sessionid => $clicksCount) {
                    $clicks = $clicks + $clicksCount;
                }
            } else {
                $uniqueClicks = 0;
            }

            if ($views > 0) {
                $ctr = $clicks / $views * 100;
            } else {
                $ctr = 0;
            }

            if ($uniqueViews > 0) {
                $uniqueCtr = $uniqueClicks / $uniqueViews * 100;
            } else {
                $uniqueCtr = 0;
            }

            $advertReport = $this->newEntity([
                'model_id' => $entityId,
                'model_name' => $modelName,
                'report_date' => $reportDate,
                'status' => ($reportDate == date('Y-m-d') ? self::STATUS_INCOMPLETE_ID : self::STATUS_COMPLETE_ID),
                'views_count' => $views,
                'unique_views_count' => $uniqueViews,
                'clicks_count' => $clicks,
                'unique_clicks_count' => $uniqueClicks,
                'ctr' => $ctr,
                'unique_ctr' => $uniqueCtr
            ]);

            if ($this->save($advertReport)) {
                $completedDates[] = $reportDate;
                $reportDate = $this->getNextReportDate($entityId, $modelName, $table, $completedDates);
            } else {
                return false;
            }
        }

        return true;
    }

    private function getNextReportDate($entityId, $modelName, $table, $completedDates)
    {
        $reportDateEntity = $table
            ->find()
            ->contain('AdvertViews', function ($q) use ($completedDates) {
                if (!empty($completedDates)) {
                    return $q->where(['DATE(AdvertViews.created) NOT IN' => $completedDates])->limit(1);
                }
                return $q->limit(1);
            })
            ->matching('AdvertViews', function ($q) use ($completedDates) {
                if (!empty($completedDates)) {
                    return $q->where(['DATE(AdvertViews.created) NOT IN' => $completedDates])->limit(1);
                }
                return $q->limit(1);
            })
            ->where(["$modelName.id" => $entityId])
            ->first();

        if (!empty($reportDateEntity)) {
            return $reportDateEntity->advert_views[0]->created->format('Y-m-d');
        } else {
            return null;
        }
    }

    public function getTotalFromAdvertReports($advertReports)
    {
        $data = [
            'unique_clicks_count' => 0,
            'clicks_count' => 0,
            'views_count' => 0,
            'unique_views_count' => 0,
            'ctr' => 0,
            'unique_ctr' => 0
        ];

        foreach ($advertReports as $advertReport) {
            $data['unique_clicks_count'] += $advertReport->unique_clicks_count;
            $data['clicks_count'] += $advertReport->clicks_count;
            $data['unique_views_count'] += $advertReport->unique_views_count;
            $data['views_count'] += $advertReport->views_count;
        }

        if ($data['views_count'] > 0) {
            $data['ctr'] = $data['clicks_count'] / $data['views_count'] * 100;
        }

        if ($data['unique_views_count'] > 0) {
            $data['unique_ctr'] = $data['unique_clicks_count'] / $data['unique_views_count'] * 100;
        }

        return $data;
    }

    public function getStatusesList()
    {
        return [
            self::STATUS_COMPLETE_ID => 'Completo',
            self::STATUS_INCOMPLETE_ID => 'Incompleto'
        ];
    }
}
