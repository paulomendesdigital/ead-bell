<?php

namespace App\Model\Table;

use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Adverts Model
 *
 * @property \App\Model\Table\AdvertCampaignsTable&\Cake\ORM\Association\BelongsTo $AdvertCampaigns
 * @property \App\Model\Table\AdvertViewsTable&\Cake\ORM\Association\HasMany $AdvertViews
 * @property \App\Model\Table\AdvertZonesTable&\Cake\ORM\Association\BelongsToMany $AdvertZones
 *
 * @method \App\Model\Entity\Advert get($primaryKey, $options = [])
 * @method \App\Model\Entity\Advert newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Advert[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Advert|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Advert saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Advert patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Advert[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Advert findOrCreate($search, callable $callback = null, $options = [])
 *
 * @property \App\Model\Behavior\UtilityBehavior $Utility
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AdvertsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('adverts');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Uuid');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Utility');

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'image' => [
                'path' => 'webroot{DS}files{DS}{model}{DS}{field}{DS}{field-value:uuid}',
                'fields' => [
                    'dir' => 'image_dir',
                ],
            ],
        ]);

        $this->belongsTo('AdvertCampaigns', [
            'foreignKey' => 'advert_campaign_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('AdvertViews', [
            'foreignKey' => 'advert_id'
        ]);
        $this->hasMany('AdvertClicks', [
            'foreignKey' => 'advert_id'
        ]);
        $this->belongsToMany('AdvertZones', [
            'foreignKey' => 'advert_id',
            'targetForeignKey' => 'advert_zone_id',
            'joinTable' => 'adverts_advert_zones',
            'saveStrategy' => 'append'
        ]);

        $this->addBehavior('DistinctBelongsToMany', [
            'associations' => [
                'AdvertZones'
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->setProvider('upload', \Josegonzalez\Upload\Validation\UploadValidation::class);

        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('uuid')
            ->maxLength('uuid', 255)
            ->add('uuid', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('url')
            ->maxLength('url', 255)
            ->allowEmptyString('url');

        $validator
            //->scalar('image')
            //->maxLength('image', 255)
            ->allowEmptyFile('image');

        $validator
            ->add('image', 'fileBelowMaxSize', [
                'rule' => ['isBelowMaxSize', 153600],
                'message' => 'A imagem deve possuir no máximo 150KB',
                'provider' => 'upload'
            ]);

        $validator
            ->scalar('image_dir')
            ->maxLength('image_dir', 255)
            //->allowEmptyFile('image_dir')
        ;

        $validator
            ->integer('unique_views_count')
            ->notEmptyString('unique_views_count');

        $validator
            ->integer('views_count')
            ->notEmptyString('views_count');

        $validator
            ->integer('status')
            ->notEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['uuid']));
        $rules->add($rules->existsIn(['advert_campaign_id'], 'AdvertCampaigns'));

        return $rules;
    }

    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        if (strpos($entity->image_dir, 'webroot') == 0) {
            $entity->image_dir = substr($entity->image_dir, 7);
        }

        //Na edição, o caminho da imagem estava sendo sobreescrito mesmo quando não era enviada uma nova imagem
        if (!$entity->isDirty('image')) {
            $entity->setDirty('image_dir', false);
        }
    }

}
