<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Utility\Text;

/**
 * Players Model
 *
 * @method \App\Model\Entity\Player get($primaryKey, $options = [])
 * @method \App\Model\Entity\Player newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Player[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Player|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Player saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Player patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Player[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Player findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PlayersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('players');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Utility');

        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('live')
            ->allowEmptyString('live');

        $validator
            ->scalar('podcast')
            ->allowEmptyString('podcast');

        $validator
            //->dateTime('start')
            ->requirePresence('start', 'create')
            ->notEmptyDateTime('start');

        $validator
            //->dateTime('finish')
            ->allowEmptyDateTime('finish');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['company_id'], 'Companies'));

        return $rules;
    }

    public function beforeSave($event, $entity, $options){

        if( $entity->start ){
            $entity->start = $this->dateFormatBeforeSave($entity->start) . ' 00:00:01';
        }
        if( $entity->finish ){
            $entity->finish = $this->dateFormatBeforeSave($entity->finish) . ' 23:59:59';
        }
        
        return true;
    }

    /**
     * findWebsitePlayers
     *
     * @param  Cake\ORM\Query $query
     * @param  array $options
     *
     * @return Cake\ORM\Query
     */
    public function findWebsitePlayers(Query $query, array $options){
        $company = $this->Companies->findByCode($options['company_code'])->first();        
        if(empty($company)){
            throw new \Cake\Http\Exception\NotFoundException('company_code não encontrado');
        }
        return $query
            ->where([
                'Players.company_id' => $company->id
            ])
            ->order([
                'Players.created' => 'ASC'
            ]);
    }
}
