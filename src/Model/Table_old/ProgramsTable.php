<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Utility\Text;

/**
 * Programs Model
 *
 * @property \App\Model\Table\CompaniesTable&\Cake\ORM\Association\BelongsTo $Companies
 *
 * @method \App\Model\Entity\Program get($primaryKey, $options = [])
 * @method \App\Model\Entity\Program newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Program[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Program|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Program saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Program patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Program[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Program findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProgramsTable extends Table
{
    CONST IMAGE = '350x300';
    CONST IMAGE_HOME = '350x300';

    CONST PROGRAMA = 1;
    CONST AGENDA = 2;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('programs');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('Contents', [
            'foreignKey' => 'program_id'
        ]);

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'image' => [
                //'path' =>  'webroot{DS}files{DS}{model}{DS}{field}{DS}{primaryKey}',
                'fields' => [
                    'dir' => 'image_path',
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $data['name'] = Text::slug( date('YmdHis') . $entity->name ) . '.' . strtolower($extension);
                    return $data['name'];
                },
                'deleteCallback' => function ($path, $entity, $field, $settings) {
                    return [
                        $path . $entity->{$field},
                    ];
                },
                'keepFilesOnDelete' => false
            ],
            'image_home' => [
                //'path' =>  'webroot{DS}files{DS}{model}{DS}{field}{DS}{primaryKey}',
                'fields' => [
                    //'dir' => 'image_path',
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $data['name'] = Text::slug( date('YmdHis') . $entity->name ) . '.' . strtolower($extension);
                    return $data['name'];
                },
                'deleteCallback' => function ($path, $entity, $field, $settings) {
                    return [
                        $path . $entity->{$field},
                    ];
                },
                'keepFilesOnDelete' => false
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 145)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->allowEmptyString('sutbtitle');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
           // ->scalar('image')
            //->maxLength('image', 255)
            ->allowEmptyFile('image');

        $validator
            //->scalar('image_path')
            //->maxLength('image_path', 255)
            ->allowEmptyFile('image_path');

        $validator
            //->scalar('image_path')
            //->maxLength('image_path', 255)
            ->allowEmptyFile('image_home');

         $validator
            ->allowEmptyFile('info_hour');

        $validator
            ->allowEmptyFile('program_type');

        $validator
            ->integer('status')
            ->allowEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['company_id'], 'Companies'));

        return $rules;
    }

    /**
     * findWebsitePrograms
     *
     * @param  Cake\ORM\Query $query
     * @param  array $options
     *
     * @return Cake\ORM\Query
     */
    public function findWebsitePrograms(Query $query, array $options){
        $company = $this->Companies->findByCode($options['company_code'])->first();        
        if(empty($company)){
            throw new \Cake\Http\Exception\NotFoundException('company_code não encontrado');
        }

        if( isset($options['program_type']) and !empty($options['program_type']) ){

            return $query
                ->where([
                    'Programs.status' => 1,
                    'Programs.company_id' => $company->id,
                    'Programs.program_type' => $options['program_type']
                ])
                ->order([
                    'Programs.ordination' => 'ASC',
                    'Programs.created' => 'ASC'
                ]);            
        }

        return $query
            ->where([
                'Programs.status' => 1,
                'Programs.company_id' => $company->id
            ])
            ->order([
                'Programs.ordination' => 'ASC',
                'Programs.created' => 'ASC'
            ]);
    }

    public function getListProgramTypes(){
        return [
            self::PROGRAMA =>'Programas',
            self::AGENDA => 'Agenda'
        ];
    }
}
