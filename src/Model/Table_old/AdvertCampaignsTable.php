<?php

namespace App\Model\Table;

use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AdvertCampaigns Model
 *
 * @property \App\Model\Table\AdvertisersTable&\Cake\ORM\Association\BelongsTo $Advertisers
 * @property \App\Model\Table\CompaniesTable&\Cake\ORM\Association\BelongsTo $Companies
 * @property \App\Model\Table\AdvertViewsTable&\Cake\ORM\Association\HasMany $AdvertViews
 * @property \App\Model\Table\AdvertsTable&\Cake\ORM\Association\HasMany $Adverts
 * @property \App\Model\Table\AdvertReportsTable&\Cake\ORM\Association\HasMany $AdvertReports
 *
 * @method \App\Model\Entity\AdvertCampaign get($primaryKey, $options = [])
 * @method \App\Model\Entity\AdvertCampaign newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AdvertCampaign[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AdvertCampaign|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AdvertCampaign saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AdvertCampaign patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AdvertCampaign[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AdvertCampaign findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AdvertCampaignsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('advert_campaigns');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Advertisers', [
            'foreignKey' => 'advertiser_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('AdvertViews', [
            'foreignKey' => 'advert_campaign_id'
        ]);
        $this->hasMany('AdvertClicks', [
            'foreignKey' => 'advert_campaign_id'
        ]);
        $this->hasMany('Adverts', [
            'foreignKey' => 'advert_campaign_id'
        ]);
        $this->hasMany('AdvertReports', [
            'foreignKey' => 'model_id',
            'conditions' => [
                'AdvertReports.model_name' => 'AdvertCampaigns'
            ],
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmptyString('name');

        /*$validator
            ->dateTime('start_date')
            ->allowEmptyDateTime('start_date');

        $validator
            ->dateTime('finish_date')
            ->allowEmptyDateTime('finish_date');*/

        $validator
            ->integer('weight')
            ->notEmptyString('weight');

        $validator
            ->integer('max_views')
            ->allowEmptyString('max_views');

        $validator
            ->integer('unique_views_count')
            ->notEmptyString('unique_views_count');

        $validator
            ->integer('views_count')
            ->notEmptyString('views_count');

        $validator
            ->integer('status')
            ->notEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['advertiser_id'], 'Advertisers'));
        $rules->add($rules->existsIn(['company_id'], 'Companies'));

        return $rules;
    }

    public function dateFormatBeforeSave($data)
    {
        //return date('Y-m-d', strtotime($dateString));
        $hora = substr($data, 10, 20);
        $data = substr($data, 0, 10);

        $explode = explode("/", $data);
        if (count($explode) > 1) {
            if ($hora) {
                $data = $explode[2] . "-" . $explode[1] . "-" . $explode[0] . $hora . ":00";
            } else {
                $data = $explode[2] . "-" . $explode[1] . "-" . $explode[0];
            }
        }

        return $data;
    }

    public function beforeSave($event, $entity, $options)
    {
        if (!empty($entity->start_date)) {
            $entity->start_date = $this->dateFormatBeforeSave($entity->start_date);
        }
        if (!empty($entity->finish_date)) {
            $entity->finish_date = $this->dateFormatBeforeSave($entity->finish_date);
        }
        return true;
    }

    public function dateFormatAfterFind($dateString, $hasHour = false)
    {
        if (!$hasHour)
            return $dateString->i18nFormat('dd/MM/Y');
        else
            return $dateString->i18nFormat('dd/MM/Y h:mm');
    }
}
