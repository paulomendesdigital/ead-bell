<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * HomeLayouts Model
 *
 * @property \App\Model\Table\CompaniesTable&\Cake\ORM\Association\BelongsTo $Companies
 * @property \App\Model\Table\HomeLayoutItemsTable&\Cake\ORM\Association\HasMany $HomeLayoutItems
 *
 * @method \App\Model\Entity\HomeLayout get($primaryKey, $options = [])
 * @method \App\Model\Entity\HomeLayout newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\HomeLayout[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\HomeLayout|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\HomeLayout saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\HomeLayout patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\HomeLayout[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\HomeLayout findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class HomeLayoutsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('home_layouts');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('HomeLayoutItems', [
            'foreignKey' => 'home_layout_id',
            //Quando o layout for salvo com a chave home_layout_items, qualquer item que não esteja presente será excluído
            'saveStrategy' => 'replace',
            'dependent' => true
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 45)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        /*$validator
            ->dateTime('start')
            ->requirePresence('start', 'create')
            ->notEmptyDateTime('start');

        $validator
            ->dateTime('finish')
            ->requirePresence('finish', 'create')
            ->notEmptyDateTime('finish');*/

        $validator
            ->integer('status')
            ->notEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['company_id'], 'Companies'));

        return $rules;
    }

    public function dateFormatBeforeSave($data) {
        //return date('Y-m-d', strtotime($dateString));
        $hora = substr($data, 10, 20);
        $data = substr($data, 0, 10);

        $explode = explode("/", $data);
        if( count($explode) > 1 ){
            if ($hora) {
                $data = $explode[2] . "-" . $explode[1] . "-" . $explode[0] . $hora . ":00";                
            } else {
                $data = $explode[2] . "-" . $explode[1] . "-" . $explode[0];
            }
        }

        return $data;
    }

    public function dateFormatAfterFind($dateString, $hasHour=false) {
        if( !$hasHour )
            return $dateString->i18nFormat('dd/MM/Y');
        else            
            return $dateString->i18nFormat('dd/MM/Y h:mm');
    }

    public function beforeSave($event, $entity, $options){        
        if( !empty($entity->start) ){
            $entity->start = $this->dateFormatBeforeSave($entity->start);
        }
        if( !empty($entity->finish) ){
            $entity->finish = $this->dateFormatBeforeSave($entity->finish);
        }        
        return true;
    }    
}
