<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Winners Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\ContentsTable&\Cake\ORM\Association\BelongsTo $Contents
 *
 * @method \App\Model\Entity\Winner get($primaryKey, $options = [])
 * @method \App\Model\Entity\Winner newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Winner[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Winner|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Winner saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Winner patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Winner[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Winner findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WinnersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('winners');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Contents', [
            'foreignKey' => 'content_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Participants', [
            'foreignKey' => 'participant_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['company_id'], 'Companies'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['content_id'], 'Contents'));
        $rules->add($rules->existsIn(['participant_id'], 'Participants'));

        return $rules;
    }

    /**
     * findWebsiteWinners
     *
     * @param  Cake\Database\Query $query
     * @param  array $options
     *
     * @return Cake\Database\Query
     */
    public function findWebsiteWinners(Query $query, array $options){
        $company = $this->Companies->findByCode($options['company_code'])->first();        
        if(empty($company)){
            throw new \Cake\Http\Exception\NotFoundException('company_code não encontrado');
        }
        return $query
            ->where([
                //'Winners.status' => 1,
                'Winners.company_id' => $company->id                
            ])
            ->order([
                'Winners.id' => 'DESC'
            ]);
    }

    public function findWinnersByPromotionId(Query $query, array $options){
        return $query
            ->where([
                'Winners.content_id' => $options['content_id']
            ]);
    }

    public function changeAutoAdd($content_id)
    {
        $participant = $this->Contents->Participants->find()
            ->notMatching('Winners', function ($q) use ($content_id) {
                return $q->where([
                    'Winners.content_id' => $content_id
                ]);
            })
            ->notMatching('WinnersParticipantsUsers', function ($q) use ($content_id) {
                return $q->where([
                    'WinnersParticipantsUsers.content_id' => $content_id
                ]);
            })
            ->where([
                'Participants.content_id' => $content_id,
                'Participants.status' => 1
            ])
            ->order('rand()')
            ->first();
        //die(debug( $participant ));
        if( !empty($participant) ){
            $query = $this->query();
            $query->insert(['participant_id', 'user_id', 'content_id','company_id','created','modified'])
                ->values([
                    'participant_id' => $participant->id,
                    'user_id' => $participant->user_id,
                    'content_id' => $content_id,
                    'company_id' => $participant->company_id,
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                ])
                ->execute();
            return true;
        }
        return false;
    }
}
