<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AdvertZones Model
 *
 * @property \App\Model\Table\CompaniesTable&\Cake\ORM\Association\BelongsTo $Companies
 * @property \App\Model\Table\AdvertViewsTable&\Cake\ORM\Association\HasMany $AdvertViews
 * @property \App\Model\Table\AdvertsTable&\Cake\ORM\Association\BelongsToMany $Adverts
 *
 * @method \App\Model\Entity\AdvertZone get($primaryKey, $options = [])
 * @method \App\Model\Entity\AdvertZone newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AdvertZone[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AdvertZone|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AdvertZone saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AdvertZone patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AdvertZone[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AdvertZone findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AdvertZonesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('advert_zones');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('AdvertViews', [
            'foreignKey' => 'advert_zone_id'
        ]);
        $this->hasMany('AdvertClicks', [
            'foreignKey' => 'advert_zone_id'
        ]);
        $this->belongsToMany('Adverts', [
            'foreignKey' => 'advert_zone_id',
            'targetForeignKey' => 'advert_id',
            'joinTable' => 'adverts_advert_zones'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('code')
            ->maxLength('code', 45)
            ->requirePresence('code', 'create')
            ->notEmptyString('code');

        $validator
            ->integer('width')
            ->allowEmptyString('width');

        $validator
            ->integer('height')
            ->allowEmptyString('height');

        $validator
            ->integer('unique_views_count')
            ->notEmptyString('unique_views_count');

        $validator
            ->integer('views_count')
            ->notEmptyString('views_count');

        $validator
            ->integer('status')
            ->notEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['company_id'], 'Companies'));

        return $rules;
    }
}
