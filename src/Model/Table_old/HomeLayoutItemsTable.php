<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * HomeLayoutItems Model
 *
 * @property \App\Model\Table\HomeLayoutsTable&\Cake\ORM\Association\BelongsTo $HomeLayouts
 * @property \App\Model\Table\ContentsTable&\Cake\ORM\Association\BelongsTo $Contents
 *
 * @method \App\Model\Entity\HomeLayoutItem get($primaryKey, $options = [])
 * @method \App\Model\Entity\HomeLayoutItem newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\HomeLayoutItem[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\HomeLayoutItem|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\HomeLayoutItem saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\HomeLayoutItem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\HomeLayoutItem[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\HomeLayoutItem findOrCreate($search, callable $callback = null, $options = [])
 */
class HomeLayoutItemsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('home_layout_items');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('HomeLayouts', [
            'foreignKey' => 'home_layout_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Contents', [
            'foreignKey' => 'content_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('size')
            ->requirePresence('size', 'create')
            ->notEmptyString('size');

        $validator
            ->integer('row')
            ->requirePresence('row', 'create')
            ->notEmptyString('row');

        $validator
            ->integer('ordination')
            ->requirePresence('ordination', 'create')
            ->notEmptyString('ordination');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['home_layout_id'], 'HomeLayouts'));
        $rules->add($rules->existsIn(['content_id'], 'Contents'));

        return $rules;
    }
}
