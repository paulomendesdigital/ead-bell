<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ContentCategories Model
 *
 * @property \App\Model\Table\CompaniesTable&\Cake\ORM\Association\BelongsTo $Companies
 * @property \App\Model\Table\ContentsTable&\Cake\ORM\Association\HasMany $Contents
 *
 * @method \App\Model\Entity\ContentCategory get($primaryKey, $options = [])
 * @method \App\Model\Entity\ContentCategory newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ContentCategory[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ContentCategory|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ContentCategory saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ContentCategory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ContentCategory[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ContentCategory findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ContentCategoriesTable extends Table
{
    CONST NOTICIA   = 1;
    CONST DESTAQUE  = 2;
    CONST PROMOCAO  = 3;
    CONST PODCAST   = 4;
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('content_categories');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Utility');

        /*$this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);*/
        $this->hasMany('Contents', [
            'foreignKey' => 'content_category_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('code')
            ->maxLength('code', 45)
            ->requirePresence('code', 'create')
            ->notEmptyString('code');

        $validator
            ->scalar('name')
            ->maxLength('name', 45)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');
            
        $validator
            ->scalar('button_label')
            ->maxLength('button_label', 45)
            ->requirePresence('button_label', 'create')
            ->notEmptyString('button_label');

        $validator
            ->scalar('button_label')
            ->maxLength('button_label', 45)
            ->allowEmptyString('button_label');

        $validator
            ->integer('status')
            ->notEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    /*public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['company_id'], 'Companies'));

        return $rules;
    }*/

    public function beforeSave($event, $entity, $options){
        //Campo code não pode ser alterado ou pode quebrar aplicações já existentes
        if(!empty($entity->id) && $entity->dirty('code')){
            return false;
        }
        if( !empty($entity->code) ){
            $entity->code = strtoupper( $this->__Normalize($entity->code) );
        }

        return true;
    }

    public function getPromocaoId(){
        return self::PROMOCAO;
    }

    public function getPodcastId(){
        return self::PODCAST;
    }

    /**
     * findWebsiteContentCategories
     *
     * @param  Cake\Database\Query $query
     * @param  array $options
     *
     * @return Cake\Database\Query
     */
    public function findWebsiteContentCategories(Query $query, array $options){        
        return $query
            ->where([
                'ContentCategories.status' => 1
            ]);
    }
}
