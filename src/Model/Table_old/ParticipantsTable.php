<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

/**
 * Participants Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\ContentsTable&\Cake\ORM\Association\BelongsTo $Contents
 * @property \App\Model\Table\QuestionsTable&\Cake\ORM\Association\BelongsTo $Questions
 * @property \App\Model\Table\AlternativesTable&\Cake\ORM\Association\BelongsTo $Alternatives
 *
 * @method \App\Model\Entity\Participant get($primaryKey, $options = [])
 * @method \App\Model\Entity\Participant newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Participant[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Participant|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Participant saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Participant patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Participant[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Participant findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ParticipantsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('participants');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Contents', [
            'foreignKey' => 'content_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Questions', [
            'foreignKey' => 'question_id'
        ]);
        $this->belongsTo('Alternatives', [
            'foreignKey' => 'alternative_id'
        ]);
        $this->belongsTo('AccessOrigins', [
            'foreignKey' => 'access_origin_id'
        ]);

        $this->hasMany('Winners', [
            'foreignKey' => 'participant_id'
        ]);

        $this->hasMany('WinnersParticipantsUsers', [
            'foreignKey' => 'user_id',
            'className' => 'Winners',
            'bindingKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('status')
            ->allowEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['company_id'], 'Companies'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['content_id'], 'Contents'));
        //$rules->add($rules->existsIn(['question_id'], 'Questions'));
        //$rules->add($rules->existsIn(['alternative_id'], 'Alternatives'));

        return $rules;
    }

    public function removeParticipation($user_id = null, $content_id = null)
    {

        $conn = ConnectionManager::get('default');
        if ($user_id and $content_id) {
            return $conn->execute("DELETE FROM participants WHERE user_id = {$user_id} AND content_id = {$content_id}");
        }
        return false;
    }

    public function getCountAccessOrigins($content_id=null){
        
        $accessOrigins = $this->AccessOrigins->find()->toArray();
        $countAccessOrigins = [];
        foreach ($accessOrigins as $accessOrigin) {
            $countAccessOrigins[$accessOrigin['name']]['total'] = $this->find()
                ->where([
                    'Participants.content_id' => $content_id,
                    'Participants.access_origin_id' => $accessOrigin['id']
                ])
                ->count();
        }
        return $countAccessOrigins;
    }
}
