<?php

namespace App\Model\Table;

use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AdvertViews Model
 *
 * @property \App\Model\Table\AdvertZonesTable&\Cake\ORM\Association\BelongsTo $AdvertZones
 * @property \App\Model\Table\AdvertCampaignsTable&\Cake\ORM\Association\BelongsTo $AdvertCampaigns
 * @property \App\Model\Table\AdvertsTable&\Cake\ORM\Association\BelongsTo $Adverts
 * @property \App\Model\Table\AdvertsAdvertZonesTable&\Cake\ORM\Association\BelongsTo $AdvertsAdvertZones
 *
 * @method \App\Model\Entity\AdvertView get($primaryKey, $options = [])
 * @method \App\Model\Entity\AdvertView newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AdvertView[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AdvertView|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AdvertView saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AdvertView patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AdvertView[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AdvertView findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AdvertViewsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('advert_views');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('AdvertZones', [
            'foreignKey' => 'advert_zone_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('AdvertCampaigns', [
            'foreignKey' => 'advert_campaign_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Adverts', [
            'foreignKey' => 'advert_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('AdvertsAdvertZones', [
            'foreignKey' => 'advert_advert_zone_id',
            'joinType' => 'INNER'
        ]);

        $this->addBehavior('CounterCache', [
            'AdvertsAdvertZones' => [
                'views_count',
                'unique_views_count' => [
                    'finder' => 'uniqueViews'
                ]
            ],
            'AdvertCampaigns' => [
                'views_count',
                'unique_views_count' => [
                    'finder' => 'uniqueViews'
                ]
            ],
            'AdvertZones' => [
                'views_count',
                'unique_views_count' => [
                    'finder' => 'uniqueViews'
                ]
            ],
            'Adverts' => [
                'views_count',
                'unique_views_count' => [
                    'finder' => 'uniqueViews'
                ]
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('sessionid')
            ->maxLength('sessionid', 255)
            ->requirePresence('sessionid', 'create')
            ->notEmptyString('sessionid');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['advert_zone_id'], 'AdvertZones'));
        $rules->add($rules->existsIn(['advert_campaign_id'], 'AdvertCampaigns'));
        $rules->add($rules->existsIn(['advert_id'], 'Adverts'));
        $rules->add($rules->existsIn(['advert_advert_zone_id'], 'AdvertsAdvertZones'));

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        if (empty($data['advert_advert_zone_id'])) return;

        $advertAdvertZone = $this->AdvertsAdvertZones->find()
            ->contain([
                'Adverts'
            ])
            ->where(['AdvertsAdvertZones.id' => $data['advert_advert_zone_id']])
            ->first();

        if (empty($advertAdvertZone)) return;

        $data['advert_id'] = $advertAdvertZone->advert_id;
        $data['advert_zone_id'] = $advertAdvertZone->advert_zone_id;
        $data['advert_campaign_id'] = $advertAdvertZone->advert->advert_campaign_id;
    }

    public function afterSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        $advertCampaign = $this->AdvertCampaigns->find()
            ->where(['AdvertCampaigns.id' => $entity->advert_campaign_id])
            ->first();

        if (!is_null($advertCampaign->max_views) && $advertCampaign->views_count >= $advertCampaign->max_views) {
            $advertCampaign->status = 0;
        }

        if (!is_null($advertCampaign->max_unique_views) && $advertCampaign->unique_views_count >= $advertCampaign->max_unique_views) {
            $advertCampaign->status = 0;
        }

        $this->AdvertCampaigns->save($advertCampaign);
    }

    public function findUniqueViews(Query $query, array $options)
    {
        return $query->group(['AdvertViews.sessionid', 'DATE(AdvertViews.created)']);
    }
}
