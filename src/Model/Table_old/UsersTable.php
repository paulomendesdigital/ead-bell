<?php

namespace App\Model\Table;

use ArrayObject;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Event\Event;

//use Cake\Datasource\ConnectionManager;

/**
 * Users Model
 *
 * @property \App\Model\Table\CompaniesTable&\Cake\ORM\Association\BelongsTo $Companies
 * @property \App\Model\Table\GroupsTable&\Cake\ORM\Association\BelongsTo $Groups
 * @property \App\Model\Table\UserProvidersTable&\Cake\ORM\Association\HasMany $UserProviders
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        //$this->addBehavior('Acl.Acl', ['type' => 'requester']);

        $this->setTable('users');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Utility');

        $this->addBehavior('Crypt', [
            'fields' => [
                'name',
                'email',
                'username',
                'cpf',
                'rg',
                'gender',
                'phone',
                'cep',
                'address',
                'number',
                'complement',
                'neighborhood',
                'city',
                'schooling',
                'state'
            ]
        ]);

        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Groups', [
            'foreignKey' => 'group_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('UserProviders', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Participants', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Winners', [
            'foreignKey' => 'user_id'
        ]);

        //Trata as alterações feitas por causa da criptografia para que as views não precisem ser alteradas individualmente
        $this->getSchema()->setColumnType('cep', 'string');
        $this->getSchema()->setColumnType('name', 'string');
        $this->getSchema()->setColumnType('email', 'string');
        $this->getSchema()->setColumnType('username', 'string');
        $this->getSchema()->setColumnType('cpf', 'string');
        $this->getSchema()->setColumnType('rg', 'string');
        $this->getSchema()->setColumnType('gender', 'string');
        $this->getSchema()->setColumnType('phone', 'string');
        $this->getSchema()->setColumnType('cep', 'string');
        $this->getSchema()->setColumnType('address', 'string');
        $this->getSchema()->setColumnType('number', 'string');
        $this->getSchema()->setColumnType('complement', 'string');
        $this->getSchema()->setColumnType('neighborhood', 'string');
        $this->getSchema()->setColumnType('city', 'string');
        $this->getSchema()->setColumnType('schooling', 'string');
        $this->getSchema()->setColumnType('state', 'string');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            //->scalar('firebase_uid')
            //->maxLength('firebase_uid', 255)
            ->allowEmptyString('firebase_uid');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->scalar('username')
            ->maxLength('username', 100)
            ->allowEmptyString('username');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->allowEmptyString('password');

        $validator
            ->integer('status')
            ->allowEmptyString('status');

        return $validator;
    }

    public function validationApiLoginFirebase(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('firebase_uid')
            ->maxLength('firebase_uid', 255)
            ->requirePresence('firebase_uid')
            ->notEmptyString('firebase_uid');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->requirePresence('user_providers')
            ->notEmptyArray('user_providers')
            ->hasAtLeast('user_providers', 1)
            ->hasAtMost('user_providers', 1);

        return $validator;
    }

    public function validationApiLogin(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->scalar('username')
            ->maxLength('username', 255)
            ->requirePresence('username', 'create')
            ->notEmptyString('username');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->requirePresence('password', 'create')
            ->notEmptyString('password');

        return $validator;
    }

    public function validationNewAros(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->scalar('username')
            ->maxLength('username', 255)
            ->requirePresence('username', 'create')
            ->notEmptyString('username');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        //Email e username podem se repetir para diferentes companies
        $rules->add($rules->isUnique(['company_id', 'email'], 'Este email já encontra-se em uso.'));
        $rules->add($rules->isUnique(['company_id', 'cpf'], 'Este cpf já encontra-se em uso.'));
        $rules->add($rules->isUnique(['company_id', 'username'], 'Este email já encontra-se em uso.'));
        $rules->add($rules->existsIn(['company_id'], 'Companies'));
        $rules->add($rules->existsIn(['group_id'], 'Groups'));

        return $rules;
    }

    public function getListSchooolings()
    {
        return [
            'fundamental' => 'Ensino fundamental',
            'medio' => 'Ensino médio',
            'superior' => 'Superior (Graduação)',
            'posgraduado' => 'Pós-graduação',
            'mestrado' => 'Mestrado',
            'doutorado' => 'Doutorado'
        ];
    }

    public function findPartipantsContents(Query $query, array $options)
    {
        $company = $this->Companies->findByCode($options['company_code'])->first();
        if (empty($company)) {
            throw new \Cake\Http\Exception\NotFoundException('company_code não encontrado');
        }

        return $query
            ->where([
                'Users.status' => 1,
                'Users.group_id' => 2,
            ])
            ->order([
                'Users.created' => 'DESC'
            ])
            ->limit(10);
    }
}
