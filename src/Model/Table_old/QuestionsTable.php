<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Questions Model
 *
 * @property \App\Model\Table\ContentsTable&\Cake\ORM\Association\BelongsTo $Contents
 * @property \App\Model\Table\AlternativesTable&\Cake\ORM\Association\HasMany $Alternatives
 * @property \App\Model\Table\ParticipantsTable&\Cake\ORM\Association\HasMany $Participants
 *
 * @method \App\Model\Entity\Question get($primaryKey, $options = [])
 * @method \App\Model\Entity\Question newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Question[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Question|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Question saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Question patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Question[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Question findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class QuestionsTable extends Table
{
    CONST QUESTION_TYPE_TEXT = "TEXT";
    CONST QUESTION_TYPE_RADIO_BUTTON = "RADIO";
    CONST QUESTION_TYPE_CHECKBOX = "CHECKBOX";
    CONST QUESTION_TYPE_PREMIO = "PREMIO";

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('questions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Contents', [
            'foreignKey' => 'content_id'
        ]);
        $this->hasMany('Alternatives', [
            'foreignKey' => 'question_id',
            'joinType' => 'LEFT',
            'saveStrategy' => 'replace'
        ]);
        $this->hasMany('Participants', [
            'foreignKey' => 'question_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('type')
            ->maxLength('type', 255)
            ->allowEmptyString('type');

        $validator
            ->scalar('question')
            ->requirePresence('question', 'create')
            ->notEmptyString('question');

        $validator
            ->integer('status')
            ->allowEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['content_id'], 'Contents'));

        return $rules;
    }

    public function getQuestionTypes(){
        return [
            self::QUESTION_TYPE_TEXT => 'Texto',
            self::QUESTION_TYPE_RADIO_BUTTON => 'Radio Button',
            self::QUESTION_TYPE_CHECKBOX => 'Checkbox',
            self::QUESTION_TYPE_PREMIO => 'Premio'
        ];
    }

    public function getQuestionTypeTextId(){
        return self::QUESTION_TYPE_TEXT;
    }

    public function getQuestionTypeRadioButtonId(){
        return self::QUESTION_TYPE_RADIO_BUTTON;
    }

    public function getQuestionTypeCheckboxId(){
        return self::QUESTION_TYPE_CHECKBOX;
    }

    public function getQuestionTypePremioId(){
        return self::QUESTION_TYPE_PREMIO;
    }
}
