<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Utility\Text;
use Cake\Datasource\ConnectionManager;

/**
 * Advertisements Model
 *
 * @property \App\Model\Table\CompaniesTable&\Cake\ORM\Association\BelongsTo $Companies
 *
 * @method \App\Model\Entity\Advertisement get($primaryKey, $options = [])
 * @method \App\Model\Entity\Advertisement newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Advertisement[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Advertisement|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Advertisement saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Advertisement patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Advertisement[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Advertisement findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AdvertisementsTable extends Table
{
    CONST HOME_SUPERBANNER_SIZE     = '900×250';
    CONST HOME_RETANGULO_SIZE       = '300x250';

    CONST INTERNA_SUPERBANNER_SIZE  = '900x250';

    CONST PLAYER_CABECALHO_SIZE     = '728x90';
    CONST PLAYER_RETANGULO_SIZE     = '300x250';

    CONST MOBILE_SIZE               = '300×250';


    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('advertisements');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Utility');

        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'image' => [
                'fields' => ['dir' => 'image_path'],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $data['name'] = Text::slug( date('YmdHis') . $entity->name ) . '.' . strtolower($extension);
                    return $data['name'];
                },
                'deleteCallback' => function ($path, $entity, $field, $settings) {
                    return [$path . $entity->{$field}];
                },
                'keepFilesOnDelete' => false
            ],
            'image_mobile' => [
                'fields' => ['dir' => 'image_mobile_path'],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $data['name'] = Text::slug( date('YmdHis') . $entity->name ) . '.' . strtolower($extension);
                    return $data['name'];
                },
                'deleteCallback' => function ($path, $entity, $field, $settings) {
                    return [$path . $entity->{$field}];
                },
                'keepFilesOnDelete' => false
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 145)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->date('start')
            ->requirePresence('start', 'create')
            ->notEmptyDate('start');
        
        $validator
            ->allowEmptyFile('image');

        $validator
            ->allowEmptyFile('image_path');

        $validator
            ->allowEmptyFile('image_mobile');

        $validator
            ->allowEmptyFile('image_mobile_path');

        $validator
            ->integer('status')
            ->notEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['company_id'], 'Companies'));

        return $rules;
    }

    /**
     * findWebsiteAdvertisements
     *
     * @param  Cake\Database\Query $query
     * @param  array $options
     *
     * @return Cake\Database\Query
     */
    public function findWebsiteAdvertisements(Query $query, array $options){
        $company = $this->Companies->findByCode($options['company_code'])->first();        
        if(empty($company)){
            throw new \Cake\Http\Exception\NotFoundException('company_code não encontrado');
        }

        if( isset($options['position']) and !empty($options['position']) ){
            $query = $query
                ->where([
                    'Advertisements.position' => $options['position']
                ]);
        }

        if( isset($options['type_pub']) and !empty($options['type_pub']) ){
            $query = $query
                ->where([
                    'Advertisements.type_pub' => $options['type_pub']
                ]);
        }

        return $query
            ->where([
                'Advertisements.status' => 1,
                'Advertisements.company_id' => $company->id,
                'Advertisements.start <=' => date('Y-m-d H:i:s'),
                'OR' => [
                    'Advertisements.finish >' => date('Y-m-d H:i:s'),
                    'Advertisements.finish' => '0000-00-00 00:00:00',
                    'Advertisements.finish IS NULL',
                ]                
            ])
            ->order('rand()');
    }

    public function patchRemoveImages($entity, $data){

        $conn = ConnectionManager::get('default');
        if( isset($data['image_remove']) and $data['image_remove'] == 1 ){
            $conn->execute("UPDATE advertisements SET image = NULL, image_path = NULL WHERE id = {$entity->id} LIMIT 1");
        }
        if( isset($data['image_mobile_remove']) and $data['image_mobile_remove'] == 1 ){
            $conn->execute("UPDATE advertisements SET image_mobile = NULL, image_mobile_path = NULL WHERE id = {$entity->id} LIMIT 1");
        }
    }

    public function GetTypePubs(){
        return [
            'GOOGLE' => 'GOOGLE',
            'SITE' => 'SITE'
        ];
    }

    public function GetPositions(){
        return [
            'HOME_SUPERBANNER'      => 'HOME_SUPERBANNER',
            'HOME_RETANGULO'        => 'HOME_RETANGULO',
            'INTERNA_SUPERBANNER'   => 'INTERNA_SUPERBANNER',
            'PLAYER_CABECALHO'      => 'PLAYER_CABECALHO',
            'PLAYER_RETANGULO'      => 'PLAYER_RETANGULO',
        ];
    }

    public function GetSizeByPositions(){
        return [
            'HOME_SUPERBANNER'      => '900×250',
            'HOME_RETANGULO'        => '300x250',
            'INTERNA_SUPERBANNER'   => '900x250',
            'PLAYER_CABECALHO'      => '728x90',
            'PLAYER_RETANGULO'      => '300x250',
        ];
    }
}
