<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Utility\Text;

/**
 * Speakers Model
 *
 * @property \App\Model\Table\CompaniesTable&\Cake\ORM\Association\BelongsTo $Companies
 *
 * @method \App\Model\Entity\Speaker get($primaryKey, $options = [])
 * @method \App\Model\Entity\Speaker newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Speaker[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Speaker|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Speaker saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Speaker patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Speaker[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Speaker findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SpeakersTable extends Table
{
    CONST IMAGE_SIZE = '300x300';
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('speakers');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'image' => [
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $data['name'] = Text::slug( date('YmdHis') . $entity->name ) . '.' . strtolower($extension);
                    return $data['name'];
                },
                'deleteCallback' => function ($path, $entity, $field, $settings) {
                    return [
                        $path . $entity->{$field},
                    ];
                },
                'keepFilesOnDelete' => false
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 145)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->allowEmptyFile('image');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->scalar('facebook')
            ->maxLength('facebook', 145)
            ->allowEmptyString('facebook');

        $validator
            ->scalar('instagram')
            ->maxLength('instagram', 145)
            ->allowEmptyString('instagram');

        $validator
            ->scalar('twitter')
            ->maxLength('twitter', 145)
            ->allowEmptyString('twitter');

        $validator
            ->scalar('whatsapp')
            ->maxLength('whatsapp', 45)
            ->allowEmptyString('whatsapp');

        $validator
            ->scalar('video')
            ->allowEmptyString('video');

        $validator
            ->scalar('work_schedule')
            ->maxLength('work_schedule', 145)
            ->allowEmptyString('work_schedule');

        $validator
            ->integer('ordination')
            ->allowEmptyString('ordination');

        $validator
            ->integer('status')
            ->allowEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['company_id'], 'Companies'));

        return $rules;
    }

    /**
     * findWebsiteSpeakers
     *
     * @param  Cake\Database\Query $query
     * @param  array $options
     *
     * @return Cake\Database\Query
     */
    public function findWebsiteSpeakers(Query $query, array $options){
        $company = $this->Companies->findByCode($options['company_code'])->first();        
        if(empty($company)){
            throw new \Cake\Http\Exception\NotFoundException('company_code não encontrado');
        }
        return $query
            ->where([
                'Speakers.status' => 1,
                'Speakers.company_id' => $company->id                
            ])
            ->order([
                'ISNULL(Speakers.ordination)',
                'Speakers.ordination' => 'ASC',
                'Speakers.created' => 'DESC'
            ]);
    }
}
