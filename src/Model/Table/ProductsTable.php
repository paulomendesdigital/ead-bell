<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\ORM\TableRegistry;

class ProductsTable extends Table
{
    private $categories;

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id'
        ]);

        $this->belongsToMany('Teachers');
        $this->belongsToMany('Disciplines');
    }

    public function findGetAll(Query $query, array $options)
    {
        $this->categories = TableRegistry::getTableLocator()->get('Categories');
        $category_id = !empty($options['category_id']) ? $options['category_id'] : $this->categories->getCursosOndemandId();

        $hoje = date('Y-m-d');

        $conditions['Products.status'] = 1;
        $conditions['Products.featured'] = 1;
        $conditions['Products.start <='] = $hoje;
        $conditions['Products.finish >='] = $hoje;
        $conditions['Products.category_id'] = $category_id;

        $limit = !empty($options['limit']) ? $options['limit'] : 4;

        return $query->where($conditions)
                ->contain(['Categories', 'Teachers'])
                ->order(['Products.featured_top' => 'DESC', 'Products.featured' => 'DESC', 'ISNULL(Products.ordination)' => 'ASC',  'Products.ordination' => 'ASC'])
                ->limit($limit);
    }
}
