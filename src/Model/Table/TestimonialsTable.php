<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class TestimonialsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Clients', [
            'foreignKey' => 'client_id'
        ]);
    }

    public function findGetAll(Query $query, array $options)
    {
		$conditions['Testimonials.status'] = 1;

		if (!empty($options['current_id'])) {
			$conditions['Testimonials.id !='] = $options['current_id'];
		}

        $limit = !empty($options['limit']) ? $options['limit'] : 4;

        return $query->where($conditions)
                ->order(['Testimonials.id' => 'DESC'])
                ->limit($limit);
    }
}
