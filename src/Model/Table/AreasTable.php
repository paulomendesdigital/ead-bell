<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class AreasTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Products', [
            'foreignKey' => 'area_id',
        ]);
    }

    // public function findGetList()
    // {
    //     return $this->find('list', ['conditions' => ['Area.status' => 1], 'order' => ['Area.name' => 'ASC']]);
    // }

    public function findGetAll(Query $query, array $options = [])
    {
        $conditions['Areas.status'] = 1;
        $conditions['product_count >'] = 0;

        if ($options['show_menu']) {
            $conditions['Areas.show_menu'] = (int) $options['show_menu'];
        }

        return $query->where($conditions)
                ->contain(['Products' => function($q) { return $q->select(['id', 'title', 'area_id']); }])
                ->order(['ISNULL(Areas.ordination)', 'Areas.ordination', 'Areas.name' => 'ASC']);
    }

    // public function findGetListHeader()
    // {
    //     $conditions['Area.status'] = 1;
    //     $conditions['Area.product_count >'] = 0;
    //     return $this->find('all', 
    //         array(
    //             'conditions' => $conditions,
    //             'order' => array('ISNULL(Area.ordination)', 'Area.ordination', 'Area.name' => 'ASC')
    //         )
    //     );
    // }
}
