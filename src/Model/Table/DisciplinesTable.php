<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class DisciplinesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Questions');

        $this->belongsToMany('Topics');
        $this->belongsToMany('Products');
    }

	public function findGetList(Query $query)
	{
        $conditions = ['status' => 1];

		return $query->find('list')
                    ->where($conditions)
                    ->order(['name' => 'ASC']);
	}
}
