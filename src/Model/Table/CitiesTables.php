<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class CitiesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('States', [
            'foreignKey' => 'state_id',
        ]);

        $this->hasMany('ClientAddresses', [
            'foreignKey' => 'city_id',
        ]);
    }
}
