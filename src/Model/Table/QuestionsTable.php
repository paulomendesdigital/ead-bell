<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class QuestionsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        
        $this->belongsTo('Topics');
        $this->belongsTo('Disciplines');
        $this->belongsTo('Lessons');
        // $this->belongsTo('QuestionThemes');

        // // $this->hasMany('QuestionAlternatives');
        // $this->hasMany('Responses');
    }
}
