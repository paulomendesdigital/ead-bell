<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class TopicsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Questions');

        $this->belongsToMany('Disciplines');
        $this->belongsToMany('Lessons');
    }

	public function findGetList(Query $query, array $options)
	{
        $topics = $query->order([
            'ISNULL(Topics.ordination)',
            'Topics.ordination' => 'ASC',
            'Topics.name' => 'ASC'
        ]);

        return $this->extractList($topics);
	}

    private function extractList($topics)
    {
        $list = [];

        foreach ($topics as $key => $value) {
            $list[$value['id']] = "{$value['name']} - {$value['label']}";
        }

        return $list;
    }
}
