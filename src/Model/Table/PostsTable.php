<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class PostsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Categoryposts', [
            'foreignKey' => 'categorypost_id'
        ]);
    }

    public function findGetAll(Query $query, array $options)
    {
		$conditions['Posts.status'] = 1;

		if (!empty($options['categorypost_id'])) {
			$conditions['Posts.categorypost_id !='] = $options['categorypost_id'];
		}

        $hoje = date('Y-m-d');

        $limit = !empty($options['limit']) ? $options['limit'] : 4;

        return $query->where($conditions)
                ->contain('Categoryposts')
                ->order(['Posts.id' => 'DESC'])
                ->limit($limit);
    }
}
