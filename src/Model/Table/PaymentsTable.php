<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;

use Cake\ORM\TableRegistry;

class PaymentsTable extends Table
{
	CONST PENDENTE          = 0;
    CONST AGUARDANDO_PAGTO  = 1;
    CONST EM_ANALISE        = 2;
    CONST APROVADO          = 3;
    CONST DISPONIVEL        = 4; //qdo o dinheiro está liberado para resgate
    CONST EM_DISPUTA        = 5;
    CONST DEVOLVIDO         = 6;
    CONST CANCELADO         = 7;
    CONST CHARGEBACK        = 8; //o valor da transação foi devolvido para o comprador.
    CONST CONTESTACAO       = 9; //o comprador abriu uma solicitação de chargeback junto à operadora do cartão de crédito.

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    function getStatusList()
    {
        return [
            self::PENDENTE          => "Pendente",
            self::AGUARDANDO_PAGTO  => "Aguardando Pagto",
            self::EM_ANALISE        => "Em Análise",
            self::APROVADO          => "Aprovado",
            self::DISPONIVEL        => "Disponível",
            self::EM_DISPUTA        => "Em Disputa",
            self::DEVOLVIDO         => "Devolvido",
            self::CANCELADO         => "Cancelado",
            self::CHARGEBACK        => "Chargeback debitado",
            self::CONTESTACAO       => "Em Contestação"
        ];
    }

    function getStatusListForReports()
    {
        return [
            self::PENDENTE          => "Pendente",
            self::AGUARDANDO_PAGTO  => "Aguardando Pagto",
            self::EM_ANALISE        => "Em Análise",
            self::APROVADO          => "Aprovado",
            self::EM_DISPUTA        => "Em Disputa",
            self::DEVOLVIDO         => "Devolvido",
            self::CANCELADO         => "Cancelado"
        ];
    }

    function getStatusByCode($code)
    {
        $listStatus = $this->getStatusList();
        return $listStatus[$code];
    }

    function getStatusByText($text)
    {
        switch ($text) {
            case "Pendente"             : return self::PENDENTE;
            case "Aguardando Pagto"     : return self::AGUARDANDO_PAGTO;
            case "Em Análise"           : return self::EM_ANALISE;
            case "Aprovado"             : return self::APROVADO;
            case "Disponível"           : return self::DISPONIVEL;
            case "Em Disputa"           : return self::EM_DISPUTA;
            case "Devolvido"            : return self::DEVOLVIDO;
            case "Cancelado"            : return self::CANCELADO;
            case "Chargeback debitado"  : return self::CHARGEBACK;
            case "Em Contestação"       : return self::CONTESTACAO;
            default                     : return 0;
        }
    }

    function getStatusForConditions()
    {
        return [
            self::PENDENTE,
            self::AGUARDANDO_PAGTO,
            self::EM_ANALISE,
            self::APROVADO,
            self::DISPONIVEL,
            self::EM_DISPUTA,
            self::DEVOLVIDO,
            self::CANCELADO,
            self::CHARGEBACK,
            self::CONTESTACAO
        ];
    }

    function getStatusAprovados()
    {
        return [self::APROVADO, self::DISPONIVEL];
    }

    function getStatusAprovado()
    {
        return self::APROVADO;
    }

    function getStatusAguardandoPgto()
    {
        return self::AGUARDANDO_PAGTO;
    }

    function getStatusCancelado()
    {
        return self::CANCELADO;
    }

    public function getStatusByPagarmeStatus($code)
    {
        switch ($code) {
            case "waiting_payment" : return self::AGUARDANDO_PAGTO;//Transação aguardando pagamento (status válido para Boleto bancário).
            case "pending_refund"  : return self::EM_DISPUTA;//Transação do tipo Boleto e que está aguardando confirmação do estorno solicitado.
            case "refunded"        : return self::DEVOLVIDO;//Transação estornada completamente.
            case "processing"      : return self::EM_ANALISE;
            case "authorized"      : return self::PENDENTE; //Transação foi autorizada. Cliente possui saldo na conta e este valor foi reservado para futura captura, que deve acontecer em até 5 dias para transações criadas com api_key. Caso não seja capturada, a autorização é cancelada automaticamente pelo banco emissor, e o status dela permanece como authorized.
            case "refused"         : return self::CANCELADO;//Transação recusada, não autorizada.
            case "paid"            : return self::APROVADO;//Transação paga. Foi autorizada e capturada com sucesso. Para Boleto, significa que nossa API já identificou o pagamento de seu cliente.
            case "chargedback"     : return self::CANCELADO;//Chargeback é a contestação de uma compra, feita pelo portador junto ao emissor do cartão. Na prática, significa que um problema aconteceu no meio do caminho e o comprador pediu ao banco o seu dinheiro de volta.
            default                : return self::AGUARDANDO_PAGTO;
        }
    }

    public function getPagarmeStatusByStatus($pagarme_status)
    {
        switch ($pagarme_status) {
            case self::AGUARDANDO_PAGTO : return "waiting_payment";
            case self::EM_DISPUTA       : return "pending_refund";
            case self::DEVOLVIDO        : return "refunded";
            case self::EM_ANALISE       : return "processing";
            case self::APROVADO         : return "authorized";
            case self::CANCELADO        : return "refused";
            case self::APROVADO         : return "paid";
            //case self::DISPONIVEL       : return "paid"; //não usaremos o disponivel
            default : return "Não Identificado";
        }
    }

    public function getStatusByPagarmeStatusForEmail($code)
    {
        switch ($code) {
            case "waiting_payment" : return 'AGUARDANDO PAGTO';//Transação aguardando pagamento (status válido para Boleto bancário).
            case "pending_refund"  : return 'EM DISPUTA';//Transação do tipo Boleto e que está aguardando confirmação do estorno solicitado.
            case "refunded"        : return 'DEVOLVIDO';//Transação estornada completamente.
            case "processing"      : return 'EM ANÁLISE';
            case "authorized"      : return 'PENDENTE'; //Transação foi autorizada. Cliente possui saldo na conta e este valor foi reservado para futura captura, que deve acontecer em até 5 dias para transações criadas com api_key. Caso não seja capturada, a autorização é cancelada automaticamente pelo banco emissor, e o status dela permanece como authorized.
            case "refused"         : return 'CANCELADO';//Transação recusada, não autorizada.
            case "paid"            : return 'APROVADO';//Transação paga. Foi autorizada e capturada com sucesso. Para Boleto, significa que nossa API já identificou o pagamento de seu cliente.
            case "chargedback"     : return 'CANCELADO';//Chargeback é a contestação de uma compra, feita pelo portador junto ao emissor do cartão. Na prática, significa que um problema aconteceu no meio do caminho e o comprador pediu ao banco o seu dinheiro de volta.
            default                : return 'AGUARDANDO PAGTO';
        }
    }

    /**
     * Função para tratar transactionId de consulta ao pagseguro
     * Ori: BA2BD7675EE14448A76B49FD620461D0
     * Mod: BA2BD767-5EE1-4448-A76B-49FD620461D0
     */
    function transactionIdPagseguro($data)
    {
        $dat1 = substr($data, 0, 8);
        $dat2 = substr($data, 8, 4);
        $dat3 = substr($data, 12, 4);
        $dat4 = substr($data, 16, 4);
        $dat5 = substr($data, 20, 12);

        return $dat1 . '-' . $dat2 . '-' . $dat3 . '-' . $dat4 . '-' . $dat5;
    }
}
