<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class CategoriesTable extends Table
{

	CONST CURSOS_ONDEMAND 		= 1;
    CONST CURSOS_AOVIVO 		= 2;
    CONST CURSOS_AOVIVO_GRATIS 	= 3;
    CONST CURSOS_GRATIS 	    = 4;
    CONST PRESENCIAL 			= 5;//só venda no site
    CONST LIVRARIA              = 6;
    CONST GRUPO_INTERESSE       = 7;

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Products', [
            'foreignKey' => 'category_id'
        ]);
    }

    // public function getList(){
    //     return $this->find('list',array('conditions'=>array('Category.status'=>1)));
    // }

    public function getCursosOndemandId(){
        return self::CURSOS_ONDEMAND;
    }

    public function getCursosAovivoId(){
        return self::CURSOS_AOVIVO;
    }

    public function getCursosAovivoGratisId(){
        return self::CURSOS_AOVIVO_GRATIS;
    }

    public function getCursosGratisId(){
        return self::CURSOS_GRATIS;
    }

    public function getCursosPresencialId(){
        return self::PRESENCIAL;
    }

    public function getGrupoInteresse(){
        return self::GRUPO_INTERESSE;
    }

    // public function getCategoriesList(){
    //     return $this->find('list',['conditions'=>['status'=>1]]);
    // }

    // public function getCategoriesId(){
    //     return $this->find('list',['fields'=>['id'],'conditions'=>['status'=>1]]);
    // }

    // public function getListHeader(){
    //     return $this->find('list',['fields'=>['slug','name'],'conditions'=>['status'=>1]]);
    // }
}
