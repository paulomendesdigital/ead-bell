<?php

namespace App\Model\Table;

use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

class AnswerCommentsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('answer_comments');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Comments', [
            'foreignKey' => 'comment_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    public function findAll(Query $query, array $options)
    {
        return $query->formatResults(function (\Cake\Collection\CollectionInterface $results) {

            return $results->map(function ($row) {

                if (isset($row['Answercomment']['created'])) {
                    $dataFuturo = $row['Answercomment']['created'];
                    $dataAtual = date('Y-m-d H:i:s');

                    $date_time  = new DateTime($dataAtual);
                    $diff       = $date_time->diff( new DateTime($dataFuturo));

                    $row['Answercomment']['diff']['month'] = $diff->format('%m');
                    $row['Answercomment']['diff']['day'] = $diff->format('%d');
                    $row['Answercomment']['diff']['hour'] = $diff->format('%H');
                    $row['Answercomment']['diff']['minute'] = $diff->format('%i');

                } elseif(isset($row['created'])) {
                    $dataFuturo = $row['created'];
                    $dataAtual = date('Y-m-d H:i:s');

                    $date_time  = new DateTime($dataAtual);
                    $diff       = $date_time->diff( new DateTime($dataFuturo));

                    $row['diff']['month'] = $diff->format('%m');
                    $row['diff']['day'] = $diff->format('%d');
                    $row['diff']['hour'] = $diff->format('%H');
                    $row['diff']['minute'] = $diff->format('%i');
                }
            });
        })
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmptyString('name');

        /*$validator
            ->dateTime('start_date')
            ->allowEmptyDateTime('start_date');

        $validator
            ->dateTime('finish_date')
            ->allowEmptyDateTime('finish_date');*/

        $validator
            ->integer('weight')
            ->notEmptyString('weight');

        $validator
            ->integer('max_views')
            ->allowEmptyString('max_views');

        $validator
            ->integer('unique_views_count')
            ->notEmptyString('unique_views_count');

        $validator
            ->integer('views_count')
            ->notEmptyString('views_count');

        $validator
            ->integer('status')
            ->notEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['advertiser_id'], 'Advertisers'));
        $rules->add($rules->existsIn(['company_id'], 'Companies'));

        return $rules;
    }

    public function dateFormatBeforeSave($data)
    {
        //return date('Y-m-d', strtotime($dateString));
        $hora = substr($data, 10, 20);
        $data = substr($data, 0, 10);

        $explode = explode("/", $data);
        if (count($explode) > 1) {
            if ($hora) {
                $data = $explode[2] . "-" . $explode[1] . "-" . $explode[0] . $hora . ":00";
            } else {
                $data = $explode[2] . "-" . $explode[1] . "-" . $explode[0];
            }
        }

        return $data;
    }

    public function beforeSave($event, $entity, $options)
    {
        if (!empty($entity->start_date)) {
            $entity->start_date = $this->dateFormatBeforeSave($entity->start_date);
        }
        if (!empty($entity->finish_date)) {
            $entity->finish_date = $this->dateFormatBeforeSave($entity->finish_date);
        }
        return true;
    }

    public function dateFormatAfterFind($dateString, $hasHour = false)
    {
        if (!$hasHour)
            return $dateString->i18nFormat('dd/MM/Y');
        else
            return $dateString->i18nFormat('dd/MM/Y h:mm');
    }


}
