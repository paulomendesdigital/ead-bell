<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class CategoryostsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Posts', [
            'foreignKey' => 'categorypost_id'
        ]);
    }

    public function findGetAll(Query $query, array $options)
    {
		$conditions['Categoryposts.status'] = 1;
        $conditions['post_count >'] = 0;

        return $query->where($conditions)
                ->contain(['Categoryposts' => function($q) { return $q->select(['id', 'title', 'categorypost_id']); }])
                ->order(['ISNULL(Categorypost.ordination)', 'Categorypost.ordination', 'Categorypost.name' => 'ASC']);
    }
}
