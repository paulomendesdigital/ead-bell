<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Utility\Text;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;

/**
 * Webdoors Model
 *
 * @property \App\Model\Table\CompaniesTable&\Cake\ORM\Association\BelongsTo $Companies
 *
 * @method \App\Model\Entity\Webdoor get($primaryKey, $options = [])
 * @method \App\Model\Entity\Webdoor newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Webdoor[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Webdoor|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Webdoor saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Webdoor patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Webdoor[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Webdoor findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WebdoorsTable extends Table
{
    const IMAGE_BACKGROUND_SIZE = '1920x500';
    const IMAGE_FRONT_SIZE = '621x630';
    const IMAGE_MOBILE_SIZE = '621x630';
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Utility');

        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            // You can configure as many upload fields as possible,
            // where the pattern is `field` => `config`
            //
            // Keep in mind that while this plugin does not have any limits in terms of
            // number of files uploaded per request, you should keep this down in order
            // to decrease the ability of your users to block other requests.
            'image_name' => [
                //'path' =>  'webroot{DS}files{DS}{model}{DS}{field}{DS}{primaryKey}',
                'fields' => [
                    'dir' => 'image_path', // defaults to `dir`
                    //'size' => 'photo_size', // defaults to `size`
                    //'type' => 'photo_type', // defaults to `type`
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $data['name'] = Text::slug(date('YmdHis') . $entity->name) . '.' . strtolower($extension);
                    return $data['name'];
                },
                'deleteCallback' => function ($path, $entity, $field, $settings) {
                    // When deleting the entity, both the original and the thumbnail will be removed
                    // when keepFilesOnDelete is set to false
                    return [
                        $path . $entity->{$field},
                        // $path . 'thumbnail-' . $entity->{$field}
                    ];
                },
                'keepFilesOnDelete' => false
            ],
            'image_background_name' => [
                //'path' =>  'webroot{DS}files{DS}{model}{DS}{field}{DS}{primaryKey}',
                'fields' => [
                    'dir' => 'image_background_path', // defaults to `dir`
                    //'size' => 'photo_size', // defaults to `size`
                    //'type' => 'photo_type', // defaults to `type`
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $data['name'] = Text::slug(date('YmdHis') . $entity->name) . '.' . strtolower($extension);
                    return $data['name'];
                },
                'deleteCallback' => function ($path, $entity, $field, $settings) {
                    // When deleting the entity, both the original and the thumbnail will be removed
                    // when keepFilesOnDelete is set to false
                    return [
                        $path . $entity->{$field},
                        //$path . 'thumbnail-' . $entity->{$field}
                    ];
                },
                'keepFilesOnDelete' => false
            ],
            'image_mobile_name' => [
                //'path' =>  'webroot{DS}files{DS}{model}{DS}{field}{DS}{primaryKey}',
                'fields' => [
                    'dir' => 'image_mobile_path', // defaults to `dir`
                    //'size' => 'photo_size', // defaults to `size`
                    //'type' => 'photo_type', // defaults to `type`
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $data['name'] = Text::slug(date('YmdHis') . $entity->name) . '.' . strtolower($extension);
                    return $data['name'];
                },
                /*'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);

                    // Store the thumbnail in a temporary file
                    $tmp = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;

                    // Use the Imagine library to DO THE THING
                    $size = new \Imagine\Image\Box(40, 40);
                    $mode = \Imagine\Image\ImageInterface::THUMBNAIL_INSET;
                    $imagine = new \Imagine\Gd\Imagine();

                    // Save that modified file to our temp file
                    $imagine->open($data['tmp_name'])
                        ->thumbnail($size, $mode)
                        ->save($tmp);

                    // Now return the original *and* the thumbnail
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumbnail-' . $data['name'],
                    ];
                },*/
                'deleteCallback' => function ($path, $entity, $field, $settings) {
                    // When deleting the entity, both the original and the thumbnail will be removed
                    // when keepFilesOnDelete is set to false
                    return [
                        $path . $entity->{$field},
                        //$path . 'thumbnail-' . $entity->{$field}
                    ];
                },
                'keepFilesOnDelete' => false
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        /*$validator
            ->dateTime('start')
            ->allowEmptyDateTime('start');

        $validator
            ->dateTime('finish')
            ->allowEmptyDateTime('finish');*/

        $validator
            ->scalar('title')
            ->allowEmptyString('title');

        $validator
            ->scalar('button_label')
            ->maxLength('button_label', 45)
            ->allowEmptyString('button_label');

        $validator
            ->scalar('link')
            ->maxLength('link', 255)
            ->allowEmptyString('link');

        $validator
            //->scalar('image_name')
            //->maxLength('image_name', 255)
            ->allowEmptyFile('image_name');

        $validator
            //->scalar('image_path')
            //->maxLength('image_path', 255)
            ->allowEmptyFile('image_path');

        $validator
            //->scalar('image_background_name')
            //->maxLength('image_background_name', 255)
            ->allowEmptyFile('image_background_name');

        $validator
            //->scalar('image_background_path')
            //->maxLength('image_background_path', 255)
            ->allowEmptyFile('image_background_path');

        $validator
            //->scalar('image_mobile_name')
            //->maxLength('image_mobile_name', 255)
            ->allowEmptyFile('image_mobile_name');

        $validator
            //->scalar('image_mobile_path')
            //->maxLength('image_mobile_path', 255)
            ->allowEmptyFile('image_mobile_path');

        $validator
            ->allowEmptyFile('target');

        $validator
            ->integer('ordination')
            ->allowEmptyString('ordination');

        return $validator;
    }

    public function dateFormatBeforeSave($data)
    {
        //return date('Y-m-d', strtotime($dateString));
        $hora = substr($data, 10, 20);
        $data = substr($data, 0, 10);

        $explode = explode("/", $data);
        if (count($explode) > 1) {
            if ($hora) {
                $data = $explode[2] . "-" . $explode[1] . "-" . $explode[0] . $hora . ":00";
            } else {
                $data = $explode[2] . "-" . $explode[1] . "-" . $explode[0];
            }
        }

        return $data;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['company_id'], 'Companies'));

        return $rules;
    }

    public function beforeSave($event, $entity, $options)
    {
        if (!empty($entity->start)) {
            $entity->start = $this->dateFormatBeforeSave($entity->start);
        }
        if (!empty($entity->finish)) {
            $entity->finish = $this->dateFormatBeforeSave($entity->finish);
        }
        return true;
    }

    public function dateFormatAfterFind($dateString, $hasHour = false)
    {
        if (!$hasHour)
            return $dateString->i18nFormat('dd/MM/Y');
        else
            return $dateString->i18nFormat('dd/MM/Y h:mm');
    }

    /**
     * findWebsiteWebdoors
     *
     * @param  Cake\ORM\Query $query
     * @param  array $options
     *
     * @return Cake\ORM\Query
     */
    public function findWebsiteWebdoors(Query $query, array $options)
    {
        $company = $this->Companies->findByCode($options['company_code'])->first();
        if (empty($company)) {
            throw new \Cake\Http\Exception\NotFoundException('company_code não encontrado');
        }

        /**
         * @var \App\Model\Table\ConfigurationParametersTable
         */
        $configurationParametersTable = TableRegistry::getTableLocator()->get('ConfigurationParameters');

        if ($configurationParametersTable->isRandomWebdoorOrderActive($company)) {
            $query = $query->order(['RAND()']);
        } else {
            $query = $query->order(['Webdoors.ordination ASC']);
        }

        return $query
            ->where([
                'Webdoors.status' => 1,
                'Webdoors.company_id' => $company->id,
                'Webdoors.start <=' => date('Y-m-d H:i:s'),
                'OR' => [
                    'Webdoors.finish >' => date('Y-m-d H:i:s'),
                    'Webdoors.finish' => '0000-00-00 00:00:00',
                    'Webdoors.finish IS NULL',
                ]
            ])
            ->order([
                'Webdoors.ordination' => 'ASC',
                'Webdoors.created' => 'ASC'
            ]);
    }

    public function patchRemoveImages($entity, $data)
    {

        $conn = ConnectionManager::get('default');
        if (isset($data['image_name_remove']) and $data['image_name_remove'] == 1) {
            $conn->execute("UPDATE webdoors SET image_name = NULL, image_path = NULL WHERE id = {$entity->id} LIMIT 1");
        }
        if (isset($data['image_background_name_remove']) and $data['image_background_name_remove'] == 1) {
            $conn->execute("UPDATE webdoors SET image_background_name = NULL, image_background_path = NULL WHERE id = {$entity->id} LIMIT 1");
        }
        if (isset($data['image_mobile_name_remove']) and $data['image_mobile_name_remove'] == 1) {
            $conn->execute("UPDATE webdoors SET image_mobile_name = NULL, image_mobile_path = NULL WHERE id = {$entity->id} LIMIT 1");
        }
    }
}
