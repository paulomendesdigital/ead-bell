<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ClientsTable extends Table
{
	const UNDEFINED_GENDER_ID = 0;
	const MALE_GENDER_ID = 1;
	const FEMALE_GENDER_ID = 2;

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Testimonials', [
            'foreignKey' => 'client_id',
        ]);

        $this->hasMany('ClientAddresses', [
            'foreignKey' => 'client_id',
        ]);
    }

	public function findGetRegisterByDate(Query $query, array $options)
    {
		$date = !$options['date'] ? date('Y-m-d') : $options['date'];

        $conditions['DATE(Clients.created)'] = $date;

        $clients = $query->where($conditions)
                ->select(['count' => $query->func()->count('Clients.id'),  'Clients.created'])
                ->group(['Clients.created'])->toArray();

        $values = [];
        $i = 0;

        foreach ($clients as $client) {
            $values[$i]['color'] = '#669800';
            $values[$i]['name'] = $client['created'];
            $values[$i]['data'][] = (int) $client['count'];
            $i++;
        }

      	return json_encode($values);
	}

	public function findGetRegisterPerGender(Query $query)
    {

        $clients = $query->where(['Clients.gender IS NOT NULL'])
                ->select(['count' => $query->func()->count('*'),  'Clients.gender'])
                ->group(['Clients.gender'])->toArray();

        $clientsGenders = [];
        $i = 0;
        $clientsGenders[0]['name'] = 'Quantidade';

        foreach ($clients as $client) {
            $clientsGenders[0]['data'][$i]['name'] = $this->getGendersList()[$client['gender']];
            $clientsGenders[0]['data'][$i]['y'] = (int) $client['count'];
            $i++;
        }

        return json_encode($clientsGenders);
	}

	public function findGetRegisterPerAges(Query $query)
    {
        $conditions = [
            'Clients.birth IS NOT NULL',
            'Clients.birth >=' => '1900-01-01',
            'Clients.birth <=' => date('Y-m-d')
        ];

        $clients = $query->where($conditions)
                ->select([
                    'count' => $query->func()->count('*'),  'Clients.created',
                    'age' => "YEAR('" . date('Y-m-d') . "') - YEAR(Clients.birth) - ('" . date('md') . "' < DATE_FORMAT(Clients.birth,'%m%d'))"
                ])
                ->group(["age"])->toArray();

        $clientsAges[0]['name'] = 'Quantidade';

        foreach ($clients as $key => $client) {
            $clientsAges[0]['data'][$key]['name'] = $client['age'];
            $clientsAges[0]['data'][$key]['y'] = (int) $client['count'];
        }

      	return json_encode($clientsAges);
    }

    public function findGetRegisterPerStates(Query $query)
    {
        $clients = $query->contain(['ClientAddresses' => ['Cities']]);
        $i = 0;
        $statesIndex = [];
        $clientsStates[0]['name'] = 'Quantidade';

        foreach ($clients as $client) {

            if (empty($client['client_addresses'][0]['city']['uf'])) continue;

            if (!isset($statesIndex[$client['client_addresses'][0]['city']['uf']])) {
                $statesIndex[$client['client_addresses'][0]['city']['uf']] = $i++;
                $clientsStates[0]['data'][$statesIndex[$client['client_addresses'][0]['city']['uf']]]['name'] = $client['client_addresses'][0]['city']['uf'];
            }

            if (!isset($clientsStates[0]['data'][$statesIndex[$client['client_addresses'][0]['city']['uf']]]['y'])) {
                $clientsStates[0]['data'][$statesIndex[$client['client_addresses'][0]['city']['uf']]]['y'] = 1;

            } else {
                $clientsStates[0]['data'][$statesIndex[$client['client_addresses'][0]['city']['uf']]]['y']++;
            }
        }

        return json_encode($clientsStates);
    }

	public function getGendersList()
	{
		return [
			self::UNDEFINED_GENDER_ID => 'Indefinido',
			self::MALE_GENDER_ID => 'Masculino',
			self::FEMALE_GENDER_ID => 'Feminino'
		];
	}

}
