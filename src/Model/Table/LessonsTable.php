<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class LessonsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Teachers');

        // $this->hasMany('Credits');
        // $this->hasMany('Questionnaires');
        $this->hasMany('Questions');

        $this->belongsToMany('Topics');
    }

	public function findGetList(Query $query, array $options)
    {	
        $conditions = ['status' => 1];

        $order = [
            'ISNULL(ordination)',
            'ordination'=>'ASC',
            'title' => 'ASC'
        ];

		if (!$options OR !$options['with_description']) {

			return $query->find('list')
                        ->where($conditions)
                        ->order($order);

		} else {
            $select = ['Lessons.id','Lessons.title', 'Lessons.description'];

			$lessons = $query->select($select)
                            ->where($conditions)
                            ->order($order);

			$list = [];
			
            foreach ($lessons as $key => $value) {

				if (!empty($value['description'])) {
					$list[$value['id']] = "{$value['title']} :: {$value['description']}";

				} else {
					$list[$value['id']] = "{$value['title']}";
				}
			}

			return $list;
		}
	}
}