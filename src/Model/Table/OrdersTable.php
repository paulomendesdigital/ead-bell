<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;

use App\Model\Table\PaymentsTable;

class OrdersTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    public function findGetOrderByDate(Query $query, array $options)
    {
        $date = !$options['date'] ? date('Y-d-m') : $options['date'];

        $conditions['DATE(Orders.created)'] = $date;
        $conditions['Orders.status IN'] = [PaymentsTable::APROVADO, PaymentsTable::DISPONIVEL];

        $orders = $query->where($conditions)
                ->select(['value' => 'SUM(Orders.value_net)', 'dia' => 'DATE(Orders.created)'])
                ->group(['dia']);

        $valueOrders = [];

        foreach ($orders as $key => $order) {
            $valueOrders[$key]['color'] = '#FF9837';
            $valueOrders[$key]['name'] = $order['dia'];
            $valueOrders[$key]['data'][] = (int) $order['value'];
            $valueOrders[$key]['tooltip']['valuePrefix'] = 'R$ ';
        }

        return json_encode($valueOrders);
    }

    public function findGetOrdersLastDays(Query $query, array $options)
    {
        $conditions = [
            "DATEDIFF(Orders.created, now()) >= -{$options['last_days']}", //Últimos 6 meses, 180 dias de diferença
            'Orders.status IN' => [PaymentsTable::APROVADO, PaymentsTable::DISPONIVEL],
            'Orders.value_net > 0'
        ];

        $orders = $query->where($conditions)
                ->select(['value' => 'SUM(Orders.value_net)', 'mes' => 'DATE_FORMAT(Orders.created, "%Y-%m")'])
                ->order(['mes' => 'ASC'])
                ->group(['mes']);

        $monthValueOrders = [];
        $i = 0;
        $colors = ['#FF9837','#ff8973','#ff3978','#BCF8EC','#AED9E0','#9FA0C3','#8B687F','#7B435B'];

        foreach ($orders as $order) {
            $monthValueOrders[$i]['color'] = $colors[$i];
            $monthValueOrders[$i]['name'] = __($order['mes']);
            $monthValueOrders[$i]['data'][] = (int) $order['value'];
            $monthValueOrders[$i]['tooltip']['valuePrefix'] = 'R$ ';
            $i++;
        }

      	return json_encode($monthValueOrders);
    }
}
