<?php

/**  
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright Ricardo Aranha
 * @author    Ricardo Aranha - rikarudoaranha@gmail.com  
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Auth;

use Cake\Auth\BaseAuthenticate;
use Cake\Controller\ComponentRegistry;
use Cake\Http\ServerRequest;
use Cake\Http\Response;
use Cake\Http\Exception\UnauthorizedException;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;
use Prophecy\Exception\InvalidArgumentException;

class FirebaseAuthenticate extends BaseAuthenticate
{
    protected $_idtoken;

    public function __construct(ComponentRegistry $registry, $config)
    {
        $defaultConfig = [
            'header' => 'authorization',
            'prefix' => 'bearer',
            'credentials_path' => CONFIG . DS . 'firebase' .DS . 'firebase-credentials.json'
        ];

        $this->setConfig($defaultConfig);

        parent::__construct($registry, $config);
    }

    public function authenticate(ServerRequest $request, Response $response)
    {
        return $this->getUser($request);
    }

    public function getUser(ServerRequest $request)
    {
        $idTokenString = $this->getIdToken($request);

        if (empty($idTokenString)) {
            return false;
        }

        $firebase = $this->getFirebaseFactory();

        try {
            $verifiedIdToken = $firebase->getAuth()->verifyIdToken($idTokenString);
        } catch (InvalidToken $e) {
            throw new InvalidArgumentException($e->getMessage());
        }

        $user = $this->_findUser($verifiedIdToken->getClaim('sub'));
        if (!$user) {
            return false;
        }

        return $user;
    }

    public function getIdToken($request = null)
    {
        $config = $this->_config;

        if ($request === null) {
            return $this->_idtoken;
        }

        $header = $request->getHeaderLine($config['header']);
        if ($header && strpos($header, $config['prefix']) === 0) {
            return $this->_idtoken = str_ireplace($config['prefix'] . ' ', '', $header);
        }

        return $this->_idtoken;
    }

    public function getFirebaseFactory()
    {
        $serviceAccount = $this->getFirebaseServiceAccount();
        return (new Factory)
            ->withServiceAccount($serviceAccount)
            ->create();
            
    }

    public function getFirebaseServiceAccount()
    {
        $config = $this->_config;
        return ServiceAccount::fromJsonFile($config['credentials']);
    }

    /**
     * Handles an unauthenticated access attempt by sending appropriate login headers
     *
     * @param \Cake\Http\ServerRequest $request A request object.
     * @param \Cake\Http\Response $response A response object.
     * @return void
     * @throws \Cake\Http\Exception\UnauthorizedException
     */
    public function unauthenticated(ServerRequest $request, Response $response)
    {
        throw new UnauthorizedException(__('You are not authorized to access.'));
    }
}
