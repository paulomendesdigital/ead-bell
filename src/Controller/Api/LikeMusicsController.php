<?php
namespace App\Controller\Api;

use App\Controller\Api\ApiAppController;

/**
 * LikeMusics Controller
 *
 * @property \App\Model\Table\LikeMusicsTable $LikeMusics
 *
 * @method \App\Model\Entity\LikeMusic[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LikeMusicsController extends ApiAppController
{
    public function initialize()
    {
        parent::initialize();
        $this->RequestHandler->renderAs($this, 'json');
        $this->Auth->allow(['add']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post']);
        $query_params = $this->request->getQueryParams();
        if (empty($query_params['company_code'])) {
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }
        $company = $this->LikeMusics->Companies->findByCode($query_params['company_code'])->first();
        if(empty($company)){
            throw new \Cake\Http\Exception\NotFoundException('company não encontrado');
        };
        
        $likeMusic = $this->LikeMusics->newEntity();
        $likeMusic_data = $this->request->getData();    
        $likeMusic_data['company_id'] = $company->id;

        $user = [];//$this->__getUserFromTokenSession();

        if( !empty($user) ){
            $likeMusic_data['user_id'] = $user->id;
        }

        $likeMusic = $this->LikeMusics->patchEntity($likeMusic, $likeMusic_data);
        
        if ( $this->LikeMusics->save($likeMusic) ) {            
            $this->set([
                'result' => [
                    'success' => true,
                    'data' => $likeMusic
                ],
                '_serialize' => 'result'
            ]);
        } else {
            $this->set([
                'result' => [
                    'success' => false,
                    'errors' => $likeMusic->getErrors()
                ],
                '_serialize' => 'result'
            ]);
        }                    
    }

}
