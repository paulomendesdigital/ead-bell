<?php
namespace App\Controller\Api;

use App\Controller\Api\ApiAppController;

/**
 * Speakers Controller
 *
 * @property \App\Model\Table\SpeakersTable $Speakers
 *
 * @method \App\Model\Entity\Speaker[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SpeakersController extends ApiAppController
{

    public function initialize()
    {
        parent::initialize();
        $this->RequestHandler->renderAs($this, 'json');
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->request->allowMethod(['get']);
        $query_params = $this->request->getQueryParams();
        if(empty($query_params['company_code'])){
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }
        $this->paginate = [
            'finder' => [
                'websiteSpeakers' => $query_params
            ]
        ];
        $speakers = $this->paginate($this->Speakers);
        $this->set([
            'result' => [
                'success' => true,
                'data' => $speakers
            ],
            '_serialize' => 'result'
        ]);        
    }

    /**
     * View method
     *
     * @param string|null $id Top Music id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id)
    {
        $this->request->allowMethod(['get']);
        $topMusic = $this->Speakers->get($id);
        $this->set([
            'result' => [
                'success' => true,
                'data' => $topMusic
            ],
            '_serialize' => 'result'
        ]);        
    }
    
}
