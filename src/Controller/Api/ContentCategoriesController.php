<?php
namespace App\Controller\Api;

use App\Controller\Api\ApiAppController;

/**
 * ContentCategories Controller
 *
 * @property \App\Model\Table\ContentCategoriesTable $ContentCategories
 *
 * @method \App\Model\Entity\ContentCategory[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContentCategoriesController extends ApiAppController
{

    public function initialize()
    {
        parent::initialize();
        $this->RequestHandler->renderAs($this, 'json');
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->request->allowMethod(['post','get']);

        $this->paginate = [
            'finder' => 'websiteContentCategories'
        ];

        $contentCategories = $this->paginate($this->ContentCategories);
        $this->set([
            'result' => [
                'success' => true,
                'data' => $contentCategories
            ],
            '_serialize' => 'result'
        ]);        
    }

    /**
     * View method
     *
     * @param string|null $id Content Category id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id)
    {
        $this->request->allowMethod(['get']);
        $contentCategory = $this->ContentCategories->get($id, [
            'contain' => ['Contents']
        ]);
        $this->set([
            'result' => [
                'success' => true,
                'data' => $contentCategory
            ],
            '_serialize' => 'result'
        ]);        
    }

}
