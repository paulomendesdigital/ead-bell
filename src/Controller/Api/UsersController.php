<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiAppController;
use Firebase\JWT\JWT;
use Cake\Utility\Security;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;
use Cake\Datasource\ConnectionManager;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends ApiAppController
{

    public function initialize()
    {
        parent::initialize();
        $this->RequestHandler->renderAs($this, 'json');
        $this->Auth->allow(['login', 'loginApi', 'add', 'edit', 'sendMail', 'generateTokenForPass', 'checkTokenForPass', 'sincUsersAro','getUsersWithParticipantsAndContent']);
    }

    public function login()
    {
        $this->request->allowMethod(['post']);
        $query_params = $this->request->getQueryParams();
        
        if (empty($query_params['company_code'])) {
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }

        $company = $this->Users->Companies->findByCode($query_params['company_code'])->first();
        
        if (empty($company)) {
            throw new \Cake\Http\Exception\NotFoundException('company_code não encontrado');
        }

        $verifyIdToken = $this->verifyFirebaseIdTokenFromHeader($company);
        
        if (!$verifyIdToken) {
            throw new \Cake\Http\Exception\UnauthorizedException('Invalid firebase token');
        }

        $user = $this->getFirebaseUserFromHeader($company);
        //Primeiro login resulta em cadastro
        $user_data = $this->request->getData();
        $exists = true;

        if (empty($user)) {
            $user = $this->Users->newEntity();
            $user_data['group_id'] = $this->Users->Groups->getClienteId();
            $user_data['status'] = 1;
            $user_data['company_id'] = $company->id;
            $exists = false;
            
        } else {
            //Checa se o provider enviado já existe para o usuário e o atualiza
            
            for ($i = 0; $i < count($user->user_providers); $i++) {
                
                if ($user_data['user_providers'][0]['provider'] === $user->user_providers[$i]->provider) {
                    $user_data['user_providers'][0]['id'] = $user->user_providers[$i]->id;
                    break;
                }
            }
        }

        $user = $this->Users->patchEntity($user, $user_data, ['validate' => 'apiLoginFirebase']);

        $success = false;
        
        if ($this->Users->save($user)) {
            $success = true;
        }

        $message = '';
        $errors = [];
        
        if ($exists) {
            
            if (!$success) {
                $message = 'Login bem-sucedido porém a atualização dos dados de usuário falhou.';
                $success = true;
                
            } else {
                $message = 'Seja bem-vindo!';
            }
            
        } else {
            
            if (!$success) {
                $message = 'Falha na criação do usuário. Por favor, tente novamente mais tarde.';
                $errors = $user->getErrors();
                
            } else {
                $message = 'Seja bem-vindo!';
            }
        }

        if ($success) {
            $this->set([
                'success' => true,
                'message' => $message,
                'data' => [
                    'token' => JWT::encode(
                        [
                            'sub' => $user['id'],
                            'exp' =>  time() + 100000000000
                        ],
                        Security::salt()
                    )
                ],
                '_serialize' => ['success', 'data', 'message']
            ]);

        } else {
            $this->set([
                'success' => false,
                'message' => $message,
                'errors' => $errors,
                '_serialize' => ['success', 'message', 'errors']
            ]);
        }
    }

    public function loginApi()
    {
        $this->request->allowMethod(['post']);

        $query_params = $this->request->getQueryParams();
        if (empty($query_params['company_code'])) {
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }
        $company = $this->Users->Companies->findByCode($query_params['company_code'])->first();
        if (empty($company)) {
            throw new \Cake\Http\Exception\NotFoundException('company_code não encontrado');
        }

        $password = $this->request->getData('password');
        $hasher = new DefaultPasswordHasher();
        $user = $this->Users->find()
            ->where([
                'Users.company_id' => $company->id,
                'Users.username' => $this->request->getData('username'),
            ])
            ->first();

        if (!empty($user) && !$hasher->check($password, $user->password)) {
            $user = null;
        }

        if (!empty($user)) {
            if ($user->birth) {
                $user->birth = $user->birth->format('d/m/Y');
            }
            $this->set([
                'result' => [
                    'success' => true,
                    'data' => [
                        'token' => JWT::encode(
                            [
                                'sub' => $user['id'],
                                'exp' =>  time() + 100000000000
                            ],
                            Security::salt()
                        ),
                        'User' => $user
                    ],
                ],
                '_serialize' => 'result'
            ]);
        } else {
            $this->set([
                'result' => [
                    'success' => false,
                    'message' => 'Login ou senha incorretos'
                ],
                '_serialize' => 'result'
            ]);
        }
    }

    public function add()
    {
        $this->request->allowMethod(['post']);
        $query_params = $this->request->getQueryParams();
        if (empty($query_params['company_code'])) {
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }
        $company = $this->Users->Companies->findByCode($query_params['company_code'])->first();
        if (empty($company)) {
            throw new \Cake\Http\Exception\NotFoundException('company_code não encontrado');
        }

        $user_data = $this->request->getData();
        $user = $this->Users->newEntity();
        $user_data['username'] = $user_data['email'];
        $user_data['group_id'] = $this->Users->Groups->getClienteId();
        $user_data['status'] = 1;
        $user_data['company_id'] = $company->id;
        if (isset($user_data['birth']) and !empty($user_data['birth'])) {
            $user_data['birth'] = $this->dateFormatBeforeSave($user_data['birth']);
        }

        $user = $this->Users->patchEntity($user, $user_data, ['validate' => 'apiLogin']);

        if ($this->Users->save($user)) {
            if ($user->birth) {
                $user->birth = $user->birth->format('d/m/Y');
            }
            $this->set([
                'result' => [
                    'success' => true,
                    'data' => [
                        'token' => JWT::encode(
                            [
                                'sub' => $user['id'],
                                'exp' =>  time() + 100000000000
                            ],
                            Security::salt()
                        ),
                        'User' => $user
                    ],
                ],
                '_serialize' => 'result'
            ]);
        } else {
            $this->set([
                'result' => [
                    'success' => false,
                    'errors' => $user->getErrors()
                ],
                '_serialize' => 'result'
            ]);
        }
    }

    public function checkUserByToken()
    {
        $this->request->allowMethod(['post']);
        $query_params = $this->request->getQueryParams();
        if (empty($query_params['company_code'])) {
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }
        $company = $this->Users->Companies->findByCode($query_params['company_code'])->first();
        if (empty($company)) {
            throw new \Cake\Http\Exception\NotFoundException('company_code não encontrado');
        }

        $user = $this->__getUserFromTokenSession();

        if (empty($user)) {
            $result = [
                'success' => false,
                'message' => 'Token não localizado!'
            ];
        } else {
            $result = [
                'success' => true,
                'message' => 'Token localizado!'
            ];
        }

        $this->set([
            'result' => $result,
            '_serialize' => 'result'
        ]);
    }

    public function edit()
    {
        $this->request->allowMethod(['post']);
        $query_params = $this->request->getQueryParams();
        if (empty($query_params['company_code'])) {
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }
        $company = $this->Users->Companies->findByCode($query_params['company_code'])->first();
        if (empty($company)) {
            throw new \Cake\Http\Exception\NotFoundException('company_code não encontrado');
        }

        $user = $this->__getUserFromTokenSession();

        if (empty($user)) {
            throw new \Cake\Http\Exception\NotFoundException('Não foi possível localizar seu cadastro!');
        }

        $user_data = $this->request->getData();

        if (!empty($user_data['email'])) {
            $user_data['username'] = $user_data['email'];
        }

        $user_data['group_id'] = $user->group_id;
        $user_data['company_id'] = $user->company_id;

        if (isset($user_data['birth']) and !empty($user_data['birth'])) {
            $user_data['birth'] = $this->dateFormatBeforeSave($user_data['birth']);
        }

        if (isset($user_data['password']) && empty($user_data['password'])) {
            unset($user_data['password']);
        }

        $user = $this->Users->get($user->id, [
            'contain' => []
        ]);

        $user = $this->Users->patchEntity($user, $user_data, ['validate' => 'apiLogin']);

        if ($this->Users->save($user)) {
            $this->set([
                'result' => [
                    'success' => true,
                    'message' => 'Dados gravados com sucesso!'
                ],
                '_serialize' => 'result'
            ]);
        } else {
            $this->set([
                'result' => [
                    'success' => false,
                    'errors' => $user->getErrors()
                ],
                '_serialize' => 'result'
            ]);
        }
    }

    /**
     * [generateTokenForPass usado para gerar o token de alterar senha]
     * @return [type] [description]
     */
    public function generateTokenForPass()
    {
        $this->request->allowMethod(['post']);

        $query_params = $this->request->getQueryParams();
        if (empty($query_params['company_code'])) {
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }
        $company = $this->Users->Companies->findByCode($query_params['company_code'])->first();
        if (empty($company)) {
            throw new \Cake\Http\Exception\NotFoundException('company_code não encontrado');
        }

        $user = $this->Users->find()->where(['Users.company_id' => $company->id, 'Users.email' => $this->request->getData('email')])->first();

        if (!empty($user)) {

            /*if( empty($this->existsInAros($user->id)) ){
                $this->createNodeInAro($user);
                $user = $this->Users->find()->where(['Users.company_id' => $company->id,'Users.email' => $this->request->getData('email')])->first();
            }*/

            $pass_token = JWT::encode(['sub' => $user['id'], 'exp' =>  time() + 604800], Security::salt());
            $user_data = $user->toArray();
            $user_data['pass_token'] = $pass_token;
            $user = $this->Users->get($user->id, [
                'contain' => []
            ]);
            unset($user->password);
            $user = $this->Users->patchEntity($user, $user_data, ['validate' => 'apiLogin']);

            if ($this->Users->save($user)) {
                $this->set([
                    'result' => [
                        'success' => true,
                        'data' => [
                            'pass_token' => $pass_token,
                        ],
                    ],
                    '_serialize' => 'result'
                ]);
            } else {
                $this->set([
                    'result' => [
                        'success' => false,
                        'message' => $user->getErrors() //'Não foi possível gerar o token de recuperação de senha!'
                    ],
                    '_serialize' => 'result'
                ]);
            }
        } else {
            $this->set([
                'result' => [
                    'success' => false,
                    'message' => 'Email não localizado!'
                ],
                '_serialize' => 'result'
            ]);
        }
    }

    public function checkTokenForPass($pass_token = null)
    {
        $this->request->allowMethod(['post']);

        $query_params = $this->request->getQueryParams();
        if (empty($query_params['company_code'])) {
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }
        $company = $this->Users->Companies->findByCode($query_params['company_code'])->first();
        if (empty($company)) {
            throw new \Cake\Http\Exception\NotFoundException('company_code não encontrado');
        }

        $user = $this->Users->findByPassToken($this->request->getData('pass_token'))->first();
        if (empty($user)) {
            throw new \Cake\Http\Exception\NotFoundException('Token de senha inválido ou expirado!');
        }

        $user = $this->__getUserFromPassTokenSession($this->request->getData('pass_token'));

        if (!empty($user)) {

            $user_data = $user->toArray();
            $user_data['pass_token'] = NULL;
            $user_data['emailVerified'] = true;

            $user = $this->Users->get($user->id, [
                'contain' => []
            ]);
            unset($user->password);
            $user = $this->Users->patchEntity($user, $user_data);
            $this->Users->save($user);

            $this->set([
                'result' => [
                    'success' => true,
                    'data' => [
                        'token' => JWT::encode(
                            [
                                'sub' => $user['id'],
                                'exp' =>  time() + 604800
                            ],
                            Security::salt()
                        ),
                        'User' => $user
                    ],
                ],
                '_serialize' => 'result'
            ]);
        } else {
            $this->set([
                'result' => [
                    'success' => false,
                    'message' => 'Token inválido!'
                ],
                '_serialize' => 'result'
            ]);
        }
    }

    public function getUsersWithParticipantsAndContent(){

        $this->request->allowMethod(['get']);
        $query_params = $this->request->getQueryParams();
        if (empty($query_params['company_code']) && empty($query_params['company_id'])) {
            throw new \Cake\Http\Exception\BadRequestException('company não fornecido');
        }

        $company = $this->Users->Companies->findByCode($query_params['company_code'])->first();        
        if(empty($company)){
            throw new \Cake\Http\Exception\NotFoundException('company_code não encontrado');
        }
        
        $users = $this->Users->find()
        ->contain([
            //'fields'=>["id","company_id","name","email","username","cpf","rg","gender","birth","phone","cep","address","number","complement","neighborhood","city","state","photoURL","created"],
            'Participants'=>[
                'Contents' => [
                    'fields'=>['id','name','image_large_name','image_large_path']
                ],
                'fields'=>['Participants.id','Participants.user_id','Participants.company_id','Participants.created'],
            ],
        ])
        ->where([
            'Users.status' => 1,
            'Users.group_id' => 2,
            'Users.company_id' => $company->id,
            'Users.id IN' => $this->Users->Participants->find('list',[
                'fields'=>['Participants.user_id'],
                'conditions'=>['Participants.company_id'=>$company->id],
                'group'=>['Participants.user_id']
            ])
        ])
        ->order(['Users.created'=>'DESC'])
        ->limit(1000);

        $this->set([
            'result' => [
                'success' => true,
                'data' => $users
            ],
            '_serialize' => 'result'
        ]);
    }

    public function sendMail()
    {
        $this->request->allowMethod(['post']);
        $query_params = $this->request->getQueryParams();
        if (empty($query_params['company_code'])) {
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }
        $company = $this->Users->Companies->findByCode($query_params['company_code'])->first();
        if (empty($company)) {
            throw new \Cake\Http\Exception\NotFoundException('company_code não encontrado');
        }

        $mail_data = $this->request->getData();
        $email = new Email();

        if ($query_params['company_code'] == 'RMIX') {

            $email->transport('sendGrid')
                ->template('default', 'default')
                ->emailFormat('both')
                ->from(['contato@mixriofm.com.br' => 'Rádio Mix Rio - 102,1 FM'])
                ->to($mail_data['to'])
                ->subject($mail_data['subject'])
                ->send($mail_data['message']);
        } else {

            $email->transport('sendGrid')
                ->template('default', 'paradiso')
                ->emailFormat('both')
                ->from(['contato@sulamericaparadiso.com.br' => 'Rádio SulAmérica Paradiso FM'])
                ->to($mail_data['to'])
                //->bcc( 'dayvisonsilva@gmail.com' )
                ->subject($mail_data['subject'])
                ->send($mail_data['message']);
        }

        $this->set([
            'result' => [
                'success' => true,
                'data' => ['message' => 'Email enviado com sucesso!']
            ],
            '_serialize' => 'result'
        ]);
    }

    public function existsInAros($user_id)
    {

        $conn = ConnectionManager::get('default');
        $d = $conn->execute("SELECT * FROM aros where model = 'Users' and foreign_key = {$user_id}");
        return $d->fetchAll('assoc');
    }

    public function createNodeInAro($user)
    {
        $conn = ConnectionManager::get('default');
        $conn->execute("DELETE FROM users WHERE id = {$user->id}");

        $user_data = $user->toArray();
        $user_data['company_id'] = $user->company_id;
        $user_data['group_id'] = $this->Users->Groups->getClienteId();
        $user_data['name'] = $user->name;
        $user_data['username'] = $user_data['email'] = $user->email;
        $user_data['cpf'] = $user->cpf;
        $user_data['rg'] = $user->rg;
        $user_data['gender'] = $user->gender;
        //if( !empty($user->birth) ){
        $user_data['birth'] = NULL; //$user->birth->format('Y-m-d');
        //}
        $user_data['phone'] = $user->phone;
        $user_data['cep'] = $user->cep;
        $user_data['address'] = $user->address;
        $user_data['neighborhood'] = $user->neighborhood;
        $user_data['city'] = $user->city;
        $user_data['state'] = $user->state;
        $user_data['news'] = $user->news;
        $user_data['password'] = 123456;
        $user_data['status'] = 1;

        $user = $this->Users->newEntity();
        $user = $this->Users->patchEntity($user, $user_data, ['validate' => 'NewAros']);

        return $this->Users->save($user);
        /*return $conn->execute("INSERT INTO aros SET
            parent_id = 2,
            model = 'Users',
            foreign_key = {$user->id},
            alias = '{$user->username}'
        ");*/
    }

    public function sincUsersAro()
    {
        $this->request->allowMethod(['post', 'get']);
        $conn = ConnectionManager::get('default');
        $d = $conn->execute(
            "SELECT * FROM users User WHERE User.group_id = 2 AND User.id NOT IN (SELECT foreign_key FROM aros WHERE parent_id = 2) LIMIT 5000"
        );
        $users = $d->fetchAll('assoc');

        if (!empty($users)) {
            $lft = 30008;
            $rght = 30009;
            foreach ($users as $user) {
                $lft += 2;
                $rght += 2;
                $query = "INSERT INTO aros SET parent_id = 2, model = 'Users', foreign_key = {$user['id']}, alias = '{$user['username']}', lft={$lft}, rght={$rght}";
                $conn->execute($query);
            }
            $this->set([
                'result' => [
                    'success' => true,
                ],
                '_serialize' => 'result'
            ]);
        } else {
            $this->set([
                'result' => [
                    'success' => false,
                ],
                '_serialize' => 'result'
            ]);
        }
    }
}
