<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller\Api;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Utility\Security;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Cake\Routing\Router;
use Firebase\Auth\Token\Exception\InvalidToken;
use Firebase\JWT\JWT;
use InvalidArgumentException;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class ApiAppController extends Controller
{

  public $components = [
    'Acl' => [
      'className' => 'Acl.Acl'
    ]
  ];

  /**
   * Initialization hook method.
   *
   * Use this method to add common initialization code like loading components.
   *
   * e.g. `$this->loadComponent('Security');`
   *
   * @return void
   */
  public function initialize()
  {
    parent::initialize();

    /* ADD THIS NEW LINE */
    //$this->loadComponent('Security', ['blackHoleCallback' => 'forceSSL']); // SSL SECURITY

    $this->loadComponent('RequestHandler', [
      'enableBeforeRedirect' => false,
    ]);    

    $this->loadComponent('Auth', [
      'storage' => 'Memory',
      'authorize' => 'Controller',
      'unauthorizedRedirect' => false,
      'checkAuthIn' => 'Controller.initialize',
      'authenticate' => [
        'ADmad/JwtAuth.Jwt' => [
          'userModel' => 'Users',
          'fields' => [
            'username' => 'id'
          ],
          'scope' => ['Users.status' => 1],
          'parameter' => 'token',
          // Boolean indicating whether the "sub" claim of JWT payload
          // should be used to query the Users model and get user info.
          // If set to `false` JWT's payload is directly returned.
          'queryDatasource' => true,
        ]
      ]
    ]);
  }

  public function forceSSL()
  {
    return $this->redirect('https://' . env('SERVER_NAME') . Router::url($this->request->getRequestTarget()));
  }


  private function setCorsHeaders()
  {
    $this->response = $this->response->cors($this->request)
      ->allowOrigin(['*'])
      ->allowMethods(['*'])
      ->allowHeaders(['x-xsrf-token', 'Origin', 'Content-Type', 'X-Auth-Token'])
      ->allowCredentials(['true'])
      ->exposeHeaders(['Link'])
      ->maxAge(300)
      ->build();
  }

  public function beforeRender(\Cake\Event\Event $event)
  {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: POST, GET, PUT, PATCH, DELETE, OPTIONS');
    header('Access-Control-Allow-Headers: *');
    //$this->setCorsHeaders();
  }

  public function beforeFilter(\Cake\Event\Event $event)
  {
    parent::beforeFilter($event);
    //$this->Security->requireSecure();
    header('X-Frame-Options: SAMEORIGIN');
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: POST, GET, PUT, PATCH, DELETE, OPTIONS');
    header('Access-Control-Allow-Headers: *');
    if ($this->request->is('options')) {
      //$this->setCorsHeaders();
      return $this->response;
    }
  }

  public function isAuthorized($user)
  {
    return true;
    $Collection = new ComponentRegistry();
    $acl = new AclComponent($Collection);
    $controller = $this->request->getParam('controller');
    $action = $this->request->getParam('action');
    $check = $acl->check($user['id'], "$controller/$action");
    $check2 = $acl->check($user['group_id'], "$controller/$action");
    return ($check || $check2);
  }

  public function getFirebaseUserFromHeader($company)
  {
    $idTokenString = $this->getFirebaseIdTokenFromHeader();

    if (empty($idTokenString)) {
      return false;
    }

    $firebase = $this->getFirebaseFactory($company->code);

    try {
      $verifiedIdToken = $firebase->getAuth()->verifyIdToken($idTokenString);
    } catch (InvalidToken $e) {
      throw new InvalidArgumentException($e->getMessage());
    }

    $this->loadModel('Users');
    $user = $this->Users->find()
      ->contain([
        'UserProviders'
      ])
      ->where([
        'Users.firebase_uid' => $verifiedIdToken->getClaim('sub'),
        'Users.company_id' => $company->id
      ])
      ->first();

    if (empty($user)) {
      return false;
    }

    return $user;
  }

  public function verifyFirebaseIdTokenFromHeader($company)
  {
    $idTokenString = $this->getFirebaseIdTokenFromHeader();

    if (empty($idTokenString)) {
      return false;
    }

    $firebase = $this->getFirebaseFactory($company->code);
    try {
      $verifiedIdToken = $firebase->getAuth()->verifyIdToken($idTokenString);
    } catch (InvalidToken $e) {
      return false;
    }

    return true;
  }

  public function getFirebaseIdTokenFromHeader()
  {
    $header = $this->request->getHeaderLine('Authorization');
    if ($header && strpos($header, 'Bearer') === 0) {
      return str_ireplace('Bearer' . ' ', '', $header);
    }
    return false;
  }

  public function getFirebaseFactory($company_code)
  {
    $serviceAccount = $this->getFirebaseServiceAccount($company_code);
    return (new Factory)
      ->withServiceAccount($serviceAccount)
      ->create();
  }

  public function getFirebaseServiceAccount($company_code)
  {
    return ServiceAccount::fromJsonFile(CONFIG . 'firebase' . DS . $company_code . DS . 'firebase-credentials.json');
  }

  public function __getUserIdFromJWT()
  {
    if( empty($this->request->header('Authorization')) ){
      return false;
    }

    if( !strstr($this->request->header('Authorization'), 'Bearer') ){
      return false;
    }

    $token = explode('Bearer ', $this->request->header('Authorization'))[1];
    $token_param = JWT::decode($token, Security::getSalt(), [Configure::read('JWT_Alg')]);
    $id = !empty($token_param->sub) ? $token_param->sub : 0;
    return $id;
  }

  public function __getUserFromTokenSession()
  {
    $this->loadModel('Users');
    $id = $this->__getUserIdFromJWT();

    if( !$id ){
      return false;
    }

    $user = $this->Users->get($this->__getUserIdFromJWT(), [
      'contain' => [
        'UserProviders'
      ]
    ]);
    return $user;
  }

  public function __getUserIdFromPassJWT($token=false)
  {
    $token_param = JWT::decode($token, Security::getSalt(), [Configure::read('JWT_Alg')]);
    $id = !empty($token_param->sub) ? $token_param->sub : 0;
    return $id;
  }
  
  public function __getUserFromPassTokenSession($token=false)
  {
    $this->loadModel('Users');
    $user = $this->Users->get($this->__getUserIdFromPassJWT($token), [
      'contain' => [
        'UserProviders'
      ]
    ]);
    return $user;
  }

  public function dateFormatBeforeSave($data=false) {
        if( !$data ){
          return false;
        }

        $hora = substr($data, 10, 20);
        $data = substr($data, 0, 10);

        $explode = explode("/", $data);
        if( count($explode) > 1 ){
            if ($hora) {
                $data = $explode[2] . "-" . $explode[1] . "-" . $explode[0] . $hora . ":00";
            } else {
                $data = $explode[2] . "-" . $explode[1] . "-" . $explode[0];
            }
        }

        return $data;
    }
}
