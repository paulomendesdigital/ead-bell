<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiAppController;
use Cake\Log\Log;

/**
 * AdvertClicks Controller
 *
 * @property \App\Model\Table\AdvertClicksTable $AdvertClicks
 *
 * @method \App\Model\Entity\AdvertClick[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdvertClicksController extends ApiAppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['add', 'redirectClick']);
        $this->RequestHandler->renderAs($this, 'json');
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post']);

        $query_params = $this->request->getQueryParams();
        if (empty($query_params['company_code']) && empty($query_params['company_id'])) {
            throw new \Cake\Http\Exception\BadRequestException('company não fornecido');
        }

        $companyCode = $query_params['company_code'];

        if (!empty($companyCode)) {
            $company = $this->AdvertClicks->AdvertZones->Companies->findByCode($companyCode)->first();
        } else {
            $company = $this->AdvertClicks->AdvertZones->Companies->findById($companyCode)->first();
        }

        if (empty($company)) {
            throw new \Cake\Http\Exception\NotFoundException('company não encontrado');
        }

        $advertAdvertZone = $this->AdvertClicks->AdvertsAdvertZones->find()
            ->contain(['AdvertZones'])
            ->where(['AdvertsAdvertZones.id' => $this->request->getData('advert_advert_zone_id')])
            ->first();

        if ($advertAdvertZone->advert_zone->company_id != $company->id) {
            throw new \Cake\Http\Exception\BadRequestException('Zona de anúncio não pertence à companhia fornecida');
        }

        $advertClick = $this->AdvertClicks->newEntity();
        $advertClick = $this->AdvertClicks->patchEntity($advertClick, $this->request->getData());

        if ($this->AdvertClicks->save($advertClick)) {
            $this->set([
                'result' => [
                    'success' => true,
                    'data' => $advertClick
                ],
                '_serialize' => 'result'
            ]);
        } else {
            $this->set([
                'result' => [
                    'success' => false,
                    'errors' => $advertClick->getErrors()
                ],
                '_serialize' => 'result'
            ]);
        }
    }

    public function redirectClick()
    {
        $this->request->allowMethod('get');

        $query_params = $this->request->getQueryParams();
        if (empty($query_params['company_code']) && empty($query_params['company_id'])) {
            throw new \Cake\Http\Exception\BadRequestException('company não fornecido');
        }

        $companyCode = $query_params['company_code'];

        if (!empty($companyCode)) {
            $company = $this->AdvertClicks->AdvertZones->Companies->findByCode($companyCode)->first();
        } else {
            $company = $this->AdvertClicks->AdvertZones->Companies->findById($companyCode)->first();
        }

        if (empty($company)) {
            throw new \Cake\Http\Exception\NotFoundException('company não encontrado');
        }

        if (empty($query_params['advert_advert_zone_id'])) {
            throw new \Cake\Http\Exception\BadRequestException('advert_advert_zone_id não fornecido');
        }

        if (empty($query_params['sessionid'])) {
            throw new \Cake\Http\Exception\BadRequestException('sessionid não fornecido');
        }

        $advertAdvertZone = $this->AdvertClicks->AdvertsAdvertZones->find()
            ->contain(['AdvertZones', 'Adverts'])
            ->where(['AdvertsAdvertZones.id' => $query_params['advert_advert_zone_id']])
            ->first();

        if ($advertAdvertZone->advert_zone->company_id != $company->id) {
            throw new \Cake\Http\Exception\BadRequestException('Zona de anúncio não pertence à companhia fornecida');
        }

        if (empty($advertAdvertZone->advert->url)) {
            throw new \Cake\Http\Exception\BadRequestException('Anúncio não possui url de redirecionamento');
        }

        $requestData = [
            'advert_advert_zone_id' => $query_params['advert_advert_zone_id'],
            'sessionid' => $query_params['sessionid']
        ];

        $advertClick = $this->AdvertClicks->newEntity();
        $advertClick = $this->AdvertClicks->patchEntity($advertClick, $requestData);

        if (!$this->AdvertClicks->save($advertClick)) {
            //TODO gravar log de erro
            Log::write('error', json_encode([
                'message' => 'Erro ao salvar redirecionamento de clique',
                'advertAdvertZone' => $advertAdvertZone->toArray(),
                'errors' => $advertClick->getErrors()
            ]));
        }

        return $this->redirect($advertAdvertZone->advert->url);
    }
}
