<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiAppController;
use Cake\Core\Configure;

/**
 * Newsletters Controller
 *
 * @property \App\Model\Table\NewslettersTable $Newsletters
 *
 * @method \App\Model\Entity\Newsletter[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class NewslettersController extends ApiAppController
{

    public function initialize()
    {
        parent::initialize();
        $this->RequestHandler->renderAs($this, 'json');
        $this->Auth->allow(['add']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post']);
        $query_params = $this->request->getQueryParams();
        if (empty($query_params['company_code'])) {
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }
        $company = $this->Newsletters->Companies->findByCode($query_params['company_code'])->first();
        if (empty($company)) {
            throw new \Cake\Http\Exception\NotFoundException('company não encontrado');
        };

        $newsletter_data = $this->request->getData();
        $newsletter_data['company_id'] = $company->id;

        $newsletter = $this->Newsletters->newEntity();
        $newsletter = $this->Newsletters->patchEntity($newsletter, $newsletter_data);

        if ($this->Newsletters->save($newsletter)) {
            $this->set([
                'result' => [
                    'success' => true,
                    'data' => $newsletter
                ],
                '_serialize' => 'result'
            ]);
        } else {
            $this->set([
                'result' => [
                    'success' => false,
                    'errors' => $newsletter->getErrors()
                ],
                '_serialize' => 'result'
            ]);
        }
    }
}
