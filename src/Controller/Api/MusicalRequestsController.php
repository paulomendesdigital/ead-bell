<?php
namespace App\Controller\Api;

use App\Controller\Api\ApiAppController;

/**
 * MusicalRequests Controller
 *
 * @property \App\Model\Table\MusicalRequestsTable $MusicalRequests
 *
 * @method \App\Model\Entity\MusicalRequest[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MusicalRequestsController extends ApiAppController
{
    public function initialize()
    {
        parent::initialize();
        $this->RequestHandler->renderAs($this, 'json');
        $this->Auth->allow(['add']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post']);
        $query_params = $this->request->getQueryParams();
        if (empty($query_params['company_code'])) {
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }
        $company = $this->MusicalRequests->Companies->findByCode($query_params['company_code'])->First();
        if(empty($company)){
            throw new \Cake\Http\Exception\NotFoundException('company não encontrado');
        };
        
        $musicalRequest = $this->MusicalRequests->newEntity();
        $musicalRequest_data = $this->request->getData();    
        $musicalRequest_data['company_id'] = $company->id; 

        //$user = $this->__getUserFromTokenSession();

        //if( !empty($user) ){
        //    $musicalRequest['user_id'] = $user->id;
        //}

        $musicalRequest = $this->MusicalRequests->patchEntity($musicalRequest, $musicalRequest_data);
        
        if ( $this->MusicalRequests->save($musicalRequest) ) {            
            $this->set([
                'result' => [
                    'success' => true,
                    'data' => $musicalRequest
                ],
                '_serialize' => 'result'
            ]);
        } else {
            $this->set([
                'result' => [
                    'success' => false,
                    'errors' => $musicalRequest->getErrors()
                ],
                '_serialize' => 'result'
            ]);
        }                    
    }

}
