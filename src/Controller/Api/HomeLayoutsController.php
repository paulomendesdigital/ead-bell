<?php
namespace App\Controller\Api;

use App\Controller\Api\ApiAppController;

/**
 * HomeLayouts Controller
 *
 * @property \App\Model\Table\HomeLayoutsTable $HomeLayouts
 *
 * @method \App\Model\Entity\HomeLayout[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HomeLayoutsController extends ApiAppController
{

    public function initialize()
    {
        parent::initialize();
        $this->RequestHandler->renderAs($this, 'json');
        $this->Auth->allow(['viewCurrent']);
    }

    public function viewCurrent(){
        $this->request->allowMethod(['get']);
        $query_params = $this->request->getQueryParams();
        if(empty($query_params['company_code'])){
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }
        $company = $this->HomeLayouts->Companies->findByCode($query_params['company_code'])->first();        
        if(empty($company)){
            throw new \Cake\Http\Exception\NotFoundException('company_code não encontrado');
        }
        $homeLayout = $this->HomeLayouts->find()
        ->contain([
            'HomeLayoutItems' => [
                'sort' => [
                    'HomeLayoutItems.row',
                    'HomeLayoutItems.ordination'
                ],
                'Contents' => [
                    'ContentCategories'
                ]
            ]
        ])
        ->where([
            'HomeLayouts.status' => 1,
            'HomeLayouts.company_id' => $company->id,
            'HomeLayouts.start <=' => date('Y-m-d H:i:s'),
            'HomeLayouts.finish >=' => date('Y-m-d H:i:s')
        ])
        ->first();
        $this->set([
            'result' => [
                'success' => true,
                'data' => $homeLayout
            ],
            '_serialize' => 'result'
        ]);
    }
}
