<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiAppController;
use Cake\Core\Configure;

/**
 * Participants Controller
 *
 * @property \App\Model\Table\ParticipantsTable $Participants
 *
 * @method \App\Model\Entity\Participant[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ParticipantsController extends ApiAppController
{

    public function initialize()
    {
        parent::initialize();
        $this->RequestHandler->renderAs($this, 'json');
        $this->Auth->allow(['add']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($promotion_id)
    {
        $this->request->allowMethod(['post']);
        $query_params = $this->request->getQueryParams();
        if (empty($query_params['company_code'])) {
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }
        $company = $this->Participants->Users->Companies->findByCode($query_params['company_code'])->first();
        if (empty($company)) {
            throw new \Cake\Http\Exception\NotFoundException('company não encontrado');
        };

        $this->loadComponent(
            'AccessOriginComponent',
            ['className' => 'AccessOrigins']
        );

        $accessOriginId = $this->AccessOriginComponent->getAccessOriginIdFromRequest();

        $content = $this->Participants->Contents->find()
            ->where([
                'Contents.content_category_id' => $this->Participants->Contents->ContentCategories->getPromocaoId(),
                'Contents.id' => $promotion_id
            ])
            ->first();

        if (empty($content)) {
            throw new \Cake\Http\Exception\NotFoundException('promoção não encontrada');
        }

        $user = $this->__getUserFromTokenSession();

        if (empty($user)) {
            throw new \Cake\Http\Exception\NotFoundException('usuário não localizado!');
        }

        $content = $this->__isUserInPromotionHelper($promotion_id, $user->id);
        if (!empty($content)) {
            throw new \Cake\Http\Exception\ForbiddenException('usuário já está participando da promoção');
        }

        $can_participate_new_promotions = !$this->__isUserInLastDaysPromotions($user->id, Configure::read('PromotionNewParticipantBlockDays'));

        $participants_data = $this->request->getData('participants');

        if (empty($participants_data)) {
            throw new \Cake\Http\Exception\BadRequestException("Dados de participação não enviados");
        }

        for ($i = 0; $i < count($participants_data); $i++) {
            $participants_data[$i]['user_id'] = $user->id;
            $participants_data[$i]['company_id'] = $company->id;
            $participants_data[$i]['access_origin_id'] = $accessOriginId;
            $participants_data[$i]['content_id'] = $promotion_id;
            if (isset($participants_data[$i]['correct'])) {
                unset($participants_data[$i]['correct']);
            }
            if (isset($participants_data[$i]['alternative_id']) and !empty($participants_data[$i]['alternative_id'])) {
                $alternative = $this->Participants->Alternatives->find()
                    ->where(['Alternatives.id' => $participants_data[$i]['alternative_id']])
                    ->first();
                if ($alternative['is_correct'] == 1) {
                    $participants_data[$i]['correct'] = 1;
                }
            }

            if (!$can_participate_new_promotions) {
                $participants_data[$i]['status'] = 0;
            }
        }

        //@TODO Criar validação para cadastro em promoção
        $participants = $this->Participants->newEntities($participants_data);
        //@TODO Não tentar salvar caso exista algum registro com falha na validação
        if ($this->Participants->saveMany($participants)) {
            $this->set([
                'result' => [
                    'success' => true,
                    'data' => $participants
                ],
                '_serialize' => 'result'
            ]);
        } else {
            $errors = [];
            for ($i = 0; $i < count($participants); $i++) {
                $errors[] = $participants[$i]->getErrors();
            }
            $this->set([
                'result' => [
                    'success' => false,
                    'errors' => $errors
                ],
                '_serialize' => 'result'
            ]);
        }
    }

    public function isUserInPromotion($promotion_id)
    {
        $this->request->allowMethod(['get']);
        $user_id = $this->__getUserIdFromJWT();
        $content = $this->__isUserInPromotionHelper($promotion_id, $user_id);
        $this->set([
            'result' => [
                'success' => true,
                'data' => $content
            ],
            '_serialize' => 'result'
        ]);
    }

    private function __isUserInPromotionHelper($promotion_id, $user_id)
    {
        return $this->Participants->Contents->find()
            ->where([
                'Contents.id' => $promotion_id
            ])
            ->matching("Participants", function ($q) use ($user_id) {
                return $q->where([
                    'Participants.user_id' => $user_id
                ]);
            })
            ->first();
    }

    private function __isUserInLastDaysPromotions($user_id, $days)
    {
        $participant = $this->Participants->find()
            ->where([
                'Participants.user_id' => $user_id,
                'Participants.created >= DATE_ADD(curdate(), INTERVAL -' . $days . ' DAY)'
            ])
            ->first();
        if (!empty($participant)) {
            return true;
        }
        return false;
    }
}
