<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiAppController;
use Cake\Datasource\ConnectionManager;
use Cake\Http\Exception\NotFoundException;

/**
 * Contents Controller
 *
 * @property \App\Model\Table\ContentsTable $Contents
 *
 * @method \App\Model\Entity\Content[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContentsController extends ApiAppController
{

    public function initialize()
    {
        parent::initialize();
        $this->RequestHandler->renderAs($this, 'json');
        $this->Auth->allow(['index', 'view', 'tagList', 'promotionsWithResults', 'promotionsReportData']);
    }

    /**
     * Index method
     * https://api.dialbrasil.com.br/api/contents/?company_code=SPARADISO&content_category_code=PROMOCAO
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->request->allowMethod(['get']);
        $query_params = $this->request->getQueryParams();
        if (empty($query_params['company_code']) && empty($query_params['company_id'])) {
            throw new \Cake\Http\Exception\BadRequestException('company não fornecido');
        }

        $this->paginate = [
            'finder' => [
                'websiteContents' => $query_params
            ],
            'contain' => [
                'ContentCategories',
                /*
                retirado a pedido da empresa do aplitivo, pois deixava o app lento, devido a qtde de aprticipantes
                'Participants' => [
                    'Users' => [
                        'UserProviders'
                    ]
                ],*/
                'Winners' => [
                    'Users' => [
                        'UserProviders'
                    ]
                ],
                'Questions' => [
                    'Alternatives'
                ]
            ]
        ];

        try {
            $contents = $this->paginate($this->Contents)->toArray();
            //gambiarra para atender ao PV, tem que enviar o nó de participants vazio pelo manos
            foreach ($contents as $key => $value) {
                $contents[$key]['participants'] = [];
            }
          
            $this->set([
                'result' => [
                    'success' => true,
                    'data' => $contents
                ],
                '_serialize' => 'result'
            ]);
        } catch (NotFoundException $e) {
            $this->set([
                'result' => [
                    'success' => true,
                    'data' => []
                ],
                '_serialize' => 'result'
            ]);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Content id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id)
    {
        $this->request->allowMethod(['get']);
        $exists = $this->Contents->exists(['Contents.id' => $id]);
        $content = false;

        if ($exists) {

            $content = $this->Contents->get($id, [
                'contain' => [
                    'ContentCategories',
                    'Questions' => [
                        'Alternatives',
                        'Participants' => [
                            'Alternatives',
                            'Users'
                        ],
                    ],
                    //'Participants' => [
                    //    'Users'
                    //],
                    'Winners' => [
                        'Users'
                    ]
                ]
            ]);
        }
        $content->participants = [];
        die(debug($content));
        $this->set([
            'result' => [
                'success' => true,
                'data' => $content
            ],
            '_serialize' => 'result'
        ]);
    }

    public function tagList()
    {
        $this->request->allowMethod(['get']);
        $query_params = $this->request->getQueryParams();
        if (empty($query_params['company_code'])) {
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }
        $company = $this->Contents->Companies->findByCode($query_params['company_code'])->first();
        //Explicação sobre execução de queries no cakephp 3
        //https://stackoverflow.com/a/26319806/7015083
        $conn = ConnectionManager::get('default');
        //Conta o máximo de tags que um content possui
        $stmt = $conn->execute(
            'SELECT MAX(ROUND (   
            (
                LENGTH(tags)
                - LENGTH( REPLACE ( tags, ";", "") ) 
            ) / LENGTH(";")        
            )) AS max_count_tags
        FROM contents 
        WHERE tags IS NOT NULL
              AND tags <> ""
              AND company_id = ' . $company->id

        );
        $max_count_tags = $stmt->fetch('assoc')['max_count_tags'];
        if ($max_count_tags == 0) {
            $this->set([
                'result' => [
                    'success' => true,
                    'data' => []
                ],
                '_serialize' => 'result'
            ]);
        } else {
            //Explicação sobre como pegar as tags de forma separada
            //https://stackoverflow.com/a/17942691/7015083
            $numbers_query = "SELECT 1 n";
            for ($i = 2; $i <= $max_count_tags; $i++) {
                $numbers_query .= " UNION ALL SELECT {$i}";
            }
            $query =
                "SELECT DISTINCT
            TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(contents.tags, ';', numbers.n), ';', -1)) name
            FROM ({$numbers_query}) numbers 
            INNER JOIN contents
            ON contents.tags IS NOT NULL 
               AND contents.tags <> '' 
               AND CHAR_LENGTH(contents.tags)
               -CHAR_LENGTH(REPLACE(contents.tags, ';', ''))>=numbers.n-1
            WHERE SUBSTRING_INDEX(SUBSTRING_INDEX(contents.tags, ';', numbers.n), ';', -1) IS NOT NULL 
            AND SUBSTRING_INDEX(SUBSTRING_INDEX(contents.tags, ';', numbers.n), ';', -1) <> ''
            AND contents.company_id = " . $company->id;

            //Paginação
            $query_params = $this->request->getQueryParams();
            if (!empty($query_params['limit'])) {
                $query .= " LIMIT " . $query_params['limit'];
                if (!empty($query_params['page']) && $query_params['page'] > 1) {
                    $query .= " OFFSET " . $query_params['limit'] * ($query_params['page'] - 1);
                }
            }
            $stmt = $conn->execute($query);
            $this->set([
                'result' => [
                    'success' => true,
                    'data' => $stmt->fetchAll('assoc')
                ],
                '_serialize' => 'result'
            ]);
        }
    }

    public function promotionsWithResults()
    {

        $this->request->allowMethod(['get']);
        $query_params = $this->request->getQueryParams();
        if (empty($query_params['company_code']) && empty($query_params['company_id'])) {
            throw new \Cake\Http\Exception\BadRequestException('company não fornecido');
        }

        if (!empty($query_params['company_code'])) {
            $company = $this->Contents->Companies->findByCode($query_params['company_code'])->first();
        } else {
            $company = $this->Contents->Companies->findById($query_params['company_id'])->first();
        }

        if (empty($company)) {
            throw new \Cake\Http\Exception\NotFoundException('company não encontrado');
        }

        $this->paginate = [
            'contain' => [
                'ContentCategories',
                'Winners' => [
                    'Users' => [
                        'UserProviders'
                    ]
                ],
            ],
            'conditions' => [
                'Contents.company_id' => $company->id,
                'Contents.status' => 1,
                'Contents.id IN' => $this->Contents->Winners->find('list', [
                    'fields' => ['Winners.content_id'],
                    'conditions' => ['Winners.company_id' => $company->id]
                ])
            ],
            'order' => [
                'Contents.finish' => 'DESC'
            ]

        ];
        $contents = $this->paginate($this->Contents);
        $this->set([
            'result' => [
                'success' => true,
                'data' => $contents
            ],
            '_serialize' => 'result'
        ]);
    }

    /**
     * [promotionsReportData endpoint usado no relatorio do google studio]
     * @return [type] [description]
     */
    public function promotionsReportData()
    {
        $query_params = $this->request->getQueryParams();
        if (empty($query_params['company_code']) && empty($query_params['company_id'])) {
            throw new \Cake\Http\Exception\BadRequestException('company não fornecido');
        }

        if (!empty($query_params['company_code'])) {
            $company = $this->Contents->Companies->findByCode($query_params['company_code'])->first();
        } else {
            $company = $this->Contents->Companies->findById($query_params['company_id'])->first();
        }

        if (empty($company)) {
            throw new \Cake\Http\Exception\NotFoundException('company não encontrado');
        }

        $contents = $this->Contents->find('all', [
            'fields' => [
                'id',
                'name',
                'title',
                'subtitle',
                'button_label',
                'image_small_name',
                'image_small_path',
                'image_medium_name',
                'image_medium_path',
                'image_large_name',
                'image_large_path',
                'image_internal',
                'image_internal_path',
                'start',
                'finish',
                'finish_promotion',
                'created'
            ]
        ])
            ->contain([
                'Participants' => [
                    'fields' => [
                        'id',
                        'company_id',
                        'content_id',
                        'user_id',
                        'content_name' => 'Contents.name',
                        'created',
                    ],
                    'Users' => [
                        'fields' => [
                            'id',
                            'name',
                            'gender',
                            'birth',
                            'cep',
                            'address',
                            'number',
                            'complement',
                            'neighborhood',
                            'city',
                            'state',
                            'created'
                        ]
                    ],
                    'Contents' => [
                        'fields' => [
                            'name'
                        ]
                    ]
                ],
            ])
            ->where([
                'Contents.content_category_id' => $this->Contents->ContentCategories->getPromocaoId(),
                'Contents.company_id' => $company->id
            ]);

        $date = date_create_from_format('Y-m-d', date('Y-m-d'));
        $date->add(date_interval_create_from_date_string('-2 months'));

        $contents = $contents->where([
            'DATE(Contents.created) >=' => $date->format('Y-m-d')
        ]);

        $this->set([
            'result' => [
                'success' => true,
                'data' => $contents
            ],
            '_serialize' => 'result'
        ]);
    }
}
