<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiAppController;
use Cake\ORM\TableRegistry;

/**
 * AdvertsAdvertZones Controller
 *
 * @property \App\Model\Table\AdvertsAdvertZonesTable $AdvertsAdvertZones
 *
 * @method \App\Model\Entity\AdvertsAdvertZone[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdvertsAdvertZonesController extends ApiAppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['indexForSite']);
        $this->RequestHandler->renderAs($this, 'json');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function indexForSite()
    {
        $this->request->allowMethod(['get']);

        $query_params = $this->request->getQueryParams();
        if (empty($query_params['company_code']) && empty($query_params['company_id'])) {
            throw new \Cake\Http\Exception\BadRequestException('company não fornecido');
        }

        $companyCode = $query_params['company_code'];

        if (!empty($companyCode)) {
            $company = $this->AdvertsAdvertZones->AdvertZones->Companies->findByCode($companyCode)->first();
        } else {
            $company = $this->AdvertsAdvertZones->AdvertZones->Companies->findById($companyCode)->first();
        }

        if (empty($company)) {
            throw new \Cake\Http\Exception\NotFoundException('company não encontrado');
        }

        $this->paginate = [
            'contain' => ['Adverts', 'AdvertZones']
        ];

        $con = $this->AdvertsAdvertZones->getConnection();

        $result = $con->execute("
            SELECT a.*
              FROM (SELECT CASE WHEN ac.weight = 10 THEN 20 ELSE FLOOR(RAND()*(10-0+1))+ac.weight END as weight,
                           az.code,
                           aaz.id
                      FROM adverts_advert_zones aaz
                           JOIN advert_zones az
                             ON aaz.advert_zone_id = az.id
                           JOIN adverts a
                             ON aaz.advert_id = a.id
                           JOIN advert_campaigns ac
                             ON a.advert_campaign_id = ac.id
                     WHERE az.company_id = {$company->id}
                           AND ac.company_id = {$company->id}
                           AND az.status = 1
                           AND ac.status = 1
                           AND a.status = 1
                           AND aaz.status = 1
                           AND (ac.start_date IS NULL OR ac.start_date < now())
                           AND (ac.finish_date IS NULL OR ac.finish_date > now())) a
                    ORDER BY a.weight DESC;
        ")->fetchAll('assoc');

        $advertAdvertZones = [];

        foreach ($result as $rankedAdvertZone) {
            if (!isset($advertAdvertZones[$rankedAdvertZone['code']])) {
                $advertAdvertZones[$rankedAdvertZone['code']] = $rankedAdvertZone;
                continue;
            }

            if (
                $advertAdvertZones[$rankedAdvertZone['code']]['weight'] < $rankedAdvertZone['weight']
                || ($advertAdvertZones[$rankedAdvertZone['code']]['weight'] == $rankedAdvertZone['weight']
                    && ($advertAdvertZones[$rankedAdvertZone['code']]['weight'] + rand(0, 10)) < ($rankedAdvertZone['weight'] + rand(0, 10)))
            ) {
                $advertAdvertZones[$rankedAdvertZone['code']] = $rankedAdvertZone;
            }
        }

        $advertAdvertZonesIds = array_column($advertAdvertZones, 'id');

        if (!empty($advertAdvertZonesIds)) {
            $advertsAdvertZones = $this->AdvertsAdvertZones->find()
                ->contain([
                    'Adverts',
                    'AdvertZones'
                ])
                ->where(['AdvertsAdvertZones.id IN' => $advertAdvertZonesIds]);

            if (!empty($query_params['advert_zones'])) {
                foreach ($query_params['advert_zones'] as $key => $value) {
                    $advertsAdvertZones = $advertsAdvertZones
                        ->where(["AdvertZones.{$key}" => $value]);
                }
            }

            $advertAdvertZones = $advertsAdvertZones->toArray();
        } else {
            $advertsAdvertZones = [];
        }

        $this->set([
            'result' => [
                'success' => true,
                'data' => $advertsAdvertZones
            ],
            '_serialize' => 'result'
        ]);

        //Registra visualizações caso os parâmetros sejam passados
        if (!empty($query_params['sessionid']) && isset($query_params['add_view_count']) && $query_params['add_view_count'] == 1) {
            if (!empty($advertAdvertZones)) {
                $advertViewsTable = TableRegistry::getTableLocator()->get('AdvertViews');
                foreach ($advertAdvertZones as $advertAdvertZone) {
                    $advertView = $advertViewsTable->newEntity();
                    $advertView = $advertViewsTable->patchEntity($advertView, [
                        'advert_advert_zone_id' => $advertAdvertZone->id,
                        'sessionid' => $query_params['sessionid']
                    ]);
                    $advertViewsTable->save($advertView);
                }
            }
        }
    }
}
