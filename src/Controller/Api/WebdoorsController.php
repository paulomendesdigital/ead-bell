<?php
namespace App\Controller\Api;

use App\Controller\Api\ApiAppController;

/**
 * Webdoors Controller
 *
 * @property \App\Model\Table\WebdoorsTable $Webdoors
 *
 * @method \App\Model\Entity\Webdoor[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class WebdoorsController extends ApiAppController
{

    public function initialize()
    {
        parent::initialize();
        $this->RequestHandler->renderAs($this, 'json');
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->request->allowMethod(['get']);        
        $query_params = $this->request->getQueryParams();
        if(empty($query_params['company_code'])){
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }
        $this->paginate = [
            'finder' => [
                'websiteWebdoors' => $query_params
            ]
        ];
        $webdoors = $this->paginate($this->Webdoors);
        $this->set([
            'result' => [
                'success' => true,
                'data' => $webdoors
            ],
            '_serialize' => 'result'
        ]);        
    }

    /**
     * View method
     *
     * @param string|null $id Webdoor id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id)
    {
        $this->request->allowMethod(['get']);
        $webdoor = $this->Webdoors->get($id);
        $this->set([
            'result' => [
                'success' => true,
                'data' => $webdoor
            ],
            '_serialize' => 'result'
        ]);        
    }   
}
