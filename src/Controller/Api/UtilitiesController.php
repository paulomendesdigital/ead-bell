<?php

namespace App\Controller\Api;

use Cake\Http\Client;
use Cake\Http\Exception\NotFoundException;
use Cake\Utility\Xml;

class UtilitiesController extends ApiAppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow();
    }

    private function __escapeStringEntitiesToUtf8($string)
    {
        $string = str_replace("&", $this->__escapeStringEntityToUtf8("&"), $string);
        $string = str_replace("!", $this->__escapeStringEntityToUtf8("!"), $string);
        $string = str_replace("?", $this->__escapeStringEntityToUtf8("?"), $string);
        return $string;
    }

    private function __escapeStringEntityToUtf8($char)
    {
        $s = html_entity_decode($char, ENT_QUOTES, 'UTF-8');
        return htmlspecialchars($char, ENT_QUOTES, 'UTF-8', false);
    }


    public function getPulsarXml()
    {
        $this->RequestHandler->renderAs($this, 'xml');
        $this->request->allowMethod('get');
        $query_params = $this->request->getQueryParams();
        if (empty($query_params['company_code'])) {
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }
        $this->loadModel('Companies');
        $company = $this->Companies->findByCode($query_params['company_code'])->first();
        if (empty($company)) {
            throw new \Cake\Http\Exception\NotFoundException('company_code não encontrado');
        }

        if ($query_params['company_code'] == 'SPARADISO') {
            $filedir = 'https://sulamericaparadiso.com.br/sulamerica.xml';
        } else {
            $filedir = 'https://mixriofm.com.br/pulsar.xml';
        }

        $http = new Client();
        $response = $http->get($filedir);
        $xmlString = $response->getStringBody();
        $xmlString = $this->__escapeStringEntitiesToUtf8($xmlString);
        $xml = Xml::build($xmlString);

        $arrayXml = Xml::toArray($xml);
        $rootNode = null;
        $response = [];

        foreach ($arrayXml as $key => $value) {
            if (empty($rootNode)) {
                $rootNode = $key;
                $this->set('_rootNode', $rootNode);
                continue;
            }
        }
        foreach ($arrayXml[$rootNode] as $key => $value) {
            $this->set($key, $value);
            $response[] = $key;
        }

        $this->set('_serialize', $response);
    }

    public function getQualicorpXml()
    {
        $this->RequestHandler->renderAs($this, 'xml');
        $this->request->allowMethod('get');

        $filedir = 'https://mixriofm.com.br/qualicorp.xml';

        $http = new Client();
        $response = $http->get($filedir);
        $xmlString = $response->getStringBody();
        $xmlString = $this->__escapeStringEntitiesToUtf8($xmlString);
        $xml = Xml::build($xmlString);

        $arrayXml = Xml::toArray($xml);
        $rootNode = null;
        $response = [];

        foreach ($arrayXml as $key => $value) {
            if (empty($rootNode)) {
                $rootNode = $key;
                $this->set('_rootNode', $rootNode);
                continue;
            }
        }
        foreach ($arrayXml[$rootNode] as $key => $value) {
            $this->set($key, $value);
            $response[] = $key;
        }

        $this->set('_serialize', $response);
    }
}
