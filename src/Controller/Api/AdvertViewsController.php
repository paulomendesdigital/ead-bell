<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiAppController;
use Cake\Http\Exception\ForbiddenException;
use Exception;

/**
 * AdvertViews Controller
 *
 * @property \App\Model\Table\AdvertViewsTable $AdvertViews
 *
 * @method \App\Model\Entity\AdvertView[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdvertViewsController extends ApiAppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow('add');
        $this->RequestHandler->renderAs($this, 'json');
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post']);

        $query_params = $this->request->getQueryParams();
        if (empty($query_params['company_code']) && empty($query_params['company_id'])) {
            throw new \Cake\Http\Exception\BadRequestException('company não fornecido');
        }

        $companyCode = $query_params['company_code'];

        if (!empty($companyCode)) {
            $company = $this->AdvertViews->AdvertZones->Companies->findByCode($companyCode)->first();
        } else {
            $company = $this->AdvertViews->AdvertZones->Companies->findById($companyCode)->first();
        }

        if (empty($company)) {
            throw new \Cake\Http\Exception\NotFoundException('company não encontrado');
        }

        $advertAdvertZone = $this->AdvertViews->AdvertsAdvertZones->find()
            ->contain(['AdvertZones'])
            ->where(['AdvertsAdvertZones.id' => $this->request->getData('advert_advert_zone_id')])
            ->firstOrFail();

        if ($advertAdvertZone->advert_zone->company_id != $company->id) {
            throw new \Cake\Http\Exception\BadRequestException('Zona de anúncio não pertence à companhia fornecida');
        }

        $advertView = $this->AdvertViews->newEntity();
        $advertView = $this->AdvertViews->patchEntity($advertView, $this->request->getData());

        //Em casos de deadlock, haverão outras tentativas após 3 segundos.
        //Em caso de precisar de uma terceira tentativa, ela será a única e em caso de erro ela criará uma exceção
        for ($i = 0; $i < 3; $i++) {
            if ($i < 2) {
                try {
                    $result = $this->AdvertViews->save($advertView);
                    break;
                } catch (Exception $e) {
                    $result = false;
                    sleep(3);
                }
            } else {
                $result = $this->AdvertViews->save($advertView);
            }
        }

        if ($result) {
            $this->set([
                'result' => [
                    'success' => true,
                    'data' => $advertView
                ],
                '_serialize' => 'result'
            ]);
        } else {
            $this->set([
                'result' => [
                    'success' => false,
                    'errors' => $advertView->getErrors()
                ],
                '_serialize' => 'result'
            ]);
        }
    }
}
