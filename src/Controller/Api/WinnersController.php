<?php
namespace App\Controller\Api;

use App\Controller\Api\ApiAppController;

/**
 * Winners Controller
 *
 * @property \App\Model\Table\WinnersTable $Winners
 *
 * @method \App\Model\Entity\Winner[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class WinnersController extends ApiAppController
{

    public function initialize()
    {
        parent::initialize();
        $this->RequestHandler->renderAs($this, 'json');
        $this->Auth->allow(['index', 'view', 'autoAdd']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->request->allowMethod(['get']);
        $query_params = $this->request->getQueryParams();
        if(empty($query_params['company_code'])){
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }

        if(empty($query_params['content_id'])){
            throw new \Cake\Http\Exception\BadRequestException('promoção não fornecido');
        }

        $winners = $this->Winners->Users->find()
        ->contain([
            'Participants'=>[
                'conditions'=>[
                    'Participants.content_id'=>$query_params['content_id'],
                    'Participants.user_id IN' => $this->Winners->find('list',[
                        'fields'=>['Winners.user_id'],
                        'conditions'=>['Winners.content_id'=>$query_params['content_id']]
                    ])
                ],
                'Questions',
                'Alternatives',
            ],
            'Winners'=>[
                'conditions'=>[
                    'Winners.user_id IN' => $this->Winners->find('list',[
                        'fields'=>['Winners.user_id'],
                        'conditions'=>['Winners.content_id'=>$query_params['content_id']]
                    ])
                ]
            ]
        ])
        ->where([
            'Users.status' => 1,
            'Users.group_id' => 2,
            'Users.id IN' => $this->Winners->find('list',[
                'fields'=>['Winners.user_id'],
                'conditions'=>['Winners.content_id'=>$query_params['content_id']]
            ])
        ]);

        $this->set([
            'result' => [
                'success' => true,
                'data' => $winners
            ],
            '_serialize' => 'result'
        ]);        
    } 

    public function autoAdd()
    {
        $this->request->allowMethod(['get']);
        $query_params = $this->request->getQueryParams();
        if(empty($query_params['company_code'])){
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }

        $company = $this->Winners->Companies->findByCode($query_params['company_code'])->first();
        if (empty($company)) {
            throw new \Cake\Http\Exception\NotFoundException('company_code não encontrado');
        }

        $contents = $this->Winners->Contents->find()
        ->contain([
            'ContentCategories',
            'Companies',
            'Participants'=>[
                'fields'=>['DISTINCT Participants.user_id','Participants.content_id']
            ],
        ])
        ->where([
            'Contents.company_id' => $company->id,
            'Contents.content_category_id' => $this->Winners->Contents->getPromotionId(),
            'Contents.status' => 1,
            'DATE_FORMAT(Contents.finish_promotion,"%Y%m%d")' => date('Ymd'),
            'DATE_FORMAT(Contents.finish_promotion,"%Y%m%d%H%i%s") <=' => date('YmdHis'),
            'Contents.auto_draw' => 1,
            'Contents.limitwinners >'=>0
        ])
        ->order([
            'Contents.finish_promotion'=>'ASC'
        ])
        ->all();

        if( !empty($contents) ){

            $qtdewinners = 0;

            foreach ( $contents as $content ) {

                $qtdewinners = $this->Winners->find()->where(['Winners.content_id' => $content->id])->count();
                $qtdeparticipants = $this->Winners->Contents->Participants->find()->where(['Participants.content_id'=>$content->id,'Participants.status'=>1])->count();
                
                if( $qtdeparticipants > 0 ){

                    while ( $qtdewinners < $content->limitwinners ) {
                        
                        //executo o sorteio automático
                        if( !$this->Winners->changeAutoAdd( $content->id ) ){
                            //se retornar false é pq não havia participante
                            //neste caso, eu saio do while
                            break 1;
                        }
                        
                        $qtdewinners = $this->Winners->find()->where(['Winners.content_id' => $content->id])->count();
                    
                    }
                }
                
                debug( "
                    <li><b>Promoção: {$content->name}</b></li>
                    <li>Limite de ganhadores: {$content->limitwinners}</li>
                    <li>Ganhadores: {$qtdewinners}</li>
                " );
            }
        }

        die(debug( "Fim!" ));
    }   
}
