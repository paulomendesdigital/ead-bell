<?php
namespace App\Controller\Api;

use App\Controller\Api\ApiAppController;

/**
 * Programs Controller
 *
 * @property \App\Model\Table\ProgramsTable $Programs
 *
 * @method \App\Model\Entity\Program[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProgramsController extends ApiAppController
{

    public function initialize()
    {
        parent::initialize();
        $this->RequestHandler->renderAs($this, 'json');
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->request->allowMethod(['get']);        
        $query_params = $this->request->getQueryParams();
        if(empty($query_params['company_code'])){
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }
        $this->paginate = [
            'finder' => [
                'websitePrograms' => $query_params
            ]
        ];
        $programs = $this->paginate($this->Programs);
        $this->set([
            'result' => [
                'success' => true,
                'data' => $programs
            ],
            '_serialize' => 'result'
        ]);        
    }

    /**
     * View method
     *
     * @param string|null $id Program id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id)
    {
        $this->request->allowMethod(['get']);
        //$program = $this->Programs->get($id);
        $program = $this->Programs->find()
        ->contain([
            'Contents' => [
                'sort' => ['Contents.start'=>'DESC'],
                'ContentCategories',
                'Questions'=>[
                    'Alternatives',
                    'Participants' => [
                        'Alternatives',
                        'Users'
                    ],
                ],
                'Participants' => [
                    'Users'
                ],
                'Winners' => [
                    'Users'
                ]
            ],
        ])
        ->where([
            'Programs.status' => 1,
            'Programs.id' => $id
        ])
        ->first();
        $this->set([
            'result' => [
                'success' => true,
                'data' => $program
            ],
            '_serialize' => 'result'
        ]);        
    }   
}
