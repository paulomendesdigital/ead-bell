<?php

namespace App\Controller\Api;

use App\Controller\Api\ApiAppController;

/**
 * Advertisements Controller
 *
 * @property \App\Model\Table\AdvertisementsTable $Advertisements
 *
 * @method \App\Model\Entity\Advertisement[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdvertisementsController extends ApiAppController
{

    public function initialize()
    {
        parent::initialize();
        $this->RequestHandler->renderAs($this, 'json');
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->request->allowMethod(['get']);
        $query_params = $this->request->getQueryParams();
        if (empty($query_params['company_code'])) {
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }

        $this->paginate = [
            'finder' => [
                'websiteAdvertisements' => $query_params
            ]
        ];
        $advertisements = $this->paginate($this->Advertisements);

        $this->set([
            'result' => [
                'success' => true,
                'data' => $advertisements->first()
            ],
            '_serialize' => 'result'
        ]);
    }

    /**
     * View method
     *
     * @param string|null $id Top Music id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id)
    {
        $this->request->allowMethod(['get']);
        $advertisement = $this->Advertisements->get($id);
        $this->set([
            'result' => [
                'success' => true,
                'data' => $advertisement
            ],
            '_serialize' => 'result'
        ]);
    }
}
