<?php
namespace App\Controller\Api;

use App\Controller\Api\ApiAppController;

/**
 * TopMusics Controller
 *
 * @property \App\Model\Table\TopMusicsTable $TopMusics
 *
 * @method \App\Model\Entity\TopMusic[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TopMusicsController extends ApiAppController
{

    public function initialize()
    {
        parent::initialize();
        $this->RequestHandler->renderAs($this, 'json');
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->request->allowMethod(['get']);
        $query_params = $this->request->getQueryParams();
        if(empty($query_params['company_code'])){
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }
        $this->paginate = [
            'finder' => [
                'websiteTopMusics' => $query_params
            ]
        ];
        $topMusics = $this->paginate($this->TopMusics);
        $this->set([
            'result' => [
                'success' => true,
                'data' => $topMusics
            ],
            '_serialize' => 'result'
        ]);        
    }

    /**
     * View method
     *
     * @param string|null $id Top Music id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id)
    {
        $this->request->allowMethod(['get']);
        $topMusic = $this->TopMusics->get($id);
        $this->set([
            'result' => [
                'success' => true,
                'data' => $topMusic
            ],
            '_serialize' => 'result'
        ]);        
    }
    
}
