<?php
namespace App\Controller\Api;

use App\Controller\Api\ApiAppController;

/**
 * MidiaKits Controller
 *
 * @property \App\Model\Table\MidiaKitsTable $MidiaKits
 *
 * @method \App\Model\Entity\MidiaKit[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MidiaKitsController extends ApiAppController
{

    public function initialize()
    {
        parent::initialize();
        $this->RequestHandler->renderAs($this, 'json');
        $this->Auth->allow(['viewCurrent']);
    }

    public function viewCurrent(){
        $this->request->allowMethod(['get']);
        $query_params = $this->request->getQueryParams();
        if(empty($query_params['company_code'])){
            throw new \Cake\Http\Exception\BadRequestException('company_code não fornecido');
        }
        $company = $this->MidiaKits->Companies->findByCode($query_params['company_code'])->first();        
        if(empty($company)){
            throw new \Cake\Http\Exception\NotFoundException('company_code não encontrado');
        }
        $midiaKit = $this->MidiaKits->find()
        ->where([
            'MidiaKits.status' => 1,
            'MidiaKits.company_id' => $company->id
        ])
        ->order(['MidiaKits.id'=>'DESC'])
        ->first();

        if( !empty($midiaKit) ){
            $midiaKit->pdf = "/files/MidiaKits/pdf/{$midiaKit->pdf}";
        }

        $this->set([
            'result' => [
                'success' => true,
                'data' => $midiaKit
            ],
            '_serialize' => 'result'
        ]);
    }
}
