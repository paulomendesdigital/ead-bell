<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * Spreadsheet component
 */
class SpreadsheetComponent extends Component
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public  function __writeSpreadsheetRow($row, &$spreadsheet, $rowNumber)
    {
        $colNumber = 0;
        foreach ($row as $col) {
            $spreadsheet->getActiveSheet()->setCellValue($this->num2alpha($colNumber) . $rowNumber, $col);
            $colNumber++;
        }
    }

    /**
     * Converts an integer into the alphabet base (A-Z).
     *
     * @param int $n This is the number to convert.
     * @return string The converted number.
     * @author Theriault
     * 
     */
    public function num2alpha($n)
    {
        $r = '';
        for ($i = 1; $n >= 0 && $i < 10; $i++) {
            $r = chr(0x41 + ($n % pow(26, $i) / pow(26, $i - 1))) . $r;
            $n -= pow(26, $i);
        }
        return $r;
    }
    /**
     * Converts an alphabetic string into an integer.
     *
     * @param int $n This is the number to convert.
     * @return string The converted number.
     * @author Theriault
     * 
     */
    public function alpha2num($a)
    {
        $r = 0;
        $l = strlen($a);
        for ($i = 0; $i < $l; $i++) {
            $r += pow(26, $i) * (ord($a[$l - $i - 1]) - 0x40);
        }
        return $r - 1;
    }

    public  function writeSpreadsheetRow($row, &$spreadsheet, $rowNumber, &$activeSheet = null)
    {
        $colNumber = 0;
        foreach ($row as $col) {
            if (empty($activeSheet)) {
                $spreadsheet->getActiveSheet()->setCellValue($this->num2alpha($colNumber) . $rowNumber, $col);
            } else {
                $activeSheet->setCellValue($this->num2alpha($colNumber) . $rowNumber, $col);
            }
            $colNumber++;
        }
    }
}
