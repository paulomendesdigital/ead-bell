<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * AccessOrigins component
 */
class AccessOriginsComponent extends Component
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function getAccessOriginIdFromRequest()
    {
        $queryParams = $this->getController()->getRequest()->getQueryParams();

        //Caso não seja especificado, considerará que é do aplicativo
        if (empty($queryParams['access_origin_code'])) {
            $code = 'APP';
        } else {
            $code = $queryParams['access_origin_code'];
        }

        $accessOriginsTable = TableRegistry::getTableLocator()->get('AccessOrigins');

        $accessOrigin = $accessOriginsTable->find()
            ->where(['AccessOrigins.code' => $code])
            ->first();

        if (!empty($accessOrigin)) {
            return $accessOrigin->id;
        } else {
            return null;
        }
    }
}
