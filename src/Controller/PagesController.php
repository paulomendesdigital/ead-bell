<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

use Cake\ORM\TableRegistry;

class PagesController extends AppController
{
    private $webdoors;
    private $products;
    private $categories;
    private $areas;
    private $notices;
    private $testimonials;
    private $posts;

    public function index()
    {
        $notices = $testimonials = $posts = $areas = false;

        $this->webdoors = TableRegistry::getTableLocator()->get('Webdoors');
        $webdoors = $this->webdoors->find();
        
        $this->products = TableRegistry::getTableLocator()->get('Products');
        $products = $this->products->find();

        $this->categories = TableRegistry::getTableLocator()->get('Categories');

        $products = $this->products->find('getAll', ['limit' => Configure::read('Systems.LimitResultPage.lastPosts')]);
        $presencials = $this->products->find('getAll', ['limit' => Configure::read('Systems.LimitResultPage.lastPosts'), 'category_cursos' => $this->categories->getCursosPresencialId()]);

        if (Configure::read('Modules.Areas')) {
            $this->areas = TableRegistry::getTableLocator()->get('Areas');
            $areas = $this->areas->find('getAll', ['show_menu' => true]);
        }

        if (Configure::read('Modules.Notices')) {
            $this->notices = TableRegistry::getTableLocator()->get('Notices');
            $notices = $this->notices->find('getAll', ['limit' => Configure::read('Systems.LimitResultPage.lastNews')]);
        }

        if (Configure::read('Modules.Testimonials')) {
            $this->testimonials = TableRegistry::getTableLocator()->get('Testimonials');
            $testimonials = $this->testimonials->find('getAll', ['limit' => 10]);
        }

        if (Configure::read('Modules.Blog')) {
            $this->posts = TableRegistry::getTableLocator()->get('Posts');
            $posts = $this->posts->find('getAll', ['limit' => Configure::read('Systems.LimitResultPage.lastPosts')]);
        }

        $this->set(compact('products', 'webdoors', 'areas', 'notices', 'testimonials', 'presencials', 'posts'));
    }

    public function twitteriframe(){
        $this->layout = false;
    }
    public function twitter(){
        $this->layout = false;
    }
    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display(...$path)
    {
        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }
}
