<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller\Landing;

use App\Controller\Landing\LandingAppController;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Mailer\Email;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class NewslettersController extends LandingAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $newsletters = $this->paginate($this->Newsletters);

        $this->set(compact('newsletters'));
    }

    /**
     * View method
     *
     * @param string|null $id Newsletter id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $newsletter = $this->Newsletters->get($id, [
            'contain' => []
        ]);

        $this->set('newsletter', $newsletter);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

        $this->layout = false;
        $this->autoRender = false;

        $newsletter = $this->Newsletters->newEntity();
        if ($this->request->is('post')) {

            $this->request->data['message'] = NULL;
            
            $newsletter = $this->Newsletters->patchEntity($newsletter, $this->request->getData());
            if ( $this->Newsletters->save($newsletter) ) {

                $this->response->body(json_encode([ 'status' => 'success', 'message' => 'send', 'data' => $this->request->data ]) ); 
                return $this->response;
            }

            $this->response->body(json_encode([ 'status' => 'error', 'message' => 'save fail', 'data' => $this->request->data ]) ); 
            return $this->response;

        }
        $this->set(compact('newsletter'));
    }

    
    public function __sendMail($data) { 

        $corporate  = Configure::read('Corporate');
        $template   = isset($data['template']) ? $data['template'] : 'default';
        $layout     = isset($data['layout']) ? $data['layout'] : 'default';

        $email = new Email();
        $email->setEmailFormat('html')
            ->setTransport('default')
            ->setFrom([$corporate['EmailFrom'] => $corporate['Name']])
            ->setTo($data['to'])
            ->setSubject($data['subject'])
            ->setViewVars(['data' => $data])
            ->viewBuilder()->setTemplate($template)//use src/Template/Email/html/view_welcome.ctp
            ->setLayout($layout);//src/Template/Layout/Email/html/fancy.ctp for the layout.


        if ( !empty($corporate['EmailBcc']) ) {
            $email->setBcc($corporate['EmailBcc']);
        }
        if (!empty($corporate['EmailCc'])) {
            $email->setCc($corporate['EmailCc']);
        }

        try {
            $email->send($data['content']);
        }catch(Exception $e) {
            throw new Exception("Exception caught with message: " . $e->getMessage().'\n'.$email->smtpError);
        }
        return true;
    } 

}
