<?php

namespace App\Controller\Manager;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Database\Type;
use Cake\I18n\Date;
use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
use Cake\I18n\I18n;
use Cake\I18n\Time;

class ManagerAppController extends Controller
{

    public $components = [
        /*'Acl' => [
      'className' => 'Acl.Acl'
    ]*/];

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        $this->loadComponent('Cookie');

        $this->loadComponent('Auth', [
            /*'authorize' => [
        'Acl.Actions' => ['actionPath' => 'controllers/']
      ],*/
            'authorize' => 'Controller',
            'loginAction' => [
                'plugin' => false,
                'controller' => 'Users',
                'action' => 'login',
                'prefix' => 'manager'
            ],
            'loginRedirect' => [
                'plugin' => false,
                'controller' => 'Pages',
                'action' => 'welcome',
                'prefix' => 'manager'
            ],
            'logoutRedirect' => [
                'plugin' => false,
                'controller' => 'Users',
                'action' => 'login',
                'prefix' => 'manager'
            ],
            'unauthorizedRedirect' => [
                'controller' => 'Users',
                'action' => 'login',
                'prefix' => 'manager'
            ],
            'authError' => 'You are not authorized to access that location.',
            'flash' => [
                'element' => 'error'
            ]
        ]);

        //$this->Auth->allow();

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
    }

    public function beforeRender(\Cake\Event\Event $event)
    {
    }

    public function beforeFilter(\Cake\Event\Event $event)
    {
        $this->viewBuilder()->setLayout('manager');

        $this->__CookieLogin();

        $this->set('User', $this->Auth->user());
        $this->set('Corporate', Configure::read('Corporate'));
        $this->set('Developer', Configure::read('Developer'));

        $this->Auth->setConfig('loginAction', ['controller' => 'Users', 'action' => 'login']);
    }

    public function isAuthorized($user)
    {
        return true;
        /*$Collection = new ComponentRegistry();
    $acl = new AclComponent($Collection);
    $controller = $this->request->getParam('controller');
    $action = $this->request->getParam('action');
    $check = $acl->check($user['id'], "$controller/$action");
    $check2 = $acl->check($user['group_id'], "$controller/$action");
    return ($check || $check2);*/
    }

    public function __CookieLogin()
    {

        //Avatar
        if ($this->Cookie->check('public_url_avatar')) :
            //Existe foto na sessão
            if ($this->Auth->user('public_url_avatar')) :
                //Existe foto no usuário
                if ($this->Auth->user('public_url_avatar') != $this->Cookie->read('public_url_avatar') && $this->Cookie->check('public_keep')) :
                    //A foto é diferente da sessão, atualiza ela
                    $this->Cookie->write('public_url_avatar', $this->Auth->user('public_url_avatar'));
                endif;
            else :
                if ($this->Auth->user('id')) :
                    //Não existe foto no usuário
                    $this->Cookie->delete('public_url_avatar');
                endif;
            endif;
            //Exibe a foto
            $this->set('public_url_avatar', $this->Cookie->read('public_url_avatar'));
        else :
            //Não existe foto na sessão
            if ($this->Auth->user('public_url_avatar') && $this->Cookie->check('public_keep')) :
                //Existe foto no usuário e ele deixou gravar a foto na sessão
                $this->Cookie->write('public_url_avatar', $this->Auth->user('public_url_avatar'));
            endif;
        endif;

        if ($this->Cookie->check('public_login')) :
            //Existe foto na sessão
            if ($this->Auth->user('public_login')) :
                //Existe foto no usuário
                if ($this->Auth->user('public_login') != $this->Cookie->read('public_login') && $this->Cookie->check('public_keep')) :
                    //A foto é diferente da sessão, atualiza ela
                    $this->Cookie->write('public_login', $this->Auth->user('public_login'));
                endif;
            else :
                if ($this->Auth->user('id')) :
                    //Não existe foto no usuário
                    $this->Cookie->delete('public_login');
                endif;
            endif;
            //Exibe a foto
            $this->set('public_login', $this->Cookie->read('public_login'));
        else :
            //Não existe foto na sessão
            if ($this->Auth->user('public_login') && $this->Cookie->check('public_keep')) :
                //Existe foto no usuário e ele deixou gravar a foto na sessão
                $this->Cookie->write('public_login', $this->Auth->user('public_login'));
            endif;
        endif;

        $this->set('public_keep', $this->Cookie->read('public_keep'));
    }

    public function dateFormatBeforeSave($data = false)
    {
        if (!$data) {
            return false;
        }

        $hora = substr($data, 10, 20);
        $data = substr($data, 0, 10);

        $explode = explode("/", $data);
        if (count($explode) > 1) {
            if ($hora) {
                $data = $explode[2] . "-" . $explode[1] . "-" . $explode[0] . $hora . ":00";
            } else {
                $data = $explode[2] . "-" . $explode[1] . "-" . $explode[0];
            }
        }

        return $data;
    }

    public function __ShowValidationErrors($errors, $html = false)
    {
        $err = '';
        foreach ($errors as $key => $value) {
            $err .= "{$key}: ";
            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    $err .= "{$v}\n";
                }
            } else {
                if ($html) {
                    $err .= "<li>{$value}</li>";
                } else {
                    $err .= "{$value}";
                }
            }
        }
        return $err;
    }

    public function dateFormatBeforeFilterFind($data = false, $returnTime = false)
    {
        if (!$data) {
            return false;
        }

        $hora = substr($data, 10, 20);
        $data = substr($data, 0, 10);

        $explode = explode("/", $data);
        if (count($explode) > 1) {
            if ($hora and $returnTime) {
                $data = $explode[2] . "-" . $explode[1] . "-" . $explode[0] . $hora . ":00";
            } else {
                $data = $explode[2] . "-" . $explode[1] . "-" . $explode[0];
            }
        }

        return $data;
    }

    public function __getConditionsForSearch($conditions = [], $model, $likeFields)
    {
        if ($this->request->getQuery()) {
            foreach ($this->request->getQuery() as $key => $value) {
                if (!empty($value)) {
                    if (!in_array($key, ['page', 'sort', 'direction'])) {
                        if (in_array($key, $likeFields)) {
                            if (in_array($key, ['created', 'date', 'start', 'finish'])) {
                                $this->request->data[$key] = $value;
                                $value = $this->dateFormatBeforeFilterFind($value);
                            } else {
                                $this->request->data[$key] = $value;
                            }
                            $conditions["{$model}.{$key} LIKE"] = "%{$value}%";
                        } else {
                            $conditions["{$model}.{$key}"] = $this->request->data[$key] = $value;
                        }
                    }
                }
            }
        }
        return $conditions;
    }

    public function isAdministrator($group_id = false)
    {
        return $group_id == 1;
    }
}
