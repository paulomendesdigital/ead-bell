<?php
namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Http\Exception\NotFoundException;

class LessonsController extends ManagerAppController
{
    private $teachers;
    private $disciplines;
    private $topics;

    public function index()
    {
        $likeFields = ['title'];
        $conditions = ['Lessons.id >='=>'0'];
        $conditions = $this->__getConditionsForSearch($conditions, 'Lessons', $likeFields);

        $this->paginate = [
            'contain' => ['Teachers'],
            'conditions' => $conditions,
            'order' => ['Lessons.title' => 'ASC'],
            'limit' => Configure::read('Systems.ResultPage')
        ];

        $lessons = $this->paginate($this->Lessons);

        die(debug($lessons));

        $title = 'Aulas';
        $this->set(compact('lessons', 'title'));
    }

    public function view($id = null)
    {
        if (!$this->Lessons->exists($id)) {
            $this->Flash->error(__('The {0} has been not found.', 'Aula'));
            return $this->redirect(['action' => 'index']);
        }

        $options = ['contain' => ['Teachers']];

        $lesson = $this->Lessons->get($id, $options);

        die(debug($lesson));

        $this->set(compact('lesson'));
    }

    public function add()
    {
		$this->viewBuilder()->setLayout('ajax');

        $lesson = $this->Lessons->newEntity();

		if ($this->request->is('post')) {
			$this->autoRender = false;

            $lesson = $this->Lessons->patchEntity($lesson, $this->request->getData());

			if ($this->Lessons->save($lesson)) {
                return json_encode(["status" => 'success', "data" => [ "status" => "add", "id" => $lesson->id, "title" => $lesson['title']]]);

			} else {
				return json_encode(['status' => 'error', 'data' => $lesson]);
			}
		}

        $this->teachers = TableRegistry::getTableLocator()->get('Teachers');
		$teachers = $this->teachers->find('getList');

        die(debug($teachers));

		$this->set(compact('teachers'));
    }

    public function edit($id = null)
    {
		$this->viewBuilder()->setLayout('ajax');

		if (!$this->Lessons->exists($id)) {
			throw new NotFoundException(__('Invalid {0}', 'Aula'));
		}

        $lesson = $this->Lessons->get($id, ['contain' => ['Teachers']]);

		if ($this->request->is(['patch', 'post', 'put'])) {

			$this->autoRender = false;
            $lesson = $this->Lessons->patchEntity($lesson, $this->request->getData());

			if ($this->Lessons->save($lesson)) {
				return json_encode(['status' => 'success', 'data' => $lesson]);
				
			} else {
				return json_encode(['status' => 'error', 'data' => $lesson]);
			}
		}

        $this->teachers = TableRegistry::getTableLocator()->get('Teachers');
        $teachers = $this->teachers->find('list');

        debug($lesson);
        debug($teachers);
        die;

        $this->set(compact('lesson', 'teachers'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete','get']);

		$this->autoRender = false;
		$this->viewBuilder()->setLayout('ajax');

		if (!$this->Lessons->exists($id)) {
            return json_encode(["status" => 'error', "data" => $this->request->getData()]);
		}

		if (!$this->Lessons->delete($id)) {
            return json_encode(["status" => 'error', "data" => __('The {0} has been deleted.', 'Aula')]);
		}

        return json_encode(["status" => 'success', "data" => __('The {0} could not be deleted. Please, try again.', 'Aula')]);
    }

    public function removeFile($id, $field)
    {
        $lesson = $this->Lessons->get($id);

        if (empty($lesson)) {
            throw new NotFoundException(__('Invalid lesson'));
        }

        $lesson->$field = '';

        $this->Lessons->saveField($lesson);
        return $this->redirect($this->referer());
    }

    public function build()
    {
        $this->disciplines = TableRegistry::getTableLocator()->get('Disciplines');
        $this->topics = TableRegistry::getTableLocator()->get('Topics');

        $disciplines = $this->disciplines->find('getList');
        $topics = $this->topics->find('getList');
        $lessons = $this->Lessons->find('getList');

        $allDisciplines = $this->disciplines->find()
                                        ->contain([
                                            'Topics' => function($q) {
                                                return $q->contain(['Lessons']);
                                            }
                                        ]);

        // debug($disciplines->toArray());
        // debug($topics);
        // debug($lessons->toArray());
        // debug($allDisciplines->toArray());
        die;
        
        $this->set(compact('disciplines', 'topics', 'lessons', 'allDisciplines'));
    }

	public function ajaxGetListByTopic($topicId = null)
	{

		$this->autoRender = false;
		$this->viewBuilder()->setLayout('ajax');

        $this->topics = TableRegistry::getTableLocator()->get('Topics');

        $topics = $this->topics->get($topicId);

		if (empty($topics)) {
			throw new NotFoundException(__('Invalid topic'));
		}

		$topic = $this->topics->get($topicId, [
            'contain' => ['Lessons' => function($q) { return $q->select(['id', 'title']); }]
        ]);
		$options = "<option value=''>Selecione a aula</option>";

        foreach ($topic['lessons'] as $lesson) {
            $options .= "<option value='{$lesson['id']}'>{$lesson['title']}</option>";            
        }

        die(debug($options));

        return $options;
    }
}
