<?php
namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * Webdoors Controller
 *
 * @property \App\Model\Table\WebdoorsTable $Webdoors
 *
 * @method \App\Model\Entity\Webdoor[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class WebdoorsController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $likeFields = ['name', 'title'];
        $conditions['Webdoors.id >'] = 0;
        $conditions = $this->__getConditionsForSearch($conditions, 'Webdoors', $likeFields);

        $this->paginate = [
            'contain' => ['Companies'],
            'conditions'=>[
                $conditions
            ],
            'order'=>['Webdoors.id'=>'DESC']
        ];
        
        $webdoors = $this->paginate($this->Webdoors);
        $companies = $this->Webdoors->Companies->find('list');

        $this->set(compact('webdoors','companies'));
    }

    /**
     * View method
     *
     * @param string|null $id Webdoor id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $webdoor = $this->Webdoors->get($id, [
            'contain' => ['Companies']
        ]);

        $this->set('webdoor', $webdoor);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $image_background_size = $this->Webdoors::IMAGE_BACKGROUND_SIZE;
        $image_front_size = $this->Webdoors::IMAGE_FRONT_SIZE;
        $image_mobile_size = $this->Webdoors::IMAGE_MOBILE_SIZE;
        
        $webdoor = $this->Webdoors->newEntity();
        if ($this->request->is('post')) {
            $webdoor = $this->Webdoors->patchEntity($webdoor, $this->request->getData());
            if ($this->Webdoors->save($webdoor)) {
                $this->Flash->success(__('The webdoor has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The webdoor could not be saved. Please, try again.'));
        }
        $companies = $this->Webdoors->Companies->find('list', ['limit' => 200]);
        $this->set(compact('webdoor', 'companies','image_background_size','image_front_size','image_mobile_size'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Webdoor id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $image_background_size = $this->Webdoors::IMAGE_BACKGROUND_SIZE;
        $image_front_size = $this->Webdoors::IMAGE_FRONT_SIZE;
        $image_mobile_size = $this->Webdoors::IMAGE_MOBILE_SIZE;

        $webdoor = $this->Webdoors->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $webdoor = $this->Webdoors->patchEntity($webdoor, $this->request->getData());
            //$webdoor = $this->Webdoors->patchEntity($webdoor, $this->Webdoors->__patchData($this->request->getData()) );
            if ( $this->Webdoors->save($webdoor) ) {

                $this->Webdoors->patchRemoveImages( $webdoor, $this->request->getData() );

                $this->Flash->success(__('The {0} has been saved.', 'webdoor'));

                if( isset($this->request->getData()['refer']) ){
                    return $this->redirect( $this->referer() );
                }
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'webdoor'));
        }
        else{

            if( !empty($webdoor->start) ){
                $webdoor->start = $webdoor->start->format('d/m/Y H:i');
            }
            if( !empty($webdoor->finish) ){
                $webdoor->finish = $webdoor->finish->format('d/m/Y H:i');
            }
        }
        
        $companies = $this->Webdoors->Companies->find('list', ['limit' => 200]);
        $this->set(compact('webdoor', 'companies','image_background_size','image_front_size','image_mobile_size'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Webdoor id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        if( !$id ){
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', $this->name));
            return $this->redirect( $this->referer() );
        }

        $this->request->allowMethod(['post', 'delete','get']);
        $webdoor = $this->Webdoors->get($id);
        if ($this->Webdoors->delete($webdoor)) {
            $this->Flash->success(__('The {0} has been deleted.', $this->name));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', $this->name));
        }

        return $this->redirect( $this->referer() );
    }
}
