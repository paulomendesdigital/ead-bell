<?php
namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

use Cake\Core\Configure;

class AreasController extends ManagerAppController
{
    public function index()
    {
        $likeFields = ['name', 'slug'];
        $conditions = ['Areas.id >='=>'0'];
        $conditions = $this->__getConditionsForSearch($conditions, 'Areas', $likeFields);

        $this->paginate = [
            'conditions' => $conditions,
            'order' => ['Areas.id' => 'ASC'],
            'limit' => Configure::read('Systems.ResultPage')
        ];

        $areas = $this->paginate($this->Areas);

        die(debug($areas));

        $title = 'Áreas';
        $this->set(compact('areas', 'title'));
    }

    public function view($id = null)
    {
        if (!$this->Areas->exists($id)) {
            $this->Flash->error(__('The {0} has been not found.', 'Área'));
            return $this->redirect(['action' => 'index']);
        }

        $area = $this->Areas->get($id);

        die(debug($area));

        $this->set(compact('area'));
    }

    public function add()
    {
        $area = $this->Areas->newEntity();

        if ($this->request->is('post')) {
            $area = $this->Areas->patchEntity($area, $this->request->getData());

            if ($this->Areas->save($area)) {
                $this->Flash->success(__('The {0} has been saved.', 'Área'));
                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Área'));
        }

        $this->set(compact('area'));
    }

    public function edit($id = null)
    {
        if (!$this->Areas->exists($id)) {
            $this->Flash->error(__('The {0} has been not found.', 'Área'));
            return $this->redirect(['action' => 'index']);
        }

        $area = $this->Areas->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $area = $this->Areas->patchEntity($area, $this->request->getData());

            if ($this->Page->save($area)) {
                $this->Flash->success(__('The {0} has been saved.', 'Área'));

                if (isset($area['aplicar'])) {
                    return $this->redirect($this->referer());
                }

                return $this->redirect(['action' => 'edit', $id]);
            }

            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Área'));
        }

        die(debug($area));
        $this->set(compact('area'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete','get']);

        $area = $this->Areas->get($id);

        if ($this->Areas->delete($area)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Área'));

        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Área'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
