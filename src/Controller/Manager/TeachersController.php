<?php
namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

class TeachersController extends ManagerAppController
{
    public function index()
    {
        $likeFields = ['name', 'email'];
        $conditions = ['Teachers.id >='=>'0'];
        $conditions = $this->__getConditionsForSearch($conditions, 'Teachers', $likeFields);

        $this->paginate = [
            'conditions' => $conditions,
            'order' => ['Teachers.id' => 'ASC'],
            'limit' => Configure::read('Systems.ResultPage')
        ];

        $teachers = $this->paginate($this->Teachers);

        die(debug($teachers));

        $title = 'Professores';
        $this->set(compact('teachers', 'title'));
    }

    public function view($id = null)
    {
        if (!$this->Teachers->exists($id)) {
            $this->Flash->error(__('The {0} has been not found.', 'Professor'));
            return $this->redirect(['action' => 'index']);
        }

        $teachers = $this->Teachers->get($id);

        die(debug($teachers));

        $this->set(compact('teachers'));
    }

    public function add()
    {
        $teacher = $this->Teachers->newEntity();

        if ($this->request->is('post')) {
            $teacher = $this->Teachers->patchEntity($teacher, $this->request->getData());

            if ($this->Teachers->save($teacher)) {
                $this->Flash->success(__('The {0} has been saved.', 'Professor'));
                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Professor'));
        }

        $this->set(compact('teacher'));
    }

    public function edit($id = null)
    {
        if (!$this->Teachers->exists($id)) {
            $this->Flash->error(__('The {0} has been not found.', 'Professor'));
            return $this->redirect(['action' => 'index']);
        }

        $teacher = $this->Teachers->get($id, ['contain' => ['Users']]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $teacher = $this->Teachers->patchEntity($teacher, $this->request->getData());

            if ($this->Page->save($teacher)) {
                $this->Flash->success(__('The {0} has been saved.', 'Professor'));

                if (isset($teacher['aplicar'])) {
                    return $this->redirect($this->referer());
                }

                return $this->redirect(['action' => 'edit', $id]);
            }

            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Professor'));
        }

        die(debug($teacher));
        $this->set(compact('teacher'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete','get']);

        $teacher = $this->Teachers->get($id);

        if ($this->Teachers->delete($teacher)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Professor'));

        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Professor'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
