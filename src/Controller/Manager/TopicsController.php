<?php
namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Http\Exception\NotFoundException;

class TopicsController extends ManagerAppController
{
    private $lessons;
    private $disciplines;

    public function index()
    {
        $likeFields = ['name', 'label'];
        $conditions = ['Topics.id >='=>'0'];
        $conditions = $this->__getConditionsForSearch($conditions, 'Topics', $likeFields);

        $this->paginate = [
            'conditions' => $conditions,
            'order' => ['Topics.name' => 'ASC'],
            'limit' => Configure::read('Systems.ResultPage')
        ];

        $topics = $this->paginate($this->Topics);

        die(debug($topics));

        $title = 'Tópicos';
        $this->set(compact('topics', 'title'));
    }

    public function view($id = null)
    {
        if (!$this->Topics->exists($id)) {
			throw new NotFoundException(__('Invalid {0}', 'Tópico'));
        }

        $topic = $this->Topics->get($id);

        die(debug($topic));

        $this->set(compact('topic'));
    }

    public function add()
    {
		$this->viewBuilder()->setLayout('ajax');

        $topic = $this->Topics->newEntity();

		if ($this->request->is('post')) {
			$this->autoRender = false;

            $topic = $this->Topics->patchEntity($topic, $this->request->getData());

			if ($this->Topics->save($topic)) {
                return json_encode(["status" => 'success', "data" => [ "status" => "add", "id" => $topic->id, "title" => $topic['name']]]);

			} else {
				return json_encode(['status' => 'error', 'data' => $topic]);
			}
		}

        $this->lessons = TableRegistry::getTableLocator()->get('Lessons');
		$lessons = $this->lessons->find('getList', ['wich_description' => 1]);

        die(debug($lessons));

		$this->set(compact('lessons'));
    }

    public function edit($id = null)
    {
		$this->viewBuilder()->setLayout('ajax');

		if (!$this->Topics->exists($id)) {
			throw new NotFoundException(__('Invalid {0}', 'Tópico'));
		}

        $topic = $this->Topics->get($id, ['contain' => ['Lessons']]);

		if ($this->request->is(['patch', 'post', 'put'])) {

			$this->autoRender = false;
            $topic = $this->Topics->patchEntity($topic, $this->request->getData());

			if ($this->Topics->save($topic)) {
				return json_encode(['status' => 'success', 'data' => $topic]);
				
			} else {
				return json_encode(['status' => 'error', 'data' => $topic]);
			}
		}

        $this->lessons = TableRegistry::getTableLocator()->get('Lessons');
        $lessons = $this->lessons->find('getList', ['wich_description' => 1]);

        debug($topic);
        debug($lessons);
        die;

        $this->set(compact('topic', 'lessons'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete','get']);

		$this->autoRender = false;
		$this->viewBuilder()->setLayout('ajax');

		if (!$this->Topics->exists($id)) {
            return json_encode(["status" => 'error', "data" => $this->request->getData()]);
		}

        $topic = $this->Topics->get($id);

		if (!$this->Topics->delete($id)) {
            return json_encode(["status" => 'error', "data" => $topic]);
		}

        return json_encode(["status" => 'success', "data" => $topic]);
    }

	public function ajaxGetListByDiscipline($disciplineId = null)
	{
        $this->autoRender = false;
		$this->viewBuilder()->setLayout('ajax');
		
        $this->disciplines = TableRegistry::getTableLocator()->get('Disciplines');

		if (!$this->disciplines->exists($disciplineId)) {
			throw new NotFoundException(__('Invalid {0}', 'Tópico'));
		}

		$discipline = $this->disciplines->get($disciplineId, [
            'contain' => ['Topics' => function($q) { return $q->select(['id', 'name']); }]
        ]);

		$options = "<option value=''>Selecione o Tópico</option>";

        foreach ($discipline->topics as $topic) {
            $options .= "<option value='{$topic['id']}'>{$topic['name']}</option>";            
		}

        die(debug($options));

        return $options;
    }
}
