<?php
namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Http\Exception\NotFoundException;

class DisciplinesController extends ManagerAppController
{
    private $topics;

    public function index()
    {
        $likeFields = ['name', 'label'];
        $conditions = ['Disciplines.id >='=>'0'];
        $conditions = $this->__getConditionsForSearch($conditions, 'Disciplines', $likeFields);

        $this->paginate = [
            'conditions' => $conditions,
            'order' => ['Disciplines.name' => 'ASC'],
            'limit' => Configure::read('Systems.ResultPage')
        ];

        $disciplines = $this->paginate($this->Disciplines);

        die(debug($disciplines));

        $title = 'Disciplina';
        $this->set(compact('disciplines', 'title'));
    }

    public function view($id = null)
    {
        if (!$this->Disciplines->exists($id)) {
			throw new NotFoundException(__('Invalid {0}', 'Disciplina'));
        }

        $discipline = $this->Disciplines->get($id);

        die(debug($discipline));

        $this->set(compact('discipline'));
    }

    public function add()
    {
		$this->viewBuilder()->setLayout('ajax');

        $discipline = $this->Disciplines->newEntity();

		if ($this->request->is('post')) {
			$this->autoRender = false;

            $discipline = $this->Disciplines->patchEntity($discipline, $this->request->getData());

			if ($this->Disciplines->save($discipline)) {
                return json_encode(["status" => 'success', "data" => [ "status" => "add", "id" => $discipline->id, "title" => $discipline['name']]]);

			} else {
				return json_encode(['status' => 'error', 'data' => $discipline]);
			}
		}

        $this->topics = TableRegistry::getTableLocator()->get('Topics');
		$topics = $this->topics->find('getList');

        die(debug($topics));

		$this->set(compact('topics'));
    }

    public function edit($id = null)
    {
		$this->viewBuilder()->setLayout('ajax');

		if (!$this->Disciplines->exists($id)) {
			throw new NotFoundException(__('Invalid {0}', 'Disciplina'));
		}

        $discipline = $this->Disciplines->get($id);

		if ($this->request->is(['patch', 'post', 'put'])) {

			$this->autoRender = false;
            $discipline = $this->Disciplines->patchEntity($discipline, $this->request->getData());

			if ($this->Disciplines->save($discipline)) {
				return json_encode(['status' => 'success', 'data' => $discipline]);
				
			} else {
				return json_encode(['status' => 'error', 'data' => $discipline]);
			}
		}

        $this->topics = TableRegistry::getTableLocator()->get('Topics');
        $topics = $this->topics->find('getList');

        debug($discipline);
        debug($topics);
        die;

        $this->set(compact('discipline', 'topics'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete','get']);

		$this->autoRender = false;
		$this->viewBuilder()->setLayout('ajax');

		if (!$this->Disciplines->exists($id)) {
            return json_encode(["status" => 'error', "data" => $this->request->getData()]);
		}

        $discipline = $this->Disciplines->get($id);

		if (!$this->Disciplines->delete($id)) {
            return json_encode(["status" => 'error', "data" => $discipline]);
		}

        return json_encode(["status" => 'success', "data" => $discipline]);
    }

    public function getTopics($id)
    {
		$this->autoRender = false;
		$this->viewBuilder()->setLayout('ajax');

        if (!$this->Disciplines->exists($id)) {
            throw new NotFoundException(__('Invalid {0}', 'Disciplina'));
        }

        $discipline = $this->Disciplines->get($id, [
            'contain' => ['Topics' => function($q) { return $q->select(['id', 'name']); }],
            'fields' => ['id']
        ]);

        $topicCheckbox = "";

        foreach ($discipline->topics as $key => $topic) {
            $topicCheckbox .= "<label class='checkbox'>";
                $topicCheckbox .= "<input type='hidden' name='data[Discipline][Discipline][{$key}][discipline_id]' value='{$discipline['Discipline']['id']}'>";
                $topicCheckbox .= "<input type='checkbox' name='data[Discipline][Discipline][{$key}][topic_id]' value='{$topic['id']}'>";
                $topicCheckbox .= $topic['name'];
            $topicCheckbox .= "</label>";
        }

        die(debug($topicCheckbox));
    }
}
