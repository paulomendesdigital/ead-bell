<?php
namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

use Cake\Core\Configure;

class CategoriesController extends ManagerAppController
{
    public function index()
    {
        $likeFields = ['name', 'email'];
        $conditions = ['Categories.id >='=>'0'];
        $conditions = $this->__getConditionsForSearch($conditions, 'Categories', $likeFields);

        $this->paginate = [
            'conditions' => $conditions,
            'order' => ['Categories.id' => 'ASC'],
            'limit' => Configure::read('Systems.ResultPage')
        ];

        $categories = $this->paginate($this->Categories);

        die(debug($categories));

        $title = 'Categorias';
        $this->set(compact('categories', 'title'));
    }

    public function view($id = null)
    {
        if (!$this->Categories->exists($id)) {
            $this->Flash->error(__('The {0} has been not found.', 'Categoria'));
            return $this->redirect(['action' => 'index']);
        }

        $category = $this->Categories->get($id);

        die(debug($category));

        $this->set(compact('category'));
    }

    public function add()
    {
        $category = $this->Categories->newEntity();

        if ($this->request->is('post')) {
            $category = $this->Categories->patchEntity($category, $this->request->getData());

            if ($this->Categories->save($category)) {
                $this->Flash->success(__('The {0} has been saved.', 'Categoria'));
                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Categoria'));
        }

        $this->set(compact('category'));
    }

    public function edit($id = null)
    {
        if (!$this->Categories->exists($id)) {
            $this->Flash->error(__('The {0} has been not found.', 'Categoria'));
            return $this->redirect(['action' => 'index']);
        }

        $category = $this->Categories->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $category = $this->Categories->patchEntity($category, $this->request->getData());

            if ($this->Page->save($category)) {
                $this->Flash->success(__('The {0} has been saved.', 'Categoria'));

                if (isset($category['aplicar'])) {
                    return $this->redirect($this->referer());
                }

                return $this->redirect(['action' => 'edit', $id]);
            }

            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Categoria'));
        }

        die(debug($category));
        $this->set(compact('category'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete','get']);

        $category = $this->Categories->get($id);

        if ($this->Categories->delete($category)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Categoria'));

        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Categoria'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
