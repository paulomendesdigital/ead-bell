<?php
namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

use Cake\ORM\TableRegistry;

class PagesController extends ManagerAppController
{
    private $clients;
    private $orders;

    public function welcome()
    {
        if ($this->isAdministrator($this->Auth->user('group_id'))) {
            $today = date('Y-m-d');

            $this->orders = TableRegistry::getTableLocator()->get('Orders');
            $valueOrders = $this->orders->find('getOrderByDate', ['date' => $today]);
            $monthValueOrders = $this->orders->find('getOrdersLastDays', ['last_days' => 180]);

            $this->clients = TableRegistry::getTableLocator()->get('Clients');
            $values = $this->clients->find('getRegisterByDate', ['date' => $today]);
            $clientsGenders = $this->clients->find('getRegisterPerGender');
            $clientsAges = $this->clients->find(('getRegisterPerAges'));
            $clientsStates = $this->clients->find(('getRegisterPerStates'));

            // debug($today);
            // debug($valueOrders);
            // debug($monthValueOrders);
            // debug($values);
            // debug($clientsGenders);
            // debug($clientsAges);
            // debug($clientsStates);
            // die;

            $this->set(compact('today', 'valueOrders', 'monthValueOrders', 'values', 'clientsGenders', 'clientsAges', 'clientsStates'));
        }
    }

    public function index()
    {
        return $this->redirect(['controller' => 'pages', 'action' => 'welcome']);
    }

    public function edit($id = null)
    {
        if (!$this->Pages->exists($id)) {
            $this->Flash->error('Página não encontrada');
            return $this->redirect(['action' => 'index']);
        }

        $page = $this->Pages->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $page = $this->Pages->patchEntity($page, $this->request->getData());

            if ($this->Pages->save($page)) {
                $this->Flash->success('A página foi salva');

                if (isset($page['aplicar'])) {
                    return $this->redirect($this->referer());
                }

                return $this->redirect(['action' => 'edit', $id]);

            }

            $this->Flash->error(__('A página não foi salva. Por favor, tente novamente.'));
        }

        die(debug($page));

        $this->set(compact('page'));
    }
}
