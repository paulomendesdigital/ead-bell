<?php
namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * MusicalRequests Controller
 *
 * @property \App\Model\Table\MusicalRequestsTable $MusicalRequests
 *
 * @method \App\Model\Entity\MusicalRequest[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MusicalRequestsController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {   
        $likeFields = ['name', 'email', 'artist','music'];
        $conditions['MusicalRequests.id >'] = 0;
        $conditions = $this->__getConditionsForSearch($conditions, 'MusicalRequests', $likeFields);

        $this->paginate = [
            'contain' => [],
            'conditions'=>[
                $conditions
            ],
            'order'=>['MusicalRequests.id'=>'DESC']
        ];

        $musicalRequests = $this->paginate($this->MusicalRequests);
        $companies = $this->MusicalRequests->Companies->find('list');

        $this->set(compact('musicalRequests','companies'));
    }

    /**
     * View method
     *
     * @param string|null $id Musical Request id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $musicalRequest = $this->MusicalRequests->get($id, [
            'contain' => []
        ]);

        $this->set('musicalRequest', $musicalRequest);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $musicalRequest = $this->MusicalRequests->newEntity();
        if ($this->request->is('post')) {
            $musicalRequest = $this->MusicalRequests->patchEntity($musicalRequest, $this->request->getData());
            if ($this->MusicalRequests->save($musicalRequest)) {
                $this->Flash->success(__('The musical request has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The musical request could not be saved. Please, try again.'));
        }
        $this->set(compact('musicalRequest'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Musical Request id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $musicalRequest = $this->MusicalRequests->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $musicalRequest = $this->MusicalRequests->patchEntity($musicalRequest, $this->request->getData());
            if ($this->MusicalRequests->save($musicalRequest)) {
                $this->Flash->success(__('The musical request has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The musical request could not be saved. Please, try again.'));
        }
        $this->set(compact('musicalRequest'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Musical Request id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete', 'get']);
        $musicalRequest = $this->MusicalRequests->get($id);
        if ($this->MusicalRequests->delete($musicalRequest)) {
            $this->Flash->success(__('The {0} has been deleted.','Pedido Musical'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.','Pedido Musical'));
        }

        return $this->redirect( $this->referer() );
    }
}
