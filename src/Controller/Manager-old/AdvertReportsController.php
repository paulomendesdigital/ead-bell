<?php
declare(strict_types=1);

namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * AdvertReports Controller
 *
 *
 * @method \App\Model\Entity\AdvertReport[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdvertReportsController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->loadComponent('GrupoGrowPaginate.GrupoGrowPaginate');
        $query = $this->AdvertReports->find();
        $advertReports = $this->GrupoGrowPaginate->paginate($query);

        $this->set(compact('advertReports'));
    }

    /**
     * View method
     *
     * @param string|null $id Advert Report id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $advertReport = $this->AdvertReports->get($id, [
            'contain' => [],
        ]);

        $this->set('advertReport', $advertReport);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $advertReport = $this->AdvertReports->newEntity();
        if ($this->request->is('post')) {
            $advertReport = $this->AdvertReports->patchEntity($advertReport, $this->request->getData());
            if ($this->AdvertReports->save($advertReport)) {
                $this->Flash->success(__('The advert report has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The advert report could not be saved. Please, try again.'));
        }
        $this->set(compact('advertReport'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Advert Report id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $advertReport = $this->AdvertReports->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $advertReport = $this->AdvertReports->patchEntity($advertReport, $this->request->getData());
            if ($this->AdvertReports->save($advertReport)) {
                $this->Flash->success(__('The advert report has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The advert report could not be saved. Please, try again.'));
        }
        $this->set(compact('advertReport'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Advert Report id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        //Individual delete
        if(!empty($id)){
            $advertReport = $this->AdvertReports->get($id);
            if ($this->AdvertReports->delete($advertReport)) {
                $this->Flash->success(__('The advert report has been deleted.'));
            } else {
                $this->Flash->error(__('The advert report could not be deleted. Please, try again.'));
            }
        }else{ //Batch delete
            $list = $this->AdvertReports->find('list');
            $entities = $this->AdvertReports->patchEntities($list, $this->request->getData()['AdvertReports']);
            if($this->AdvertReports->deleteMany($entities)){
                $this->Flash->success(__('The advertReports has been deleted.'));
            }else{
                $this->Flash->error(__('The advertReports could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(['action' => 'index']);
    }
}
