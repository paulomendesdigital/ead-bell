<?php
namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * ContentCategories Controller
 *
 * @property \App\Model\Table\ContentCategoriesTable $ContentCategories
 *
 * @method \App\Model\Entity\ContentCategory[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContentCategoriesController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $likeFields = ['name', 'code'];
        $conditions = ['ContentCategories.status >='=>'0'];
        $conditions = $this->__getConditionsForSearch($conditions, 'ContentCategories', $likeFields);

        $this->paginate = [
            //'contain' => ['Companies'],
            'conditions' => $conditions,
            'order'=>['ContentCategories.id'=>'DESC']
        ];
        $contentCategories = $this->paginate($this->ContentCategories);
        $companies = [];//$this->ContentCategories->Companies->find('list');

        $this->set(compact('contentCategories','companies'));
    }

    /**
     * View method
     *
     * @param string|null $id Content Category id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $contentCategory = $this->ContentCategories->get($id, [
            'contain' => [
                'Contents',
                //'Companies'
            ]
        ]);

        $this->set('contentCategory', $contentCategory);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $contentCategory = $this->ContentCategories->newEntity();
        if ($this->request->is('post')) {
            
            $contentCategory = $this->ContentCategories->patchEntity($contentCategory, $this->request->getData());
            $contentCategory->code = $contentCategory->name;

            if ($this->ContentCategories->save($contentCategory)) {
                $this->Flash->success(__('The {0} has been saved.','Categoria'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.','Categoria'));
        }
        $companies = [];//$this->ContentCategories->Companies->find('list');
        $this->set(compact('contentCategory','companies'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Content Category id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $contentCategory = $this->ContentCategories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contentCategory = $this->ContentCategories->patchEntity($contentCategory, $this->request->getData());

            if ($this->ContentCategories->save($contentCategory)) {
                $this->Flash->success(__('The {0} has been saved.','Categoria'));
                if( isset($this->request->getData()['refer']) ){
                    return $this->redirect( $this->referer() );
                }
                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('The {0} could not be saved. Please, try again.','Categoria'));
        }
        $companies = [];//$this->ContentCategories->Companies->find('list');
        $this->set(compact('contentCategory','companies'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Content Category id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete','get']);
        $contentCategory = $this->ContentCategories->get($id);
        if ($this->ContentCategories->delete($contentCategory)) {
            $this->Flash->success(__('The {0} has been deleted.','Categoria'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.','Categoria'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function ajaxGetList($company_id){

        $contentCategories = $this->ContentCategories->find('list', ['conditions'=>['ContentCategories.company_id'=>$company_id],'order'=>['name'=>'asc'],'limit' => 200]);
        return $this->response
        ->withType('application/json')
        ->withStringBody(json_encode([
          'data' => $contentCategories
        ]));
    }
}
