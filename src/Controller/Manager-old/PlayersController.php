<?php
namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * Players Controller
 *
 * @property \App\Model\Table\PlayersTable $Players
 *
 * @method \App\Model\Entity\Player[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PlayersController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Companies'],
            'order'=>['Players.id'=>'DESC']
        ];
        $players = $this->paginate($this->Players);

        $this->set(compact('players'));
    }

    /**
     * View method
     *
     * @param string|null $id Player id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $player = $this->Players->get($id, [
            'contain' => ['Companies']
        ]);

        $this->set('player', $player);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $player = $this->Players->newEntity();
        if ($this->request->is('post')) {
            $player = $this->Players->patchEntity($player, $this->request->getData());
            if ($this->Players->save($player)) {
                $this->Flash->success(__('The {0} has been saved.','Player'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.','Player'));
        }
        $companies = $this->Players->Companies->find('list');
        $this->set(compact('player','companies'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Player id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $player = $this->Players->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $player = $this->Players->patchEntity($player, $this->request->getData());
            if ($this->Players->save($player)) {
                $this->Flash->success(__('The {0} has been saved.','Player'));

                if( isset($this->request->getData()['refer']) ){
                    return $this->redirect( $this->referer() );
                }
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.','Player'));
        }

        if( !empty($player->start) ){
            $player->start = $player->start->format('d/m/Y');
        }
        if( !empty($player->finish) ){
            $player->finish = $player->finish->format('d/m/Y');
        }

        $companies = $this->Players->Companies->find('list');
        $this->set(compact('player','companies'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Player id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete','get']);
        $player = $this->Players->get($id);
        if ($this->Players->delete($player)) {
            $this->Flash->success(__('The {0} has been deleted.','Player'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.','Player'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
