<?php

namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $likeFields = ['name', 'username', 'email', 'cpf'];
        $conditions['Users.group_id <>'] = $this->Users->Groups->getClienteId();
        $conditions = $this->__getConditionsForSearch($conditions, 'Users', $likeFields);

        $this->paginate = [
            'contain' => ['Groups', 'Companies'],
            'conditions' => [
                $conditions
            ],
            'order' => ['Users.id' => 'DESC']
        ];

        $users = $this->paginate($this->Users);
        $title = 'Admnistradores';
        $this->set(compact('users', 'title'));
    }

    public function listeners()
    {
        $likeFields = ['name', 'username', 'email', 'cpf'];
        $conditions['Users.group_id'] = $this->Users->Groups->getClienteId();
        $conditions = $this->__getConditionsForSearch($conditions, 'Users', $likeFields);

        $this->paginate = [
            'contain' => ['Groups', 'Companies'],
            'conditions' => [
                $conditions
            ],
            'order' => ['Users.id' => 'DESC']
        ];

        $users = $this->paginate($this->Users);
        $clientGroupId = $this->Users->Groups->getClienteId();
        $companies = $this->Users->Companies->find('list');
        $title = 'Ouvintes';
        $this->set(compact('users', 'clientGroupId', 'companies', 'title'));

        //renderizando a index como view
        $this->render('index');
    }


    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Groups']
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($group_id = null)
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user_data = $this->request->getData();
            if (isset($user_data['birth']) and !empty($user_data['birth'])) {
                $user_data['birth'] = $this->dateFormatBeforeSave($user_data['birth']);
            }
            $user = $this->Users->patchEntity($user, $user_data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The {0} has been saved.', 'Usuário'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Usuário'));
        }

        if ($group_id == $this->Users->Groups->getClienteId()) {
            $groups = $this->Users->Groups->find('list', [
                'conditions' => [
                    'id' => $this->Users->Groups->getClienteId()
                ],
                'limit' => 200
            ]);
            $this->set('clientGroupId', $this->Users->Groups->getClienteId());
            $this->set('title', 'Ouvinte');
        } else {
            //só admisnutradores
            $groups = $this->Users->Groups->find('list', [
                'conditions' => [
                    'id <>' => $this->Users->Groups->getClienteId()
                ],
                'limit' => 200
            ]);
            $this->set('title', 'Administrador');
        }

        $companies = $this->Users->Companies->find('list');
        $schoolings = $this->Users->getListSchooolings();

        $birth = NULL;

        if (is_object($user->birth)) {
            $birth = $user->birth->format('d/m/Y');
        } elseif ($user->birth) {
            $birth = $user->birth;
        } else {
            $birth = NULL;
        }

        $this->set(compact('user', 'groups', 'group_id', 'schoolings', 'companies', 'birth'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $user_data = $this->request->getData();
            if (isset($user_data['birth']) and !empty($user_data['birth'])) {
                $user_data['birth'] = $this->dateFormatBeforeSave($user_data['birth']);
            }
            $user = $this->Users->patchEntity($user, $user_data);

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The {0} has been saved.', 'Usuário'));
                if (isset($this->request->getData()['refer'])) {
                    return $this->redirect($this->referer());
                }
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Usuário'));
        }

        if ($user->group_id == $this->Users->Groups->getClienteId()) {
            $groups = $this->Users->Groups->find('list', [
                'conditions' => [
                    'id' => $this->Users->Groups->getClienteId()
                ],
                'limit' => 200
            ]);
            $this->set('clientGroupId', $this->Users->Groups->getClienteId());
            $this->set('title', 'Ouvinte');
        } else {
            //só admisnitradores
            $groups = $this->Users->Groups->find('list', [
                'conditions' => [
                    'id <>' => $this->Users->Groups->getClienteId()
                ],
                'limit' => 200
            ]);
            $this->set('title', 'Administrador');
        }

        $birth = NULL;

        if (is_object($user->birth)) {
            $birth = $user->birth->format('d/m/Y');
        } elseif ($user->birth) {
            $birth = $user->birth;
        } else {
            $birth = NULL;
        }

        $group_id = $user->group_id;
        $companies = $this->Users->Companies->find('list');
        $schoolings = $this->Users->getListSchooolings();

        $this->set(compact('user', 'groups', 'group_id', 'birth', 'schoolings', 'companies'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {

        $this->request->allowMethod(['post', 'delete', 'get']);
        $user = $this->Users->findById($id)->first();
        if (empty($user)) {
            $this->Flash->error(__('Usuário não encontrado!', 'Usuário'));
            return $this->redirect($this->referer());
        }

        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Usuário'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Usuário'));
        }

        return $this->redirect($this->referer());
    }

    public function login()
    {
        $this->viewBuilder()->setTheme('GrupoGrowManager');
        $this->viewBuilder()->setLayout('GrupoGrowManager.login');

        if ($this->request->is('post')) {
            $password = $this->request->getData('password');
            $hasher = new DefaultPasswordHasher();
            $user = $this->Users->find()
                ->where(['Users.username' => $this->request->getData('username')])
                ->first();

            if (!empty($user) && !$hasher->check($password, $user->password)) {
                $user = null;
            }

            if (isset($user['group_id']) && $user['group_id'] == 1) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Login e/ou senhas incorretos.'));
        }
    }

    public function logout()
    {
        $this->Flash->success(__('Good-Bye'));
        $this->redirect($this->Auth->logout());
    }

    public function getAll()
    {

        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $name = $this->request->query['term'];
            $results = $this->Users->find('all', [
                'recursive' => -1,
                'conditions' => ['OR' => [
                    'name LIKE' => $name . '%',
                    'cpf LIKE' => $name . '%',
                ]]
            ]);
            $resultsArr = [];
            foreach ($results as $result) {
                $resultsArr[] = [
                    'label' => "{$result['name']} :: {$result['email']}",
                    'value' => $result['id']
                ];
            }
            $this->response->body(json_encode($resultsArr));
        }
    }

    public function encryptUsers()
    {
        $count = 100;
        $users = $this->Users->find()
            ->where(['Users.encrypted IS NULL'])
            ->limit($count)
            ->toArray();
        
        if (empty($users)) {
            die('Todos os registros foram criptografados');
        }

        foreach ($users as $user) {
            $user->encrypted = 1;
            $user->setDirty('name', true);
            $user->setDirty('email', true);
            $user->setDirty('username', true);
            $user->setDirty('cpf', true);
            $user->setDirty('rg', true);
            $user->setDirty('gender', true);
            $user->setDirty('phone', true);
            $user->setDirty('cep', true);
            $user->setDirty('address', true);
            $user->setDirty('number', true);
            $user->setDirty('complement', true);
            $user->setDirty('neighborhood', true);
            $user->setDirty('city', true);
            $user->setDirty('schooling', true);
            $user->setDirty('state', true);
            $this->Users->save($user);
        }
        die($count . ' registros foram criptografados');
    }
}
