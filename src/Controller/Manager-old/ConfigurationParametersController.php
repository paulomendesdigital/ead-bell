<?php

declare(strict_types=1);

namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * ConfigurationParameters Controller
 *
 * @property \App\Model\Table\ConfigurationParametersTable $ConfigurationParameters
 *
 * @method \App\Model\Entity\ConfigurationParameter[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ConfigurationParametersController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->loadComponent('GrupoGrowPaginate.GrupoGrowPaginate');
        $query = $this->ConfigurationParameters->find()
            ->contain(['Companies']);
        $configurationParameters = $this->GrupoGrowPaginate->paginate($query);

        $this->set(compact('configurationParameters'));
    }

    /**
     * View method
     *
     * @param string|null $id Configuration Parameter id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $configurationParameter = $this->ConfigurationParameters->get($id, [
            'contain' => ['Companies'],
        ]);

        $this->set('configurationParameter', $configurationParameter);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $configurationParameter = $this->ConfigurationParameters->newEntity();
        if ($this->request->is('post')) {
            $configurationParameter = $this->ConfigurationParameters->patchEntity($configurationParameter, $this->request->getData());
            if ($this->ConfigurationParameters->save($configurationParameter)) {
                $this->Flash->success(__('The configuration parameter has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The configuration parameter could not be saved. Please, try again.'));
        }
        $companies = $this->ConfigurationParameters->Companies->find('list', ['limit' => 200]);
        $this->set(compact('configurationParameter', 'companies'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Configuration Parameter id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $configurationParameter = $this->ConfigurationParameters->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $configurationParameter = $this->ConfigurationParameters->patchEntity($configurationParameter, $this->request->getData());
            if ($this->ConfigurationParameters->save($configurationParameter)) {
                $this->Flash->success(__('The configuration parameter has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The configuration parameter could not be saved. Please, try again.'));
        }
        $companies = $this->ConfigurationParameters->Companies->find('list', ['limit' => 200]);
        $this->set(compact('configurationParameter', 'companies'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Configuration Parameter id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        //Individual delete
        if (!empty($id)) {
            $configurationParameter = $this->ConfigurationParameters->get($id);
            if ($this->ConfigurationParameters->delete($configurationParameter)) {
                $this->Flash->success(__('The configuration parameter has been deleted.'));
            } else {
                $this->Flash->error(__('The configuration parameter could not be deleted. Please, try again.'));
            }
        } else { //Batch delete
            $list = $this->ConfigurationParameters->find('list');
            $entities = $this->ConfigurationParameters->patchEntities($list, $this->request->getData()['ConfigurationParameters']);
            if ($this->ConfigurationParameters->deleteMany($entities)) {
                $this->Flash->success(__('The configurationParameters has been deleted.'));
            } else {
                $this->Flash->error(__('The configurationParameters could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(['action' => 'index']);
    }
}
