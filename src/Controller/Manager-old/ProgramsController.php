<?php
namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * Programs Controller
 *
 * @property \App\Model\Table\ProgramsTable $Programs
 *
 * @method \App\Model\Entity\Program[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProgramsController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $likeFields = ['name', 'title', 'subtitle'];
        $conditions['Programs.status >='] = 0;
        $conditions = $this->__getConditionsForSearch($conditions, 'Programs', $likeFields);

        $this->paginate = [
            'contain' => ['Companies'],
            'conditions'=>[
                $conditions
            ],
            'order'=>['Programs.id'=>'DESC']
        ];
        $programs = $this->paginate($this->Programs);

        $program_types = $this->Programs->getListProgramTypes();
        $companies = $this->Programs->Companies->find('list');

        $this->set(compact('programs','program_types','companies'));
    }

    /**
     * View method
     *
     * @param string|null $id Program id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $program = $this->Programs->get($id, [
            'contain' => ['Companies']
        ]);

        $this->set('program', $program);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $program = $this->Programs->newEntity();
        if ($this->request->is('post')) {
            $program = $this->Programs->patchEntity($program, $this->request->getData());
            if ($this->Programs->save($program)) {
                $this->Flash->success(__('The {0} has been saved.','Conteúdo Mix'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.','Conteúdo Mix'));
        }

        $program_types = $this->Programs->getListProgramTypes();
        $companies = $this->Programs->Companies->find('list', ['limit' => 200]);
        $image_xvga = $this->Programs::IMAGE;
        $image_home_xvga = $this->Programs::IMAGE_HOME;
        $this->set(compact('program', 'companies', 'image_xvga', 'image_home_xvga','program_types'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Program id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $program = $this->Programs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $program = $this->Programs->patchEntity($program, $this->request->getData());
            if ($this->Programs->save($program)) {
                $this->Flash->success(__('The {0} has been saved.','Conteúdo Mix'));

                if( isset($this->request->getData()['refer']) ){
                    return $this->redirect( $this->referer() );
                }
                
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.','Conteúdo Mix'));
        }

        $program_types = $this->Programs->getListProgramTypes();
        $companies = $this->Programs->Companies->find('list', ['limit' => 200]);
        $image_xvga = $this->Programs::IMAGE;
        $image_home_xvga = $this->Programs::IMAGE_HOME;
        $this->set(compact('program', 'companies', 'image_xvga', 'image_home_xvga','program_types'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Program id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete','get']);
        $program = $this->Programs->get($id);
        if ($this->Programs->delete($program)) {
            $this->Flash->success(__('The {0} has been deleted.','Conteúdo Mix'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.','Conteúdo Mix'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function ajaxGetList($company_id){

        $programs = $this->Programs->find('list', ['conditions'=>['Programs.company_id'=>$company_id],'order'=>['name'=>'asc'],'limit' => 200]);
        
        return $this->response
        ->withType('application/json')
        ->withStringBody(json_encode([
          'data' => $programs
        ]));
    }
}
