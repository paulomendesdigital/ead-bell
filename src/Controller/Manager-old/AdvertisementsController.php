<?php
namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * Advertisements Controller
 *
 * @property \App\Model\Table\AdvertisementsTable $Advertisements
 *
 * @method \App\Model\Entity\Advertisement[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdvertisementsController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $likeFields = ['name', 'title', 'subtitle'];
        $conditions['Advertisements.status >='] = 0;
        $conditions = $this->__getConditionsForSearch($conditions, 'Advertisements', $likeFields);

        $this->paginate = [
            'contain' => ['Companies'],
            'conditions'=>[
                $conditions
            ],
            'order'=>['Advertisements.id'=>'DESC']
        ];
        $advertisements = $this->paginate($this->Advertisements);

        $companies = $this->Advertisements->Companies->find('list');
        $this->set(compact('advertisements','companies'));
    }

    /**
     * View method
     *
     * @param string|null $id Advertisement id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $advertisement = $this->Advertisements->get($id, [
            'contain' => ['Companies']
        ]);

        $this->set('advertisement', $advertisement);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $advertisement = $this->Advertisements->newEntity();
        if ($this->request->is('post')) {
            
            $data = $this->request->getData();
            $data['start'] = $this->dateFormatBeforeSave($data['start']);
            $data['finish'] = $this->dateFormatBeforeSave($data['finish']);
            
            $advertisement = $this->Advertisements->patchEntity($advertisement, $data);
            
            if ($this->Advertisements->save($advertisement)) {
                $this->Flash->success(__('The {0} has been saved.','Publicidade'));

                return $this->redirect(['action' => 'index']);
            }
            $errors = $this->__ShowValidationErrors($advertisement->getErrors());
            $this->Flash->error(__('The {0} could not be saved. Please, try again.' . $errors,'Publicidade'));
        }
        
        $companies = $this->Advertisements->Companies->find('list', ['limit' => 200]);
        
        $HOME_SUPERBANNER_SIZE      = $this->Advertisements::HOME_SUPERBANNER_SIZE;
        $HOME_RETANGULO_SIZE        = $this->Advertisements::HOME_RETANGULO_SIZE;
        $INTERNA_SUPERBANNER_SIZE   = $this->Advertisements::INTERNA_SUPERBANNER_SIZE;
        $PLAYER_CABECALHO_SIZE      = $this->Advertisements::PLAYER_CABECALHO_SIZE;
        $PLAYER_RETANGULO_SIZE      = $this->Advertisements::PLAYER_RETANGULO_SIZE;
        $MOBILE_SIZE                = $this->Advertisements::MOBILE_SIZE;

        $positions = $this->Advertisements->GetPositions();
        $type_pubs = $this->Advertisements->GetTypePubs();
        $this->set(compact('advertisement', 'companies','positions','type_pubs', 'HOME_SUPERBANNER_SIZE', 'HOME_RETANGULO_SIZE','INTERNA_SUPERBANNER_SIZE','PLAYER_CABECALHO_SIZE','PLAYER_RETANGULO_SIZE','MOBILE_SIZE'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Advertisement id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $advertisement = $this->Advertisements->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            $data = $this->request->getData();
            $data['start'] = $this->dateFormatBeforeSave($data['start']);
            $data['finish'] = $this->dateFormatBeforeSave($data['finish']);

            $advertisement = $this->Advertisements->patchEntity($advertisement, $data);
            if ($this->Advertisements->save($advertisement)) {

                $this->Advertisements->patchRemoveImages( $advertisement, $this->request->getData() );

                $this->Flash->success(__('The {0} has been saved.','Publicidade'));
                if( isset($this->request->getData()['refer']) ){
                    return $this->redirect( $this->referer() );
                }
                return $this->redirect(['action' => 'index']);
            }
            $errors = $this->__ShowValidationErrors($advertisement->getErrors());
            $this->Flash->error(__('The {0} could not be saved. Please, try again.' . $errors,'Publicidade'));
        }

        $companies = $this->Advertisements->Companies->find('list', ['limit' => 200]);
        $HOME_SUPERBANNER_SIZE      = $this->Advertisements::HOME_SUPERBANNER_SIZE;
        $HOME_RETANGULO_SIZE        = $this->Advertisements::HOME_RETANGULO_SIZE;
        $INTERNA_SUPERBANNER_SIZE   = $this->Advertisements::INTERNA_SUPERBANNER_SIZE;
        $PLAYER_CABECALHO_SIZE      = $this->Advertisements::PLAYER_CABECALHO_SIZE;
        $PLAYER_RETANGULO_SIZE      = $this->Advertisements::PLAYER_RETANGULO_SIZE;
        $MOBILE_SIZE                = $this->Advertisements::MOBILE_SIZE;

        $positions = $this->Advertisements->GetPositions();
        $type_pubs = $this->Advertisements->GetTypePubs();
        $this->set(compact('advertisement', 'companies','positions','type_pubs', 'HOME_SUPERBANNER_SIZE', 'HOME_RETANGULO_SIZE','INTERNA_SUPERBANNER_SIZE','PLAYER_CABECALHO_SIZE','PLAYER_RETANGULO_SIZE','MOBILE_SIZE'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Advertisement id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $advertisement = $this->Advertisements->get($id);
        if ($this->Advertisements->delete($advertisement)) {
            $this->Flash->success(__('The {0} has been deleted.','Publicidade'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.','Publicidade'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
