<?php
namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * LikeMusics Controller
 *
 * @property \App\Model\Table\LikeMusicsTable $LikeMusics
 *
 * @method \App\Model\Entity\LikeMusic[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LikeMusicsController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $likeFields = ['music', 'artist','created'];
        $conditions['LikeMusics.id >'] = 0;
        $conditions = $this->__getConditionsForSearch($conditions, 'LikeMusics', $likeFields);

        $this->paginate = [
            'contain' => ['Companies', 'Users'],
            'conditions'=>[
                $conditions
            ],
            'order'=>['LikeMusics.id'=>'DESC']
        ];

        $likeMusics = $this->paginate($this->LikeMusics);
        $companies = $this->LikeMusics->Companies->find('list');
        
        $this->set(compact('likeMusics','companies'));
    }

    /**
     * View method
     *
     * @param string|null $id Like Music id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $likeMusic = $this->LikeMusics->get($id, [
            'contain' => ['Companies', 'Users']
        ]);

        $this->set('likeMusic', $likeMusic);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $likeMusic = $this->LikeMusics->newEntity();
        if ($this->request->is('post')) {
            $likeMusic = $this->LikeMusics->patchEntity($likeMusic, $this->request->getData());
            if ($this->LikeMusics->save($likeMusic)) {
                $this->Flash->success(__('The like music has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The like music could not be saved. Please, try again.'));
        }
        $companies = $this->LikeMusics->Companies->find('list', ['limit' => 200]);
        $users = $this->LikeMusics->Users->find('list', ['limit' => 200]);
        $this->set(compact('likeMusic', 'companies', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Like Music id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $likeMusic = $this->LikeMusics->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $likeMusic = $this->LikeMusics->patchEntity($likeMusic, $this->request->getData());
            if ($this->LikeMusics->save($likeMusic)) {
                $this->Flash->success(__('The like music has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The like music could not be saved. Please, try again.'));
        }
        $companies = $this->LikeMusics->Companies->find('list', ['limit' => 200]);
        $users = $this->LikeMusics->Users->find('list', ['limit' => 200]);
        $this->set(compact('likeMusic', 'companies', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Like Music id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $likeMusic = $this->LikeMusics->get($id);
        if ($this->LikeMusics->delete($likeMusic)) {
            $this->Flash->success(__('The like music has been deleted.'));
        } else {
            $this->Flash->error(__('The like music could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
