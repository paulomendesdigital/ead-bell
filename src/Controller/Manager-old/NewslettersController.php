<?php
namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * Newsletters Controller
 *
 * @property \App\Model\Table\NewslettersTable $Newsletters
 *
 * @method \App\Model\Entity\Newsletter[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class NewslettersController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $likeFields = ['name', 'email', 'landing_title'];
        $conditions['Newsletters.id >'] = 0;
        $conditions = $this->__getConditionsForSearch($conditions, 'Newsletters', $likeFields);

        $this->paginate = [
            'contain' => ['Companies'],
            'conditions'=>[
                $conditions
            ],
            'order'=>['Newsletters.id'=>'DESC']
        ];

        //$campanhas = $this->Newsletters->find('list',['fields'=>['id','landing_title'],'group'=>['landing_title']]);
        $campanhas = $this->Newsletters->find('list',[
            'keyField' => 'landing_title',
            'valueField' => 'landing_title'
        ])
        ->group(['landing_title'])
        ->toArray();

        $newsletters = $this->paginate($this->Newsletters);
        $companies = $this->Newsletters->Companies->find('list');
        $this->set(compact('newsletters','companies','campanhas'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Newsletter id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete', 'get']);
        $newsletter = $this->Newsletters->get($id);
        if ($this->Newsletters->delete($newsletter)) {
            $this->Flash->success(__('The newsletter has been deleted.'));
        } else {
            $this->Flash->error(__('The newsletter could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
