<?php
namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * TopMusics Controller
 *
 * @property \App\Model\Table\TopMusicsTable $TopMusics
 *
 * @method \App\Model\Entity\TopMusic[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TopMusicsController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $likeFields = ['music', 'artist'];
        $conditions['TopMusics.id >'] = 0;
        $conditions = $this->__getConditionsForSearch($conditions, 'TopMusics', $likeFields);

        $this->paginate = [
            'contain' => ['Companies'],
            'conditions'=>[
                $conditions
            ],
            'order'=>['TopMusics.id'=>'DESC']
        ];

        $topMusics = $this->paginate($this->TopMusics);
        $companies = $this->TopMusics->Companies->find('list');

        $this->set(compact('topMusics','companies'));
    }

    /**
     * View method
     *
     * @param string|null $id Top Music id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $topMusic = $this->TopMusics->get($id, [
            'contain' => ['Companies']
        ]);

        $this->set('topMusic', $topMusic);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $topMusic = $this->TopMusics->newEntity();
        if ($this->request->is('post')) {
            $topMusic = $this->TopMusics->patchEntity($topMusic, $this->request->getData());
            if ($this->TopMusics->save($topMusic)) {
                $this->Flash->success(__('The {0} has been saved.','Top Mix'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.','Top Mix'));
        }
        $image_size = $this->TopMusics::IMAGE_SIZE;
        $companies = $this->TopMusics->Companies->find('list', ['limit' => 200]);
        $this->set(compact('topMusic', 'companies', 'image_size'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Top Music id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $topMusic = $this->TopMusics->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $topMusic = $this->TopMusics->patchEntity($topMusic, $this->request->getData());
            if ($this->TopMusics->save($topMusic)) {
                $this->Flash->success(__('The {0} has been saved.','Top Mix'));
                if( isset($this->request->getData()['refer']) ){
                    return $this->redirect( $this->referer() );
                }
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.','Top Mix'));
        }
        $image_size = $this->TopMusics::IMAGE_SIZE;
        $companies = $this->TopMusics->Companies->find('list', ['limit' => 200]);
        $this->set(compact('topMusic', 'companies', 'image_size'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Top Music id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete','get']);
        $topMusic = $this->TopMusics->get($id);
        //die(debug( $topMusic ));
        if ($this->TopMusics->delete($topMusic)) {
            $this->Flash->success(__('The {0} has been deleted.','Top Mix'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.','Top Mix'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
