<?php
declare(strict_types=1);

namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * AccessOrigins Controller
 *
 * @property \App\Model\Table\AccessOriginsTable $AccessOrigins
 *
 * @method \App\Model\Entity\AccessOrigin[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AccessOriginsController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->loadComponent('GrupoGrowPaginate.GrupoGrowPaginate');
        $query = $this->AccessOrigins->find();
        $accessOrigins = $this->GrupoGrowPaginate->paginate($query);

        $this->set(compact('accessOrigins'));
    }

    /**
     * View method
     *
     * @param string|null $id Access Origin id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $accessOrigin = $this->AccessOrigins->get($id, [
            'contain' => ['Participants'],
        ]);

        $this->set('accessOrigin', $accessOrigin);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $accessOrigin = $this->AccessOrigins->newEntity();
        if ($this->request->is('post')) {
            $accessOrigin = $this->AccessOrigins->patchEntity($accessOrigin, $this->request->getData());
            if ($this->AccessOrigins->save($accessOrigin)) {
                $this->Flash->success(__('The access origin has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The access origin could not be saved. Please, try again.'));
        }
        $this->set(compact('accessOrigin'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Access Origin id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $accessOrigin = $this->AccessOrigins->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $accessOrigin = $this->AccessOrigins->patchEntity($accessOrigin, $this->request->getData());
            if ($this->AccessOrigins->save($accessOrigin)) {
                $this->Flash->success(__('The access origin has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The access origin could not be saved. Please, try again.'));
        }
        $this->set(compact('accessOrigin'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Access Origin id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        //Individual delete
        if(!empty($id)){
            $accessOrigin = $this->AccessOrigins->get($id);
            if ($this->AccessOrigins->delete($accessOrigin)) {
                $this->Flash->success(__('The access origin has been deleted.'));
            } else {
                $this->Flash->error(__('The access origin could not be deleted. Please, try again.'));
            }
        }else{ //Batch delete
            $list = $this->AccessOrigins->find('list');
            $entities = $this->AccessOrigins->patchEntities($list, $this->request->getData()['AccessOrigins']);
            if($this->AccessOrigins->deleteMany($entities)){
                $this->Flash->success(__('The accessOrigins has been deleted.'));
            }else{
                $this->Flash->error(__('The accessOrigins could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(['action' => 'index']);
    }
}
