<?php

namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * Participants Controller
 *
 * @property \App\Model\Table\ParticipantsTable $Participants
 *
 * @method \App\Model\Entity\Participant[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ParticipantsController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index($content_id = null)
    {
        $content = $this->Participants->Contents->get($content_id, [
            'contain' => ['Companies']
        ]);

        $qtdewinners = $this->Participants->Users->Winners->find()
            ->where([
                'Winners.content_id' => $content_id
            ])->count();

        $users = $this->Participants->Users->find()
            ->contain([
                'Participants' => [
                    'conditions' => ['Participants.content_id' => $content_id],
                    'Questions',
                    'Alternatives',
                    'AccessOrigins'
                ],
                'Winners' => [
                    'conditions' => [
                        'Winners.user_id IN' => $this->Participants->Users->Winners->find('list', [
                            'fields' => ['Winners.user_id'],
                            'conditions' => ['Winners.content_id' => $content_id]
                        ])
                    ]
                ]
            ])
            ->where([
                'Users.status' => 1,
                'Users.group_id' => 2,
                'Users.id IN' => $this->Participants->find('list', [
                    'fields' => ['Participants.user_id'],
                    'conditions' => ['Participants.content_id' => $content_id]
                ])
            ]);
        
        
        $countAccessOrigins = $this->Participants->getCountAccessOrigins($content_id);
    
        $this->set(compact('users', 'content', 'qtdewinners', 'countAccessOrigins'));
    }

    /**
     * View method
     *
     * @param string|null $id Participant id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $participant = $this->Participants->get($id, [
            'contain' => ['Users', 'Contents', 'Questions', 'Alternatives']
        ]);

        $this->set('participant', $participant);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($content_id = false)
    {
        $participant = $this->Participants->newEntity();
        if ($this->request->is('post')) {
            $data_participant = $this->request->getData();
            $participant = $this->Participants->patchEntity($participant, $data_participant);
            if ($this->Participants->save($participant)) {
                $this->Flash->success(__('The {0} has been saved.', 'Participante'));
                return $this->redirect(['action' => 'index', $data_participant['content_id']]);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Participante'));
        }

        $companies = $this->Participants->Companies->find('list');
        $content = $this->Participants->Contents->find('all')
            ->contain([
                'ContentCategories',
                'Companies',
                'Questions' => [
                    'Alternatives'
                ],
            ])
            ->where(['Contents.id' => $content_id])
            ->first();

        $questions = $this->Participants->Questions->find('list', ['limit' => 200]);
        $alternatives = $this->Participants->Alternatives->find('list', ['limit' => 200]);

        $this->set(compact('participant', 'companies', 'content', 'questions', 'alternatives', 'content_id'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Participant id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $participant = $this->Participants->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $participant = $this->Participants->patchEntity($participant, $this->request->getData());
            if ($this->Participants->save($participant)) {
                $this->Flash->success(__('The participant has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The participant could not be saved. Please, try again.'));
        }
        $users = $this->Participants->Users->find('list', ['limit' => 200]);
        $contents = $this->Participants->Contents->find('list', ['limit' => 200]);
        $questions = $this->Participants->Questions->find('list', ['limit' => 200]);
        $alternatives = $this->Participants->Alternatives->find('list', ['limit' => 200]);
        $this->set(compact('participant', 'users', 'contents', 'questions', 'alternatives'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Participant id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($user_id = null, $content_id = null)
    {
        $this->request->allowMethod(['post', 'delete', 'get']);

        if ($this->Participants->removeParticipation($user_id, $content_id)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Partipação'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Partipação'));
        }

        return $this->redirect($this->referer());
    }

    public function exportPromotionParticipants($promotionId)
    {
        $this->loadComponent('Spreadsheet');

        $users = $this->Participants->Users->find()
            ->contain([
                'Participants' => [
                    'conditions' => ['Participants.content_id' => $promotionId],
                    'Questions',
                    'Alternatives',
                    'AccessOrigins'
                ],
                'Winners' => [
                    'conditions' => [
                        'Winners.user_id IN' => $this->Participants->Users->Winners->find('list', [
                            'fields' => ['Winners.user_id'],
                            'conditions' => ['Winners.content_id' => $promotionId]
                        ])
                    ]
                ]
            ])
            ->where([
                'Users.status' => 1,
                'Users.group_id' => 2,
                'Users.id IN' => $this->Participants->find('list', [
                    'fields' => ['Participants.user_id'],
                    'conditions' => ['Participants.content_id' => $promotionId]
                ])
            ])->toArray();

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);

        $rowValues = [
            "Id",
            "Nome",
            "Gênero",
            "Nascimento",
            "Idade",
            "Email",
            "CPF",
            "Fone",
            "Bairro",
            "Origem da Participação"
        ];
        $rowNum = 1;
        $this->Spreadsheet->writeSpreadsheetRow($rowValues, $spreadsheet, $rowNum++);

        foreach ($users as $user) {
            $this->Spreadsheet->writeSpreadsheetRow(
                [
                    $user->id, $user->name, $user->gender, !empty($user->birth) ? $user->birth->format('d/m/Y') : null,
                    !empty($user->birth) ? date_diff(date_create(date('Y-m-d')), date_create($user->birth->format('Y-m-d')))->format('%Y') : null,
                    $user->email,
                    $user->cpf,
                    $user->phone,
                    $user->neighborhood,
                    !empty($user->participants[0]->access_origin) ? $user->participants[0]->access_origin->name : null
                ],
                $spreadsheet,
                $rowNum++
            );
        }

        $fileDir = WWW_ROOT . 'files/promotionParticipantsSpreadsheets/';

        if (!is_dir($fileDir)) {
            mkdir($fileDir, 0777, true);
        }

        $filename = 'Participants da Promoção ' . $this->request->getSession()->id() . '.xlsx';
        $writer->save($fileDir . $filename);

        return $this->response
            ->withFile($fileDir . $filename, [
                'download' => true,
                'name' => 'Participants da Promoção.xlsx'
            ]);
    }
}
