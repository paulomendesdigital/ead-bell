<?php

namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;
use Cake\Routing\Router;

/**
 * Contents Controller
 *
 * @property \App\Model\Table\ContentsTable $Contents
 *
 * @method \App\Model\Entity\Content[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContentsController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $likeFields = ['name', 'title', 'subtitle'];
        $conditions['Contents.content_category_id <>'] = $this->Contents::PROMOTION_ID;
        $conditions = $this->__getConditionsForSearch($conditions, 'Contents', $likeFields);

        $this->paginate = [
            'contain' => ['ContentCategories', 'Companies'],
            'conditions' => [
                $conditions
            ],
            'order' => ['Contents.id' => 'DESC']
        ];
        $contents = $this->paginate($this->Contents);
        $companies = $this->Contents->Companies->find('list');
        $contentCategories = $this->Contents->ContentCategories->find('list', ['conditions' => ['ContentCategories.id <>' => $this->Contents::PROMOTION_ID], 'order' => ['name' => 'asc'], 'limit' => 200]);
        $destinations = $this->Contents->getListDestinations();
        $this->set(compact('contents', 'companies', 'contentCategories', 'destinations'));
    }

    public function promotions()
    {
        $likeFields = ['name', 'title', 'subtitle'];
        $conditions['Contents.content_category_id'] = $this->Contents::PROMOTION_ID;
        $conditions = $this->__getConditionsForSearch($conditions, 'Contents', $likeFields);

        $this->paginate = [
            'contain' => [
                'ContentCategories',
                'Companies',
                'Participants' => [
                    'fields' => ['DISTINCT Participants.user_id', 'Participants.content_id']
                ],
            ],
            'conditions' => [
                $conditions
            ],
            'order' => ['Contents.id' => 'DESC']
        ];

        $promotions = $this->paginate($this->Contents);

        $promotion_types = [
            $this->Contents::PROMOTION_TYPE_NO_ACTION => $this->Contents::PROMOTION_TYPE_NO_ACTION,
            $this->Contents::PROMOTION_TYPE_PERGUNTA => $this->Contents::PROMOTION_TYPE_PERGUNTA,
            $this->Contents::PROMOTION_TYPE_SORTEIO => $this->Contents::PROMOTION_TYPE_SORTEIO,
            $this->Contents::PROMOTION_TYPE_QUIZ => $this->Contents::PROMOTION_TYPE_QUIZ,
        ];

        $companies = $this->Contents->Companies->find('list');
        $destinations = $this->Contents->getListDestinations();
        $this->set(compact('promotions', 'promotion_types', 'companies', 'destinations'));
    }

    public function reports()
    {
        $likeFields = ['name', 'title', 'subtitle'];
        $conditions['Contents.content_category_id'] = $this->Contents::PROMOTION_ID;
        $conditions = $this->__getConditionsForSearch($conditions, 'Contents', $likeFields);

        $this->paginate = [
            'contain' => [
                'ContentCategories',
                'Companies',
                'Participants' => [
                    'Users'
                ],
            ],
            'conditions' => [
                $conditions
            ],
            'order' => ['Contents.id' => 'DESC'],
            'limit' => 10
        ];

        $promotions = $this->paginate($this->Contents)->toArray();
        
        //die(debug($promotions));
        
        foreach($promotions as $c => $content){

            $neighborhoods = $city = $user = $new_users = [];
            
            
            foreach($content['participants'] as $p => $participant){

                if(!empty($participant['user']['neighborhood'])){
                    $neighborhoods[] = ['label' => $participant['user']['neighborhood']];
                }
                if(!empty($participant['user']['city'])){
                    $city[] = ['label' => $participant['user']['city']];
                }
                if(!empty($participant['user']['id'])){
                    $user[] = ['label' => $participant['user']['id']];
                }

                $promotions[$c]['participants'][$p]['user']['idade'] = $this->dateDiff( $participant['user']['birth'] , date('Y-m-d') , '%Y' );

                if(!empty($this->dateDiff( $participant['created'] , $content['created'] , '%r' ))){
                    $new_users[] = ['label' => $participant['user']['id']];
                }
            } 

            $promotions[$c]['neighborhood'] = [];
            $promotions[$c]['city'] = [];
            $promotions[$c]['user'] = [];
            $promotions[$c]['new_users'] = [];

            if(!empty($neighborhoods)){ 
                $array =  array_count_values(array_column($neighborhoods, 'label'));
                foreach( $array as $n => $count ){
                    $promotions[$c]['neighborhood'][] = [ 'label' => $n, 'count' => $count ];
                } 
            } 
            if(!empty($city)){ 
                $array =  array_count_values(array_column($city, 'label'));
                foreach( $array as $n => $count ){
                    $promotions[$c]['city'][] = [ 'label' => $n, 'count' => $count ];
                } 
            } 
            if(!empty($user)){ 
                $array =  array_count_values(array_column($user, 'label'));
                foreach( $array as $n => $count ){
                    $promotions[$c]['user'][] = [ 'label' => $n, 'count' => $count ];
                } 
            } 
            if(!empty($new_users)){ 
                $array =  array_count_values(array_column($new_users, 'label'));
                foreach( $array as $n => $count ){
                    $promotions[$c]['new_users'][] = [ 'label' => $n, 'count' => $count ];
                } 
            } 
        }

        $promotion_types = [
            $this->Contents::PROMOTION_TYPE_NO_ACTION => $this->Contents::PROMOTION_TYPE_NO_ACTION,
            $this->Contents::PROMOTION_TYPE_PERGUNTA => $this->Contents::PROMOTION_TYPE_PERGUNTA,
            $this->Contents::PROMOTION_TYPE_SORTEIO => $this->Contents::PROMOTION_TYPE_SORTEIO,
            $this->Contents::PROMOTION_TYPE_QUIZ => $this->Contents::PROMOTION_TYPE_QUIZ,
        ];

       
        $companies = $this->Contents->Companies->find('list');
        $destinations = $this->Contents->getListDestinations();
        $this->set(compact('promotions', 'promotion_types', 'companies', 'destinations'));
    }

    /**
     * View method
     *
     * @param string|null $id Content id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $content = $this->Contents->get($id, [
            'contain' => ['ContentCategories', 'Companies']
        ]);

        $this->set('content', $content);
    }

    /**
     * View method
     *
     * @param string|null $id Content id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function viewPromotion($id = null)
    {
        $content = $this->Contents->get($id, [
            'contain' => ['ContentCategories', 'Companies']
        ]);

        $this->set('content', $content);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $content = $this->Contents->newEntity();
        if ($this->request->is('post')) {
            $content = $this->Contents->patchEntity($content, $this->request->getData());
            if ($this->Contents->save($content)) {
                $this->Flash->success(__('The {0} has been saved.', 'Publicação'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Publicação'));
        }
        $contentCategories = $this->Contents->ContentCategories->find('list', ['conditions' => ['ContentCategories.id <>' => $this->Contents::PROMOTION_ID], 'order' => ['name' => 'asc'], 'limit' => 200]);
        $companies = $this->Contents->Companies->find('list');
        $programs = $this->Contents->Programs->find('list', ['order' => ['name' => 'asc']]);

        $image_small_size = $this->Contents::IMAGE_SMALL_SIZE;
        $image_medium_size = $this->Contents::IMAGE_MEDIUM_SIZE;
        $image_large_size = $this->Contents::IMAGE_LARGE_SIZE;
        $image_internal_size = $this->Contents::IMAGE_INTERNAL_SIZE;

        $promocaoId = $this->Contents->ContentCategories->getPromocaoId();
        $podcastId = $this->Contents->ContentCategories->getPodcastId();

        $promotion_types = [
            $this->Contents::PROMOTION_TYPE_PERGUNTA => $this->Contents::PROMOTION_TYPE_PERGUNTA,
            $this->Contents::PROMOTION_TYPE_SORTEIO => $this->Contents::PROMOTION_TYPE_SORTEIO,
            //$this->Contents::PROMOTION_TYPE_QUIZ=>$this->Contents::PROMOTION_TYPE_QUIZ,
        ];

        $perguntaId = $this->Contents::PROMOTION_TYPE_PERGUNTA;
        $limitTags = $this->Contents::LIMIT_TAGS;
        $destinations = $this->Contents->getListDestinations();

        $this->set(compact('content', 'contentCategories', 'companies', 'image_small_size', 'image_medium_size', 'image_large_size', 'image_internal_size', 'programs', 'promocaoId', 'podcastId', 'promotion_types', 'perguntaId', 'limitTags', 'destinations'));
    }

    public function addPromotion()
    {
        $content = $this->Contents->newEntity();
        if ($this->request->is('post')) {
            $content = $this->Contents->patchEntity($content, $this->request->getData(), [
                'associated' => [
                    'Questions' => [
                        'associated' => [
                            'Alternatives'
                        ]
                    ]
                ]
            ]);
            if ($this->Contents->save($content, ['associated' => ['Questions' => ['associated' => ['Alternatives']]]])) {
                $this->Flash->success(__('The {0} has been saved.', 'Promoção'));

                return $this->redirect(['action' => 'promotions']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Promoção'));
        }
        $contentCategories = $this->Contents->ContentCategories->find('list', [
            'conditions' => ['ContentCategories.id' => $this->Contents::PROMOTION_ID],
            'order' => ['name' => 'asc'],
            'limit' => 200
        ]);
        $companies = $this->Contents->Companies->find('list');
        $programs = $this->Contents->Programs->find('list', ['order' => ['name' => 'asc']]);

        $image_small_size = $this->Contents::IMAGE_SMALL_SIZE;
        $image_medium_size = $this->Contents::IMAGE_MEDIUM_SIZE;
        $image_large_size = $this->Contents::IMAGE_LARGE_SIZE;
        $image_internal_size = $this->Contents::IMAGE_INTERNAL_SIZE;

        $promocaoId = $this->Contents->ContentCategories->getPromocaoId();
        $podcastId = $this->Contents->ContentCategories->getPodcastId();

        $promotion_types = [
            $this->Contents::PROMOTION_TYPE_NO_ACTION => $this->Contents::PROMOTION_TYPE_NO_ACTION,
            $this->Contents::PROMOTION_TYPE_PERGUNTA => $this->Contents::PROMOTION_TYPE_PERGUNTA,
            $this->Contents::PROMOTION_TYPE_SORTEIO => $this->Contents::PROMOTION_TYPE_SORTEIO,
            $this->Contents::PROMOTION_TYPE_QUIZ => $this->Contents::PROMOTION_TYPE_QUIZ,
        ];

        $question_types = $this->Contents->Questions->getQuestionTypes();
        $questionTypeTextId = $this->Contents->Questions->getQuestionTypeTextId();
        $questionTypeRadioButtonId = $this->Contents->Questions->getQuestionTypeRadioButtonId();
        $questionTypeCheckboxId = $this->Contents->Questions->getQuestionTypeCheckboxId();

        $perguntaId = $this->Contents::PROMOTION_TYPE_PERGUNTA;
        $quizId = $this->Contents::PROMOTION_TYPE_QUIZ;
        $limitTags = $this->Contents::LIMIT_TAGS;
        $destinations = $this->Contents->getListDestinations();

        $this->set(compact(
            'content',
            'contentCategories',
            'companies',
            'image_small_size',
            'image_medium_size',
            'image_large_size',
            'image_internal_size',
            'programs',
            'promocaoId',
            'podcastId',
            'promotion_types',
            'perguntaId',
            'limitTags',
            'question_types',
            'questionTypeTextId',
            'questionTypeRadioButtonId',
            'questionTypeCheckboxId',
            'quizId',
            'destinations'
        ));
    }

    /**
     * Edit method
     *
     * @param string|null $id Content id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $content = $this->Contents->get($id, [
            'contain' => ['Companies', 'Questions']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $content_data = $this->request->getData();
            if (!isset($content_data['questions'])) {
                $content_data['questions'] = [];
            }
            $content = $this->Contents->patchEntity($content, $content_data);
            if ($this->Contents->save($content)) {

                $this->Contents->patchRemoveImages($content, $this->request->getData());

                $this->Flash->success(__('The {0} has been saved.', 'Publicação'));
                if (isset($this->request->getData()['refer'])) {
                    return $this->redirect($this->referer());
                }
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Publicação'));
        }

        $companies = $this->Contents->Companies->find('list');

        $contentCategories = $this->Contents->ContentCategories->find('list', [
            'conditions' => [
                'ContentCategories.id <>' => $this->Contents::PROMOTION_ID,
                //'ContentCategories.company_id'=>$content->company_id
            ],
            'order' => ['name' => 'asc'],
            'limit' => 200
        ]);

        $programs = $this->Contents->Programs->find('list', [
            'conditions' => [
                'Programs.company_id' => $content->company_id
            ],
            'order' => ['name' => 'asc']
        ]);

        $image_small_size = $this->Contents::IMAGE_SMALL_SIZE;
        $image_medium_size = $this->Contents::IMAGE_MEDIUM_SIZE;
        $image_large_size = $this->Contents::IMAGE_LARGE_SIZE;
        $image_internal_size = $this->Contents::IMAGE_INTERNAL_SIZE;

        $promocaoId = $this->Contents->ContentCategories->getPromocaoId();
        $podcastId = $this->Contents->ContentCategories->getPodcastId();

        $promotion_types = [
            $this->Contents::PROMOTION_TYPE_PERGUNTA => $this->Contents::PROMOTION_TYPE_PERGUNTA,
            $this->Contents::PROMOTION_TYPE_SORTEIO => $this->Contents::PROMOTION_TYPE_SORTEIO,
            //$this->Contents::PROMOTION_TYPE_QUIZ=>$this->Contents::PROMOTION_TYPE_QUIZ,
        ];

        $perguntaId = $this->Contents::PROMOTION_TYPE_PERGUNTA;
        $limitTags = $this->Contents::LIMIT_TAGS;
        $destinations = $this->Contents->getListDestinations();

        $this->set(compact('content', 'contentCategories', 'companies', 'image_small_size', 'image_medium_size', 'image_large_size', 'image_internal_size', 'programs', 'promocaoId', 'podcastId', 'promotion_types', 'perguntaId', 'limitTags', 'destinations'));
    }

    public function editPromotion($id = null)
    {
        $content = $this->Contents->get($id, [
            'contain' => ['Companies', 'Questions' => ['Alternatives']]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $content_data = $this->request->getData();
            if (!isset($content_data['questions'])) {
                $content_data['questions'] = [];
            }
            for ($i = 0; $i < count($content_data['questions']); $i++) {
                if (!isset($content_data['questions'][$i]['alternatives'])) {
                    $content_data['questions'][$i]['alternatives'] = [];
                }
            }
            $content = $this->Contents->patchEntity($content, $content_data, [
                'associated' => [
                    'Questions' => [
                        'associated' => [
                            'Alternatives'
                        ]
                    ]
                ]
            ]);
            if ($this->Contents->save($content, ['associated' => ['Questions' => ['associated' => ['Alternatives']]]])) {

                $this->Contents->patchRemoveImages($content, $this->request->getData());

                $this->Flash->success(__('The {0} has been saved.', 'Promoção'));
                if (isset($this->request->getData()['refer'])) {
                    return $this->redirect($this->referer());
                }
                return $this->redirect(['action' => 'promotions']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Promoção'));
        }
        $contentCategories = $this->Contents->ContentCategories->find('list', [
            'conditions' => ['ContentCategories.id' => $this->Contents::PROMOTION_ID],
            'order' => ['name' => 'asc'],
            'limit' => 200
        ]);
        $companies = $this->Contents->Companies->find('list');
        $programs = $this->Contents->Programs->find('list', ['order' => ['name' => 'asc']]);

        $image_small_size = $this->Contents::IMAGE_SMALL_SIZE;
        $image_medium_size = $this->Contents::IMAGE_MEDIUM_SIZE;
        $image_large_size = $this->Contents::IMAGE_LARGE_SIZE;
        $image_internal_size = $this->Contents::IMAGE_INTERNAL_SIZE;

        $promocaoId = $this->Contents->ContentCategories->getPromocaoId();
        $podcastId = $this->Contents->ContentCategories->getPodcastId();

        $promotion_types = [
            $this->Contents::PROMOTION_TYPE_NO_ACTION => $this->Contents::PROMOTION_TYPE_NO_ACTION,
            $this->Contents::PROMOTION_TYPE_PERGUNTA => $this->Contents::PROMOTION_TYPE_PERGUNTA,
            $this->Contents::PROMOTION_TYPE_SORTEIO => $this->Contents::PROMOTION_TYPE_SORTEIO,
            $this->Contents::PROMOTION_TYPE_QUIZ => $this->Contents::PROMOTION_TYPE_QUIZ,
        ];

        $question_types = $this->Contents->Questions->getQuestionTypes();
        $questionTypeTextId = $this->Contents->Questions->getQuestionTypeTextId();
        $questionTypeRadioButtonId = $this->Contents->Questions->getQuestionTypeRadioButtonId();
        $questionTypeCheckboxId = $this->Contents->Questions->getQuestionTypeCheckboxId();

        $perguntaId = $this->Contents::PROMOTION_TYPE_PERGUNTA;
        $quizId = $this->Contents::PROMOTION_TYPE_QUIZ;
        $limitTags = $this->Contents::LIMIT_TAGS;
        $destinations = $this->Contents->getListDestinations();

        $this->set(compact(
            'content',
            'contentCategories',
            'companies',
            'image_small_size',
            'image_medium_size',
            'image_large_size',
            'image_internal_size',
            'programs',
            'promocaoId',
            'podcastId',
            'promotion_types',
            'perguntaId',
            'limitTags',
            'question_types',
            'questionTypeTextId',
            'questionTypeRadioButtonId',
            'questionTypeCheckboxId',
            'quizId',
            'destinations'
        ));
    }

    /**
     * Delete method
     *
     * @param string|null $id Content id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete', 'get']);
        $content = $this->Contents->get($id);
        if ($this->Contents->delete($content)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Publicação'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Publicação'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function uploadCkeditorFiles()
    {
        $this->request->allowMethod('POST');

        $this->viewBuilder()->setLayout('ajax');

        $error = false;

        if (!empty($this->request->getData('upload')['error'])) {
            $error = true;
        }

        if (!$error) {
            $this->loadComponent('Utils');

            $targetDir = WWW_ROOT . "/files/Contents/ckeditor_uploads/";

            if (!is_dir(WWW_ROOT . DS . "files/Contents/ckeditor_uploads")) {
                mkdir(WWW_ROOT . DS . "files/Contents/ckeditor_uploads", 0777, true);
            }

            $filename = $this->Utils->generateUuidV4() . $this->request->getData('upload')['name'];
            $targetFile = $targetDir . $filename;

            if (move_uploaded_file($this->request->getData('upload')['tmp_name'], $targetFile)) {
                $this->set([
                    'filename' => $filename,
                    'url' => Router::url("/", true) . "files/Contents/ckeditor_uploads/" . $filename
                ]);
            } else {
                $error = true;
            }
        }

        $this->set('error', $error);
    }

    public function clone($id)
    {
        $content = $this->Contents->find()
            ->contain(['Questions' => ['Alternatives']])
            ->where(['Contents.id' => $id])
            ->firstOrFail();

        //Mantém as imagens criadas anteriormente associadas ao novo conteúdo
        $imageNames = [
            "image_small_name" => $content->image_small_name,
            "image_medium_name" => $content->image_medium_name,
            "image_large_name" => $content->image_large_name,
            "image_internal" => $content->image_internal
        ];

        $content->id = null;
        $content->isNew(true);

        foreach ($content->questions as $key => $question) {
            $content->questions[$key]->id = null;
            $content->questions[$key]->isNew(true);
            foreach ($content->questions[$key]->alternatives as $keyAlternative => $alternative) {
                $content->questions[$key]->alternatives[$keyAlternative]->id = null;
                $content->questions[$key]->alternatives[$keyAlternative]->isNew(true);
            }
        }

        if (!$this->Contents->save($content, ['associated' => ['Questions' => ['associated' => ['Alternatives']]]])) {
            $this->Flash->error('Erro ao clonar publicação');
            return $this->redirect($this->referer());
        }

        $this->Contents->updateAll($imageNames, ['Contents.id' => $content->id]);

        $content = $this->Contents->find()
            ->contain(['Questions' => ['Alternatives']])
            ->where(['Contents.id' => $content->id])
            ->firstOrFail();

        if ($content->content_category_id == $this->Contents::PROMOTION_ID) {
            return $this->redirect(['action' => 'editPromotion', $content->id]);
        }

        return $this->redirect(['action' => 'edit', $content->id]);
    }

    public function dateDiff($date1, $date2, $format){
    
	if(!is_string($date1) && $date1 != null){
		$date1 = $date1->format('Y-m-d');
	}
	
	if(!is_string($date2) && $date2 != null){
		$date2 = $date2->format('Y-m-d');
	}
    
        $date = new \DateTime( $date1 );
        $interval = $date->diff( new \DateTime( $date2 ) );
       
        return $interval->format( $format );

    }
}
