<?php

declare(strict_types=1);

namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;
use Cake\ORM\TableRegistry;

/**
 * Advertisers Controller
 *
 * @property \App\Model\Table\AdvertisersTable $Advertisers
 *
 * @method \App\Model\Entity\Advertiser[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdvertisersController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->loadComponent('GrupoGrowPaginate.GrupoGrowPaginate');
        $query = $this->Advertisers->find();
        $advertisers = $this->GrupoGrowPaginate->paginate($query);

        $this->set(compact('advertisers'));
    }

    /**
     * View method
     *
     * @param string|null $id Advertiser id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $advertiser = $this->Advertisers->get($id, [
            'contain' => ['AdvertCampaigns' => ['Companies']]
        ]);

        $this->set('advertiser', $advertiser);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('ajax')){
            $this->layout = false;
        }
        
        $advertiser = $this->Advertisers->newEntity();
        if ($this->request->is('post')) {

            $advertiser = $this->Advertisers->patchEntity($advertiser, $this->request->getData());
            if ($this->Advertisers->save($advertiser)) {

                $this->request->data['id'] = $advertiser->id;

                if ($this->request->is('ajax')){
                    $this->response->body(json_encode([ 'status' => 'success', 'message' => 'Save ok', 'data' => $this->request->data ]) ); 
                    return $this->response;
                }

                $this->Flash->success(__('The advertiser has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The advertiser could not be saved. Please, try again.'));
        }
        $this->set(compact('advertiser'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Advertiser id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $advertiser = $this->Advertisers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            //refer
            //die(debug($this->request->data));

            $advertiser = $this->Advertisers->patchEntity($advertiser, $this->request->getData());
            if ($this->Advertisers->save($advertiser)) {
                $this->Flash->success(__('The advertiser has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The advertiser could not be saved. Please, try again.'));
        }
        $this->set(compact('advertiser'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Advertiser id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        //Individual delete
        if (!empty($id)) {
            $advertiser = $this->Advertisers->get($id);
            if ($this->Advertisers->delete($advertiser)) {
                $this->Flash->success(__('The advertiser has been deleted.'));
            } else {
                $this->Flash->error(__('The advertiser could not be deleted. Please, try again.'));
            }
        } else { //Batch delete
            $list = $this->Advertisers->find('list');
            $entities = $this->Advertisers->patchEntities($list, $this->request->getData()['Advertisers']);
            if ($this->Advertisers->deleteMany($entities)) {
                $this->Flash->success(__('The advertisers has been deleted.'));
            } else {
                $this->Flash->error(__('The advertisers could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(['action' => 'index']);
    }
}
