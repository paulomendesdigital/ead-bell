<?php

declare(strict_types=1);

namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * AdvertViews Controller
 *
 * @property \App\Model\Table\AdvertViewsTable $AdvertViews
 *
 * @method \App\Model\Entity\AdvertView[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdvertViewsController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->loadComponent('GrupoGrowPaginate.GrupoGrowPaginate');
        $query = $this->AdvertViews->find();

        $query = $query->contain([
            'AdvertZones', 'AdvertCampaigns', 'Adverts', 'AdvertsAdvertZones',
        ]);
        $advertViews = $this->GrupoGrowPaginate->paginate($query);
        $advertZones = $this->AdvertViews->AdvertZones->find('list');
        $advertCampaigns = $this->AdvertViews->AdvertCampaigns->find('list');
        $adverts = $this->AdvertViews->Adverts->find('list');

        $this->set(compact('advertViews', 'advertZones', 'adverts', 'advertCampaigns'));
    }

    /**
     * View method
     *
     * @param string|null $id Advert View id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $advertView = $this->AdvertViews->get($id, [
            'contain' => ['AdvertZones', 'AdvertCampaigns', 'Adverts', 'AdvertsAdvertZones']
        ]);

        $this->set('advertView', $advertView);
    }
}
