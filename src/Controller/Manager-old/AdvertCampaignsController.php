<?php

declare(strict_types=1);

namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * AdvertCampaigns Controller
 *
 * @property \App\Model\Table\AdvertCampaignsTable $AdvertCampaigns
 *
 * @method \App\Model\Entity\AdvertCampaign[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdvertCampaignsController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->loadComponent('GrupoGrowPaginate.GrupoGrowPaginate');
        $query = $this->AdvertCampaigns->find();

        $query = $query->contain([
            'Advertisers', 'Companies',
        ]);
        $advertCampaigns = $this->GrupoGrowPaginate->paginate($query);
        $companies = $this->AdvertCampaigns->Companies->find('list');
        $advertisers = $this->AdvertCampaigns->Advertisers->find('list');

        $this->set(compact('advertCampaigns', 'companies', 'advertisers'));
    }

    /**
     * View method
     *
     * @param string|null $id Advert Campaign id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $advertCampaign = $this->AdvertCampaigns->get($id, [
            'contain' => ['Advertisers', 'Companies', 'Adverts']
        ]);

        $this->set('advertCampaign', $advertCampaign);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $advertCampaign = $this->AdvertCampaigns->newEntity();
        if ($this->request->is('post')) {

            $advertCampaignData =  $this->request->getData();
            $advertCampaignData['max_unique_views'] = 
            $advertCampaignData['max_clicks'] = 
            $advertCampaignData['max_unique_clicks'] =  $advertCampaignData['max_views'] ;

            $advertCampaign = $this->AdvertCampaigns->patchEntity($advertCampaign, $advertCampaignData);
            if ($this->AdvertCampaigns->save($advertCampaign)) {
                $this->Flash->success(__('The advert campaign has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The advert campaign could not be saved. Please, try again.'));
        }
        $advertisers = $this->AdvertCampaigns->Advertisers->find('list', ['limit' => 200]);
        $companies = $this->AdvertCampaigns->Companies->find('list', ['limit' => 200]);
        $this->set(compact('advertCampaign', 'advertisers', 'companies'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Advert Campaign id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $advertCampaign = $this->AdvertCampaigns->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            $advertCampaignData =  $this->request->getData();
            $advertCampaignData['max_unique_views'] = 
            $advertCampaignData['max_clicks'] = 
            $advertCampaignData['max_unique_clicks'] =  $advertCampaignData['max_views'] ;
            
            $advertCampaign = $this->AdvertCampaigns->patchEntity($advertCampaign,$advertCampaignData);
            if ($this->AdvertCampaigns->save($advertCampaign)) {
                $this->Flash->success(__('The advert campaign has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The advert campaign could not be saved. Please, try again.'));
        } else {
            if (!empty($advertCampaign->start_date)) {
                $advertCampaign->start_date = $advertCampaign->start_date->format('d/m/Y H:i');
            }
            if (!empty($advertCampaign->finish_date)) {
                $advertCampaign->finish_date = $advertCampaign->finish_date->format('d/m/Y H:i');
            }
        }
        $advertisers = $this->AdvertCampaigns->Advertisers->find('list', ['limit' => 200]);
        $companies = $this->AdvertCampaigns->Companies->find('list', ['limit' => 200]);
        $this->set(compact('advertCampaign', 'advertisers', 'companies'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Advert Campaign id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        //Individual delete
        if (!empty($id)) {
            $advertCampaign = $this->AdvertCampaigns->get($id);
            if ($this->AdvertCampaigns->delete($advertCampaign)) {
                $this->Flash->success(__('The advert campaign has been deleted.'));
            } else {
                $this->Flash->error(__('The advert campaign could not be deleted. Please, try again.'));
            }
        } else { //Batch delete
            $list = $this->AdvertCampaigns->find('list');
            $entities = $this->AdvertCampaigns->patchEntities($list, $this->request->getData()['AdvertCampaigns']);
            if ($this->AdvertCampaigns->deleteMany($entities)) {
                $this->Flash->success(__('The advertCampaigns has been deleted.'));
            } else {
                $this->Flash->error(__('The advertCampaigns could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(['action' => 'index']);
    }

    public function advertStatistics($id)
    {
        if (!$this->AdvertCampaigns->AdvertReports->createReportsForEntity($id, 'AdvertCampaigns')) {
            $this->Flash->error("Erro ao gerar estatísticas");
        }

        $advertCampaign = $this->AdvertCampaigns->find()
            ->contain(['AdvertReports' => function ($q) {
                return $q->order(['AdvertReports.report_date ASC']);
            }])
            ->where(['AdvertCampaigns.id' => $id])
            ->firstOrFail();

        $advertReportsTotal = $this->AdvertCampaigns->AdvertReports->getTotalFromAdvertReports($advertCampaign->advert_reports);

        $this->set(compact('advertCampaign', 'advertReportsTotal'));
    }
}
