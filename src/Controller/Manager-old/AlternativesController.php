<?php
namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * Alternatives Controller
 *
 * @property \App\Model\Table\AlternativesTable $Alternatives
 *
 * @method \App\Model\Entity\Alternative[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AlternativesController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Questions']
        ];
        $alternatives = $this->paginate($this->Alternatives);

        $this->set(compact('alternatives'));
    }

    /**
     * View method
     *
     * @param string|null $id Alternative id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $alternative = $this->Alternatives->get($id, [
            'contain' => ['Questions', 'Participants']
        ]);

        $this->set('alternative', $alternative);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $alternative = $this->Alternatives->newEntity();
        if ($this->request->is('post')) {
            $alternative = $this->Alternatives->patchEntity($alternative, $this->request->getData());
            if ($this->Alternatives->save($alternative)) {
                $this->Flash->success(__('The alternative has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The alternative could not be saved. Please, try again.'));
        }
        $questions = $this->Alternatives->Questions->find('list', ['limit' => 200]);
        $this->set(compact('alternative', 'questions'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Alternative id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $alternative = $this->Alternatives->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $alternative = $this->Alternatives->patchEntity($alternative, $this->request->getData());
            if ($this->Alternatives->save($alternative)) {
                $this->Flash->success(__('The alternative has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The alternative could not be saved. Please, try again.'));
        }
        $questions = $this->Alternatives->Questions->find('list', ['limit' => 200]);
        $this->set(compact('alternative', 'questions'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Alternative id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete','get']);
        $alternative = $this->Alternatives->get($id);
        if ($this->Alternatives->delete($alternative)) {
            $this->Flash->success(__('The alternative has been deleted.'));
        } else {
            $this->Flash->error(__('The alternative could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
