<?php

declare(strict_types=1);

namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * AdvertClicks Controller
 *
 * @property \App\Model\Table\AdvertClicksTable $AdvertClicks
 *
 * @method \App\Model\Entity\AdvertClick[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdvertClicksController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->loadComponent('GrupoGrowPaginate.GrupoGrowPaginate');
        $query = $this->AdvertClicks->find();

        $query = $query->contain([
            'AdvertZones', 'AdvertCampaigns', 'Adverts', 'AdvertsAdvertZones',
        ]);
        $advertClicks = $this->GrupoGrowPaginate->paginate($query);

        $this->set(compact('advertClicks'));
    }

    /**
     * View method
     *
     * @param string|null $id Advert Click id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $advertClick = $this->AdvertClicks->get($id, [
            'contain' => ['AdvertZones', 'AdvertCampaigns', 'Adverts', 'AdvertsAdvertZones'],
        ]);

        $this->set('advertClick', $advertClick);
    }
}
