<?php
namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * MidiaKits Controller
 *
 * @property \App\Model\Table\MidiaKitsTable $MidiaKits
 *
 * @method \App\Model\Entity\MidiaKit[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MidiaKitsController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Companies']
        ];
        $midiaKits = $this->paginate($this->MidiaKits);

        $this->set(compact('midiaKits'));
    }

    /**
     * View method
     *
     * @param string|null $id Midia Kit id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $midiaKit = $this->MidiaKits->get($id, [
            'contain' => ['Companies']
        ]);

        $this->set('midiaKit', $midiaKit);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $midiaKit = $this->MidiaKits->newEntity();
        if ($this->request->is('post')) {
            $midiaKit = $this->MidiaKits->patchEntity($midiaKit, $this->request->getData());
            if ($this->MidiaKits->save($midiaKit)) {
                $this->Flash->success(__('The midia kit has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The midia kit could not be saved. Please, try again.'));
        }
        $companies = $this->MidiaKits->Companies->find('list', ['limit' => 200]);
        $this->set(compact('midiaKit', 'companies'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Midia Kit id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $midiaKit = $this->MidiaKits->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $midiaKit = $this->MidiaKits->patchEntity($midiaKit, $this->request->getData());
            if ($this->MidiaKits->save($midiaKit)) {
                $this->Flash->success(__('The midia kit has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The midia kit could not be saved. Please, try again.'));
        }
        $companies = $this->MidiaKits->Companies->find('list', ['limit' => 200]);
        $this->set(compact('midiaKit', 'companies'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Midia Kit id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete','get']);
        $midiaKit = $this->MidiaKits->get($id);
        if ($this->MidiaKits->delete($midiaKit)) {
            $this->Flash->success(__('The midia kit has been deleted.'));
        } else {
            $this->Flash->error(__('The midia kit could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
