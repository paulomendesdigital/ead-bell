<?php

namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * Winners Controller
 *
 * @property \App\Model\Table\WinnersTable $Winners
 *
 * @method \App\Model\Entity\Winner[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class WinnersController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index($content_id = null)
    {
        if (!empty($content_id)) {
            $this->paginate = [
                'contain' => ['Users', 'Contents'],
                'finder' => [
                    'winnersByPromotionId' => ['content_id' => $content_id]
                ]
            ];
        } else {
            $this->paginate = [
                'contain' => ['Users', 'Contents']
            ];
        }
        $winners = $this->paginate($this->Winners);

        $this->set(compact('winners'));
    }

    /**
     * View method
     *
     * @param string|null $id Winner id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $winner = $this->Winners->get($id, [
            'contain' => ['Users', 'Contents']
        ]);

        $this->set('winner', $winner);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($user_id = null, $content_id = null, $participant_id = null, $company_id = null)
    {
        if ($user_id and $participant_id and $content_id and $company_id) {
            $winner = $this->Winners->find()
                ->where([
                    'Winners.participant_id' => $participant_id,
                    'Winners.user_id' => $user_id,
                    'Winners.content_id' => $content_id,                
                    'Winners.company_id' => $company_id   
                ])
                ->first();
            if (empty($winner)) {
                $query = $this->Winners->query();
                $query->insert(['participant_id', 'user_id', 'content_id', 'company_id','created','modified'])
                    ->values([
                        'participant_id' => $participant_id,
                        'user_id' => $user_id,
                        'content_id' => $content_id,
                        'company_id' => $company_id,
                        'created' => date('Y-m-d H:i:s'),
                        'modified' => date('Y-m-d H:i:s')
                    ])
                    ->execute();
                $this->Flash->success(__('The {0} has been saved.', 'Vencedor'));
                return $this->redirect($this->referer());
            }
        } 

        $this->Flash->success(__('Não foi possível definir o vencedor, tente novamente', 'Vencedor'));
        return $this->redirect($this->referer());
    }

    /**
     * Edit method
     *
     * @param string|null $id Winner id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if ($this->request->is(array('ajax'))) {
            $this->viewBuilder()->setLayout('ajax');
        }

        $winner = $this->Winners->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $winner = $this->Winners->patchEntity($winner, $this->request->getData());
            if ($this->Winners->save($winner)) {
                
                $this->Flash->success(__('Instrução gravada com sucesso!', 'Vencedor'));
                return $this->redirect( $this->referer() );

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Vencedor'));
        }
        //$users = $this->Winners->Users->find('list', ['limit' => 200]);
        //$contents = $this->Winners->Contents->find('list', ['limit' => 200]);
        $this->set(compact('winner'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Winner id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete', 'get']);
        $winner = $this->Winners->get($id);
        if ($this->Winners->delete($winner)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Vencedor'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Vencedor'));
        }

        return $this->redirect( $this->referer() );
    }

    public function autoAdd($content_id)
    {
        $participant = $this->Winners->Contents->Participants->find()
            ->notMatching('Winners', function ($q) use ($content_id) {
                return $q->where([
                    'Winners.content_id' => $content_id
                ]);
            })
            ->notMatching('WinnersParticipantsUsers', function ($q) use ($content_id) {
                return $q->where([
                    'WinnersParticipantsUsers.content_id' => $content_id
                ]);
            })
            ->where([
                'Participants.content_id' => $content_id,
                'Participants.status' => 1
            ])
            ->order('rand()')
            ->first();
        
        if(!empty($participant)){
            $query = $this->Winners->query();
            $query->insert(['participant_id', 'user_id', 'content_id','company_id','created','modified'])
                ->values([
                    'participant_id' => $participant->id,
                    'user_id' => $participant->user_id,
                    'content_id' => $content_id,
                    'company_id' => $participant->company_id,
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                ])
                ->execute();
            $this->Flash->success(__('The {0} has been saved.', 'Vencedor'));
        }
        return $this->redirect($this->referer());
    }
}
