<?php

declare(strict_types=1);

namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;
use Cake\ORM\TableRegistry;

/**
 * Adverts Controller
 *
 * @property \App\Model\Table\AdvertsTable $Adverts
 *
 * @method \App\Model\Entity\Advert[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdvertsController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->loadComponent('GrupoGrowPaginate.GrupoGrowPaginate');
        $query = $this->Adverts->find();

        $query = $query->contain([
            'AdvertCampaigns',
        ]);
        $adverts = $this->GrupoGrowPaginate->paginate($query);
        $advertCampaigns = $this->Adverts->AdvertCampaigns->find('list');

        $this->set(compact('adverts', 'advertCampaigns'));
    }

    /**
     * View method
     *
     * @param string|null $id Advert id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $advert = $this->Adverts->get($id, [
            'contain' => [
                'AdvertCampaigns',
                'AdvertZones' => [
                    'Companies'
                ]
            ]
        ]);

        $this->set('advert', $advert);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        

        $advert = $this->Adverts->newEntity();
        if ($this->request->is('post')) {
        //die(debug($this->request->data));
        $advert = $this->Adverts->patchEntity($advert, $this->request->getData());
            if ($this->Adverts->save($advert)) {
                $this->Flash->success(__('The advert has been saved.'));

                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('The advert could not be saved. Please, try again.'));
        }
        $advertCampaigns = $this->Adverts->AdvertCampaigns->find('list', ['limit' => 200]);
        $companies = $this->Adverts->AdvertCampaigns->Companies->find('list')->toArray();
        $advertZonesResults = $this->Adverts->AdvertZones->find();

        $advertZones = [];

        foreach ($advertZonesResults as $key => $advertZone) {
            $advertZones[] = [
                'value' => $advertZone->id,
                'text' => $companies[$advertZone->company_id] . ' - ' . $advertZone->name,
                'data-width' => $advertZone->width,
                'data-height' => $advertZone->height
            ];
        }

        asort($advertZones);

        $this->set(compact('advert', 'advertCampaigns', 'advertZones'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Advert id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $advert = $this->Adverts->get($id, [
            'contain' => ['AdvertZones']
        ]);

        $advertsAdvertZonesTable = TableRegistry::getTableLocator()->get('AdvertsAdvertZones');
        $advertsAdvertZones = $advertsAdvertZonesTable->find()
        ->where([
            'AdvertsAdvertZones.advert_id' => $id
        ])
        ->toArray();

        $advert->adverts_advert_zones = $advertsAdvertZones;

        
        if ($this->request->is(['patch', 'post', 'put'])) {

            $requestData = $this->request->getData();        

            if(!empty($requestData['image']['error']) && !empty($advert->image)){
                unset($requestData['image']);
            } 

            foreach($advertsAdvertZones as $key => $advertAdvertZone){ 
                if( in_array( $advertsAdvertZones[$key]->advert_zone_id, $this->request->getData('advert_zones')['_ids']) ){
                    $advertsAdvertZones[$key]->status = 1;
                }else{
                    $advertsAdvertZones[$key]->status = 0;
                }
            }

            $advertsAdvertZonesTable->saveMany($advertsAdvertZones);
 
 
            $advert = $this->Adverts->patchEntity($advert, $requestData);
            if ($this->Adverts->save($advert)) {
                $this->Flash->success(__('The advert has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The advert could not be saved. Please, try again.'));
        }
        $advertCampaigns = $this->Adverts->AdvertCampaigns->find('list', ['limit' => 200]);
        $companies = $this->Adverts->AdvertCampaigns->Companies->find('list')->toArray();
        $advertZonesResults = $this->Adverts->AdvertZones->find();

        $advertZones = [];

        foreach ($advertZonesResults as $key => $advertZone) {
            $advertZones[] = [
                'value' => $advertZone->id,
                'text' => $companies[$advertZone->company_id] . ' - ' . $advertZone->name,
                'data-width' => $advertZone->width,
                'data-height' => $advertZone->height
            ];
        }

        asort($advertZones); 
        
        $this->set(compact('advert', 'advertCampaigns', 'advertZones'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Advert id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        //Individual delete
        if (!empty($id)) {
            $advert = $this->Adverts->get($id);
            if ($this->Adverts->delete($advert)) {
                $this->Flash->success(__('The advert has been deleted.'));
            } else {
                $this->Flash->error(__('The advert could not be deleted. Please, try again.'));
            }
        } else { //Batch delete
            $list = $this->Adverts->find('list');
            $entities = $this->Adverts->patchEntities($list, $this->request->getData()['Adverts']);
            if ($this->Adverts->deleteMany($entities)) {
                $this->Flash->success(__('The adverts has been deleted.'));
            } else {
                $this->Flash->error(__('The adverts could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(['action' => 'index']);
    }

    public function reports()
    {
        $topAdverts = $this->Adverts->find()
            ->contain(['AdvertCampaigns' => ['Companies']])
            ->where(['Adverts.unique_views_count > 0'])
            ->order(['Adverts.unique_views_count DESC'])
            ->limit(10);

        $topClickAdverts = $this->Adverts->find()
            ->contain(['AdvertCampaigns' => ['Companies']])
            ->where(['Adverts.unique_clicks_count > 0'])
            ->order(['Adverts.unique_clicks_count DESC'])
            ->limit(10);

        $topAdvertCampaigns = $this->Adverts->AdvertCampaigns->find()
            ->contain(['Companies'])
            ->where(['AdvertCampaigns.unique_views_count > 0'])
            ->order(['AdvertCampaigns.unique_views_count DESC'])
            ->limit(10);

        $topClickAdvertCampaigns = $this->Adverts->AdvertCampaigns->find()
            ->contain(['Companies'])
            ->where(['AdvertCampaigns.unique_clicks_count > 0'])
            ->order(['AdvertCampaigns.unique_clicks_count DESC'])
            ->limit(10);

        $topAdvertZones = $this->Adverts->AdvertZones->find()
            ->contain(['Companies'])
            ->where(['AdvertZones.unique_views_count > 0'])
            ->order(['AdvertZones.unique_views_count DESC'])
            ->limit(10);

        $topClickAdvertZones = $this->Adverts->AdvertZones->find()
            ->contain(['Companies'])
            ->where(['AdvertZones.unique_clicks_count > 0'])
            ->order(['AdvertZones.unique_clicks_count DESC'])
            ->limit(10);

        $conn = $this->Adverts->getConnection();

        $topAdvertViewDays = $conn->execute("
            SELECT *
              FROM (SELECT COUNT(DISTINCT sessionid) as count_views,
                           DATE(created) as view_date
                      FROM advert_views
                     GROUP
                        BY DATE(created)) av
             ORDER
                BY av.count_views DESC
             LIMIT 10;
        ")->fetchAll('assoc');

        $topAdvertClickDays = $conn->execute("
            SELECT *
              FROM (SELECT COUNT(DISTINCT sessionid) as count_clicks,
                           DATE(created) as click_date
                      FROM advert_clicks
                     GROUP
                        BY DATE(created)) ac
             ORDER
                BY ac.count_clicks DESC
             LIMIT 10;
        ")->fetchAll('assoc');

        $this->set(compact(
            'topAdverts',
            'topClickAdverts',
            'topAdvertCampaigns',
            'topClickAdvertCampaigns',
            'topAdvertViewDays',
            'topAdvertClickDays',
            'topAdvertZones',
            'topClickAdvertZones'
        ));
    }
}
