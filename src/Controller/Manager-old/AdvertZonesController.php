<?php

declare(strict_types=1);

namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * AdvertZones Controller
 *
 * @property \App\Model\Table\AdvertZonesTable $AdvertZones
 *
 * @method \App\Model\Entity\AdvertZone[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdvertZonesController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->loadComponent('GrupoGrowPaginate.GrupoGrowPaginate');
        $query = $this->AdvertZones->find();

        $query = $query->contain([
            'Companies',
        ]);
        $advertZones = $this->GrupoGrowPaginate->paginate($query);
        $companies = $this->AdvertZones->Companies->find('list');

        $this->set(compact('advertZones', 'companies'));
    }

    /**
     * View method
     *
     * @param string|null $id Advert Zone id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $advertZone = $this->AdvertZones->get($id, [
            'contain' => ['Companies', 'Adverts' => ['AdvertCampaigns']]
        ]);

        $this->set('advertZone', $advertZone);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $advertZone = $this->AdvertZones->newEntity();
        if ($this->request->is('post')) {
            $advertZone = $this->AdvertZones->patchEntity($advertZone, $this->request->getData());
            if ($this->AdvertZones->save($advertZone)) {
                $this->Flash->success(__('The advert zone has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The advert zone could not be saved. Please, try again.'));
        }
        $companies = $this->AdvertZones->Companies->find('list', ['limit' => 200]);
        $this->set(compact('advertZone', 'companies'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Advert Zone id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $advertZone = $this->AdvertZones->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $advertZone = $this->AdvertZones->patchEntity($advertZone, $this->request->getData());
            if ($this->AdvertZones->save($advertZone)) {
                $this->Flash->success(__('The advert zone has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The advert zone could not be saved. Please, try again.'));
        }
        $companies = $this->AdvertZones->Companies->find('list', ['limit' => 200]);
        $this->set(compact('advertZone', 'companies'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Advert Zone id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        //Individual delete
        if (!empty($id)) {
            $advertZone = $this->AdvertZones->get($id);
            if ($this->AdvertZones->delete($advertZone)) {
                $this->Flash->success(__('The advert zone has been deleted.'));
            } else {
                $this->Flash->error(__('The advert zone could not be deleted. Please, try again.'));
            }
        } else { //Batch delete
            $list = $this->AdvertZones->find('list');
            $entities = $this->AdvertZones->patchEntities($list, $this->request->getData()['AdvertZones']);
            if ($this->AdvertZones->deleteMany($entities)) {
                $this->Flash->success(__('The advertZones has been deleted.'));
            } else {
                $this->Flash->error(__('The advertZones could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(['action' => 'index']);
    }
}
