<?php

namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * HomeLayouts Controller
 *
 * @property \App\Model\Table\HomeLayoutsTable $HomeLayouts
 *
 * @method \App\Model\Entity\HomeLayout[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HomeLayoutsController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Companies']
        ];
        $homeLayouts = $this->paginate($this->HomeLayouts);

        $this->set(compact('homeLayouts'));
    }

    /**
     * View method
     *
     * @param string|null $id Home Layout id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $homeLayout = $this->HomeLayouts->get($id, [
            'contain' => ['Companies', 'HomeLayoutItems']
        ]);

        $this->set('homeLayout', $homeLayout);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($company_code='RMIX')
    {
        $homeLayout = $this->HomeLayouts->newEntity();
        if ($this->request->is('post')) {

            $homeLayout = $this->HomeLayouts->patchEntity($homeLayout, $this->request->getData());
            if ($this->HomeLayouts->save($homeLayout)) {
                $this->Flash->success(__('The {0} has been saved.','Layout da Home'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.','Layout da Home'));
        }
        
        $companies = $this->HomeLayouts->Companies->find('list', ['limit' => 200])->where(['Companies.status' => 1,'Companies.code'=>$company_code]);
        $companies_array = $companies->toArray();
        
        $company_id = array_key_first($companies_array) ? array_key_first($companies_array) : null;
        $content_category = $this->HomeLayouts->HomeLayoutItems->Contents->ContentCategories->findByCode('DESTAQUE');
        
        if(!empty($company_id)){
            $content_category = $content_category->where(['ContentCategories.company_id' => $company_id]);
        }
        $content_category = $content_category->first();
        
        $contents = $this->HomeLayouts->HomeLayoutItems->Contents->find('list')
            ->where([
                'Contents.status' => 1,
                'Contents.company_id' => $company_id
            ]);

        $this->set(compact('homeLayout', 'companies', 'contents', 'company_code'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Home Layout id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $homeLayout = $this->HomeLayouts->get($id, [
            'contain' => [
                'HomeLayoutItems' => [
                    'sort' => [
                        'HomeLayoutItems.row',
                        'HomeLayoutItems.ordination'
                    ],
                    'Contents'
                ]
            ]
        ]);
        
        $homeLayout->start = $this->HomeLayouts->dateFormatAfterFind($homeLayout->start, true);
        $homeLayout->finish = $this->HomeLayouts->dateFormatAfterFind($homeLayout->finish, true);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
 
            $homeLayout = $this->HomeLayouts->patchEntity($homeLayout, $this->request->getData());
            if ($this->HomeLayouts->save($homeLayout)) {
                $this->Flash->success(__('The {0} has been saved.','Layout da Home'));

                if( isset($this->request->getData()['refer']) ){
                    return $this->redirect( $this->referer() );
                }

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.','Layout da Home'));
        }
        
        //$companies = $this->HomeLayouts->Companies->find('list', ['limit' => 200]);
        $companies = $this->HomeLayouts->Companies->find('list', ['limit' => 200])->where(['Companies.status' => 1,'Companies.id'=>$homeLayout->company_id]);
        $companies_array = $companies->toArray();
        $company_id = array_key_first($companies_array) ? array_key_first($companies_array) : null;

        /*$content_category = $this->HomeLayouts->HomeLayoutItems->Contents->ContentCategories
            ->findByCode('DESTAQUE')
            ->where(['ContentCategories.company_id' => $homeLayout->company_id])
            ->first();*/
        $contents = $this->HomeLayouts->HomeLayoutItems->Contents->find('list')->where(['Contents.status' => 1, 'Contents.company_id' => $company_id]);
           // ->where(['Contents.status' => 1, 'Contents.content_category_id' => $content_category->id]);
        $this->set(compact('homeLayout', 'companies', 'contents'));

        //Formata os itens do layout para facilitar a criação da view
        $homeLayoutItems = [];
        $homeLayout = $homeLayout->toArray();
        foreach ($homeLayout['home_layout_items'] as $item) {
            $homeLayoutItems[$item['row'] - 1][$item['ordination'] - 1] = $item;
        }
        $this->set('homeLayoutItems', $homeLayoutItems);
    }

    /**
     * Delete method
     *
     * @param string|null $id Home Layout id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete','get']);
        $homeLayout = $this->HomeLayouts->get($id);
        if ($this->HomeLayouts->delete($homeLayout)) {
            $this->Flash->success(__('The {0} has been deleted.','Layout da Home'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.','Layout da Home'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
