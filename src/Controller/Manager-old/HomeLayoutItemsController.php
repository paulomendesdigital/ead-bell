<?php
namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * HomeLayoutItems Controller
 *
 * @property \App\Model\Table\HomeLayoutItemsTable $HomeLayoutItems
 *
 * @method \App\Model\Entity\HomeLayoutItem[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HomeLayoutItemsController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['HomeLayouts', 'Contents']
        ];
        $homeLayoutItems = $this->paginate($this->HomeLayoutItems);

        $this->set(compact('homeLayoutItems'));
    }

    /**
     * View method
     *
     * @param string|null $id Home Layout Item id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $homeLayoutItem = $this->HomeLayoutItems->get($id, [
            'contain' => ['HomeLayouts', 'Contents']
        ]);

        $this->set('homeLayoutItem', $homeLayoutItem);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $homeLayoutItem = $this->HomeLayoutItems->newEntity();
        if ($this->request->is('post')) {
            $homeLayoutItem = $this->HomeLayoutItems->patchEntity($homeLayoutItem, $this->request->getData());
            if ($this->HomeLayoutItems->save($homeLayoutItem)) {
                $this->Flash->success(__('The home layout item has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The home layout item could not be saved. Please, try again.'));
        }
        $homeLayouts = $this->HomeLayoutItems->HomeLayouts->find('list', ['limit' => 200]);
        $contents = $this->HomeLayoutItems->Contents->find('list', ['limit' => 200]);
        $this->set(compact('homeLayoutItem', 'homeLayouts', 'contents'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Home Layout Item id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $homeLayoutItem = $this->HomeLayoutItems->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $homeLayoutItem = $this->HomeLayoutItems->patchEntity($homeLayoutItem, $this->request->getData());
            if ($this->HomeLayoutItems->save($homeLayoutItem)) {
                $this->Flash->success(__('The home layout item has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The home layout item could not be saved. Please, try again.'));
        }
        $homeLayouts = $this->HomeLayoutItems->HomeLayouts->find('list', ['limit' => 200]);
        $contents = $this->HomeLayoutItems->Contents->find('list', ['limit' => 200]);
        $this->set(compact('homeLayoutItem', 'homeLayouts', 'contents'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Home Layout Item id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete','get']);
        $homeLayoutItem = $this->HomeLayoutItems->get($id);
        if ($this->HomeLayoutItems->delete($homeLayoutItem)) {
            $this->Flash->success(__('The home layout item has been deleted.'));
        } else {
            $this->Flash->error(__('The home layout item could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
