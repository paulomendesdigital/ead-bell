<?php

declare(strict_types=1);

namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * AdvertsAdvertZones Controller
 *
 * @property \App\Model\Table\AdvertsAdvertZonesTable $AdvertsAdvertZones
 *
 * @method \App\Model\Entity\AdvertsAdvertZone[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdvertsAdvertZonesController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->loadComponent('GrupoGrowPaginate.GrupoGrowPaginate');
        $query = $this->AdvertsAdvertZones->find();

        $query = $query->contain([
            'Adverts', 'AdvertZones',
        ]);
        $advertsAdvertZones = $this->GrupoGrowPaginate->paginate($query);

        $adverts = $this->AdvertsAdvertZones->Adverts->find('list');
        $companies = $this->AdvertsAdvertZones->AdvertZones->Companies->find('list')->toArray();
        $advertZones = $this->AdvertsAdvertZones->AdvertZones->find('list', [
            'valueField' => function ($e) use ($companies) {
                return $companies[$e->company_id] . " - " . $e->name;
            }
        ]);

        $this->set(compact('advertsAdvertZones', 'adverts', 'advertZones'));
    }

    /**
     * View method
     *
     * @param string|null $id Adverts Advert Zone id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $advertsAdvertZone = $this->AdvertsAdvertZones->get($id, [
            'contain' => ['Adverts', 'AdvertZones']
        ]);

        $this->set('advertsAdvertZone', $advertsAdvertZone);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $advertsAdvertZone = $this->AdvertsAdvertZones->newEntity();
        if ($this->request->is('post')) {
            $advertsAdvertZone = $this->AdvertsAdvertZones->patchEntity($advertsAdvertZone, $this->request->getData());
            if ($this->AdvertsAdvertZones->save($advertsAdvertZone)) {
                $this->Flash->success(__('The adverts advert zone has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The adverts advert zone could not be saved. Please, try again.'));
        }
        $adverts = $this->AdvertsAdvertZones->Adverts->find('list', ['limit' => 200]);
        $advertZones = $this->AdvertsAdvertZones->AdvertZones->find('list', ['limit' => 200]);
        $this->set(compact('advertsAdvertZone', 'adverts', 'advertZones'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Adverts Advert Zone id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $advertsAdvertZone = $this->AdvertsAdvertZones->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $advertsAdvertZone = $this->AdvertsAdvertZones->patchEntity($advertsAdvertZone, $this->request->getData());
            if ($this->AdvertsAdvertZones->save($advertsAdvertZone)) {
                $this->Flash->success(__('The adverts advert zone has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The adverts advert zone could not be saved. Please, try again.'));
        }
        $adverts = $this->AdvertsAdvertZones->Adverts->find('list', ['limit' => 200]);
        $advertZones = $this->AdvertsAdvertZones->AdvertZones->find('list', ['limit' => 200]);
        $this->set(compact('advertsAdvertZone', 'adverts', 'advertZones'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Adverts Advert Zone id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        //Individual delete
        if (!empty($id)) {
            $advertsAdvertZone = $this->AdvertsAdvertZones->get($id);
            if ($this->AdvertsAdvertZones->delete($advertsAdvertZone)) {
                $this->Flash->success(__('The adverts advert zone has been deleted.'));
            } else {
                $this->Flash->error(__('The adverts advert zone could not be deleted. Please, try again.'));
            }
        } else { //Batch delete
            $list = $this->AdvertsAdvertZones->find('list');
            $entities = $this->AdvertsAdvertZones->patchEntities($list, $this->request->getData()['AdvertsAdvertZones']);
            if ($this->AdvertsAdvertZones->deleteMany($entities)) {
                $this->Flash->success(__('The advertsAdvertZones has been deleted.'));
            } else {
                $this->Flash->error(__('The advertsAdvertZones could not be deleted. Please, try again.'));
            }
        }

        return $this->redirect(['action' => 'index']);
    }
}
