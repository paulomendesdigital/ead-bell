<?php
namespace App\Controller\Manager;

use App\Controller\Manager\ManagerAppController;

/**
 * Speakers Controller
 *
 * @property \App\Model\Table\SpeakersTable $Speakers
 *
 * @method \App\Model\Entity\Speaker[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SpeakersController extends ManagerAppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $likeFields = ['name', 'title', 'subtitle'];
        $conditions['Speakers.status >='] = 0;
        $conditions = $this->__getConditionsForSearch($conditions, 'Speakers', $likeFields);

        $this->paginate = [
            'contain' => ['Companies'],
            'conditions'=>[
                $conditions
            ],
            'order'=>['Speakers.id'=>'DESC']
        ];
        $speakers = $this->paginate($this->Speakers);

        $companies = $this->Speakers->Companies->find('list');
        $this->set(compact('speakers','companies'));
    }

    /**
     * View method
     *
     * @param string|null $id Speaker id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $speaker = $this->Speakers->get($id, [
            'contain' => ['Companies']
        ]);

        $this->set('speaker', $speaker);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $speaker = $this->Speakers->newEntity();
        if ($this->request->is('post')) {
            $speaker = $this->Speakers->patchEntity($speaker, $this->request->getData());
            if ($this->Speakers->save($speaker)) {
                $this->Flash->success(__('The {0} has been saved.','Locutor'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.','Locutor'));
        }
        $image_size = $this->Speakers::IMAGE_SIZE;
        $companies = $this->Speakers->Companies->find('list', ['limit' => 200]);
        $this->set(compact('speaker', 'companies','image_size'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Speaker id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $speaker = $this->Speakers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $speaker = $this->Speakers->patchEntity($speaker, $this->request->getData());
            if ($this->Speakers->save($speaker)) {
                $this->Flash->success(__('The {0} has been saved.','Locutor'));
                if( isset($this->request->getData()['refer']) ){
                    return $this->redirect( $this->referer() );
                }
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The {0} could not be saved. Please, try again.','Locutor'));
        }
        $image_size = $this->Speakers::IMAGE_SIZE;
        $companies = $this->Speakers->Companies->find('list', ['limit' => 200]);
        $this->set(compact('speaker', 'companies','image_size'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Speaker id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete','get']);
        $speaker = $this->Speakers->get($id);
        if ($this->Speakers->delete($speaker)) {
            $this->Flash->success(__('The {0} has been deleted.','Locutor'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.','Locutor'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
