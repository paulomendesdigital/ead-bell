<?php 
/**
 * @copyright Copyright 2018
 * @author Dayvison Silva - www.grupogrow.com.br
 * Page Index View
 *
*/
use Cake\Core\Configure;
 
$domain = Configure::read('Corporate.Website');
 
?>
<!DOCTYPE html>
<html class="no-js">
<head>
    <?php if( Configure::read('Corporate.Google.Mix.Analytics') ):?>
        <!-- Global site tag (gtag.js) - Google Analytics --> 
        <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo Configure::read('Corporate.Google.Mix.Analytics');?>"></script> 
        <script> window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '<?php echo Configure::read('Corporate.Google.Mix.Analytics');?>'); 
        </script>
    <?php endif;?>

    <?php if( Configure::read('Corporate.Facebook.Paradiso.Pixel') ):?>
        <!-- Facebook Pixel Code -->
        <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '<?php echo Configure::read('Corporate.Facebook.Paradiso.Pixel');?>');
          fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
          src="https://www.facebook.com/tr?id=<?php echo Configure::read('Corporate.Facebook.Paradiso.Pixel');?>&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->
    <?php endif;?>

    <?php if( Configure::read('Corporate.OneSignal.Paradiso.AppId') ):?>
        <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
        <script>
          var OneSignal = window.OneSignal || [];
          OneSignal.push(function() {
            OneSignal.init({
              appId: "<?php echo Configure::read('Corporate.OneSignal.Paradiso.AppId');?>",
            });
          });
        </script>
    <?php endif;?>
    
    <title>SulAmérica Paradiso | Rádio SulAmérica Paradiso FM</title>
    <meta charset="utf-8">
    <meta name="language" content="pt-BR">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="/assets/img/sul57.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="/assets/img/sul57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/assets/img/sul60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/img/sul72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/img/sul76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/img/sul114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/img/sul120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/img/sul152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/img/sul180.png">
    <meta name="description" content="O Portal de música, diversão, promoção e interatividade.">
    <meta name="og:url" content="sulamericaparadiso.com.br/">
    <meta name="og:description" content="O Portal de música, diversão, promoção e interatividade.">
    <meta name="og:site_name" content="Rádio SulAmérica Paradiso FM">
    <meta name="og:title" content="SulAmérica Paradiso | Rádio SulAmérica Paradiso FM">
    <meta name="og:type" content="website">
    <meta name="og:image" content="https://api.dialbrasil.com.br/files/Contents/image_large_name/20200409165004Mix-Festival-Em-Casa.jpg">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="fb:admins" content="@121918697858146">
    <meta name="twitter:creator" content="@sparadiso">
    <meta name="twitter:site" content="@sparadiso">
    <meta name="twitter:image" content="https://api.dialbrasil.com.br/files/Contents/image_large_name/20200409165004Mix-Festival-Em-Casa.jpg">
    <meta name="twitter:title" content="SulAmérica Paradiso | Rádio SulAmérica Paradiso FM">
    <meta name="twitter:description" content="O Portal de música, diversão, promoção e interatividade.">
    <meta name="twitter:url" content="sulamericaparadiso.com.br/">

    <!-- JQUERY -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <!-- EASING -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js" integrity="sha256-H3cjtrm/ztDeuhCN9I4yh4iN2Ybx/y1RM7rMmAesA0k=" crossorigin="anonymous"></script>

    <!-- SLICK SLIDER -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="/src/css/slick-theme.css"/>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>


    <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


    <!-- ICONES -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">


    <!-- FONTES -->
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,700&display=swap" rel="stylesheet">


    <!-- MASK -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous"></script>

    
	<script type="text/javascript">
      var verifyCallback = function(response) {
        var response = document.getElementById('response-recaptcha');
        response.checked = true;
      };


      var onloadCallback = function() {
        // Renders the HTML element with id 'example1' as a reCAPTCHA widget.
        // The id of the reCAPTCHA widget is assigned to 'widgetId1'.
        grecaptcha.render('g-recaptcha', {
          'sitekey' : '6LfxvLUUAAAAAPnsXqGuW3txbF5FBIH0ZGye4L1h',
          'callback' : verifyCallback
        });


      };
    </script>

        
    <!-- MAIN -->
    <link rel="stylesheet" href="<?=$domain?>/css/landing/style-paradiso.css" />
    
    <!-- SweetAlert-->
    <?php echo $this->Html->css('sweetalert/sweetalert');?>
    <?php echo $this->Html->script('sweetalert/sweetalert.min');?>
    <?php echo $this->Html->script('sweetalert/jquery.sweet-alert.custom');?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-168886259-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-168886259-1');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '707980806461043');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=707980806461043&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

    <script>
    $(function () {
     
        $('.phone-contato').mask('(00) 00000-0000');
        $('.cpf-contato').mask('000.000.000-00');

        $(document).on('submit', 'form', function(){

            var csrfToken = <?= json_encode($this->request->getParam('_csrfToken')) ?>;
            
            console.log($(this));

            $.ajax({

                url: `<?=$domain?>/api/newsletters/add/?company_code=SPARADISO`,
                method: 'POST',
                headers: { 'X-CSRF-Token': csrfToken },
                data: $(this).serializeArray(),
                success: function(res){
                    console.log('success', res);
                    $('.text-form').fadeOut(function(){
                        $('.success-form').fadeIn();
                    });
                    gtag('event', 'conversion', {'send_to': 'AW-698101029/gqTiCJK-3egBEKXa8MwC'});
                },
                error: function(error){
                    console.log('error', error);
                    $('.text-form').fadeOut(function(){
                        $('.error-form').fadeIn();
                    });
                }

            });

            $( document ).ajaxStart(function(){ $('#loading-lead').fadeIn(); });
            $( document ).ajaxStop(function(){ $('#loading-lead').fadeOut(); });

            return false;
        });
        
        
    });
    </script>

</head>
    <body id="" style="overflow-x: hidden;">
        <?= $this->Flash->render() ?>

        <div class="container-fluid">
            <div class="row align-items-center bg-white">
                

                <div class="col-md-8 desc  order-md-1 img-landing v1" style="">
                  
                </div>

                <div class="col-md-4 form-content" >
                    <div id="loading-lead"><img src="/img/loading.svg" alt=""></div>
                    
                    <div class=" text-center  px-md-4 px-2 py-5 py-md-4 ">
                        
                        <div class="pb-3 px-4">
                        <div style="width: 100%; max-width: 300px; margin: 0 auto;">
                            <a href="/"><img src="https://sulamericaparadiso.com.br/assets/img/logo_horizontal_colorida.png" class="img-fluid" alt=""></a>
                        </div>
                        </div>

                        <div class="success-form py-5" style="display: none;">
    
                            <h4 class="pt-2 mb-0">Obrigado!</h4>
                            <h6 class="font-weight-light text-muted" style="text-transform: initial;">Nosso time comercial irá entrar em contato o mais breve possível.</h6> <br>
                            <a href="/" class="btn btn-primary btn-lg btn-block mt-2 mb-2 font-weight-bold">Ir para o site.</a>
                        </div>

                        <div class="error-form py-5" style="display: none;">
                            <h5 class="pt-2 mb-0">Oops! <br> não conseguimos enviar seu contato.</h5>
                            <small class="font-weight-light text-muted">tente novamente mais tarde! </small>
                            <a href="/" class="btn btn-primary btn-lg btn-block mt-2 mb-2 font-weight-bold">ou entre no nosso site.</a>
                        </div>

                        <div class="text-form"> 
                            
                            <h5 class="pt-2 mb-0 pt-2">ANUNCIE NA <br> SULAMÉRICA PARADISO FM  </h5>
                            <h6 class="font-weight-light mb-0 pt-3">PREENCHA O FORMULÁRIO <br> PARA GARANTIR O SEU DESCONTO <span style="text-decoration: line-through;"></span></h6>

                            <!-- <h2 class="pt-0 text-dark">R$ 39,90</h2> -->
                            
                            <div class="container" style="max-width: 450px;">
                            <?php echo $this->Form->create('Newsletter',['url'=>['controller'=>'newsletters', 'action'=>'add'], 'class'=>'mt-4']);?>
                                <div class="form-group">
                                    <?php echo $this->Form->input('name',['placeholder'=>'Seu Nome','class'=>'form-control','div'=>false,'label'=>false,'required'=>true]);?>
                                    <?php echo $this->Form->input('company_id',['type'=>'hidden','value'=>'2']);?>
                                    <?php echo $this->Form->input('optin',['type'=>'hidden','value'=>'1']);?>
                                    <?php echo $this->Form->input('version',['type'=>'hidden','value'=>'1']);?>
                                    <?php echo $this->Form->input('origin',['type'=>'hidden','value'=>'LANDING_PARADISO']);?>
                                    <?php echo $this->Form->input('landing_title',['type'=>'hidden','value'=>'BLACK NOVEMBER']);?>
                                </div>

                                <div class="form-group">
                                    <?php echo $this->Form->input('email',['type'=>'email','placeholder'=>'Seu Email','class'=>'form-control','div'=>false,'label'=>false,'required'=>true]);?>
                                </div>
                                
                                <div class="form-group">
                                    <?php echo $this->Form->input('cellphone',['required' => true, 'type'=>'tel','placeholder'=>'Seu Telefone','class'=>'form-control phone-contato','div'=>false,'label'=>false]);?>
                                </div>
                                
                                <div class="form-group">
                                    <div class="" style="width: 314px; margin: 0 auto; position: relative;">
                                        <div id="g-recaptcha"></div>
                                    <!--  required=""  -->    <input type="checkbox" id="response-recaptcha" style="position: absolute; left: 36px; top: 34px;  z-index: -1;">
                                    </div>
                                </div>
                                
                                <button type="submit" class="btn btn-primary btn-lg btn-block mt-2 mb-2 font-weight-bold">QUERO ANUNCIAR</button>
                                
                            </form>
                            </div>
                        </div>

                            <h4  class="pt-4"> 
                                <?php if (!empty($Sistems['YouTube']['link'])) { ?>
                                    <a class="text-theme" target="_blank" href='<?php echo $Sistems['YouTube']['link'];?>'><i class="fab fa-youtube mr-3"></i></a>
                                <?php } ?>
                                <?php if (!empty($Sistems['Facebook']['link'])) { ?>
                                    <a class="text-theme" target="_blank" href='<?php echo $Sistems['Facebook']['link'];?>'><i class="fab fa-facebook-square mr-3"></i></a>
                                <?php } ?>
                                <?php if (!empty($Sistems['Instagram']['link'])) { ?>
                                    <a class="text-theme" target="_blank" href='<?php echo $Sistems['Instagram']['link'];?>'><i class="fab fa-instagram mr-3"></i></a>
                                <?php } ?>
                            </h4>
                    </div>
 
                    
                </div>
            </div>
        </div> 
        <!-- RECAPTCHA -->
        <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
    </body>
</html>