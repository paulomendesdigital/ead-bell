    <div class='row'>
        <div class="col-sm-10">
            <?php echo $this->Form->control('advertiser_id', ['options' => $advertisers, 'id' => 'advertiser-select', 'class' => 'form-control', 'empty' => __('Select'), 'label' => __('Advertiser')]); ?>
        </div> 
        <div class="col-sm-2">
            <label for="company-id"></label>
            <button class="btn btn-block btn-primary" data-toggle="modal"  data-form="#add-advertiser" data-target="#modalDinamic"  data-content-modal-url="/manager/advertisers/add" type="button" id="add-advertiser"> <b>+</b></button>
        </div> 
    </div>
    <div class='row'>
        <div class="col-sm-12">
            <?php echo $this->Form->control('company_id', ['options' => $companies, 'class' => 'form-control', 'empty' => __('Select'), 'label' => __('Company')]); ?>
        </div> 
    </div>
    <div class='row'>
        <div class="col-md-12">
            <?php echo $this->Form->control('name', ['class' => 'form-control input-lg', 'label' => __('Name')]); ?>
        </div> 
    </div>
    <div class='row'>
        <div class="col-md-6">
            <?php echo $this->Form->control('start_date', ['empty' => true, 'class' => 'form-control', 'label' => __('Start Date'), 'data-content' => "datetimepicker", 'type' => 'text']); ?>
        </div>
        <div class="col-md-6">
            <?php echo $this->Form->control('finish_date', ['empty' => true, 'class' => 'form-control', 'label' => __('Finish Date'), 'data-content' => "datetimepicker", 'type' => 'text']); ?>
        </div> 
    </div>
    <div class='row'>
        <div class="col-md-6">
            <?php echo $this->Form->control('max_views', ['class' => 'form-control input-lg', 'label' => __('Max Views')]); ?>
        </div>
        <div class="col-md-6">
            <?php echo $this->Form->control('weight', ['class' => 'form-control input-lg', 'min' => '0', 'max' => '10', 'label' => __('Weight')]); ?>
        </div>
    </div>
    <div class='row hidden'>
        <div class="col-md-6">
            <?php echo $this->Form->control('max_unique_views', ['class' => 'form-control input-lg', 'label' => __('Max Unique Views')]); ?>
        </div> 
        <div class="col-md-6">
            <?php echo $this->Form->control('max_clicks', ['class' => 'form-control input-lg', 'label' => __('Max Clicks')]); ?>
        </div>
        <div class="col-md-6">
            <?php echo $this->Form->control('max_unique_clicks', ['class' => 'form-control input-lg', 'label' => __('Max Unique Clicks')]); ?>
        </div> 
    </div>
    <div class='row'>
        <div class="col-md-6">
            <?php if (empty($statuses)) { ?>

                <?php

                $this->Form->setTemplates([
                    'nestingLabel' => '{{hidden}}<label{{attrs}}>{{text}}</label>{{input}}'
                ]);

                echo $this->Form->control('status', array('label' => __('Status'), 'type' => 'checkbox', 'class' => 'switch', 'data-on' => 'success', 'data-off' => 'danger', 'data-on-label' => __('Yes'), 'data-off-label' => __('No'), 'required' => false));

                ?>
            <?php } else { ?>
                <?php echo $this->Form->control('status', ['class' => 'form-control input-lg', 'label' => __('Status'), 'empty' => __('Select')]); ?>
            <?php } ?>
        </div>
    </div>

    <div>
        <hr>
    </div>

    <div class="clearfix"></div>
    <button class="btn btn-primary" type="submit" data-hide="modal"><?php echo __('Create'); ?></button>
    <button class="btn btn-info" type="submit" name="refer"><?php echo __('Create'); ?> <?php echo __('and continue'); ?></button>