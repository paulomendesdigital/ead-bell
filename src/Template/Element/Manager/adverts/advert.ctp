<div class="panel-body">
    <?php //@TODO In order to always have file upload support when model has a field named image or photo?>
    <?= $this->Form->create($advert, ['type' => 'file']) ?>

        <div class='col-md-6 col-xs-12'>
            <div class='row'>
                <div class="col-sm-12">
                    <?php echo $this->Form->control('advert_campaign_id', ['options' => $advertCampaigns, 'class' => 'form-control', 'empty' => __('Select'), 'label' => __('Advert Campaign')]); ?>
                </div>
            </div>
            <div class='row'>
                <div class="col-sm-12">
                    <?php echo $this->Form->control('name', ['class' => 'form-control input-lg', 'label' => __('Name')]); ?>
                </div>
            </div>
            <div class='row'>
                <div class="col-sm-12">
                    <?php echo $this->Form->control('url', ['class' => 'form-control input-lg', 'label' => __('Url')]); ?>
                </div>
            </div>
            
            <div class='row'>
                <div class="col-sm-12">
                    <?php if (empty($statuses)) { ?>

                        <?php

                        $this->Form->setTemplates([
                            'nestingLabel' => '{{hidden}}<label{{attrs}}>{{text}}</label>{{input}}'
                        ]);

                        echo $this->Form->control('status', array('label' => __('Status'), 'type' => 'checkbox', 'class' => 'switch', 'data-on' => 'success', 'data-off' => 'danger', 'data-on-label' => __('Yes'), 'data-off-label' => __('No'), 'required' => false));

                        ?>
                    <?php } else { ?>
                        <?php echo $this->Form->control('status', ['class' => 'form-control input-lg', 'label' => __('Status'), 'empty' => __('Select')]); ?>
                    <?php } ?>
                </div>
            </div>
            <div class='row'>
                <div class="col-sm-12">
                    <?php //echo $this->Form->control('advert_zones._ids', ['options' => $advertZones, 'class' => 'form-control', 'label' => __('')]); ?>
                </div>
            </div>
            <hr>                        
            <small>Zonas</small>
            <div class='row'>
                <div class="col-sm-12" id="advert-zones-ids">
                        
                        <?php 
                        $advertZone_ids = [];
                        if(!empty($advert->adverts_advert_zones)){

                            foreach($advert->adverts_advert_zones as $zone){
                                if($zone['status'] == 1){
                                    $advertZone_ids[] = $zone['advert_zone_id'];
                                }
                            }
                        }

                        //die(debug($advert->adverts_advert_zones ));
                        ?>
                        <?php foreach($advertZones as $advertZone){
                            ?>
                            <div style="display: flex; padding-top: 5px; align-items: center;">
                                <?= $this->Form->checkbox("advert_zones._ids[]", array(
                                    'value' => $advertZone['value'], 
                                    'id' => 'id-'.$advertZone['value'],
                                    'class' => 'switch', 
                                    'data-on' => 'success', 
                                    'data-off' => 'danger', 
                                    'data-on-label' => __('Yes'), 
                                    'data-off-label' => __('No'), 
                                    'required' => false,
                                    'checked' => in_array($advertZone['value'],  $advertZone_ids)
                                    )) ?>          
                            <label style="padding-left: 15px" for="id-<?=$advertZone['value']?>"><h4 style="padding: 0; margin: 0;"> <?=$advertZone['text']?> </h4></label>
                            </div>
                        <?php } ?>
                        <?php //foreach($advertZones as $advertZone){ ?>
                        <?php //$checked = $advertZone->has('adverts'); ?>
                        <?php //} ?>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xs-12">
            <h5 class="text-center"><b>Anúncio em forma de imagem ou em forma de html</b></h5>
            <hr />
            <div class='row'>
                <div class="col-sm-12">
                    <?php echo $this->Form->control('image', ['class' => 'form-control input-lg', 'label' => __('Image'), 'type' => 'file']); ?>
                </div>
            </div>
            <?php if(!empty($advert->image)){ ?>
                <br>
                <?= $this->Html->image($advert->image_dir . DS . $advert->image, ['style' => 'max-width: 100%']) ?>
            <?php } ?>
            <div class='row'>
                <div class="col-sm-12">
                    <h1 class="text-center">OU</h1>
                </div>
            </div>
            <div class='row'>
                <div class="col-sm-12">
                    <?php echo $this->Form->control('url_html', ['class' => 'form-control input-lg', 'label' => __('Url do Html'), 'type' => 'text']); ?>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <hr>                        
            <div class="clearfix"></div>
            <button class="btn btn-primary" type="submit" data-hide="modal"><?php echo __('Edit'); ?></button>
            <button class="btn btn-info" type="submit" name="refer"><?php echo __('Edit'); ?> <?php echo __('and continue'); ?></button>
        </div>
    <?= $this->Form->end() ?>
</div>