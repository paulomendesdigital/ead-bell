<?php
/**
 * @copyright Copyright 2019
 * @author Dayvison Silva- www.grupogrow.com.br
 * Element GrupoGrowManager Pagination
 *
*/
?>

<div class="paginator">
  <ul class="pagination">
      <?= $this->Paginator->first('<i class="fa fa-angle-double-left" aria-hidden="true"></i> ' . __('first'),['escape'=>false]) ?>
      <?= $this->Paginator->prev('<i class="fa fa-arrow-circle-left" aria-hidden="true"></i> ' . __('previous'),['escape'=>false]) ?>
      <?= $this->Paginator->numbers() ?>
      <?= $this->Paginator->next(__('próximo') . ' <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>',['escape'=>false]) ?>
      <?= $this->Paginator->last(__('último') . ' <i class="fa fa-angle-double-right" aria-hidden="true"></i>',['escape'=>false]) ?>
  </ul>
  <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>
