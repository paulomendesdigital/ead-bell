<?php if (empty($home_layout_item)) { ?>
    <div class='content-layout-wrapper'>
        <?php echo $this->Form->control('home_layout_items[{{index}}][content_id]', ['data-toggle' => 'content_id', 'options' => $contents, 'type' => 'select', 'class' => 'form-control', 'label' => __('Publicação'), 'empty' => 'Selecione o Conteúdo', 'required' => true, 'id' => 'homelayoutitems-{{index}}-content-id']); ?>
        <?php echo $this->Form->control('home_layout_items[{{index}}][size]', array('data-toggle' => 'size', 'options' => [1 => 1, 2 => 2], 'required' => true, 'class' => 'form-control', 'label' => __('size'), 'id' => 'homelayoutitems-{{index}}-size')); ?>
        <?php echo $this->Form->control('home_layout_items[{{index}}][row]', array('data-toggle' => 'row', 'type' => 'hidden', 'id' => 'homelayoutitems-{{index}}-row')); ?>
        <?php echo $this->Form->control('home_layout_items[{{index}}][ordination]', array('data-toggle' => 'ordination', 'type' => 'hidden', 'id' => 'homelayoutitems-{{index}}-ordination')); ?>
        <button type='button' data-toggle="delete-content" class='btn btn-danger'><i class='fa fa-trash'></i></button>
    </div>
<?php } else { ?>
    <div class='content-layout-wrapper'>
        <?php echo $this->Form->control('home_layout_items['.$index.'][id]', ['type' => 'hidden', 'value' => $home_layout_item['id']]); ?>
        <?php echo $this->Form->control('home_layout_items['.$index.'][content_id]', ['data-toggle' => 'content_id', 'options' => $contents, 'type' => 'select', 'class' => 'form-control', 'label' => __('Publicação'), 'empty' => 'Selecione o Conteúdo', 'required' => true, 'id' => 'homelayoutitems-'.$index.'-content-id', 'value' => $home_layout_item['content_id']]); ?>
        <?php echo $this->Form->control('home_layout_items['.$index.'][size]', array('data-toggle' => 'size', 'options' => [1 => 1, 2 => 2], 'required' => true, 'class' => 'form-control', 'label' => __('size'), 'id' => 'homelayoutitems-'.$index.'-size', 'value' => $home_layout_item['size'])); ?>
        <?php echo $this->Form->control('home_layout_items['.$index.'][row]', array('data-toggle' => 'row', 'type' => 'hidden', 'id' => 'homelayoutitems-'.$index.'-row', 'value' => $home_layout_item['row'])); ?>
        <?php echo $this->Form->control('home_layout_items['.$index.'][ordination]', array('data-toggle' => 'ordination', 'type' => 'hidden', 'id' => 'homelayoutitems-'.$index.'-ordination', 'value' => $home_layout_item['ordination'])); ?>
        <button type='button' data-toggle="delete-content" class='btn btn-danger'><i class='fa fa-trash'></i></button>
    </div>
<?php } ?>