<?php 
/**
 * @copyright Copyright 2018
 * @author Dayvison Silva - www.grupogrow.com.br
 * Element Manager Header View
 *
*/ 

use Cake\Routing\Router;
?>
<div class="header ">
   <div class="container-fluid relative">
      <div class="pull-left full-height visible-sm visible-xs">
         <div class="header-inner">
            <a href="#" class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5" data-toggle="sidebar">
            <span class="icon-set menu-hambuger"></span>
            </a>
         </div>
      </div>
      <div class="pull-center hidden-md hidden-lg">
         <div class="header-inner">
            <div class="brand inline">
               <img src="/theme/Pages/assets/img/logo-grow.png" alt="logo" data-src="/theme/Pages/assets/img/logo-grow.png" data-src-retina="/theme/Pages/assets/img/logo_2x.png" width="78" height="22">
            </div>
         </div>
      </div>
      <div class="pull-right full-height visible-sm visible-xs">
         <div class="header-inner">
            <a href="#" class="btn-link visible-sm-inline-block visible-xs-inline-block" data-toggle="quickview" data-toggle-element="#quickview">
            <span class="icon-set menu-hambuger-plus"></span>
            </a>
         </div>
      </div>
   </div>
   <div class=" pull-left sm-table hidden-xs hidden-sm">
      <div class="header-inner">
         <div class="brand inline">
            <img src="/theme/Pages/assets/img/logo-grow.png" alt="logo" data-src="/theme/Pages/assets/img/logo-grow.png" data-src-retina="/theme/Pages/assets/img/logo-grow.png" width="45" height="38">
         </div>
        <!--  <ul class="notification-list no-margin hidden-sm hidden-xs b-grey b-l b-r no-style p-l-30 p-r-20">
            <li class="p-r-15 inline">
               <div class="dropdown">
                  <a href="javascript:;" id="notification-center" class="icon-set globe-fill" data-toggle="dropdown" title="<?php //echo __('View Public Page'); ?>">
                  </a>
               </div>
            </li>
            <li class="p-r-15 inline">
               <a href="#" class="icon-set clip " title="<?php //echo __('Important links'); ?>"></a>
            </li>
            <li class="p-r-15 inline">
               <a href="#" class="icon-set grid-box" title="<?php //echo __('My pages'); ?>"></a>
            </li>
         </ul>
         <a href="#" class="search-link" data-toggle="search"><i class="pg-search"></i><?php //echo __('Type anywhere to'); ?> <span class="bold"><?php //echo __('search'); ?></span></a>  -->
      </div>
   </div>
   
   <div class=" pull-right">
      <div class="visible-lg visible-md m-t-10">
         <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
            <span class="semi-bold"><?php echo $User['name']; ?></span>
         </div>
         <div class="dropdown pull-right">
            <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="thumbnail-wrapper d32 circular inline m-t-5">
               <?php if( isset($User['avatar']) ):?>
                  <?php echo $this->Utility->avatarUser($User['id'],$User['avatar'], 'img-responsive', '', '32');?>
               <?php else:?>
                  <?php echo $this->Utility->avatarUser(NULL,NULL, 'img-responsive', '', '32');?>
               <?php endif;?>
            </span>
            </button>
            <ul class="dropdown-menu profile-dropdown" role="menu">
               <li><?php echo $this->Html->link('<i class="pg-settings_small"></i> '.__('Settings'), ['controller' => 'users', 'action' => 'edit', $User['id']],['escape' => false]); ?></li>
               <li class="bg-master-lighter">
                  <a href="<?php echo Router::url(['controller' => 'users', 'action' => 'logout']); ?>" class="clearfix">
                  <span class="pull-left">Logout</span>
                  <span class="pull-right"><i class="pg-power"></i></span>
                  </a>
               </li>
            </ul>
         </div>
      </div>
   </div>

</div>