<div class="hidden">
    <?php echo $this->Form->control('questiontypeall', ['disabled', 'data-toggle'=>'question-type-all', 'required', 'options'=> $question_types, 'empty' => 'Selecione', 'value' => '']); ?>
    <?php echo $this->Form->control('questiontypequiz', ['disabled', 'data-toggle'=>'question-type-quiz', 'required', 'options'=> [$questionTypeRadioButtonId => 'Radio Button'], 'empty' => 'Selecione', 'value' => '']); ?>
</div>