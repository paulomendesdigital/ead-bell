<div class="col-xs-12 questions-list" data-toggle="questions-list">
    <div class="panel panel-default question" data-toggle='question-wrapper' data-question-index="0">
        <div class="panel-body">
            <button type='button' data-toggle="remove-question" class='btn btn-danger'><i class="fa fa-trash"></i></button>
            <div class="row clearfix">
                <div class="col-xs-8">
                    <?php echo $this->Form->control('questions.0.question', ['class' => 'form-control', 'label' => 'Pergunta', 'disabled', 'data-toggle' => 'question', 'required', 'type' => 'text']); ?>
                </div>
                <div class="col-xs-4">
                    <?php echo $this->Form->control('questions.0.type', ['class' => 'form-control', 'label' => 'Tipo de Pergunta', 'disabled', 'data-toggle' => 'question-type', 'required', 'options' => $question_types, 'empty' => 'Selecione', 'value' => '']); ?>
                </div>
                <div class='col-xs-12 alternatives hidden' data-toggle='alternatives-wrapper'>
                    <br />
                    <label class="padding-20 bg-master-light">Alternativas <button type='button' data-toggle="add-alternative" class='btn btn-primary'><i class="fa fa-plus"></i></button></label>
                    <?php echo $this->Element('Manager/promotions/base-alternatives-list'); ?>
                </div>
            </div>
        </div>
    </div>
</div>