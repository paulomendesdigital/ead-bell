<div class="alternatives-list" data-toggle='alternatives-list'>
    <div class="row alternative mt-1" data-alternative-index="0" data-toggle='alternative-wrapper'>
        <div class="col-xs-10">
            <?php echo $this->Form->control('questions.0.alternatives.0.alternative', ['class' => 'form-control', 'label' => 'Alternativa', 'disabled', 'required', 'data-toggle' => "alternative", 'type' => 'text', 'maxlength' => '255']); ?>
        </div>
        <div class="col-xs-2">
            <div class="row">
                <div class="col-xs-9">
                    <?php echo $this->Form->control('questions.0.alternatives.0.is_correct', ['class' => 'form-control', 'label' => 'Correta?', 'disabled', 'required', 'data-toggle' => "alternative-is-correct", 'options' => ['0' => 'Não', '1' => 'Sim'], 'empty' => 'Selecione']); ?>
                </div>
                <div class="col-xs-3">
                    <label style='color: transparent'>foo</label>
                    <button type='button' data-toggle="remove-alternative" class='btn btn-danger'><i class="fa fa-trash"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>