<!-- Modal -->
<div class="modal fade" id="modalDinamic" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" data-content="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div data-content="modal-content-fluid" class="modal-body">
        ...
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>
<div class="container-fluid container-fixed-lg footer">
   <div class="copyright sm-text-center">
      <p class="small no-margin pull-left sm-pull-reset">
         <span class="hint-text">Copyright &copy; <?php echo date('Y'); ?> </span>
         <span class="font-montserrat"><?php echo $Corporate['Name']; ?></span>.
         <span class="hint-text"><?php echo __('All rights reserved'); ?>. </span>
      </p>
      <p class="small no-margin pull-right sm-pull-reset">
         <span class="hint-text"><?php echo __('Develop by'); ?> <a target="_blank" title="<?php echo __('Develop by'); ?> <?php echo $Developer['Author']; ?>" href="<?php echo $Developer['Website']; ?>"><?php echo $Developer['Author']; ?></a></span>
      </p>
      <div class="clearfix"></div>
   </div>
</div>