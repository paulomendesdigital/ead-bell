<?php

/**
 * @copyright Copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 * Element GrupoGrowManager Sidebar
 *
 */
?>
<nav class="page-sidebar" data-pages="sidebar">
    <div class="sidebar-header">
        <img src="/theme/Pages/assets/img/logo_white.png?<?php echo time(); ?>" alt="logo" class="brand" data-src="/theme/Pages/assets/img/logo_white.png" data-src-retina="/theme/Pages/assets/img/logo_white_2x.png?<?php echo time(); ?>" width="45" height="38">
        <div class="sidebar-header-controls">
            <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar"><i class="fa fs-12"></i></button>
        </div>
    </div>
    <div class="sidebar-menu">
        <?php if (isset($User['id']) and !empty($User['id'])) : ?>

            <ul class="menu-items">

                <?php $class = $this->request->getParam('controller') == 'Pages' ? 'open active ' : ''; ?>
                <li class="m-t-30 <?php echo $class; ?> hidden">
                    <?php echo $this->Html->link('<span class="title">Páginas</span><span class="details">Cadastro de páginas</span>', ['controller' => 'pages', 'action' => 'index'], ['class' => 'detailed', 'escape' => false]); ?>
                    <span class="icon-thumbnail"><i class="pg-home"></i></span>
                </li>

                <?php $class = $this->request->getParam('controller') == 'HomeLayouts' ? 'open active ' : ''; ?>
                <li class="m-t-10 <?php echo $class; ?>">
                    <?php echo $this->Html->link('<span class="title">Layouts</span><span class="details">Cadastro dos layouts</span>', ['controller' => 'home_layouts', 'action' => 'index'], ['class' => 'detailed', 'escape' => false]); ?>
                    <span class="icon-thumbnail"><i class="pg-layouts2"></i></span>
                </li>

                <?php $class = $this->request->getParam('controller') == 'Webdoors' ? 'open active ' : ''; ?>
                <li class="m-t-10 <?php echo $class; ?>">
                    <?php echo $this->Html->link('<span class="title">Webdoors</span><span class="details">Banner página inicial</span>', ['controller' => 'webdoors', 'action' => 'index'], ['class' => 'detailed', 'escape' => false]); ?>
                    <span class="icon-thumbnail"><i class="pg-image"></i></span>
                </li>

                <?php $class = $this->request->getParam('controller') == 'ContentCategories' ? 'open active ' : ''; ?>
                <li class="m-t-10 <?php echo $class; ?>">
                    <?php echo $this->Html->link('<span class="title">Categorias</span><span class="details">Categorias dos conteúdos</span>', ['controller' => 'content_categories', 'action' => 'index'], ['class' => 'detailed', 'escape' => false]); ?>
                    <span class="icon-thumbnail"><i class="pg-settings_small"></i></span>
                </li>

                <?php $class = $this->request->getParam('controller') == 'Contents' ? 'open active ' : ''; ?>
                <li class="m-t-10 <?php echo $class; ?>">
                    <?php echo $this->Html->link('<span class="title">Publicações</span><span class="details">Cadastro dos conteúdos</span>', ['controller' => 'contents', 'action' => 'index'], ['class' => 'detailed', 'escape' => false]); ?>
                    <span class="icon-thumbnail"><i class="pg-layouts"></i></span>
                </li>

                <?php $class =  '';
                if ($this->request->getParam('controller') == 'Contents'){
                    $class = ($this->request->getParam('action') == 'promotions') || ($this->request->getParam('action') == 'reports')  ? 'open active ' : '' ;
                }?>
                <li class="m-t-10 <?php echo $class; ?>">
                    <a href="javascript:;">
                        <span class="title">Promoções</span>
                        <span class="arrow"></span>
                    </a>
                    <span class="icon-thumbnail">P</span>
                    <ul class="sub-menu">
                        <li>
                            <?= $this->Html->link('<span class="title">Relatórios</span>', ['controller' => 'contents', 'action' => 'reports'], ['escape' => false]) ?>
                        </li>
                        <li>
                            <?= $this->Html->link('<span class="title">Promoções</span>', ['controller' => 'contents', 'action' => 'promotions'], ['escape' => false]) ?>
                        </li>
                    </ul>
                </li>

                <?php $class = $this->request->getParam('controller') == 'Programs' ? 'open active ' : ''; ?>
                <li class="m-t-10 <?php echo $class; ?>">
                    <?php echo $this->Html->link('<span class="title">Conteúdos Mix</span><span class="details">Cadastro dos programas</span>', ['controller' => 'programs', 'action' => 'index'], ['class' => 'detailed', 'escape' => false]); ?>
                    <span class="icon-thumbnail"><i class="pg-desktop"></i></span>
                </li>

                <?php $class = $this->request->getParam('controller') == 'Advertisements' ? 'open active ' : ''; ?>
                <li class="m-t-10 hidden <?php echo $class; ?>">
                    <?php echo $this->Html->link('<span class="title">Legado - Publicidades</span><span class="details">Cadastro dos anúncios</span>', ['controller' => 'advertisements', 'action' => 'index'], ['class' => 'detailed', 'escape' => false]); ?>
                    <span class="icon-thumbnail">P</span>
                </li>

                <?php $class = $this->request->getParam('controller') == 'Adverts' ? 'open active ' : ''; ?>
                <li class="m-t-10 <?php echo $class; ?>">
                    <a href="javascript:;">
                        <span class="title">Anúncios</span>
                        <span class="arrow"></span>
                    </a>
                    <span class="icon-thumbnail">P</span>
                    <ul class="sub-menu">
                        <li>
                            <?= $this->Html->link('<span class="title">Relatórios</span>', ['controller' => 'Adverts', 'action' => 'reports'], ['escape' => false]) ?>
                        </li>
                        <li>
                            <?= $this->Html->link('<span class="title">Anunciantes</span>', ['controller' => 'Advertisers', 'action' => 'index'], ['escape' => false]) ?>
                        </li>
                        <li>
                            <?= $this->Html->link('<span class="title">Campanhas</span>', ['controller' => 'AdvertCampaigns', 'action' => 'index'], ['escape' => false]) ?>
                        </li>
                        <li>
                            <?= $this->Html->link('<span class="title">Anúncios</span>', ['controller' => 'Adverts', 'action' => 'index'], ['escape' => false]) ?>
                        </li>
                        <!--
                        <li>
                            <?= $this->Html->link('<span class="title">Zonas de Anúncio</span>', ['controller' => 'AdvertZones', 'action' => 'index'], ['escape' => false]) ?>
                        </li>
                         <li>
                            <?= $this->Html->link('<span class="title">Anúncios - Zonas de Anúncio</span>', ['controller' => 'AdvertsAdvertZones', 'action' => 'index'], ['escape' => false]) ?>
                        </li>
                        <li>
                            <?= $this->Html->link('<span class="title">Visualizações de Anúncio</span>', ['controller' => 'AdvertViews', 'action' => 'index'], ['escape' => false]) ?>
                        </li>
                        <li>
                            <?= $this->Html->link('<span class="title">Cliques de Anúncio</span>', ['controller' => 'AdvertClicks', 'action' => 'index'], ['escape' => false]) ?>
                        </li> -->
                    </ul>
                </li>

                <?php $class = in_array($this->request->getParam('controller'), ['TopMusics']) ? 'open active ' : ''; ?>
                <li class="m-t-10 <?php echo $class; ?>">
                    <a href="javascript:;">
                        <span class="title">Músicas</span>
                        <span class="arrow"></span>
                    </a>
                    <span class="icon-thumbnail"><i class="fa fa-music"></i></span>
                    <ul class="sub-menu">
                        <li class="">
                            <?php echo $this->Html->link('<span class="title">Top Mix</span>', ['controller' => 'top_musics', 'action' => 'index'], ['class' => 'detailed', 'escape' => false]); ?>
                            <span class="icon-thumbnail"><i class="fa fa-edit"></i></span>
                        </li>
                        <li class="">
                            <?php echo $this->Html->link('<span class="title">Pedido Musical</span>', ['controller' => 'musical_requests', 'action' => 'index'], ['class' => 'detailed', 'escape' => false]); ?>
                            <span class="icon-thumbnail"><i class="fa fa-list"></i></span>
                        </li>
                        <li class="">
                            <?php echo $this->Html->link('<span class="title">Like Musical</span>', ['controller' => 'like_musics', 'action' => 'index'], ['class' => 'detailed', 'escape' => false]); ?>
                            <span class="icon-thumbnail"><i class="fa fa-heart"></i></span>
                        </li>
                    </ul>
                </li>

                <?php $class = $this->request->getParam('controller') == 'Speakers' ? 'open active ' : ''; ?>
                <li class="m-t-10 <?php echo $class; ?>">
                    <?php echo $this->Html->link('<span class="title">Locutores</span><span class="details">Cadastro dos locutores</span>', ['controller' => 'Speakers', 'action' => 'index'], ['class' => 'detailed', 'escape' => false]); ?>
                    <span class="icon-thumbnail"><i class="fa fa-microphone"></i></span>
                </li>

                <?php $class = $this->request->getParam('controller') == 'MidiaKits' ? 'open active ' : ''; ?>
                <li class="m-t-10 <?php echo $class; ?>">
                    <?php echo $this->Html->link('<span class="title">Midia Kits</span><span class="details">Cadastro de midia kits</span>', ['controller' => 'MidiaKits', 'action' => 'index'], ['class' => 'detailed', 'escape' => false]); ?>
                    <span class="icon-thumbnail"><i class="fa fa-file-pdf-o"></i></span>
                </li>

                <?php $class = $this->request->getParam('controller') == 'Players' ? 'open active ' : ''; ?>
                <li class="m-t-10 <?php echo $class; ?>">
                    <?php echo $this->Html->link('<span class="title">Player</span><span class="details">Cadastro de player</span>', ['controller' => 'Players', 'action' => 'index'], ['class' => 'detailed', 'escape' => false]); ?>
                    <span class="icon-thumbnail"><i class="fa fa-play"></i></span>
                </li>

                <?php $class = ($this->request->getParam('controller') == 'Users') ? 'open active ' : ''; ?>
                <li class="m-t-10 <?php echo $class; ?>">
                    <a href="javascript:;">
                        <span class="title">Usuários</span>
                        <span class="arrow"></span>
                    </a>
                    <span class="icon-thumbnail"><i class="fa fa-users"></i></span>
                    <ul class="sub-menu">
                        <li class="">
                            <?php echo $this->Html->link('<span class="title">Administradores</span>', ['controller' => 'users', 'action' => 'index'], ['class' => 'detailed', 'escape' => false]); ?>
                            <span class="icon-thumbnail"><i class="pg-settings_small"></i></span>
                        </li>
                        <li class="">
                            <?php echo $this->Html->link('<span class="title">Ouvintes</span>', ['controller' => 'users', 'action' => 'listeners'], ['class' => 'detailed', 'escape' => false]); ?>
                            <span class="icon-thumbnail"><i class="fa fa-volume-up"></i></span>
                        </li>
                    </ul>
                </li>
                <?php $class = $this->request->getParam('controller') == 'Newsletters' ? 'open active ' : ''; ?>
                <li class="m-t-10 <?php echo $class; ?>">
                    <?php echo $this->Html->link('<span class="title">Leads</span><span class="details">Cadastro de leads</span>', ['controller' => 'Newsletters', 'action' => 'index'], ['class' => 'detailed', 'escape' => false]); ?>
                    <span class="icon-thumbnail"><i class="fa fa-newspaper-o"></i></span>
                </li>

                <?php $class = $this->request->getParam('controller') == 'ConfigurationParameters' ? 'open active ' : ''; ?>
                <li class="m-t-10 <?php echo $class; ?>">
                    <?php echo $this->Html->link('<span class="title">Parâmetros de Configuração</span><span class="details">Cadastro de parâmetros de configuração</span>', ['controller' => 'ConfigurationParameters', 'action' => 'index'], ['class' => 'detailed', 'escape' => false]); ?>
                    <span class="icon-thumbnail"><i class=" pg-settings"></i></span>
                </li>

                <?php if (in_array($User['id'], $Corporate['SuperAdmins'])) : ?>

                    <?php $class = $this->request->getParam('controller') == 'Companies' ? 'open active ' : ''; ?>
                    <li class="m-t-10 <?php echo $class; ?>">
                        <?php echo $this->Html->link('<span class="title">Empresas</span><span class="details">Listar Empresas</span>', ['controller' => 'companies', 'action' => 'index'], ['class' => 'detailed', 'escape' => false]); ?>
                        <span class="icon-thumbnail"><i class="pg-home"></i></span>
                    </li>

                    <?php $class = $this->request->getParam('controller') == 'Groups' ? 'open active ' : ''; ?>
                    <li class="m-t-10 <?php echo $class; ?>">
                        <?php echo $this->Html->link('<span class="title">Grupos</span><span class="details">Cadastro de Grupos</span>', ['controller' => 'groups', 'action' => 'index'], ['class' => 'detailed', 'escape' => false]); ?>
                        <span class="icon-thumbnail"><i class="fa fa-users"></i></span>
                    </li>
                <?php endif; ?>
            </ul>

        <?php endif; ?>
        <div class="clearfix"></div>
    </div>
</nav>
