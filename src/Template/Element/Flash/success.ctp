<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="message success hidden" onclick="this.classList.add('hidden')"><?= $message ?></div>

<script type="text/javascript">
jQuery(document).ready(function ($) {swal(swal({
  title: 'Sucesso!',
  text: '<?php echo $message; ?>',
  timer: 10000,
  type: 'success'
}));});
</script>