<?php 
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
//echo $this->Html->script('../theme/Pages/assets/plugins/jquery/jquery-1.11.1.min');
?>
<div class="message error hidden" onclick="this.classList.add('hidden');"><?= $message ?></div>

<script type="text/javascript">
	jQuery(document).ready(function ($) {swal(swal({
	  title: 'Ops!',
	  text: '<?php echo $message; ?>',
	  timer: 10000,
	  type: 'error',
	  html:true
	}));});
</script>
