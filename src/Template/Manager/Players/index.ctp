<?php
/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Player[]|\Cake\Collection\CollectionInterface $players
 */
?>

<?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' =>'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page">Players</li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">
    <div class="panel panel-default">
        <div class="table-responsive">
            <table cellpadding="0" cellspacing="0" class="table table-striped table-hover  order">
                <thead>
                    <tr>
                        <th scope="col">
                            <?= $this->Paginator->sort('id') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'id'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('start') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'start'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('finish') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'finish'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('created') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'created'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('modified') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'modified'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?php echo __('Actions'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($players as $player){ ?>
                        <tr>
                            <td><?= $this->Number->format($player->id) ?></td>
                            <td><?= h($player->start ? $player->start->format('d/m/Y') : '') ?></td>
                            <td><?= h($player->finish ? $player->finish->format('d/m/Y') : '') ?></td>
                            <td><?= h($player->created->format('d/m/Y')) ?></td>
                            <td><?= h($player->modified->format('d/m/Y')) ?></td>
                            <td class="actions">
                                <?= $this->Html->link('<i class="fa fa-eye" aria-hidden="true"></i>', ['action' => 'view', $player->id],['class'=>'btn btn-info btn-xs text-center','escape'=>false]) ?>
                                <?= $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i>', ['action' => 'edit', $player->id],['class'=>'btn btn-primary btn-xs text-center','escape'=>false]) ?>
                                <?= $this->Html->link('<i class="fa fa-trash" aria-hidden="true"></i>', ['action' => 'delete', $player->id],['class'=>'btn btn-danger btn-xs text-center','escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $player->id)]); ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="panel-footer">
            <?php echo $this->Element('Manager/pagination');?>
        </div>
    </div>
</div>
