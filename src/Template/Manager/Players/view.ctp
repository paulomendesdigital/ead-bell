<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Webdoor $webdoor
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Player'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Ver').' '.__('Player'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li class="hidden-xs"><?= $this->Html->link('<i class="fa fa-list"></i> '.__('Listar Players'), ['action' => 'index'],['class'=>'btn btn-small btn-info','escape'=>false]) ?></li>
        <li class="hidden-xs"><?= $this->Html->link('<i class="fa fa-edit"></i> '.__('Editar Player'), ['action' => 'edit', $player->id],['class'=>'btn btn-small btn-success','escape'=>false]) ?></li>
        <li><?= $this->Form->postLink('<i class="fa fa-trash"></i> '.__('Delete'),['action' => 'delete', $player->id],['confirm' => __('Are you sure you want to delete # {0}?', $player->id), 'class'=>'btn btn-small btn-danger','escape'=>false]) ?></li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="webdoors view large-9 medium-8 columns content">
        <h3>Nome: <?= h($player->name) ?></h3>
        <table class="vertical-table">
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($player->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Company') ?>:</th>
                <td><?= $player->has('company') ? $this->Html->link($player->company->name, ['controller' => 'Companies', 'action' => 'view', $player->company->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Ao Vivo') ?>:</th>
                <td><?= $player->live; ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Podcast') ?>:</th>
                <td><?= $player->podcast; ?></td>
            </tr>
            
            <tr>
                <th scope="row"><?= __('Start') ?></th>
                <td><?= $player->start->format('d/m/Y') ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Finish') ?></th>
                <td><?= $player->finish->format('d/m/Y') ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($player->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($player->modified) ?></td>
            </tr>
        </table>
    </div>
</div>
