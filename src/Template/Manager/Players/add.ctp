<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Player $player
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Players'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Add').' '.__('Player'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link('<i class="fa fa-list"></i> '.__('Listar Players'), ['action' => 'index'],['class'=>'btn btn-small btn-info','escape'=>false]) ?></li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="players form large-9 medium-8 columns content">
        <?= $this->Form->create($player) ?>
        <fieldset>
            <legend><?= __('Adicionar Player') ?></legend>
            <div class="row clearfix mt-2">
                <div class="col-sm-6 col-xs-12">
                    <?php echo $this->Form->control('company_id', ['options' => $companies, 'type'=>'select','class' => 'form-control', 'label' => __('company')]);?>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="col-sm-6 col-xs-12">
                    <?php echo $this->Form->control('live', ['class' => 'form-control input-lg',  'label' => __('Ao Vivo')]);?>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="col-sm-6 col-xs-12">
                    <?php echo $this->Form->control('podcast', ['class' => 'form-control input-lg',  'label' => __('Podcast')]);?>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="col-sm-3 col-xs-12">
                    <?php echo $this->Form->control('start', ['type'=>'text', 'class' => 'form-control input-lg', 'data-content' => "datepicker", 'label' => __('Publicar em:')]);?>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <?php echo $this->Form->control('finish', ['type'=>'text', 'class' => 'form-control input-lg', 'data-content' => "datepicker", 'label' => __('Sair de Publicação em:')]);?>
                </div>
            </div>
            <div class="row clearfix mt-2 mb-2">
                <div class="col-sm-12 col-xs-12">
                    <?= $this->Form->button(__('Add'),['class'=>'btn btn-primary']) ?>
                </div>
            </div>
        </fieldset>
        <?= $this->Form->end() ?>
    </div>
</div>

