<?php

/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Advert[]|\Cake\Collection\CollectionInterface $adverts
 */
?>


<?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' => 'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page"><?php echo __('Adverts'); ?></li>
        </ol>
    </nav>
</div>
    
<div class="container-fluid container-fixed-lg">
    <div class="panel panel-default">
        <div class="table-responsive">
            <?= $this->Form->create(null, ['method' => 'get', 'valueSources' => ['query', 'context']]) ?>
            <div class="row">

                <div class="col-md-4">
                    <?= $this->Form->control('Adverts.id', ['class' => 'form-control']) ?>
                </div>

                <div class="col-md-4">
                    <?= $this->Form->control('Adverts.advert_campaign_id', ['class' => 'form-control', 'empty' => 'Selecione']) ?>
                </div>

                <div class="col-md-4">
                    <?= $this->Form->control('Adverts.name LIKE', ['class' => 'form-control', 'label' => __('Name')]) ?>
                </div>
            </div>
            <div class="row">

                <div class="col-md-4">
                    <?= $this->Form->control('Adverts.status', ['class' => 'form-control', 'empty' => 'Selecione', 'options' => [0 => 'Inativo', 1 => 'Ativo']]) ?>
                </div>

                <div class="col-md-4">
                    <?= $this->Form->control('Adverts.created >=', ['class' => 'form-control', 'data-mask-input' => 'date', 'label' => 'Data de Criação - Início']) ?>
                </div>

                <div class="col-md-4">
                    <label>&nbsp;</label>
                    <button type="submit" class="btn btn-primary" style="width: 100%">Buscar</button>
                </div>
            </div>
            <?= $this->Form->end() ?>

            <?php echo $this->Form->create(null, ['url' => ['action' => 'delete']]); ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">
                            <button class="btn btn-link" type='submit'><i class="pg-trash"></i></button>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('id', __('Id')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'id') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('advert_campaign_id', __('Advert Campaign')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'advert_campaign_id') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('name', __('Name')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'name') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('clicks_count', __('Clicks Count')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'clicks_count') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('views_count', __('Views Count')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'views_count') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('status', __('Status')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'status') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('created', __('Created')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'created') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('modified', __('Modified')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'modified') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?php echo __('Actions'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($adverts as $advert) { ?>
                        <tr>
                            <td>
                                <input name="Adverts[][id]" value="<? echo $advert->id; ?>" id="advert-<?php echo $advert->id; ?>" type="checkbox" />
                                <label for="page-<?php echo $advert->id; ?>" class="no-padding no-margin"></label>
                            </td>

                            <td><?= $this->Number->format($advert->id) ?></td>

                            <td><?= $advert->has('advert_campaign') ? $this->Html->link($advert->advert_campaign->name, ['controller' => 'AdvertCampaigns', 'action' => 'view', $advert->advert_campaign->id]) : '' ?></td>

                            <td><?= h($advert->name) ?></td>

                            <td><?= $this->Number->format($advert->clicks_count) ?></td>

                            <td><?= $this->Number->format($advert->views_count) ?></td>

                            <td><?= $this->Utility->__FormatStatus($advert->status) ?></td>

                            <td><?= h($advert->created) ?></td>

                            <td><?= h($advert->modified) ?></td>

                            <td class="actions">
                                <?= $this->Html->link('<button type="button" class="btn btn-info"><i class="fa-eye fa"></i></button>', ['action' => 'view', $advert->id], ['escape' => false]) ?>
                                <?= $this->Html->link('<button type="button" class="btn btn-primary"><i class="fa-pencil fa"></i></button>', ['action' => 'edit', $advert->id], ['escape' => false]) ?>
                                <?= $this->Form->postLink('<button type="button" class="btn btn-danger"><i class="fa-trash fa"></i></button>', ['action' => 'delete', $advert->id], ['confirm' => __('Are you sure you want to delete # {0}?', $advert->id), 'escape' => false]) ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php echo $this->Form->end(); ?>
        </div>
        <div class="panel-footer">
            <?php echo $this->Element('Manager/pagination');?>
        </div>
    </div>
</div>
