<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Advert $advert
 */
?>
<div class="container-fluid container-fixed-lg">
    <ol class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Adverts'), ['controller' => 'Adverts', 'action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Edit') . ' ' . __('Adverts'); ?></a></li>
    </ol>
</div>

<div class="container-fluid container-fixed-lg">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-transparent">
                <?= $this->Element('Manager/adverts/advert') ?>
            </div>
        </div>
    </div>

</div>

<?= $this->Html->script('manager/adverts') ?>
