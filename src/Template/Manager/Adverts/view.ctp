<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Advert $advert
 */
?>

<div class="container-fluid container-fixed-lg">
    <ol class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Adverts'), ['controller' => 'Adverts', 'action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('View') . ' ' . __('Adverts'); ?></a></li>
    </ol>
</div>

<div class="container-fluid container-fixed-lg">

    <!-- BASIC INFORMATIONS -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title"><?php echo __('Basic information'); ?></div>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered">
                <tr>
                    <td><strong><?= __('Uuid') ?></strong></td>
                    <td><?= h($advert->uuid) ?></td>
                </tr>
                <tr>
                    <td>
                        <strong><?php echo __('Advert Campaign'); ?></strong>
                    </td>
                    <td>
                        <?= $advert->has('advert_campaign') ? $this->Html->link($advert->advert_campaign->name, ['controller' => 'AdvertCampaigns', 'action' => 'view', $advert->advert_campaign->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <td><strong><?= __('Name') ?></strong></td>
                    <td><?= h($advert->name) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Url') ?></strong></td>
                    <td><?= h($advert->url) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Image') ?></strong></td>
                    <td>
                        <?= h($advert->image) ?>
                        <br>
                        <?= $this->Html->image($advert->image_dir . DS . $advert->image, ['style' => 'max-width: 100%']) ?>
                    </td>
                </tr>
                <tr>
                    <td><strong><?= __('Image Dir') ?></strong></td>
                    <td><?= h($advert->image_dir) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Id') ?></strong></td>
                    <td><?= $this->Number->format($advert->id) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Unique Views Count') ?></strong></td>
                    <td><?= $this->Number->format($advert->unique_views_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Views Count') ?></strong></td>
                    <td><?= $this->Number->format($advert->views_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Clicks Count') ?></strong></td>
                    <td><?= $this->Number->format($advert->unique_clicks_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Clicks Count') ?></strong></td>
                    <td><?= $this->Number->format($advert->clicks_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Status') ?></strong></td>
                    <td><?= $this->Utility->__FormatStatus($advert->status) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Created') ?></strong></td>
                    <td><?= h($advert->created) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Modified') ?></strong></td>
                    <td><?= h($advert->modified) ?></td>
                </tr>

            </table>

            <div class="related">
                <h4><?= __('Related Advert Zones') ?> - <?= $this->Html->link('Listar Associações', ['controller' => 'AdvertsAdvertZones', 'action' => 'index', '?' => ['Adverts[id]' => $advert->id]], ['class' => 'btn btn-primary']) ?></h4>
                <?php if (!empty($advert->advert_zones)) : ?>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th><?= __('Id') ?></th>
                                <th><?= __('Company') ?></th>
                                <th><?= __('Name') ?></th>
                                <th><?= __('Code') ?></th>
                                <th><?= __('Width') ?></th>
                                <th><?= __('Height') ?></th>
                                <th><?= __('Status') ?></th>
                                <th><?= __('Created') ?></th>
                                <th><?= __('Modified') ?></th>
                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                            <?php foreach ($advert->advert_zones as $advertZones) : ?>
                                <tr>
                                    <td><?= h($advertZones->id) ?></td>
                                    <td><?= h($advertZones->company->name) ?></td>
                                    <td><?= $this->Html->link($advertZones->name, ['controller' => 'AdvertZones', 'action' => 'view', $advertZones->id]) ?></td>
                                    <td><?= h($advertZones->code) ?></td>
                                    <td><?= h($advertZones->width) ?></td>
                                    <td><?= h($advertZones->height) ?></td>
                                    <td><?= $this->Utility->__FormatStatus($advertZones->status) ?></td>
                                    <td><?= h($advertZones->created) ?></td>
                                    <td><?= h($advertZones->modified) ?></td>
                                    <td class="actions">
                                        <?= $this->Html->link(__('View'), ['controller' => 'AdvertZones', 'action' => 'view', $advertZones->id]) ?>
                                        <?= $this->Html->link(__('Edit'), ['controller' => 'AdvertZones', 'action' => 'edit', $advertZones->id]) ?>
                                        <?//= $this->Form->postLink(__('Delete'), ['controller' => 'AdvertZones', 'action' => 'delete', $advertZones->id], ['confirm' => __('Are you sure you want to delete # {0}?', $advertZones->id)]) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                <?php endif; ?>
            </div>

            <div class="row"></div>
            <hr />
            <div class='row'>
                <div class="actions">
                    <?php echo $this->Html->link(__('Edit register'), array('action' => 'edit', $this->request->getAttribute('params')['pass'][0]), array('class' => 'btn btn-info')); ?>
                </div>
            </div>
        </div>
    </div>

</div>
