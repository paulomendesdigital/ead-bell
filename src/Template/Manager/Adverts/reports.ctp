<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Advert[] $topAdverts
 * @var \App\Model\Entity\AdvertCampaign[] $topAdvertCampaigns
 * @var \App\Model\Entity\AdvertZone[] $topAdvertZones
 */
?>

<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page"><?php echo __('Adverts'); ?> - Relatórios</li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">
    <div class="panel panel-default">
        <div class="table-responsive">
            <h2>Anúncios com mais visualizações únicas</h2>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col"><?= __('Company') ?></th>
                            <th scope="col"><?= __('Advert Campaign') ?></th>
                            <th scope="col"><?= __('Advert') ?></th>
                            <th scope="col"><?= __('Unique Views Count') ?></th>
                            <th scope="col"><?= __('Views Count') ?></th>
                            <th scope="col"><?= __('Unique Clicks Count') ?></th>
                            <th scope="col"><?= __('Clicks Count') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($topAdverts as $advert) { ?>
                            <tr>
                                <td>
                                    <?= $this->Html->link($advert->advert_campaign->company->name, ['controller' => 'Companies', 'action' => 'view', $advert->advert_campaign->company->id]) ?>
                                </td>
                                <td>
                                    <?= $this->Html->link($advert->advert_campaign->name, ['controller' => 'AdvertCampaigns', 'action' => 'view', $advert->advert_campaign->id]) ?>
                                </td>
                                <td>
                                    <?= $this->Html->link($advert->name, ['controller' => 'Adverts', 'action' => 'view', $advert->id]) ?>
                                </td>
                                <td>
                                    <?= $advert->unique_views_count ?>
                                </td>
                                <td>
                                    <?= $advert->views_count ?>
                                </td>
                                <td>
                                    <?= $advert->unique_clicks_count ?>
                                </td>
                                <td>
                                    <?= $advert->clicks_count ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>

            <h2>Anúncios com mais cliques únicos</h2>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col"><?= __('Company') ?></th>
                            <th scope="col"><?= __('Advert Campaign') ?></th>
                            <th scope="col"><?= __('Advert') ?></th>
                            <th scope="col"><?= __('Unique Views Count') ?></th>
                            <th scope="col"><?= __('Views Count') ?></th>
                            <th scope="col"><?= __('Unique Clicks Count') ?></th>
                            <th scope="col"><?= __('Clicks Count') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($topClickAdverts as $advert) { ?>
                            <tr>
                                <td>
                                    <?= $this->Html->link($advert->advert_campaign->company->name, ['controller' => 'Companies', 'action' => 'view', $advert->advert_campaign->company->id]) ?>
                                </td>
                                <td>
                                    <?= $this->Html->link($advert->advert_campaign->name, ['controller' => 'AdvertCampaigns', 'action' => 'view', $advert->advert_campaign->id]) ?>
                                </td>
                                <td>
                                    <?= $this->Html->link($advert->name, ['controller' => 'Adverts', 'action' => 'view', $advert->id]) ?>
                                </td>
                                <td>
                                    <?= $advert->unique_views_count ?>
                                </td>
                                <td>
                                    <?= $advert->views_count ?>
                                </td>
                                <td>
                                    <?= $advert->unique_clicks_count ?>
                                </td>
                                <td>
                                    <?= $advert->clicks_count ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>

            <h2>Campanhas com mais visualizações únicas</h2>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col"><?= __('Company') ?></th>
                            <th scope="col"><?= __('Advert Campaign') ?></th>
                            <th scope="col"><?= __('Unique Views Count') ?></th>
                            <th scope="col"><?= __('Views Count') ?></th>
                            <th scope="col"><?= __('Unique Clicks Count') ?></th>
                            <th scope="col"><?= __('Clicks Count') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($topAdvertCampaigns as $advertCampaign) { ?>
                            <tr>
                                <td>
                                    <?= $this->Html->link($advertCampaign->company->name, ['controller' => 'Companies', 'action' => 'view', $advert->advert_campaign->company->id]) ?>
                                </td>
                                <td>
                                    <?= $this->Html->link($advertCampaign->name, ['controller' => 'AdvertCampaigns', 'action' => 'view', $advert->advert_campaign->id]) ?>
                                </td>
                                <td>
                                    <?= $advertCampaign->unique_views_count ?>
                                </td>
                                <td>
                                    <?= $advertCampaign->views_count ?>
                                </td>
                                <td>
                                    <?= $advertCampaign->unique_clicks_count ?>
                                </td>
                                <td>
                                    <?= $advertCampaign->clicks_count ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>

            <h2>Campanhas com mais cliques únicos</h2>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col"><?= __('Company') ?></th>
                            <th scope="col"><?= __('Advert Campaign') ?></th>
                            <th scope="col"><?= __('Unique Views Count') ?></th>
                            <th scope="col"><?= __('Views Count') ?></th>
                            <th scope="col"><?= __('Unique Clicks Count') ?></th>
                            <th scope="col"><?= __('Clicks Count') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($topClickAdvertCampaigns as $advertCampaign) { ?>
                            <tr>
                                <td>
                                    <?= $this->Html->link($advertCampaign->company->name, ['controller' => 'Companies', 'action' => 'view', $advert->advert_campaign->company->id]) ?>
                                </td>
                                <td>
                                    <?= $this->Html->link($advertCampaign->name, ['controller' => 'AdvertCampaigns', 'action' => 'view', $advert->advert_campaign->id]) ?>
                                </td>
                                <td>
                                    <?= $advertCampaign->unique_views_count ?>
                                </td>
                                <td>
                                    <?= $advertCampaign->views_count ?>
                                </td>
                                <td>
                                    <?= $advertCampaign->unique_clicks_count ?>
                                </td>
                                <td>
                                    <?= $advertCampaign->clicks_count ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>

            <h2>Zonas de Anúncio com mais visualizações únicas</h2>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col"><?= __('Company') ?></th>
                            <th scope="col"><?= __('Unique Views Count') ?></th>
                            <th scope="col"><?= __('Views Count') ?></th>
                            <th scope="col"><?= __('Unique Clicks Count') ?></th>
                            <th scope="col"><?= __('Clicks Count') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($topAdvertZones as $advertZone) { ?>
                            <tr>
                                <td>
                                    <?= $this->Html->link($advertZone->company->name, ['controller' => 'Companies', 'action' => 'view', $advertZone->company->id]) ?>
                                </td>
                                <td>
                                    <?= $advertZone->unique_views_count ?>
                                </td>
                                <td>
                                    <?= $advertZone->views_count ?>
                                </td>
                                <td>
                                    <?= $advertZone->unique_clicks_count ?>
                                </td>
                                <td>
                                    <?= $advertZone->clicks_count ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>

            <h2>Zonas de Anúncio com mais cliques únicos</h2>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col"><?= __('Company') ?></th>
                            <th scope="col"><?= __('Unique Views Count') ?></th>
                            <th scope="col"><?= __('Views Count') ?></th>
                            <th scope="col"><?= __('Unique Clicks Count') ?></th>
                            <th scope="col"><?= __('Clicks Count') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($topClickAdvertZones as $advertZone) { ?>
                            <tr>
                                <td>
                                    <?= $this->Html->link($advertZone->company->name, ['controller' => 'Companies', 'action' => 'view', $advertZone->company->id]) ?>
                                </td>
                                <td>
                                    <?= $advertZone->unique_views_count ?>
                                </td>
                                <td>
                                    <?= $advertZone->views_count ?>
                                </td>
                                <td>
                                    <?= $advertZone->unique_clicks_count ?>
                                </td>
                                <td>
                                    <?= $advertZone->clicks_count ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <h2>Dias com mais visualizações únicas</h2>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Data</th>
                                    <th><?= __('Views Count') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($topAdvertViewDays as $viewDay) { ?>
                                    <tr>
                                        <td><?= date('d/m/Y', strtotime($viewDay['view_date'])) ?></td>
                                        <td><?= $viewDay['count_views'] ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-md-6">
                    <h2>Dias com mais cliques únicos</h2>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Data</th>
                                    <th><?= __('Clicks Count') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($topAdvertClickDays as $clickDay) { ?>
                                    <tr>
                                        <td><?= date('d/m/Y', strtotime($clickDay['click_date'])) ?></td>
                                        <td><?= $clickDay['count_clicks'] ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
