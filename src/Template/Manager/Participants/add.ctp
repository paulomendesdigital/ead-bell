<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Participant $participant
 */
echo $this->Html->script('//code.jquery.com/jquery-1.10.2.js');
echo $this->Html->script('//code.jquery.com/ui/1.11.4/jquery-ui.js');
echo $this->Html->css('//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css');
?>
<?php use Cake\Routing\Router; ?>

<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Participantes'), ['action' => 'index', $content_id], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Add').' '.__('Participante'); ?></a></li>
    </ul>
</div>
<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link('<i class="fa fa-list"></i> '.__('Listar Participantes'), ['action' => 'index'],['class'=>'btn btn-small btn-info','escape'=>false]) ?></li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <h3 class="text-center">
        <?php echo $content->name;?><br />
        Promoção do tipo "<?php echo $content->type_promotion;?>"
    </h3>
    <div class="participants form large-9 medium-8 columns content">
        <?= $this->Form->create($participant) ?>
        <fieldset class="col-sm-12">
            <legend><?= __('Adicionar Participante') ?></legend>
            <div class="row clearfix mt-2">
                <div class="col-sm-3 col-xs-12">
                    <?php echo $this->Form->control('status', array('type' => 'hidden' ,'value' => '1'));?>
                    <?php echo $this->Form->control('content_id', array('type' => 'hidden' ,'value' => $content->id));?>
                    <?php echo $this->Form->control('company_id', ['options' => $companies, 'type'=>'select','class' => 'form-control', 'label' => __('company')]);?>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <?php echo $this->Form->control('user_id', ['type'=>'text','class' => 'form-control', 'label' => __('Ouvinte')]);?>
                </div>
                <div class="col-sm-3 col-xs-12 result" style="display: none;">
                    <?php echo $this->Form->control('user_name', ['readonly'=>true, 'type'=>'text', 'id'=>'user-id-value','class' => 'form-control', 'label' => 'Nome do Ouvinte']);?>
                </div>
            </div>
            <?php if( !empty($content->questions) ):?>
                <h3>Responda: </h3>
                <?php foreach($content->questions as $question):?>
                    <div class="row clearfix mt-2">
                        <div class="col-sm-12 col-xs-12">
                            <?php echo $this->Form->control('question_id', ['type'=>'hidden','value' => $question->id]);?>
                            <?php if( $question->type == 'TEXT' ):?>
                                <?php echo $this->Form->control('response_in_text', ['type'=>'textarea','class' => 'form-control', 'label' => $question->question]);?>
                            <?php elseif( in_array($question->type, ['PREMIO','RADIO']) ): ?>
                                <?php foreach($question->alternatives as $alternative):?>
                                    <div class="row clearfix mt-2">
                                        <div class="col-sm-12 col-xs-12">
                                            <input required type="radio" name="alternative_id" value="<?php echo $alternative->id;?>" /> &nbsp;<?php echo $alternative->alternative;?>
                                        </div>
                                    </div>
                                <?php endforeach;?>
                            <?php endif;?>
                        </div>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
            
            <div class="row clearfix mt-2 mb-2">
                <div class="col-sm-12 col-xs-12">
                    <?= $this->Form->button(__('Add'),['class'=>'btn btn-primary']) ?>
                </div>
            </div>
        </fieldset>
        <?= $this->Form->end() ?>
    </div>
</div>
<script>
    $('#user-id').autocomplete({
        source:'<?php echo Router::url(array('controller' => 'Users', 'action' => 'getAll')); ?>',
        minLength: 3,
        focus: function(event, ui) {
            // prevent autocomplete from updating the textbox
            event.preventDefault();
            // manually update the textbox
            $(this).val(ui.item.label);
        },
        select: function(event, ui) {
            // prevent autocomplete from updating the textbox
            event.preventDefault();
            // manually update the textbox and hidden field
            $(this).val(ui.item.label);
            $("#user-id").val(ui.item.value);
            $(".result").show();
            $("#user-id-value").val(ui.item.label);
        }
    });
</script>