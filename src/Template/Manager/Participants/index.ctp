<?php

/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Participant[]|\Cake\Collection\CollectionInterface $participants
 */

use Cake\Routing\Router;

echo $this->Html->script('//code.jquery.com/jquery-3.3.1.js');
echo $this->Html->script('//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js');
echo $this->Html->script('https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js');

//echo $this->Html->css('https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css');
echo $this->Html->css([
    // 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
    // 'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css'
]);

$showBtAddWinner = false;
?>

<?php //echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' =>'tooltip', 'data-placement' => 'left']);
?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li><?php echo $this->Html->link(__('Promoções'), ['controller' => 'contents', 'action' => 'promotions'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page">Participantes</li>
        </ol>
    </nav>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link('<i class="fa fa-download"></i> ' . __('Exportar Participantes'), ['controller' => 'participants', 'action' => 'export-promotion-participants', $content->id], ['class' => 'btn btn-small btn-warning', 'escape' => false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-list"></i> ' . __('Listar Promoções'), ['controller' => 'contents', 'action' => 'promotions'], ['class' => 'btn btn-small btn-info', 'escape' => false]) ?></li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <h3 class="text-center">
        <?php echo $content->name; ?><br />
        <?php if ($content->limitwinners) : ?>
            <small>"promoção com limite de <?php echo $content->limitwinners; ?> vencedor(es)"</small>
        <?php else : ?>
            <small>"promoção sem limite de vencedores"</small>
        <?php endif; ?>
    </h3>
    <?php if (!empty($countAccessOrigins)) { ?>
        <hr>
        <h3 class="text-center">
            Origem das Inscrições
        </h3>
        <div class="text-center">
            <?php foreach ($countAccessOrigins as $index => $countAccessOrigin) { ?>
                <div class='btn'>
                    <h5><?= $index ?></h5>
                    <h4><?= $countAccessOrigin['total'] ?></h4>
                </div>
            <?php } ?>
        </div>
        <div class="text-center">
            <small>Site :: <?php echo $content->company->name;?></small>
        </div>
    <?php } ?>
    <fieldset class="text-right mb-1">
        <?php if (!$content->limitwinners or ($content->limitwinners > $qtdewinners)) {
            $showBtAddWinner = true; ?>
            <?= $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i> sortear vencedor', ['controller' => 'winners', 'action' => 'autoAdd', $content->id], ['class' => 'btn btn-warning btn-xs text-center', 'escape' => false]) ?>
        <?php } ?>
    </fieldset>
    <?php if ($qtdewinners) : ?>
        <fieldset class="text-right mb-1">
            <?php echo $qtdewinners == 1 ? "{$qtdewinners} vencedor" : "{$qtdewinners} vencedores"; ?>
        </fieldset>
    <?php endif; ?>

    <div class="">
        <div class="">
            <table id="search" class="table table-striped table-bordered" style="width:100%; border: 1px solid silver;">
                <thead>
                    <tr>
                        <th scope="col">OUVINTE</th>
                        <th scope="col">PERGUNTAS/RESPOSTAS</th>
                        <th scope="col" nowrap="nowrap">DATA PARTICIPAÇÃO</th>
                        <th scope="col"><?php echo __('Actions'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $user) { ?>
                        <?php $bgInline = !empty($user->winners) ? 'style="background: lightgreen !important; color: black; border: 1px dotted black"' : ''; ?>
                        <tr>
                            <td nowrap="nowrap" valign="middle" <?php echo $bgInline; ?>>
                                <li class="list-style-none"><b>Id: </b><?php echo $user->id; ?></li>
                                <li class="list-style-none"><b>Nome: </b><?php echo $user->name; ?></li>
                                <li class="list-style-none"><b>Gênero: </b><?php echo $user->gender; ?></li>
                                <li class="list-style-none"><b>Nascimento: </b><?php echo !empty($user->birth) ? $user->birth->format('d/m/Y') : null; ?></li>
                                <?php if (!empty($user->birth)) { ?>
                                    <li class="list-style-none"><b>Idade: </b><?php echo date_diff(date_create(date('Y-m-d')), date_create($user->birth->format('Y-m-d')))->format('%Y'); ?></li>
                                <?php } ?>
                                <li class="list-style-none"><b>Email: </b><?php echo $user->email; ?></li>
                                <li class="list-style-none"><b>CPF: </b><?php echo $user->cpf; ?></li>
                                <li class="list-style-none"><b>Fone: </b><?php echo $user->phone; ?></li>
                                <li class="list-style-none"><b>Bairro: </b><?php echo $user->neighborhood; ?></li>
                                <li class="list-style-none"><b>Origem da Participação: </b><?php echo !empty($user->participants[0]->access_origin) ? $user->participants[0]->access_origin->name : null; ?></li>
                                <?php if (!empty($user->winners)) : ?>
                                    <li class="list-style-none text-success"><b>VENCEDOR</b></li>
                                <?php endif; ?>
                            </td>
                            <td <?php echo $bgInline; ?>>
                                <?php foreach ($user->participants as $participant) : ?>
                                    <?php if (!empty($participant->question)) : ?>
                                        <li class="list-style-none">
                                            <b><?php echo $participant->question->question; ?></b><br />
                                            <?php if (!empty($participant->response_in_text)) {
                                                echo "R.: {$participant->response_in_text}";
                                            } elseif (!empty($participant->alternative->alternative)) {
                                                echo "R.: {$participant->alternative->alternative}";
                                            } else {
                                                echo '';
                                            } ?>
                                            <?php //if( !empty($user->winners) and $user->winners[0]->participant_id == $participant->id ):
                                            ?>
                                            <?php //echo  $this->Html->link('<i class="fa fa-check" aria-hidden="true"></i> reposta vencedora', 'javascript:void(0);',['class'=>'btn btn-success btn-xs text-center','escape'=>false])
                                            ?>
                                            <?php //else:
                                            ?>
                                            <?php //echo $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i> definir respoata vencedora', ['controller'=>'winners','action' => 'add', $user->id, $content->id, $participant->id, $user->company_id],['class'=>'btn btn-primary btn-xs text-center','escape'=>false])
                                            ?>
                                            <?php //endif;
                                            ?>
                                        </li>
                                        <li class="list-style-none">
                                            <hr style="margin-bottom: 5px; margin-top: 5px;" />
                                        </li>
                                    <?php else : ?>
                                        <?php echo $content->type_promotion; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <?php if (!empty($user->winners)) : ?>
                                    <?php if (!empty($user->winners[0]->instrution)) : ?>
                                        <small><?php echo $user->winners[0]->instrution; ?></small>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                            <td <?php echo $bgInline; ?>><?php echo $user->participants[0]->created->format('d/m/Y H:i'); ?></td>
                            <td>
                                <?php if (!empty($user->winners)) : ?>
                                    <?= $this->Html->link('<i class="fa fa-check-square-o" aria-hidden="true"></i> vencedor', 'javascript:void(0);', ['class' => 'btn btn-success btn-xs text-center', 'escape' => false]) ?>
                                    <hr style='margin: 2px; border:0px;' />
                                    <?php echo $this->Html->link('<i class="fa fa-info-circle" aria-hidden="true"></i> Instruir Vencedor', 'javascript:void(0);', ['data-href' => Router::url(array('controller' => 'winners', 'action' => 'edit', $user->winners[0]->id)), 'class' => 'btn btn-info btn-xs text-center openPopup', 'escape' => false]); ?>
                                    <hr style='margin: 2px; border:0px;' />
                                    <?= $this->Html->link('<i class="fa fa-trash" aria-hidden="true"></i> excluir vencedor', ['controller' => 'winners', 'action' => 'delete', $user->winners[0]->id], ['class' => 'btn btn-danger btn-xs text-center', 'title' => 'Excluir Participação', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $user->winners[0]->id)]); ?>
                                <?php else : ?>
                                    <?php if ($showBtAddWinner) : ?>
                                        <?= $this->Html->link('<i class="fa fa-plus-square-o" aria-hidden="true"></i> definir vencedor', ['controller' => 'winners', 'action' => 'add', $user->id, $content->id, isset($user->participants[0]->id) ? $user->participants[0]->id : NULL, $user->company_id], ['class' => 'btn btn-primary btn-xs text-center', 'escape' => false]) ?>
                                        <hr style='margin: 2px; border:0px;' />
                                    <?php endif; ?>
                                    <?= $this->Html->link('<i class="fa fa-trash" aria-hidden="true"></i> excluir participação', ['action' => 'delete', $user->id, $content->id], ['class' => 'btn btn-danger btn-xs text-center', 'title' => 'Excluir Participação', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $user->id)]); ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" style="padding: 20px;">

            </div>
            <div class="modal-footer">
                <h4 class="modal-title text-right">
                    <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                </h4>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.openPopup').on('click', function() {
            var dataURL = $(this).attr('data-href');
            $('.modal-body').load(dataURL, function() {
                $('#myModal').modal({
                    show: true
                });
            });
        });
    });
</script>
<script>
    $(document).ready(function() {
        // Setup - add a text input to each footer cell
        $('#search thead tr').clone(true).appendTo('#search thead');
        $('#search thead tr:eq(1) th').each(function(i) {
            var title = $(this).text();
            if (title != 'Ações' && title != 'PERGUNTAS/RESPOSTAS') {
                $(this).html('<input class="form-control input-sm" style="width: 100%;" type="text" placeholder="Filtrar por ' + title + '" />');
            } else {
                $(this).html('');
            }

            $('input', this).on('keyup change', function() {
                if (table.column(i).search() !== this.value) {
                    table
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });
        });

        var table = $('#search').DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            'paging': false,
            'ordering': false,
            'language': {
                "decimal": "",
                "emptyTable": "Sem dados disponíveis na tabela",
                "info": "Exibindo _START_ a _END_ de _TOTAL_ registros",
                "infoEmpty": "Exibindo 0 a 0 de 0 registros",
                "infoFiltered": "(filtrado de _MAX_ total de registros)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Exibir _MENU_ registros",
                "loadingRecords": "Carregando...",
                "processing": "Precessando...",
                "search": "Busca:",
                "zeroRecords": "Não encontramos registros",
                "paginate": {
                    "first": "Primeiro",
                    "last": "Último",
                    "next": "Próximo",
                    "previous": "Anterior"
                },
                "aria": {
                    "sortAscending": ": ativar para classificar a coluna crescente",
                    "sortDescending": ": ativar para classificar a coluna decrescente"
                }
            }
        });
        $('#search_filter').hide('slow/400/fast', function() {

        });
        //$('#container').css( 'display', 'block' );
        //table.columns.adjust().draw();
    });
</script>
