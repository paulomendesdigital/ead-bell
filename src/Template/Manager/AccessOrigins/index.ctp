<?php
/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AccessOrigin[]|\Cake\Collection\CollectionInterface $accessOrigins
 */
?>


<?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' =>'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page"><?php echo __('Access Origins'); ?></li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">

    <div class="panel panel-default">

        <div class="panel-body">

            <?= $this->Form->create(null, ['method' => 'get', 'valueSources' => ['query', 'context']]) ?>
            <div class="row">
                   
                <div class="col-md-3">
                    <?= $this->Form->control('AccessOrigins.id', ['class' => 'form-control']) ?>
                </div>
                   
                <div class="col-md-3">
                    <?= $this->Form->control('AccessOrigins.name', ['class' => 'form-control']) ?>
                </div>
                   
                <div class="col-md-3">
                    <?= $this->Form->control('AccessOrigins.code', ['class' => 'form-control']) ?>
                </div>
                   
                <div class="col-md-3">
                    <?= $this->Form->control('AccessOrigins.status', ['class' => 'form-control']) ?>
                </div>
                   
                <div class="col-md-3">
                    <?= $this->Form->control('AccessOrigins.created', ['class' => 'form-control']) ?>
                </div>
                   
                <div class="col-md-3">
                    <?= $this->Form->control('AccessOrigins.modified', ['class' => 'form-control']) ?>
                </div>
                                
                <div class="col-md-3">
                    <label>&nbsp;</label>
                    <button type="submit" class="btn btn-primary" style="width: 100%">Buscar</button>
                </div>
            </div>
            <?= $this->Form->end() ?>

            <?php echo $this->Form->create(null, ['url' => ['action' => 'delete']]); ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">
                            <button class="btn btn-link" type='submit'><i class="pg-trash"></i></button>
                        </th>
                       
                        <th scope="col">
                            <?= $this->Paginator->sort('id', __('Id')) ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'id'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                       
                        <th scope="col">
                            <?= $this->Paginator->sort('name', __('Name')) ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'name'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                       
                        <th scope="col">
                            <?= $this->Paginator->sort('code', __('Code')) ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'code'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                       
                        <th scope="col">
                            <?= $this->Paginator->sort('status', __('Status')) ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'status'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                       
                        <th scope="col">
                            <?= $this->Paginator->sort('created', __('Created')) ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'created'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                       
                        <th scope="col">
                            <?= $this->Paginator->sort('modified', __('Modified')) ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'modified'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        
                        <th scope="col">
                            <?php echo __('Actions'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($accessOrigins as $accessOrigin){ ?>
                        <tr>
                            <td>
                                <input name="Access Origins[][id]" value="<? echo $accessOrigin->id; ?>" id="accessOrigin-<?php echo $accessOrigin->id; ?>" type="checkbox"/>
                                <label for="page-<?php echo $accessOrigin->id;?>" class="no-padding no-margin"></label>
                            </td>
                                                                              
                            <td><?= $this->Number->format($accessOrigin->id) ?></td>
                                                                                                            
                            <td><?= h($accessOrigin->name) ?></td>
                                                                                                            
                            <td><?= h($accessOrigin->code) ?></td>
                                                                                                          
                            <td><?= $this->Number->format($accessOrigin->status) ?></td>
                                                                                                            
                            <td><?= h($accessOrigin->created) ?></td>
                                                                                                            
                            <td><?= h($accessOrigin->modified) ?></td>
                                                                 
                            <td class="actions">
                                <?= $this->Html->link('<button type="button" class="btn btn-info"><i class="fa-eye fa"></i></button>', ['action' => 'view', $accessOrigin->id], ['escape' => false]) ?>
                                <?= $this->Html->link('<button type="button" class="btn btn-primary"><i class="fa-pencil fa"></i></button>', ['action' => 'edit', $accessOrigin->id], ['escape' => false]) ?>
                                <?= $this->Form->postLink('<button type="button" class="btn btn-danger"><i class="fa-trash fa"></i></button>', ['action' => 'delete', $accessOrigin->id], ['confirm' => __('Are you sure you want to delete # {0}?', $accessOrigin->id), 'escape' => false]) ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php echo $this->Form->end(); ?>

            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
            </div>

        </div>
    </div>
</div>
