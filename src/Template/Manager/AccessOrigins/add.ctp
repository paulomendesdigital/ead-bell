<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AccessOrigin $accessOrigin
 */
?>
<div class="container-fluid container-fixed-lg">
	<ol class="breadcrumb">
		<li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
		<li><?php echo $this->Html->link(__('Access Origins'), ['controller' => 'AccessOrigins', 'action' => 'index'], ['class' => '']); ?></li>
		<li><a class="active"><?php echo __('Add') . ' ' . __('Access Origins'); ?></a></li>
	</ol>
</div>

<div class="container-fluid container-fixed-lg">

	<div class="row">
		<div class="col-lg-7 col-md-6 ">
			<div class="panel panel-transparent">
				<div class="panel-body">
                    <?php //@TODO In order to always have file upload support when model has a field named image or photo ?>
                    <?= $this->Form->create($accessOrigin, ['type' => 'file']) ?>
 
                    <div class='row'>
                        <div class="col-sm-12">                            
                            <?php echo $this->Form->control('name', ['class' => 'form-control input-lg', 'label' => __('Name')]); ?>
                        </div>
                    </div> 
                    <div class='row'>
                        <div class="col-sm-12">                            
                            <?php echo $this->Form->control('code', ['class' => 'form-control input-lg', 'label' => __('Code')]); ?>
                        </div>
                    </div> 
                    <div class='row'>
                        <div class="col-sm-12">                        
                        <?php if(empty($statuses)){ ?>

                            <?php
                            
                            $this->Form->setTemplates([
                                'nestingLabel' => '{{hidden}}<label{{attrs}}>{{text}}</label>{{input}}'
                            ]);

                            echo $this->Form->control('status', array('label' => __('Status'), 'type' => 'checkbox', 'class' => 'switch', 'data-on' => 'success', 'data-off' => 'danger', 'data-on-label' => __('Yes'), 'data-off-label' => __('No'), 'required' => false));

                            ?>
                            <?php }else{ ?>
                                <?php echo $this->Form->control('status', ['class' => 'form-control input-lg', 'label' => __('Status'), 'empty' => __('Select')]); ?>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="clearfix"></div>
					<button class="btn btn-primary" type="submit" data-hide="modal"><?php echo __('Create'); ?></button>
					<button class="btn btn-info" type="submit" name="refer"><?php echo __('Create'); ?> <?php echo __('and continue'); ?></button>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>

</div>