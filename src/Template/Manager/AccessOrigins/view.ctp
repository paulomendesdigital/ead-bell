<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AccessOrigin $accessOrigin
 */
?>

<div class="container-fluid container-fixed-lg">
	<ol class="breadcrumb">
		<li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
		<li><?php echo $this->Html->link(__('Access Origins'), ['controller' => 'AccessOrigins', 'action' => 'index'], ['class' => '']); ?></li>
		<li><a class="active"><?php echo __('View') . ' ' . __('Access Origins'); ?></a></li>
	</ol>
</div>

<div class="container-fluid container-fixed-lg">

	<!-- BASIC INFORMATIONS -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="panel-title"><?php echo __('Basic information'); ?></div>
		</div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
                <tr>
                    <td><strong><?= __('Name') ?></strong></td>
                    <td><?= h($accessOrigin->name) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Code') ?></strong></td>
                    <td><?= h($accessOrigin->code) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Id') ?></strong></td>
                    <td><?= $this->Number->format($accessOrigin->id) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Status') ?></strong></td>
                    <td><?= $this->Number->format($accessOrigin->status) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Created') ?></strong></td>
                    <td><?= h($accessOrigin->created) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Modified') ?></strong></td>
                    <td><?= h($accessOrigin->modified) ?></td>
                </tr>
                
            </table>
             
            <div class="related">
                <h4><?= __('Related Participants') ?></h4>
                <?php if (!empty($accessOrigin->participants)) : ?>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Company Id') ?></th>
                            <th><?= __('Access Origin Id') ?></th>
                            <th><?= __('User Id') ?></th>
                            <th><?= __('Content Id') ?></th>
                            <th><?= __('Question Id') ?></th>
                            <th><?= __('Alternative Id') ?></th>
                            <th><?= __('Correct') ?></th>
                            <th><?= __('Response In Text') ?></th>
                            <th><?= __('Status') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($accessOrigin->participants as $participants) : ?>
                        <tr>
                            <td><?= h($participants->id) ?></td>
                            <td><?= h($participants->company_id) ?></td>
                            <td><?= h($participants->access_origin_id) ?></td>
                            <td><?= h($participants->user_id) ?></td>
                            <td><?= h($participants->content_id) ?></td>
                            <td><?= h($participants->question_id) ?></td>
                            <td><?= h($participants->alternative_id) ?></td>
                            <td><?= h($participants->correct) ?></td>
                            <td><?= h($participants->response_in_text) ?></td>
                            <td><?= h($participants->status) ?></td>
                            <td><?= h($participants->created) ?></td>
                            <td><?= h($participants->modified) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Participants', 'action' => 'view', $participants->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Participants', 'action' => 'edit', $participants->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Participants', 'action' => 'delete', $participants->id], ['confirm' => __('Are you sure you want to delete # {0}?', $participants->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="row"></div>
            <hr/>
            <div class='row'>
                <div class="actions">
    				<?php echo $this->Html->link(__('Edit register'), array('action' => 'edit', $this->request->getAttribute('params')['pass'][0]), array('class' => 'btn btn-info')); ?>
			    </div>
            </div>
        </div>
    </div>

</div>