<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AdvertClick $advertClick
 */
?>

<div class="container-fluid container-fixed-lg">
	<ol class="breadcrumb">
		<li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
		<li><?php echo $this->Html->link(__('Advert Clicks'), ['controller' => 'AdvertClicks', 'action' => 'index'], ['class' => '']); ?></li>
		<li><a class="active"><?php echo __('View') . ' ' . __('Advert Clicks'); ?></a></li>
	</ol>
</div>

<div class="container-fluid container-fixed-lg">

	<!-- BASIC INFORMATIONS -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="panel-title"><?php echo __('Basic information'); ?></div>
		</div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
                <tr>
                    <td>
                        <strong><?php echo __('Advert Zone'); ?></strong>
                    </td>
                    <td>
                        <?= $advertClick->has('advert_zone') ? $this->Html->link($advertClick->advert_zone->name, ['controller' => 'AdvertZones', 'action' => 'view', $advertClick->advert_zone->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong><?php echo __('Advert Campaign'); ?></strong>
                    </td>
                    <td>
                        <?= $advertClick->has('advert_campaign') ? $this->Html->link($advertClick->advert_campaign->name, ['controller' => 'AdvertCampaigns', 'action' => 'view', $advertClick->advert_campaign->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong><?php echo __('Advert'); ?></strong>
                    </td>
                    <td>
                        <?= $advertClick->has('advert') ? $this->Html->link($advertClick->advert->name, ['controller' => 'Adverts', 'action' => 'view', $advertClick->advert->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong><?php echo __('Adverts Advert Zone'); ?></strong>
                    </td>
                    <td>
                        <?= $advertClick->has('adverts_advert_zone') ? $this->Html->link($advertClick->adverts_advert_zone->id, ['controller' => 'AdvertsAdvertZones', 'action' => 'view', $advertClick->adverts_advert_zone->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <td><strong><?= __('Sessionid') ?></strong></td>
                    <td><?= h($advertClick->sessionid) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Id') ?></strong></td>
                    <td><?= $this->Number->format($advertClick->id) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Created') ?></strong></td>
                    <td><?= h($advertClick->created) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Modified') ?></strong></td>
                    <td><?= h($advertClick->modified) ?></td>
                </tr>
                
            </table>
                        <div class="row"></div>
            <hr/>
            <div class='row'>
                <div class="actions">
    				<?php echo $this->Html->link(__('Edit register'), array('action' => 'edit', $this->request->getAttribute('params')['pass'][0]), array('class' => 'btn btn-info')); ?>
			    </div>
            </div>
        </div>
    </div>

</div>