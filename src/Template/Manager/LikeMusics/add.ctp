<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LikeMusic $likeMusic
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Like Musics'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="likeMusics form large-9 medium-8 columns content">
    <?= $this->Form->create($likeMusic) ?>
    <fieldset>
        <legend><?= __('Add Like Music') ?></legend>
        <?php
            echo $this->Form->control('company_id', ['options' => $companies]);
            echo $this->Form->control('user_id', ['options' => $users, 'empty' => true]);
            echo $this->Form->control('code');
            echo $this->Form->control('music');
            echo $this->Form->control('artist');
            echo $this->Form->control('action');
            echo $this->Form->control('program');
            echo $this->Form->control('status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
