<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LikeMusic $likeMusic
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Like Music'), ['action' => 'edit', $likeMusic->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Like Music'), ['action' => 'delete', $likeMusic->id], ['confirm' => __('Are you sure you want to delete # {0}?', $likeMusic->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Like Musics'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Like Music'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="likeMusics view large-9 medium-8 columns content">
    <h3><?= h($likeMusic->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Company') ?></th>
            <td><?= $likeMusic->has('company') ? $this->Html->link($likeMusic->company->name, ['controller' => 'Companies', 'action' => 'view', $likeMusic->company->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $likeMusic->has('user') ? $this->Html->link($likeMusic->user->name, ['controller' => 'Users', 'action' => 'view', $likeMusic->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Music') ?></th>
            <td><?= h($likeMusic->music) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Artist') ?></th>
            <td><?= h($likeMusic->artist) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Action') ?></th>
            <td><?= h($likeMusic->action) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Program') ?></th>
            <td><?= h($likeMusic->program) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($likeMusic->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Code') ?></th>
            <td><?= $this->Number->format($likeMusic->code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($likeMusic->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($likeMusic->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($likeMusic->modified) ?></td>
        </tr>
    </table>
</div>
