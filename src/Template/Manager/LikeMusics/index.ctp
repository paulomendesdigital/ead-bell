<?php
/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\LikeMusic[]|\Cake\Collection\CollectionInterface $likeMusics
 */
?>


<?php //echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' =>'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page">Like Musics</li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">
    <fieldset class="box-search">
        <?php echo $this->Form->create(null,['type'=>'get']);?>
        <?php if( isset($companies) ):?>
            <div class="col-sm-2 col-xs-12">
                <?php echo $this->Form->control('company_id', ['options'=>$companies,'empty'=>'Filtrar por Empresa','class' => 'form-control', 'label' => __('Empresa')]);?>
            </div>
        <?php endif;?>
        <div class="col-sm-2 col-xs-12">
            <?php echo $this->Form->control('created', ['data-content' => "datepicker", 'class' => 'form-control', 'label' => __('Data')]);?>
        </div>
        <div class="col-sm-2 col-xs-12">
            <?php echo $this->Form->control('artist', ['class' => 'form-control', 'label' => __('Artista')]);?>
        </div>
        <div class="col-sm-2 col-xs-12">
            <?php echo $this->Form->control('music', ['class' => 'form-control', 'label' => __('Música')]);?>
        </div>
        <div class="col-sm-2 col-xs-12">
            <?= $this->Form->button(__('buscar'),['class'=>'btn btn-primary']) ?>
            <?= $this->Html->link(__('limpar'),['action'=>'index'],['class'=>'btn btn-warning']) ?>
        </div>
        <div class="row clearfix mt-2 mb-2">
        </div>
        <?php echo $this->Form->end();?>
    </fieldset>
</div>

<div class="container-fluid container-fixed-lg">
    <div class="panel panel-default">
        <div class="table-responsive">
            <table cellpadding="0" cellspacing="0" class="table table-striped table-hover  order">
                <thead>
                    <tr>
                        <th scope="col">
                            <?= $this->Paginator->sort('id') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'id'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('user_id') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'user_id'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('code','Código') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'code'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('music','Música') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'music'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('artist','Artista') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'artist'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('action','Voto') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'action'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('program','Programa') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'program'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('created') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'created'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?php echo __('Actions'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($likeMusics as $likeMusic){ ?>
                        <tr>
                            <td><?= $this->Number->format($likeMusic->id) ?></td>
                            <td><?= $likeMusic->has('user') ? $likeMusic->user->name : '' ?></td>
                            <td><?= h($likeMusic->code) ?></td>
                            <td><?= h($likeMusic->music) ?></td>
                            <td><?= h($likeMusic->artist) ?></td>
                            <td><?= $this->Utility->__FormatLikeDislike($likeMusic->action) ?></td>
                            <td><?= h($likeMusic->program) ?></td>
                            <td><?= h($likeMusic->created->format('d/m/Y H:i')) ?></td>
                            <td class="actions">
                                <?= $this->Html->link('<i class="fa fa-trash" aria-hidden="true"></i>', ['action' => 'delete', $likeMusic->id],['class'=>'btn btn-danger btn-xs text-center','escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $likeMusic->id)]);;?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="panel-footer">
            <?php echo $this->Element('Manager/pagination');?>
        </div>
    </div>
</div>