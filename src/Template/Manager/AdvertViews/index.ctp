<?php

/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AdvertView[]|\Cake\Collection\CollectionInterface $advertViews
 */
?>

<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page"><?php echo __('Advert Views'); ?></li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">

    <div class="panel panel-default">

        <div class="panel-body">

            <?= $this->Form->create(null, ['method' => 'get', 'valueSources' => ['query', 'context']]) ?>
            <div class="row">

                <div class="col-md-4">
                    <?= $this->Form->control('AdvertViews.id', ['class' => 'form-control']) ?>
                </div>

                <div class="col-md-4">
                    <?= $this->Form->control('AdvertViews.advert_zone_id', ['class' => 'form-control', 'empty' => 'Selecione']) ?>
                </div>

                <div class="col-md-4">
                    <?= $this->Form->control('AdvertViews.advert_campaign_id', ['class' => 'form-control', 'empty' => 'Selecione']) ?>
                </div>
            </div>
            <div class='row'>

                <div class="col-md-4">
                    <?= $this->Form->control('AdvertViews.advert_id', ['class' => 'form-control', 'empty' => 'Selecione']) ?>
                </div>

                <div class="col-md-4">
                    <?= $this->Form->control('AdvertViews.created >=', ['class' => 'form-control', 'data-mask-input' => 'date', 'label' => 'Data de Criação - Início']) ?>
                </div>

                <div class="col-md-4">
                    <label>&nbsp;</label>
                    <button type="submit" class="btn btn-primary" style="width: 100%">Buscar</button>
                </div>
            </div>
            <?= $this->Form->end() ?>

            <?php echo $this->Form->create(null, ['url' => ['action' => 'delete']]); ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">
                            <button class="btn btn-link" type='submit'><i class="pg-trash"></i></button>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('id', __('Id')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'id') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('advert_zone_id', __('Advert Zone')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'advert_zone_id') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('advert_campaign_id', __('Advert Campaign')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'advert_campaign_id') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('advert_id', __('Advert')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'advert_id') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('advert_advert_zone_id', __('Advert Advert Zone')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'advert_advert_zone_id') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('sessionid', __('Sessionid')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'sessionid') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('created', __('Created')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'created') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?php echo __('Actions'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($advertViews as $advertView) { ?>
                        <tr>
                            <td>
                                <input name="Advert Views[][id]" value="<? echo $advertView->id; ?>" id="advertView-<?php echo $advertView->id; ?>" type="checkbox" />
                                <label for="page-<?php echo $advertView->id; ?>" class="no-padding no-margin"></label>
                            </td>

                            <td><?= $this->Number->format($advertView->id) ?></td>

                            <td><?= $advertView->has('advert_zone') ? $this->Html->link($advertView->advert_zone->name, ['controller' => 'AdvertZones', 'action' => 'view', $advertView->advert_zone->id]) : '' ?></td>

                            <td><?= $advertView->has('advert_campaign') ? $this->Html->link($advertView->advert_campaign->name, ['controller' => 'AdvertCampaigns', 'action' => 'view', $advertView->advert_campaign->id]) : '' ?></td>

                            <td><?= $advertView->has('advert') ? $this->Html->link($advertView->advert->name, ['controller' => 'Adverts', 'action' => 'view', $advertView->advert->id]) : '' ?></td>

                            <td><?= $advertView->has('adverts_advert_zone') ? $this->Html->link($advertView->adverts_advert_zone->id, ['controller' => 'AdvertsAdvertZones', 'action' => 'view', $advertView->adverts_advert_zone->id]) : '' ?></td>

                            <td><?= h($advertView->sessionid) ?></td>

                            <td><?= h($advertView->created) ?></td>

                            <td class="actions">
                                <?= $this->Html->link('<button type="button" class="btn btn-info"><i class="fa-eye fa"></i></button>', ['action' => 'view', $advertView->id], ['escape' => false]) ?>
                                <?php //echo $this->Html->link('<button type="button" class="btn btn-primary"><i class="fa-pencil fa"></i></button>', ['action' => 'edit', $advertView->id], ['escape' => false]) ?>
                                <?php //echo $this->Form->postLink('<button type="button" class="btn btn-danger"><i class="fa-trash fa"></i></button>', ['action' => 'delete', $advertView->id], ['confirm' => __('Are you sure you want to delete # {0}?', $advertView->id), 'escape' => false]) ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php echo $this->Form->end(); ?>

            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
            </div>

        </div>
    </div>
</div>
