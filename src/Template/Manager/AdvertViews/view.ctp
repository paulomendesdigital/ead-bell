<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AdvertView $advertView
 */
?>

<div class="container-fluid container-fixed-lg">
    <ol class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Advert Views'), ['controller' => 'AdvertViews', 'action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('View') . ' ' . __('Advert Views'); ?></a></li>
    </ol>
</div>

<div class="container-fluid container-fixed-lg">

    <!-- BASIC INFORMATIONS -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title"><?php echo __('Basic information'); ?></div>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered">
                <tr>
                    <td>
                        <strong><?php echo __('Advert Zone'); ?></strong>
                    </td>
                    <td>
                        <?= $advertView->has('advert_zone') ? $this->Html->link($advertView->advert_zone->name, ['controller' => 'AdvertZones', 'action' => 'view', $advertView->advert_zone->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong><?php echo __('Advert Campaign'); ?></strong>
                    </td>
                    <td>
                        <?= $advertView->has('advert_campaign') ? $this->Html->link($advertView->advert_campaign->name, ['controller' => 'AdvertCampaigns', 'action' => 'view', $advertView->advert_campaign->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong><?php echo __('Advert'); ?></strong>
                    </td>
                    <td>
                        <?= $advertView->has('advert') ? $this->Html->link($advertView->advert->name, ['controller' => 'Adverts', 'action' => 'view', $advertView->advert->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong><?php echo __('Adverts Advert Zone'); ?></strong>
                    </td>
                    <td>
                        <?= $advertView->has('adverts_advert_zone') ? $this->Html->link($advertView->adverts_advert_zone->id, ['controller' => 'AdvertsAdvertZones', 'action' => 'view', $advertView->adverts_advert_zone->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <td><strong><?= __('Sessionid') ?></strong></td>
                    <td><?= h($advertView->sessionid) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Id') ?></strong></td>
                    <td><?= $this->Number->format($advertView->id) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Created') ?></strong></td>
                    <td><?= h($advertView->created) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Modified') ?></strong></td>
                    <td><?= h($advertView->modified) ?></td>
                </tr>

            </table>
            <div class="row"></div>
            <hr />
            <div class='row'>
                <div class="actions">
                    <?php //echo $this->Html->link(__('Edit register'), array('action' => 'edit', $this->request->getAttribute('params')['pass'][0]), array('class' => 'btn btn-info'));
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>
