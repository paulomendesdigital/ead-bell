<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Webdoor $webdoor
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Webdoors'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Edit').' '.__('Webdoors'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li class="hidden-xs"><?= $this->Html->link('<i class="fa fa-list"></i> '.__('List Webdoors'), ['action' => 'index'],['class'=>'btn btn-small btn-info','escape'=>false]) ?></li>
        <li><?= $this->Form->postLink('<i class="fa fa-trash"></i> '.__('Delete'),['action' => 'delete', $webdoor->id],['confirm' => __('Are you sure you want to delete # {0}?', $webdoor->id), 'class'=>'btn btn-small btn-danger','escape'=>false]) ?></li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="webdoors form large-9 medium-8 columns content">
        <?= $this->Form->create($webdoor, ['type' => 'file']); ?>
            <fieldset class="col-sm-12">
                <legend><?= __('Edit Webdoor') ?></legend>
                <div class="row clearfix mt-2">
                    <div class="col-sm-3 col-xs-12">
                        <?php echo $this->Form->control('id');?>
                        <?php echo $this->Form->control('company_id', ['options' => $companies, 'type'=>'select','class' => 'form-control', 'label' => __('company')]);?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('start', array('type'=>'text','class'=>'form-control', 'data-content'=>"datetimepicker", 'label' => __('start')));?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('finish', array('type'=>'text','class'=>'form-control', 'data-content'=>"datetimepicker", 'label' => __('Despublicar Em')));?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('ordination', array('class' => 'form-control', 'label' => __('ordination')));?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="col-sm-12">
                        <?php echo $this->Form->control('name', array('class' => 'form-control input-lg', 'label' => __('name')));?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="col-sm-3 col-xs-12">
                        <?php echo $this->Form->control('link', array('type'=>'text','class'=>'form-control', 'label' => __('Link do Webdoor')));?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('button_label', array('type'=>'text','class'=>'form-control', 'label' => __('Texto do link do botão')));?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('button_bg_color', array('type'=>'color','class'=>'form-control', 'label' => __('Cor de fundo do botão')));?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('button_text_color', array('type'=>'color','class'=>'form-control', 'label' => __('Cor de texto do botão')));?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('target', array('options'=>['_self'=>'Mesma janela','_blank'=>'Nova janela'],'class'=>'form-control', 'label' => __('Target')));?>
                    </div>
                </div>
                    
                <div class="row clearfix mt-2">
                    <div class="col-sm-12 col-xs-12">
                        <?php echo $this->Form->control('title', array('class' => 'form-control', 'id'=>'ckfinderDescription', 'label' => __('Texto do Webdoor'), 'div' => false));?>
                    </div>
                </div>

                <div class="row clearfix mt-2">
                    <div class="col-sm-4 col-xs-12">
                        <fieldset style="border: 1px solid silver; padding: 15px; border-radius: 4px;">  
                            <?php echo $this->Form->control('image_name', array('class' => 'form-control  input-lg', 'type'=>'file', 'label' => "Imagem Frente Webdoor ({$image_front_size})"));?>
                            <?php if( !empty($webdoor->image_name) ):?>
                                <?php 
                                $image_path = str_replace('webroot', '', $webdoor->image_path);
                                echo $this->Html->image("{$image_path}/{$webdoor->image_name}",['class'=>'img-responsive']);?>
                                <div class="form-group row text-center" data-hide="modal">
                                    <label class="col-sm-12" class="">Remover Imagem?</label>
                                    <div class="col-sm-12"><?php echo $this->Form->control('image_name_remove', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false)); ?></div>
                                </div>
                                <hr />
                            <?php endif;?>
                        </fieldset>
                    </div>
                
                    <div class="col-sm-4 col-xs-12">
                        <fieldset style="border: 1px solid silver; padding: 15px; border-radius: 4px;">  
                            <?php echo $this->Form->control('image_background_name', array('class' => 'form-control  input-lg', 'type'=>'file', 'label' => "Imagem BG Webdoor ({$image_background_size})"));?>
                            <?php if( !empty($webdoor->image_background_name) ): ?>
                                <?php 
                                $image_background_path = str_replace('webroot', '', $webdoor->image_background_path);
                                echo $this->Html->image("{$image_background_path}/{$webdoor->image_background_name}",['class'=>'img-responsive']);
                                ?>
                                <div class="form-group row text-center" data-hide="modal">
                                    <label class="col-sm-12" class="">Remover Imagem?</label>
                                    <div class="col-sm-12"><?php echo $this->Form->control('image_background_name_remove', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false)); ?></div>
                                </div>
                                <hr />
                            <?php endif;?>
                        </fieldset>
                    </div>

                    <div class="col-sm-4 col-xs-12">
                        <fieldset style="border: 1px solid silver; padding: 15px; border-radius: 4px;">  
                            <?php echo $this->Form->control('image_mobile_name', array('class' => 'form-control  input-lg', 'type'=>'file', 'label' => "Imagem BG Celular ({$image_mobile_size})"));?>
                            <?php if( !empty($webdoor->image_mobile_name) ): ?>
                                <?php 
                                $image_mobile_path = str_replace('webroot', '', $webdoor->image_mobile_path);
                                echo $this->Html->image("{$image_mobile_path}/{$webdoor->image_mobile_name}",['class'=>'img-responsive']);?>
                                <div class="form-group row text-center" data-hide="modal">
                                    <label class="col-sm-12" class="">Remover Imagem?</label>
                                    <div class="col-sm-12"><?php echo $this->Form->control('image_mobile_name_remove', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false)); ?></div>
                                </div>
                                <hr />
                            <?php endif;?>
                        </fieldset>
                    </div>
                </div>

                <div class="row clearfix mt-2">
                    <div class="form-group" data-hide="modal">
                        <label class="col-sm-12" class="">Status</label>
                        <div class="col-sm-12"><?php echo $this->Form->control('status', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false)); ?></div>
                    </div>
                </div>
                <div class="row clearfix mt-2 mb-2">
                    <div class="col-sm-12 col-xs-12">
                        <?= $this->Form->button(__('Edit'),['class'=>'btn btn-primary']) ?>
                        <?= $this->Form->button(__('Edit and continue'),['name'=>'refer','class'=>'btn btn-info']) ?>
                    </div>
                </div>
            </fieldset>
        <?= $this->Form->end() ?>
    </div>
</div>
