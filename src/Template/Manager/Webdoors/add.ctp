<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Webdoor $webdoor
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Webdoors'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Add').' '.__('Webdoors'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link('<i class="fa fa-list"></i> '.__('List Programs'), ['action' => 'index'],['class'=>'btn btn-small btn-info','escape'=>false]) ?></li>
    </ul>
</nav>
<div class="container-fluid container-fixed-lg">
    <div class="webdoors form large-9 medium-8 columns content">
        <?= $this->Form->create($webdoor, ['type' => 'file']); ?>
            <fieldset class="col-sm-12">
                <legend><?= __('Add Webdoor') ?></legend>
                <div class="row clearfix mt-2">
                    <div class="col-sm-3 col-xs-12">
                        <?php echo $this->Form->control('company_id', ['options' => $companies, 'type'=>'select','class' => 'form-control', 'label' => __('company')]);?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('start', array('type'=>'text','class'=>'form-control input-lg', 'data-content'=>"datetimepicker", 'label' => __('start')));?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('finish', array('type'=>'text','class'=>'form-control input-lg', 'data-content'=>"datetimepicker", 'label' => __('Despublicar Em')));?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('ordination', array('class' => 'form-control', 'label' => __('ordination')));?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="col-sm-12">
                        <?php echo $this->Form->control('name', array('class' => 'form-control input-lg', 'label' => __('name')));?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="col-sm-3 col-xs-12">
                        <?php echo $this->Form->control('link', array('type'=>'text','class'=>'form-control', 'label' => __('Link do Webdoor')));?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('button_label', array('type'=>'text','class'=>'form-control', 'label' => __('Texto do link do botão')));?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('button_bg_color', array('type'=>'color','class'=>'form-control', 'label' => __('Cor de fundo do botão')));?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('button_text_color', array('type'=>'color', 'value'=>'#FFFFFF','class'=>'form-control', 'label' => __('Cor de texto do botão')));?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('target', array('options'=>['_self'=>'Mesma janela','_blank'=>'Nova janela'],'class'=>'form-control', 'label' => __('Target')));?>
                    </div>
                </div>
                    
                <div class="row clearfix mt-2">
                    <div class="col-sm-12 col-xs-12">
                        <?php echo $this->Form->control('title', array('class' => 'form-control', 'id'=>'ckfinderDescription', 'label' => __('Texto do Webdoor'), 'div' => false));?>
                    </div>
                </div>

                <div class="row clearfix mt-2">
                    <div class="col-sm-4 col-xs-12">
                        <?php echo $this->Form->control('image_name', array('class' => 'form-control  input-lg', 'type'=>'file', 'label' => "Imagem Frente Webdoor ({$image_front_size})"));?>
                    </div>
                
                    <div class="col-sm-4 col-xs-12">
                        <?php echo $this->Form->control('image_background_name', array('class' => 'form-control  input-lg', 'type'=>'file', 'label' => "Imagem BG Webdoor ({$image_background_size})"));?>
                    </div>

                    <div class="col-sm-4 col-xs-12">
                        <?php echo $this->Form->control('image_mobile_name', array('class' => 'form-control  input-lg', 'type'=>'file', 'label' => "Imagem BG Celular ({$image_mobile_size})"));?>
                    </div>
                </div>

                <div class="row clearfix mt-2">
                    <div class="form-group" data-hide="modal">
                        <label class="col-sm-12" class="">Status</label>
                        <div class="col-sm-12"><?php echo $this->Form->control('status', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false, 'checked' => 'checked')); ?></div>
                    </div>
                </div>
                <div class="row clearfix mt-2 mb-2">
                    <div class="col-sm-12 col-xs-12">
                        <?= $this->Form->button(__('Add'),['class'=>'btn btn-primary']) ?>
                    </div>
                </div>
            </fieldset>
        <?= $this->Form->end() ?>
    </div>
</div>

