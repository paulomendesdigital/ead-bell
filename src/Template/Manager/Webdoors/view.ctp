<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Webdoor $webdoor
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Webdoors'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Ver').' '.__('Webdoor'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li class="hidden-xs"><?= $this->Html->link('<i class="fa fa-list"></i> '.__('List Webdoors'), ['action' => 'index'],['class'=>'btn btn-small btn-info','escape'=>false]) ?></li>
        <li class="hidden-xs"><?= $this->Html->link('<i class="fa fa-edit"></i> '.__('Edit Webdoor'), ['action' => 'edit', $webdoor->id],['class'=>'btn btn-small btn-success','escape'=>false]) ?></li>
        <li><?= $this->Form->postLink('<i class="fa fa-trash"></i> '.__('Delete'),['action' => 'delete', $webdoor->id],['confirm' => __('Are you sure you want to delete # {0}?', $webdoor->id), 'class'=>'btn btn-small btn-danger','escape'=>false]) ?></li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="webdoors view large-9 medium-8 columns content">
        <h3>Nome: <?= h($webdoor->name) ?></h3>
        <table class="vertical-table">
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($webdoor->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Company') ?>:</th>
                <td><?= $webdoor->has('company') ? h($webdoor->company->name) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Texto do link do botão') ?>:</th>
                <td><?= h($webdoor->button_label) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Link do Webdoor') ?>:</th>
                <td><?= h($webdoor->link) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Imagem Frente Webdoor') ?>:</th>
                <td><?= h($webdoor->image_name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Image Path') ?>:</th>
                <td><?= h($webdoor->image_path) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Imagem BG Webdoor') ?>:</th>
                <td><?= h($webdoor->image_background_name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Image Background Path') ?>:</th>
                <td><?= h($webdoor->image_background_path) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Imagem BG Celular') ?>:</th>
                <td><?= h($webdoor->image_mobile_name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Image Mobile Path') ?>:</th>
                <td><?= h($webdoor->image_mobile_path) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Status') ?></th>
                <td><?= $this->Number->format($webdoor->status) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Ordination') ?></th>
                <td><?= $this->Number->format($webdoor->ordination) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Start') ?></th>
                <td><?= $webdoor->start->format('d/m/Y') ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Finish') ?></th>
                <td><? if(isset($webdoor->finish)) $webdoor->finish->format('d/m/Y') ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($webdoor->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($webdoor->modified) ?></td>
            </tr>
        </table>
        <div class="row">
            <h4><?= __('Title') ?></h4>
            <?= $this->Text->autoParagraph(h($webdoor->title)); ?>
        </div>
        <div class="row col-12">
            <?php 
            $image_background_path = str_replace('webroot', '', $webdoor->image_background_path);
            echo $this->Html->image("{$image_background_path}/{$webdoor->image_background_name}",['class'=>'img-responsive']);?>
        </div>
    </div>
</div>
