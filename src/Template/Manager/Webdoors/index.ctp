<?php
/**
 * @copyright 2019
 * @author Dayvison Silva - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Webdoor[]|\Cake\Collection\CollectionInterface $webdoors
 */
?>

<?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' =>'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page">Webdoors</li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">
    <fieldset class="box-search">
        <?php echo $this->Form->create(null,['type'=>'get']);?>
        <?php if( isset($companies) ):?>
            <div class="col-sm-2 col-xs-12">
                <?php echo $this->Form->control('company_id', ['options'=>$companies,'empty'=>'Filtrar por Empresa','class' => 'form-control', 'label' => __('Empresa')]);?>
            </div>
        <?php endif;?>
        <div class="col-sm-2 col-xs-12">
            <?php echo $this->Form->control('name', ['class' => 'form-control', 'label' => __('Nome')]);?>
        </div>
        <div class="col-sm-2 col-xs-12">
            <?= $this->Form->button(__('buscar'),['class'=>'btn btn-primary']) ?>
        </div>
        <div class="row clearfix mt-2 mb-2">
            
        </div>
        <?php echo $this->Form->end();?>
    </fieldset>
</div>

<div class="container-fluid container-fixed-lg">
    <div class="panel panel-default">
        <div class="table-responsive">
            <?php echo $this->Form->create($this->request->getParam('controller'), ['url' => ['action' => 'delete']]); ?>
                <table cellpadding="0" cellspacing="0" class="table table-striped table-hover  order">
                    <thead>
                        <tr>
                            <th scope="col">
                                <?= $this->Paginator->sort('id') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'id'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('company_id',__('company_id')) ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'company_id'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('name',__('name')) ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'name'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('start','Período') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'start'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col" nowrap="nowrap">
                                <?= $this->Paginator->sort('button_label',__('button_label')) ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'button_label'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('link') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'link'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('image_name','Imagens') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'image_name'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('status') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'status'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('ordination',__('ordination')) ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'ordination'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('created',__('created')) ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'created'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col" class="hidden">
                                <?= $this->Paginator->sort('modified',__('modified')) ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'modified'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?php echo __('Actions'); ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($webdoors as $webdoor){ ?>
                            <tr>
                                <td><?= $this->Number->format($webdoor->id) ?></td>
                                <td><?= $webdoor->has('company') ? $webdoor->company->name : '' ?></td>
                                <td style="min-width: 200px;"><?= h($webdoor->name) ?></td>
                                <td nowrap="nowrap">
                                    <?php if( $webdoor->start ):?>
                                        Início: <?= $webdoor->start ? $webdoor->start->format('d/m/Y') : '--'; ?><br />
                                        Fim: <?= $webdoor->finish ? $webdoor->finish->format('d/m/Y') : ''; ?>
                                    <?php else:?>
                                        --
                                    <?php endif;?>
                                </td>
                                <td><?= h($webdoor->button_label) ?></td>
                                <td><a class='btn btn-warning btn-xs text-center' href="<?= $this->Utility->__GenerateManagerLinkWebdoor($webdoor) ?>"target="_blank"><i class="fa fa-link" aria-hidden="true"></i></a></td>
                                <td nowrap="nowrap">
                                    <?= $webdoor->image_name ? "Frente - <i class='text-success fa fa-check'></i>" : "Frente - <i class='text-danger fa fa-ban'></i>"; ?><br />
                                    <?= $webdoor->image_background_name ? "Background - <i class='text-success fa fa-check'></i>" : "Background - <i class='text-danger fa fa-ban'></i>"; ?><br />
                                    <?= $webdoor->image_mobile_name ? "Imagem Celular - <i class='text-success fa fa-check'></i>" : "Imagem Celular - <i class='text-danger fa fa-ban'></i>"; ?><br />
                                </td>
                                <td><?php echo $this->Utility->__FormatStatus( $this->Number->format($webdoor->status) );?></td>
                                <td><?= $webdoor->ordination ?></td>
                                <td><?= $webdoor->created->format('d/m/Y H:i'); ?></td>
                                <td  class="hidden"><?= $webdoor->modified->format('d/m/Y'); ?></td>
                                <td class="actions" nowrap="nowrap">
                                    <?= $this->Html->link('<i class="fa fa-eye" aria-hidden="true"></i>', ['action' => 'view', $webdoor->id],['class'=>'btn btn-info btn-xs text-center','escape'=>false]) ?>
                                    <?= $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i>', ['action' => 'edit', $webdoor->id],['class'=>'btn btn-primary btn-xs text-center','escape'=>false]) ?>
                                    <?= $this->Html->link('<i class="fa fa-trash" aria-hidden="true"></i>', ['action' => 'delete', $webdoor->id],['class'=>'btn btn-danger btn-xs text-center','escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $webdoor->id)]); ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php echo $this->Form->end();?>
        </div>
        <div class="panel-footer">
            <?php echo $this->Element('Manager/pagination');?>
        </div>
    </div>
</div>

