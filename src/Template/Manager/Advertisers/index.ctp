<?php

/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Advertiser[]|\Cake\Collection\CollectionInterface $advertisers
 */
?>


<?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' => 'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page"><?php echo __('Advertisers'); ?></li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">
    <fieldset class="box-search">
       <?= $this->Form->create(null, ['method' => 'get', 'valueSources' => ['query', 'context']]) ?>
            <div class="col-md-3 col-xs-3">
                <?= $this->Form->control('Advertisers.id', ['class' => 'form-control']) ?>
            </div>
            <div class="col-md-3 col-xs-9">
                <?= $this->Form->control('Advertisers.name LIKE', ['class' => 'form-control', 'label' => __('Name')]) ?>
            </div>
            <div class="col-md-3 col-xs-6">
                <?= $this->Form->control('Advertisers.status', ['class' => 'form-control', 'empty' => 'Selecione', 'options' => [0 => 'Inativo', 1 => 'Ativo']]) ?>
            </div>
            <div class="col-md-3 col-xs-6">
                <button type="submit" class="btn btn-primary" style="width: 100%">Buscar</button>
            </div>
        <?php echo $this->Form->end();?>
    </fieldset>
</div>

<div class="container-fluid container-fixed-lg">
    <div class="panel panel-default">
        <div class="table-responsive">
            <?php echo $this->Form->create($this->request->getParam('controller'), ['url' => ['action' => 'delete']]); ?>
            <table cellpadding="0" cellspacing="0" class="table table-striped table-hover  order">
                <thead>
                    <tr>
                        <th scope="col" class="hidden">
                            <button class="btn btn-link" type='submit'><i class="pg-trash"></i></button>
                        </th>

                        <th scope="col" nowrap="nowrap">
                            <?= $this->Paginator->sort('id', __('Id')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'id') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col" nowrap="nowrap">
                            <?= $this->Paginator->sort('name', __('Name')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'name') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col" nowrap="nowrap">
                            <?= $this->Paginator->sort('status', __('Status')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'status') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col" nowrap="nowrap">
                            <?= $this->Paginator->sort('created', __('Created')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'created') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col" nowrap="nowrap">
                            <?= $this->Paginator->sort('modified', __('Modified')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'modified') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?php echo __('Actions'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($advertisers as $advertiser) { ?>
                        <tr>
                            <td class="hidden">
                                <input name="Advertisers[][id]" value="<? echo $advertiser->id; ?>" id="advertiser-<?php echo $advertiser->id; ?>" type="checkbox" />
                                <label for="page-<?php echo $advertiser->id; ?>" class="no-padding no-margin"></label>
                            </td>

                            <td><?= $this->Number->format($advertiser->id) ?></td>

                            <td><?= h($advertiser->name) ?></td>

                            <td><?= $this->Utility->__FormatStatus($advertiser->status) ?></td>

                            <td><?= h($advertiser->created->format('d/m/Y H:i:s')) ?></td>

                            <td><?= h($advertiser->modified->format('d/m/Y H:i:s')) ?></td>

                            <td class="actions" nowrap="nowrap">
                                <?= $this->Html->link('<button type="button" class="btn btn-info"><i class="fa-eye fa"></i></button>', ['action' => 'view', $advertiser->id], ['escape' => false]) ?>
                                <?= $this->Html->link('<button type="button" class="btn btn-primary"><i class="fa-pencil fa"></i></button>', ['action' => 'edit', $advertiser->id], ['escape' => false]) ?>
                                <?= $this->Form->postLink('<button type="button" class="btn btn-danger"><i class="fa-trash fa"></i></button>', ['action' => 'delete', $advertiser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $advertiser->id), 'escape' => false]) ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php echo $this->Form->end(); ?>
        </div>
        <div class="panel-footer">
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
            </div>
        </div>
    </div>
</div>
