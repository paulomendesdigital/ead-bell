<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Advertiser $advertiser
 */
?>

<div class="container-fluid container-fixed-lg">
    <ol class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Advertisers'), ['controller' => 'Advertisers', 'action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('View') . ' ' . __('Advertisers'); ?></a></li>
    </ol>
</div>

<div class="container-fluid container-fixed-lg">

    <!-- BASIC INFORMATIONS -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title"><?php echo __('Basic information'); ?></div>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered">
                <tr>
                    <td><strong><?= __('Name') ?></strong></td>
                    <td><?= h($advertiser->name) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Id') ?></strong></td>
                    <td><?= $this->Number->format($advertiser->id) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Status') ?></strong></td>
                    <td><?= $this->Utility->__FormatStatus($advertiser->status) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Created') ?></strong></td>
                    <td><?= h($advertiser->created) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Modified') ?></strong></td>
                    <td><?= h($advertiser->modified) ?></td>
                </tr>

            </table>

            <div class="related">
                <h4><?= __('Related Advert Campaigns') ?></h4>
                <?php if (!empty($advertiser->advert_campaigns)) : ?>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th><?= __('Id') ?></th>
                                <th><?= __('Company') ?></th>
                                <th><?= __('Name') ?></th>
                                <th><?= __('Start Date') ?></th>
                                <th><?= __('Finish Date') ?></th>
                                <th><?= __('Weight') ?></th>
                                <th><?= __('Max Views') ?></th>
                                <th><?= __('Unique Views Count') ?></th>
                                <th><?= __('Views Count') ?></th>
                                <th><?= __('Status') ?></th>
                                <th><?= __('Created') ?></th>
                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                            <?php foreach ($advertiser->advert_campaigns as $advertCampaigns) : ?>
                                <tr>
                                    <td><?= h($advertCampaigns->id) ?></td>
                                    <td><?= h($advertCampaigns->company->name)?></td>
                                    <td><?= $this->Html->link($advertCampaigns->name, ['controller' => 'AdvertCampaigns', 'action' => 'view', $advertCampaigns->id]) ?></td>
                                    <td><?= h($advertCampaigns->start_date) ?></td>
                                    <td><?= h($advertCampaigns->finish_date) ?></td>
                                    <td><?= h($advertCampaigns->weight) ?></td>
                                    <td><?= h($advertCampaigns->max_views) ?></td>
                                    <td><?= h($advertCampaigns->unique_views_count) ?></td>
                                    <td><?= h($advertCampaigns->views_count) ?></td>
                                    <td><?= $this->Utility->__FormatStatus($advertCampaigns->status) ?></td>
                                    <td><?= h($advertCampaigns->created) ?></td>
                                    <td class="actions">
                                        <?= $this->Html->link(__('View'), ['controller' => 'AdvertCampaigns', 'action' => 'view', $advertCampaigns->id]) ?>
                                        <?= $this->Html->link(__('Edit'), ['controller' => 'AdvertCampaigns', 'action' => 'edit', $advertCampaigns->id]) ?>
                                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'AdvertCampaigns', 'action' => 'delete', $advertCampaigns->id], ['confirm' => __('Are you sure you want to delete # {0}?', $advertCampaigns->id)]) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                <?php endif; ?>
            </div>
            <div class="row"></div>
            <hr />
            <div class='row'>
                <div class="actions">
                    <?php echo $this->Html->link(__('Edit register'), array('action' => 'edit', $this->request->getAttribute('params')['pass'][0]), array('class' => 'btn btn-info')); ?>
                </div>
            </div>
        </div>
    </div>

</div>
