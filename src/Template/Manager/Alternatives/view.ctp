<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Alternative $alternative
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Alternative'), ['action' => 'edit', $alternative->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Alternative'), ['action' => 'delete', $alternative->id], ['confirm' => __('Are you sure you want to delete # {0}?', $alternative->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Alternatives'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Alternative'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Questions'), ['controller' => 'Questions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Question'), ['controller' => 'Questions', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Participants'), ['controller' => 'Participants', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Participant'), ['controller' => 'Participants', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="alternatives view large-9 medium-8 columns content">
    <h3><?= h($alternative->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Question') ?></th>
            <td><?= $alternative->has('question') ? $this->Html->link($alternative->question->id, ['controller' => 'Questions', 'action' => 'view', $alternative->question->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($alternative->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Correct') ?></th>
            <td><?= $this->Number->format($alternative->is_correct) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($alternative->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($alternative->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($alternative->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Alternative') ?></h4>
        <?= $this->Text->autoParagraph(h($alternative->alternative)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Participants') ?></h4>
        <?php if (!empty($alternative->participants)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Content Id') ?></th>
                <th scope="col"><?= __('Question Id') ?></th>
                <th scope="col"><?= __('Alternative Id') ?></th>
                <th scope="col"><?= __('Correct') ?></th>
                <th scope="col"><?= __('Response In Text') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($alternative->participants as $participants): ?>
            <tr>
                <td><?= h($participants->id) ?></td>
                <td><?= h($participants->user_id) ?></td>
                <td><?= h($participants->content_id) ?></td>
                <td><?= h($participants->question_id) ?></td>
                <td><?= h($participants->alternative_id) ?></td>
                <td><?= h($participants->correct) ?></td>
                <td><?= h($participants->response_in_text) ?></td>
                <td><?= h($participants->status) ?></td>
                <td><?= h($participants->created) ?></td>
                <td><?= h($participants->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['controller' => 'Participants', 'action' => 'view', $participants->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Participants', 'action' => 'edit', $participants->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Participants', 'action' => 'delete', $participants->id], ['confirm' => __('Are you sure you want to delete # {0}?', $participants->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
