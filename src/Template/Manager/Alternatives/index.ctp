<?php
/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Alternative[]|\Cake\Collection\CollectionInterface $alternatives
 */
?>


<?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' =>'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page">Alternatives</li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">

    <div class="panel panel-default">

        <div class="panel-body">

            <?php echo $this->Form->create($this->request->getParam('controller'), ['url' => ['action' => 'delete']]); ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">
                            <button class="btn btn-link" type='submit'><i class="pg-trash"></i></button>
                        </th>
                                                    <th scope="col">
                                <?= $this->Paginator->sort('id') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'id'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                                                    <th scope="col">
                                <?= $this->Paginator->sort('question_id') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'question_id'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                                                    <th scope="col">
                                <?= $this->Paginator->sort('is_correct') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'is_correct'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                                                    <th scope="col">
                                <?= $this->Paginator->sort('status') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'status'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                                                    <th scope="col">
                                <?= $this->Paginator->sort('created') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'created'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                                                    <th scope="col">
                                <?= $this->Paginator->sort('modified') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'modified'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                                                <th scope="col">
                            <?php echo __('Actions'); ?>
                        </th>
                    </tr>
                </thead>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($alternatives as $alternative){ ?>
                <tr>
                    <td>
                        <input name="data[Alternatives][][Alternatives][id]" value="<? echo $alternative->id; ?>" id="alternative-<?php echo $alternative->id; ?>" type="checkbox"/>
                        <label for="page-<?php echo $alternative->id;?>" class="no-padding no-margin"></label>
                    </td>
                                                                                                                                                                                                                                        <td><?= $this->Number->format($alternative->id) ?></td>
                                                                                                                                                                                                                    <td><?= $alternative->has('question') ? $this->Html->link($alternative->question->id, ['controller' => 'Questions', 'action' => 'view', $alternative->question->id]) : '' ?></td>
                                                                                                                                                                                                                                                                                                                    <td><?= $this->Number->format($alternative->is_correct) ?></td>
                                                                                                                                                                                                                                                                                            <td><?= $this->Number->format($alternative->status) ?></td>
                                                                                                                                                                                                                                                                                            <td><?= h($alternative->created) ?></td>
                                                                                                                                                                                                                                                                                            <td><?= h($alternative->modified) ?></td>
                                                                                                                <td class="actions">
                        <?= $this->Html->link(__('Ver'), ['action' => 'view', $alternative->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $alternative->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $alternative->id], ['confirm' => __('Are you sure you want to delete # {0}?', $alternative->id)]) ?>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <?php echo $this->Form->end(); ?>

    <?php echo $this->Element('Manager/pagination');?>

</div></div></div>
