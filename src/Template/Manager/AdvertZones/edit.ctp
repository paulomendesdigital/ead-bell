<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AdvertZone $advertZone
 */
?>
<div class="container-fluid container-fixed-lg">
    <ol class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Advert Zones'), ['controller' => 'AdvertZones', 'action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Edit') . ' ' . __('Advert Zones'); ?></a></li>
    </ol>
</div>

<div class="container-fluid container-fixed-lg">

    <div class="row">
        <div class="col-lg-7 col-md-6 ">
            <div class="panel panel-transparent">
                <div class="panel-body">
                    <?php //@TODO In order to always have file upload support when model has a field named image or photo
                    ?>
                    <?= $this->Form->create($advertZone, ['type' => 'file']) ?>

                    <div class='row'>
                        <div class="col-sm-12">
                            <?php echo $this->Form->control('company_id', ['options' => $companies, 'class' => 'form-control', 'empty' => __('Select'), 'label' => __('Company Id')]); ?>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="col-sm-12">
                            <?php echo $this->Form->control('name', ['class' => 'form-control input-lg', 'label' => __('Name')]); ?>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="col-sm-12">
                            <?php echo $this->Form->control('code', ['class' => 'form-control input-lg', 'label' => __('Code')]); ?>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="col-sm-12">
                            <?php echo $this->Form->control('width', ['class' => 'form-control input-lg', 'label' => __('Width')]); ?>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="col-sm-12">
                            <?php echo $this->Form->control('height', ['class' => 'form-control input-lg', 'label' => __('Height')]); ?>
                        </div>
                    </div>
                    <div class='row'>
                        <div class="col-sm-12">
                            <?php if (empty($statuses)) { ?>

                                <?php

                                $this->Form->setTemplates([
                                    'nestingLabel' => '{{hidden}}<label{{attrs}}>{{text}}</label>{{input}}'
                                ]);

                                echo $this->Form->control('status', array('label' => __('Status'), 'type' => 'checkbox', 'class' => 'switch', 'data-on' => 'success', 'data-off' => 'danger', 'data-on-label' => __('Yes'), 'data-off-label' => __('No'), 'required' => false));

                                ?>
                            <?php } else { ?>
                                <?php echo $this->Form->control('status', ['class' => 'form-control input-lg', 'label' => __('Status'), 'empty' => __('Select')]); ?>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <button class="btn btn-primary" type="submit" data-hide="modal"><?php echo __('Edit'); ?></button>
                    <button class="btn btn-info" type="submit" name="refer"><?php echo __('Edit'); ?> <?php echo __('and continue'); ?></button>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>

</div>
