<?php

/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AdvertZone[]|\Cake\Collection\CollectionInterface $advertZones
 */
?>


<?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' => 'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page"><?php echo __('Advert Zones'); ?></li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">

    <div class="panel panel-default">

        <div class="panel-body">

            <?= $this->Form->create(null, ['method' => 'get', 'valueSources' => ['query', 'context']]) ?>
            <div class="row">

                <div class="col-md-4">
                    <?= $this->Form->control('AdvertZones.id', ['class' => 'form-control']) ?>
                </div>

                <div class="col-md-4">
                    <?= $this->Form->control('AdvertZones.company_id', ['class' => 'form-control', 'empty' => 'Selecione']) ?>
                </div>

                <div class="col-md-4">
                    <?= $this->Form->control('AdvertZones.name LIKE', ['class' => 'form-control', 'label' => __('Name')]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?= $this->Form->control('AdvertZones.code LIKE', ['class' => 'form-control', 'label' => __('Code')]) ?>
                </div>

                <div class="col-md-4">
                    <?= $this->Form->control('AdvertZones.status', ['class' => 'form-control', 'options' => [0 => 'Inativo', 1 => 'Ativo'], 'empty' => 'Selecione']) ?>
                </div>

                <div class="col-md-4">
                    <label>&nbsp;</label>
                    <button type="submit" class="btn btn-primary" style="width: 100%">Buscar</button>
                </div>
            </div>
            <?= $this->Form->end() ?>

            <?php echo $this->Form->create(null, ['url' => ['action' => 'delete']]); ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">
                            <button class="btn btn-link" type='submit'><i class="pg-trash"></i></button>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('id', __('Id')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'id') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('company_id', __('Company')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'company_id') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('name', __('Name')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'name') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('code', __('Code')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'code') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('width', __('Width')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'width') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('height', __('Height')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'height') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('clicks_count', __('Clicks Count')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'clicks_count') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('views_count', __('Views Count')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'views_count') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('status', __('Status')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'status') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('created', __('Created')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'created') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('modified', __('Modified')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'modified') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?php echo __('Actions'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($advertZones as $advertZone) { ?>
                        <tr>
                            <td>
                                <input name="Advert Zones[][id]" value="<? echo $advertZone->id; ?>" id="advertZone-<?php echo $advertZone->id; ?>" type="checkbox" />
                                <label for="page-<?php echo $advertZone->id; ?>" class="no-padding no-margin"></label>
                            </td>

                            <td><?= $this->Number->format($advertZone->id) ?></td>

                            <td><?= $advertZone->has('company') ? $this->Html->link($advertZone->company->name, ['controller' => 'Companies', 'action' => 'view', $advertZone->company->id]) : '' ?></td>

                            <td><?= h($advertZone->name) ?></td>

                            <td><?= h($advertZone->code) ?></td>

                            <td><?= !is_null($advertZone->width) ? $this->Number->format($advertZone->width) . 'px' : null ?></td>

                            <td><?= !is_null($advertZone->height) ? $this->Number->format($advertZone->height) . 'px' : null ?></td>

                            <td><?= $this->Number->format($advertZone->clicks_count) ?></td>

                            <td><?= $this->Number->format($advertZone->views_count) ?></td>

                            <td><?= $this->Utility->__FormatStatus($advertZone->status) ?></td>

                            <td><?= h($advertZone->created) ?></td>

                            <td><?= h($advertZone->modified) ?></td>

                            <td class="actions">
                                <?= $this->Html->link('<button type="button" class="btn btn-info"><i class="fa-eye fa"></i></button>', ['action' => 'view', $advertZone->id], ['escape' => false]) ?>
                                <?= $this->Html->link('<button type="button" class="btn btn-primary"><i class="fa-pencil fa"></i></button>', ['action' => 'edit', $advertZone->id], ['escape' => false]) ?>
                                <?= $this->Form->postLink('<button type="button" class="btn btn-danger"><i class="fa-trash fa"></i></button>', ['action' => 'delete', $advertZone->id], ['confirm' => __('Are you sure you want to delete # {0}?', $advertZone->id), 'escape' => false]) ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php echo $this->Form->end(); ?>

            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
            </div>

        </div>
    </div>
</div>
