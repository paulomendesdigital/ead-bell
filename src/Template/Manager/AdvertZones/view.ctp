<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AdvertZone $advertZone
 */
?>

<div class="container-fluid container-fixed-lg">
    <ol class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Advert Zones'), ['controller' => 'AdvertZones', 'action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('View') . ' ' . __('Advert Zones'); ?></a></li>
    </ol>
</div>

<div class="container-fluid container-fixed-lg">

    <!-- BASIC INFORMATIONS -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title"><?php echo __('Basic information'); ?></div>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered">
                <tr>
                    <td>
                        <strong><?php echo __('Company'); ?></strong>
                    </td>
                    <td>
                        <?= $advertZone->has('company') ? $this->Html->link($advertZone->company->name, ['controller' => 'Companies', 'action' => 'view', $advertZone->company->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <td><strong><?= __('Name') ?></strong></td>
                    <td><?= h($advertZone->name) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Code') ?></strong></td>
                    <td><?= h($advertZone->code) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Id') ?></strong></td>
                    <td><?= $this->Number->format($advertZone->id) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Width') ?></strong></td>
                    <td><?= !is_null($advertZone->width) ? $this->Number->format($advertZone->width) . 'px' : $advertZone->width ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Height') ?></strong></td>
                    <td><?= !is_null($advertZone->height) ? $this->Number->format($advertZone->height) . 'px' : $advertZone->height ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Unique Views Count') ?></strong></td>
                    <td><?= $this->Number->format($advertZone->unique_views_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Views Count') ?></strong></td>
                    <td><?= $this->Number->format($advertZone->views_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Unique Clicks Count') ?></strong></td>
                    <td><?= $this->Number->format($advertZone->unique_clicks_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Clicks Count') ?></strong></td>
                    <td><?= $this->Number->format($advertZone->clicks_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Status') ?></strong></td>
                    <td><?= $this->Utility->__FormatStatus($advertZone->status) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Created') ?></strong></td>
                    <td><?= h($advertZone->created) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Modified') ?></strong></td>
                    <td><?= h($advertZone->modified) ?></td>
                </tr>

            </table>

            <div class="related">
                <h4><?= __('Related Adverts') ?></h4>
                <?php if (!empty($advertZone->adverts)) : ?>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th><?= __('Id') ?></th>
                                <th><?= __('Advert Campaign') ?></th>
                                <th><?= __('Name') ?></th>
                                <th><?= __('Url') ?></th>
                                <th><?= __('Unique Views Count') ?></th>
                                <th><?= __('Views Count') ?></th>
                                <th><?= __('Unique Clicks Count') ?></th>
                                <th><?= __('Clicks Count') ?></th>
                                <th><?= __('Status') ?></th>
                                <th><?= __('Created') ?></th>
                                <th><?= __('Modified') ?></th>
                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                            <?php foreach ($advertZone->adverts as $adverts) : ?>
                                <tr>
                                    <td><?= h($adverts->id) ?></td>
                                    <td><?= $this->Html->link($adverts->advert_campaign->name, ['controller' => 'AdvertCampaigns', 'action' => 'view', $adverts->advert_campaign_id]) ?></td>
                                    <td><?= h($adverts->name) ?></td>
                                    <td><?= h($adverts->url) ?></td>
                                    <td><?= h($adverts->unique_views_count) ?></td>
                                    <td><?= h($adverts->views_count) ?></td>
                                    <td><?= h($adverts->unique_clicks_count) ?></td>
                                    <td><?= h($adverts->clicks_count) ?></td>
                                    <td><?= $this->Utility->__FormatStatus($adverts->status) ?></td>
                                    <td><?= h($adverts->created) ?></td>
                                    <td><?= h($adverts->modified) ?></td>
                                    <td class="actions">
                                        <?= $this->Html->link(__('View'), ['controller' => 'Adverts', 'action' => 'view', $adverts->id]) ?>
                                        <?= $this->Html->link(__('Edit'), ['controller' => 'Adverts', 'action' => 'edit', $adverts->id]) ?>
                                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'Adverts', 'action' => 'delete', $adverts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $adverts->id)]) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                <?php endif; ?>
            </div>

            <div class="row"></div>
            <hr />
            <div class='row'>
                <div class="actions">
                    <?php echo $this->Html->link(__('Edit register'), array('action' => 'edit', $this->request->getAttribute('params')['pass'][0]), array('class' => 'btn btn-info')); ?>
                </div>
            </div>
        </div>
    </div>

</div>
