<?php
/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\HomeLayout[]|\Cake\Collection\CollectionInterface $homeLayouts
 */
?>



<?php //echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' =>'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page">Home Layouts</li>
        </ol>
    </nav>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li><?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i> Layout Mix', ['action' => 'add','RMIX'], ['escape' => false, 'class' => 'btn btn-small btn-primary', 'title' => __('Add new register')]); ?></li>
        <li><?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>  Layout Paradiso', ['action' => 'add','SPARADISO'], ['escape' => false, 'class' => 'btn btn-small btn-info', 'title' => __('Add new register')]); ?></li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="panel panel-default">
        <div class="table-responsive">
            <?php echo $this->Form->create($this->request->getParam('controller'), ['url' => ['action' => 'delete']]); ?>
            <table cellpadding="0" cellspacing="0" class="table table-striped table-hover  order">
                <thead>
                    <tr>
                        <th scope="col" class="hidden">
                            <button class="btn btn-link" type='submit'><i class="pg-trash"></i></button>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('id') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'id'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col" class="">
                            <?= $this->Paginator->sort('company_id') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'company_id'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('name') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'name'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('start','Início') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'start'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('finish','Fim') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'finish'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('status') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'status'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('created') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'created'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('modified') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'modified'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?php echo __('Actions'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($homeLayouts as $homeLayout){ ?>
                        <tr>
                            <td class="hidden">
                                <input name="data[Home Layouts][][Home Layouts][id]" value="<? echo $homeLayout->id; ?>" id="homeLayout-<?php echo $homeLayout->id; ?>" type="checkbox"/>
                                <label for="page-<?php echo $homeLayout->id;?>" class="no-padding no-margin"></label>
                            </td>
                            <td><?= $this->Number->format($homeLayout->id) ?></td>
                            <td class=""><?= $homeLayout->company->name;?></td>
                            <td><?= h($homeLayout->name) ?></td>
                            <td><?= h($homeLayout->start->format('d/m/Y')) ?></td>
                            <td><?= h($homeLayout->finish->format('d/m/Y')) ?></td>
                            <td><?= $this->Utility->__FormatStatus($homeLayout->status) ?></td>
                            <td><?= h($homeLayout->created) ?></td>
                            <td><?= h($homeLayout->modified) ?></td>
                            <td class="actions" nowrap="nowrap">
                                <?php //echo $this->Html->link('<i class="fa fa-eye" aria-hidden="true"></i>', ['action' => 'view', $homeLayout->id],['class'=>'btn btn-info btn-xs text-center','escape'=>false]) ?>
                                <?= $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i>', ['action' => 'edit', $homeLayout->id],['class'=>'btn btn-primary btn-xs text-center','escape'=>false]) ?>
                                <?php if( $homeLayouts->count() > 1 ):?>
                                    <?= $this->Html->link('<i class="fa fa-trash" aria-hidden="true"></i>', ['action' => 'delete', $homeLayout->id],['class'=>'btn btn-danger btn-xs text-center','escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $homeLayout->id)]); ?>
                                <?php endif;?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php echo $this->Form->end(); ?>
        </div>
        <div class="panel-footer">
            <?php echo $this->Element('Manager/pagination');?>
        </div>
    </div>
</div>
