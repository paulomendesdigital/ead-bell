<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\HomeLayout $homeLayout
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('HomeLayouts'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Editar') . ' ' . __('Layout da Home'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li class="hidden-xs"><?= $this->Html->link('<i class="fa fa-list"></i> ' . __('Listar Layouts'), ['action' => 'index'], ['class' => 'btn btn-small btn-info', 'escape' => false]) ?></li>
        <li><?php //echo $this->Form->postLink('<i class="fa fa-trash"></i> ' . __('Delete'), ['action' => 'delete', $homeLayout->id], ['confirm' => __('Are you sure you want to delete # {0}?', $homeLayout->id), 'class' => 'btn btn-small btn-danger', 'escape' => false]) ?></li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="homeLayouts form large-9 medium-8 columns content">
        <?= $this->Form->create($homeLayout); ?>
        <fieldset class="col-sm-12">
            <legend><?= __('Editar  Layout da Home') ?></legend>
            <div class="row clearfix mt-2">
                <div class="col-sm-3 col-xs-12">
                    <?php echo $this->Form->control('id'); ?>
                    <?php echo $this->Form->control('company_id', ['data-toggle'=>'company_id', 'options' => $companies, 'type' => 'select', 'class' => 'form-control  input-lg', 'label' => __('company')]); ?>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <?php echo $this->Form->control('name', array('class' => 'form-control input-lg', 'label' => __('name'))); ?>
                </div>
  
                <div class="col-sm-3 col-xs-12">
                    <?php echo $this->Form->control('start', array('type' => 'text', 'class' => 'form-control input-lg', 'data-content' => "datetimepicker", 'label' => __('start'), 'required')); ?>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <?php echo $this->Form->control('finish', array('type' => 'text', 'class' => 'form-control input-lg', 'data-content' => "datetimepicker", 'label' => __('finish'), 'required')); ?>
                </div>
            </div>

            <div class="row clearfix mt-2">
                <div class="form-group" data-hide="modal">
                    <label class="col-sm-12" class="">Status</label>
                    <div class="col-sm-12"><?php echo $this->Form->control('status', array('type' => 'checkbox', 'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false, 'checked' => 'checked')); ?></div>
                </div>
            </div>

            <legend><?= __('Montar Layout da Home') ?></legend>

            <div class="row clearfix mt-2 draw-homelayout">
                <div class="col-12">
                    <?php $index = 0; ?>
                    <?php for ($i = 1; $i <= 4; $i++) { ?>
                        <div class="row row-contents <?php echo $i > 1 ? 'mt-1' : ''; ?>" data-row='<?php echo $i; ?>'>
                            <?php for ($j = 1; $j <= 3; $j++) { ?>
                                <?php $has_content = !empty($homeLayoutItems[$i - 1][$j - 1]) ? true : false; ?>
                                <?php $previous_has_content = !empty($homeLayoutItems[$i - 1][$j - 2]) ? true : false; ?>                                
                                <?php $last_has_content = $has_content && empty($homeLayoutItems[$i - 1][$j]) ? true : false; ?>
                                <div class="col-sm-<?php echo !$has_content || $homeLayoutItems[$i - 1][$j - 1]['size'] == 1 ? 4 : 8; ?> col-content 
                                    <?php echo $has_content ? 'has-content' : ''; ?> <?php echo $last_has_content ? 'last-has-content' : ''; ?>" data-col="<?php echo $j; ?>">
                                    <button type='button' data-toggle='add-content' class="btn btn-block btn-light btn-lg" <?php echo (!$previous_has_content && $j > 1) || ($i > 1 && $j == 3)  ? 'disabled' : ''; ?>><i class="fa fa-plus-circle"></i></button>
                                    <?php if ($has_content) { ?>
                                        <?php echo $this->Element('Manager/content-layout-wrapper', ['index' => $index, 'home_layout_item' => $homeLayoutItems[$i - 1][$j - 1]]); ?>
                                    <?php } ?>
                                </div>
                                <?php $index++; ?>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <script>
                        var content_index_view = <?php echo ++$index; ?>;
                        console.log(content_index_view);
                    </script>
                </div>
            </div>

            <div class="row clearfix mt-2 mb-2">
                <div class="col-sm-12 col-xs-12">
                    <?= $this->Form->button(__('Edit'), ['class' => 'btn btn-primary']) ?>
                    <?= $this->Form->button(__('Edit and continue'), ['name' => 'refer', 'class' => 'btn btn-info']) ?>
                </div>
            </div>
        </fieldset>
        <?= $this->Form->end() ?>
    </div>
</div>

<?php echo $this->Element('Manager/content-form-structures'); ?>
<?php echo $this->Html->css('manager/homelayouts'); ?>
<?php echo $this->Html->script('manager/homelayouts'); ?>