<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\HomeLayout $homeLayout
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('HomeLayouts'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Adicionar') . ' ' . __('Layout da Home'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link('<i class="fa fa-list"></i> ' . __('Listar Layouts'), ['action' => 'index'], ['class' => 'btn btn-small btn-info', 'escape' => false]) ?></li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="homeLayouts form large-9 medium-8 columns content">
        <?= $this->Form->create($homeLayout, ['type' => 'file']); ?>
        <fieldset class="col-sm-12">
            <legend><?= __('Adicionar Layout da Home') ?></legend>
            <div class="row clearfix mt-2">
                <div class="col-sm-3 col-xs-12">
                    <?php echo $this->Form->control('company_id', ['data-toggle'=>'company_id', 'options' => $companies, 'type' => 'select', 'class' => 'form-control  input-lg', 'label' => __('company')]); ?>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <?php echo $this->Form->control('name', array('class' => 'form-control input-lg', 'label' => __('name'))); ?>
                </div>
            
                <div class="col-sm-3 col-xs-12">
                    <?php echo $this->Form->control('start', array('type' => 'text', 'class' => 'form-control input-lg', 'data-content' => "datetimepicker", 'label' => __('start'), 'required')); ?>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <?php echo $this->Form->control('finish', array('type' => 'text', 'class' => 'form-control input-lg', 'data-content' => "datetimepicker", 'label' => __('finish'), 'required')); ?>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="form-group" data-hide="modal">
                    <label class="col-sm-12" class="">Status</label>
                    <div class="col-sm-12"><?php echo $this->Form->control('status', array('type' => 'checkbox', 'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false, 'checked' => 'checked')); ?></div>
                </div>
            </div>

            <legend><?= __('Montar Layout da Home') ?></legend>

            <div class="row clearfix mt-2 draw-homelayout">
                <div class="col-12">
                    <div class="row row-contents" data-row='1'>
                        <div class="col-sm-4 col-content" data-col="1">
                            <button type='button' data-toggle='add-content' class="btn btn-block btn-light btn-lg"><i class="fa fa-plus-circle"></i></button>
                        </div>
                        <div class="col-sm-4 col-content" data-col="2">
                            <button type='button' data-toggle='add-content' class="btn btn-block btn-light btn-lg" disabled><i class="fa fa-plus-circle"></i></button>
                        </div>
                        <div class="col-sm-4 col-content" data-col="3">
                            <button type='button' data-toggle='add-content' class="btn btn-block btn-light btn-lg" disabled><i class="fa fa-plus-circle"></i></button>
                        </div>
                    </div>
                    <div class="row row-contents mt-1" data-row='2'>
                        <div class="col-sm-4 col-content" data-col="1">
                            <button type='button' data-toggle='add-content' class="btn btn-block btn-light btn-lg"><i class="fa fa-plus-circle"></i></button>
                        </div>
                        <div class="col-sm-4 col-content" data-col="2">
                            <button type='button' data-toggle='add-content' class="btn btn-block btn-light btn-lg" disabled><i class="fa fa-plus-circle"></i></button>
                        </div>
                        <div class="col-sm-4 col-content" data-col="3">
                            <button type='button' class="btn btn-block btn-light btn-lg" disabled><i class="fa fa-plus-circle"></i></button>
                        </div>
                    </div>
                    <div class="row row-contents mt-1" data-row='3'>
                        <div class="col-sm-4 col-content" data-col="1">
                            <button type='button' data-toggle='add-content' class="btn btn-block btn-light btn-lg"><i class="fa fa-plus-circle"></i></button>
                        </div>
                        <div class="col-sm-4 col-content" data-col="2">
                            <button type='button' data-toggle='add-content' class="btn btn-block btn-light btn-lg" disabled><i class="fa fa-plus-circle"></i></button>
                        </div>
                        <div class="col-sm-4 col-content" data-col="3">
                            <button type='button' class="btn btn-block btn-light btn-lg" disabled><i class="fa fa-plus-circle"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix mt-2 mb-2">
                <div class="col-sm-12 col-xs-12">
                    <?= $this->Form->button(__('Add'), ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        </fieldset>
        <?= $this->Form->end(); ?>
    </div>
</div>

<?php echo $this->Element('Manager/content-form-structures'); ?>
<?php echo $this->Html->css('manager/homelayouts'); ?>
<?php echo $this->Html->script('manager/homelayouts'); ?>