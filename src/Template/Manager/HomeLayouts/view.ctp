<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\HomeLayout $homeLayout
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Home Layout'), ['action' => 'edit', $homeLayout->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Home Layout'), ['action' => 'delete', $homeLayout->id], ['confirm' => __('Are you sure you want to delete # {0}?', $homeLayout->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Home Layouts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Home Layout'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Home Layout Items'), ['controller' => 'HomeLayoutItems', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Home Layout Item'), ['controller' => 'HomeLayoutItems', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="homeLayouts view large-9 medium-8 columns content">
    <h3><?= h($homeLayout->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Company') ?></th>
            <td><?= $homeLayout->has('company') ? $this->Html->link($homeLayout->company->name, ['controller' => 'Companies', 'action' => 'view', $homeLayout->company->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($homeLayout->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($homeLayout->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($homeLayout->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Start') ?></th>
            <td><?= h($homeLayout->start) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Finish') ?></th>
            <td><?= h($homeLayout->finish) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($homeLayout->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($homeLayout->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Home Layout Items') ?></h4>
        <?php if (!empty($homeLayout->home_layout_items)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Home Layout Id') ?></th>
                <th scope="col"><?= __('Content Id') ?></th>
                <th scope="col"><?= __('Size') ?></th>
                <th scope="col"><?= __('Row') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($homeLayout->home_layout_items as $homeLayoutItems): ?>
            <tr>
                <td><?= h($homeLayoutItems->id) ?></td>
                <td><?= h($homeLayoutItems->home_layout_id) ?></td>
                <td><?= h($homeLayoutItems->content_id) ?></td>
                <td><?= h($homeLayoutItems->size) ?></td>
                <td><?= h($homeLayoutItems->row) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['controller' => 'HomeLayoutItems', 'action' => 'view', $homeLayoutItems->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'HomeLayoutItems', 'action' => 'edit', $homeLayoutItems->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'HomeLayoutItems', 'action' => 'delete', $homeLayoutItems->id], ['confirm' => __('Are you sure you want to delete # {0}?', $homeLayoutItems->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
