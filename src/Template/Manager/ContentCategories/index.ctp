<?php
/**
 * @copyright 2019
 * @author Dayvison Silva - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ContentCategory[]|\Cake\Collection\CollectionInterface $contentCategories
 */
?>


<?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' =>'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page">Categorias de Conteúdos</li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">
    <fieldset class="box-search">
        <?php echo $this->Form->create(null,['type'=>'get']);?>
            <div class="col-sm-3 col-xs-12">
                <?php echo $this->Form->control('name', ['class' => 'form-control', 'label' => __('Nome')]);?>
            </div>
            <div class="col-sm-2 col-xs-12">
                <?= $this->Form->button(__('buscar'),['class'=>'btn btn-primary']) ?>
            </div>
            <div class="row clearfix mt-2 mb-2"></div>
        <?php echo $this->Form->end();?>
    </fieldset>
</div>

<div class="container-fluid container-fixed-lg">
    <div class="panel panel-default">
        <div class="table-responsive">
            <?php echo $this->Form->create($this->request->getParam('controller'), ['url' => ['action' => 'delete']]); ?>
                <table cellpadding="0" cellspacing="0" class="table table-striped table-hover  order">
                    <thead>
                        <tr>
                            <th scope="col" class="hidden">
                                <button class="btn btn-link" type='submit'><i class="pg-trash"></i></button>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('id') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'id'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('code','Código') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'code'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('name') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'name'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('button_label', 'Label do Botão') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'button_label'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('status') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'status'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('created') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'created'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('modified') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'modified'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?php echo __('Actions'); ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($contentCategories as $contentCategory){ ?>
                            <tr>
                                <td class="hidden">
                                    <input name="data[Content Categories][][Content Categories][id]" value="<? echo $contentCategory->id; ?>" id="contentCategory-<?php echo $contentCategory->id; ?>" type="checkbox"/>
                                    <label for="page-<?php echo $contentCategory->id;?>" class="no-padding no-margin"></label>
                                </td>
                                <td><?= $this->Number->format($contentCategory->id) ?></td>
                                <td><?= h($contentCategory->code) ?></td>
                                <td><?= h($contentCategory->name) ?></td>
                                <td><?= h($contentCategory->button_label) ?></td>
                                <td><?= $this->Utility->__FormatStatus($contentCategory->status) ?></td>
                                <td><?= h($contentCategory->created->format('d/m/Y H:i')) ?></td>
                                <td><?= h($contentCategory->modified->format('d/m/Y H:i')) ?></td>
                                <td class="actions" nowrap="nowrap">
                                    <?= $this->Html->link('<i class="fa fa-eye" aria-hidden="true"></i>', ['action' => 'view', $contentCategory->id],['class'=>'btn btn-info btn-xs text-center','escape'=>false]) ?>
                                    <?= $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i>', ['action' => 'edit', $contentCategory->id],['class'=>'btn btn-primary btn-xs text-center','escape'=>false]) ?>
                                    <?= $this->Html->link('<i class="fa fa-trash" aria-hidden="true"></i>', ['action' => 'delete', $contentCategory->id],['class'=>'btn btn-danger btn-xs text-center','escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $contentCategory->id)]); ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php echo $this->Form->end(); ?>

        </div>
        <div class="panel-footer">
            <?php echo $this->Element('Manager/pagination');?>
        </div>
    </div>
</div>
