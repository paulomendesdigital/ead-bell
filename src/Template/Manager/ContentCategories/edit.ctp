<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ContentCategory $contentCategory
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Categorias de Conteúdo'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Edit').' '.__('Categoria'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link('<i class="fa fa-list"></i> '.__('List Content Categories'), ['action' => 'index'],['class'=>'btn btn-small btn-info','escape'=>false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-list"></i> '.__('List Contents'), ['controller' => 'contents', 'action' => 'index'],['class'=>'btn btn-small btn-primary','escape'=>false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-plus"></i> '.__('New Content'), ['controller' => 'contents', 'action' => 'add'],['class'=>'btn btn-small btn-success','escape'=>false]) ?></li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="contentCategories form large-9 medium-8 columns content">
        <?= $this->Form->create($contentCategory) ?>
            <fieldset class="col-sm-12">
                <legend><?= __('Edit Content Category') ?></legend>
                <?php echo $this->Form->control('id');?>
                <div class="row clearfix mt-2">
                    <div class="col-sm-3 col-xs-12">
                        <?php //echo $this->Form->control('company_id', ['type'=>'select','class' => 'form-control', 'label' => __('company')]);?>
                    </div>
                    <div class="col-sm-3 col-xs-12 hidden">
                        <?php //echo $this->Form->control('code', array('class' => 'form-control', 'label' => __('Código do Conteúdo')));?>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <?php //echo $this->Form->control('button_label', ['class' => 'form-control', 'label' => __('Label do Botão')]);?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    
                    <div class="col-sm-6 col-xs-12">
                        <?php echo $this->Form->control('name', array('class' => 'form-control input-lg', 'label' => __('name')));?>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <?php echo $this->Form->control('button_label', ['class' => 'form-control  input-lg', 'label' => __('Label do Botão')]);?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="form-group" data-hide="modal">
                        <label class="col-sm-12" class="">Status</label>
                        <div class="col-sm-12"><?php echo $this->Form->control('status', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false, 'checked' => 'checked')); ?></div>
                    </div>
                </div>
                <div class="row clearfix mt-2 mb-2">
                    <div class="col-sm-12 col-xs-12">
                        <?= $this->Form->button(__('Edit'),['class'=>'btn btn-primary']); ?>
                        <?= $this->Form->button(__('Edit and continue'),['name'=>'refer','class'=>'btn btn-info']); ?>
                    </div>
                </div>
            </fieldset>
        <?= $this->Form->end() ?>
    </div>
</div>
