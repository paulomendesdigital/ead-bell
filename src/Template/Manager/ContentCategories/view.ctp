<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ContentCategory $contentCategory
 */
?>

<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Categorias de Conteúdo'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Ver').' '.__('Categoria'); ?></a></li>
    </ul>
</div>
<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link('<i class="fa fa-edit"></i> '.__('Edit'), ['action' => 'edit', $contentCategory->id],['class'=>'btn btn-small btn-warning','escape'=>false]) ?></li>
        <li><?= $this->Form->postLink('<i class="fa fa-trash"></i> '.__('Delete'), ['action' => 'delete', $contentCategory->id], ['class'=>'btn btn-small btn-danger','escape'=>false,'confirm' => __('Are you sure you want to delete # {0}?', $contentCategory->id)])?> </li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="contentCategories view large-9 medium-8 columns content">
        <h3>Categoria: <?= h($contentCategory->name) ?></h3>
        <table class="vertical-table">
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($contentCategory->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Code') ?></th>
                <td><?= h($contentCategory->code) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($contentCategory->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Button Label') ?></th>
                <td><?= h($contentCategory->button_label) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Status') ?></th>
                <td><?= $this->Utility->__FormatStatus($contentCategory->status) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($contentCategory->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($contentCategory->modified) ?></td>
            </tr>
        </table>
        <div class="related">
            <?php if (!empty($contentCategory->contents)): ?>
                <hr />
                <h4><?= __('Publicações da Categoria') ?></h4>
                <table class="table table-striped">
                    <tr>
                        <th scope="col"><?= __('Id') ?></th>
                        <th scope="col"><?= __('Name') ?></th>
                        <th scope="col"><?= __('Publicação') ?></th>
                        <th scope="col"><?= __('Imagens') ?></th>
                        <th scope="col"><?= __('Status') ?></th>
                        <th scope="col"><?= __('Created') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                    <?php foreach ($contentCategory->contents as $content): ?>
                        <tr>
                            <td><?= $this->Number->format($content->id) ?></td>
                            <td>
                                <li class="list-style-none">Nome: <?= h($content->name ? $content->name : '--') ?></li>
                                <li style="list-style: none;"><hr style="margin-top: 5px; margin-bottom: 5px;" /></li>
                                <li class="list-style-none">Título: <?= h($content->title ? $content->title : '--') ?></li>
                                <li style="list-style: none;"><hr style="margin-top: 5px; margin-bottom: 5px;" /></li>
                                <li class="list-style-none">Subtitulo: <?= h($content->subtitle ? $content->subtitle : '--') ?></li>
                            </td>
                            <td nowrap="nowrap">
                                <li class="list-style-none">De: <?= $content->start ? $content->start->format('d/m/Y') : '--' ?></li>
                                <li class="list-style-none">Até: <?= $content->finish ? $content->finish->format('d/m/Y') : '--' ?></li>
                                <?php if( $content->finish_promotion ):?>
                                    <li>Válida até: <?php echo $content->finish_promotion->format('d/m/Y');?></li>
                                <?php endif;?>
                            </td>
                            <td nowrap="nowrap">
                                <li style="list-style: none;"><?= $content->image_medium_name ? "Destaque Home Quadrado - <i class='text-success fa fa-check'></i>" : "Imagem média - <i class='text-danger fa fa-ban'></i>"; ?></li>
                                <li style="list-style: none;"><?= $content->image_large_name ? "Destaque Home Retangular - <i class='text-success fa fa-check'></i>" : "Imagem grande - <i class='text-danger fa fa-ban'></i>"; ?></li>
                                <li style="list-style: none;"><?= $content->image_internal ? "Página Interna - <i class='text-success fa fa-check'></i>" : "Imagem Interna - <i class='text-danger fa fa-ban'></i>"; ?></li>
                            </td>
                            <td><?php echo $this->Utility->__FormatStatus( $this->Number->format($content->status) ); ?></td>
                            <td><?= h($content->created->format('d/m/Y')) ?></td>
                            <td class="actions" nowrap="nowrap">
                                <?php if( isset($content->participants) and !empty($content->participants) ):?>
                                    <?= $this->Html->link('<i class="fa fa-users" aria-hidden="true"></i>', ['controller'=>'participants','action' => 'index', $content->id],['class'=>'btn btn-success btn-xs text-center','title'=>'Participantes','escape'=>false]) ?>
                                <?php endif;?>
                                <?= $this->Html->link('<i class="fa fa-eye" aria-hidden="true"></i>', ['controller' => 'contents', 'action' => 'view', $content->id],['class'=>'btn btn-info btn-xs text-center','escape'=>false]) ?>
                                <?= $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i>', ['controller' => 'contents', 'action' => 'edit', $content->id],['class'=>'btn btn-primary btn-xs text-center','escape'=>false]) ?>
                                <?= $this->Html->link('<i class="fa fa-trash" aria-hidden="true"></i>', ['controller' =>'contents', 'action' => 'delete', $content->id],['class'=>'btn btn-danger btn-xs text-center','escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $content->id)]); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            <?php endif; ?>
        </div>
    </div>
</div>
