<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MidiaKit $midiaKit
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Midia Kits'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Ver').' '.__('Midia Kit'); ?></a></li>
    </ul>
</div>
<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li class="hidden-xs"><?= $this->Html->link('<i class="fa fa-list"></i> '.__('Listar Midia Kits'), ['action' => 'index'],['class'=>'btn btn-small btn-info','escape'=>false]) ?></li>
        <li class="hidden-xs"><?= $this->Html->link('<i class="fa fa-edit"></i> '.__('Editar Midia Kit'), ['action' => 'edit', $midiaKit->id],['class'=>'btn btn-small btn-success','escape'=>false]) ?></li>
        <li><?= $this->Form->postLink('<i class="fa fa-trash"></i> '.__('Delete'),['action' => 'delete', $midiaKit->id],['confirm' => __('Are you sure you want to delete # {0}?', $midiaKit->id), 'class'=>'btn btn-small btn-danger','escape'=>false]) ?></li>
    </ul>
</nav>
<div class="container-fluid container-fixed-lg">
    <div class="midiaKits view large-9 medium-8 columns content">
        <h3><?= h($midiaKit->name) ?></h3>
        <table class="vertical-table">
            <tr>
                <th scope="row"><?= __('Company') ?></th>
                <td><?= $midiaKit->has('company') ? $this->Html->link($midiaKit->company->name, ['controller' => 'Companies', 'action' => 'view', $midiaKit->company->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($midiaKit->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Pdf') ?></th>
                <td><?php echo $this->Html->link('abrir',"/files/MidiaKits/pdf/{$midiaKit->pdf}",['target'=>'_blank']);?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($midiaKit->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Status') ?></th>
                <td><?= $this->Utility->__FormatStatus($midiaKit->status) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($midiaKit->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($midiaKit->modified) ?></td>
            </tr>
        </table>
    </div>
</div>