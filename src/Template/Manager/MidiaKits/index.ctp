<?php
/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MidiaKit[]|\Cake\Collection\CollectionInterface $midiaKits
 */
?>


<?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' =>'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page">Midia Kits</li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">
    <div class="panel panel-default">
        <div class="table-responsive">
            <?php echo $this->Form->create($this->request->getParam('controller'), ['url' => ['action' => 'delete']]); ?>
                <table cellpadding="0" cellspacing="0" class="table table-striped table-hover  order">
                    <thead>
                        <tr>
                            <th scope="col">
                                <?= $this->Paginator->sort('id') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'id'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('name') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'name'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('pdf') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'pdf'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('status') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'status'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('created') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'created'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('modified') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'modified'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?php echo __('Actions'); ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($midiaKits as $midiaKit){ ?>
                            <tr>
                                <td><?= $this->Number->format($midiaKit->id) ?></td>
                                <td><?= h($midiaKit->name) ?></td>
                                <td><?= h($midiaKit->pdf) ?></td>
                                <td><?= $this->Utility->__FormatStatus($midiaKit->status) ?></td>
                                <td><?= h($midiaKit->created->format('d/m/Y')) ?></td>
                                <td><?= h($midiaKit->modified->format('d/m/Y')) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link('<i class="fa fa-eye" aria-hidden="true"></i>', ['action' => 'view', $midiaKit->id],['class'=>'btn btn-info btn-xs text-center','escape'=>false]) ?>
                                    <?= $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i>', ['action' => 'edit', $midiaKit->id],['class'=>'btn btn-primary btn-xs text-center','escape'=>false]) ?>
                                    <?= $this->Html->link('<i class="fa fa-trash" aria-hidden="true"></i>', ['action' => 'delete', $midiaKit->id],['class'=>'btn btn-danger btn-xs text-center','escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $midiaKit->id)]); ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php echo $this->Form->end(); ?>
        </div>
        <div class="panel-footer">
            <?php echo $this->Element('Manager/pagination');?>
        </div>
    </div>
</div>
    