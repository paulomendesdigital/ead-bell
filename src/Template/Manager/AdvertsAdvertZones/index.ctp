<?php

/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AdvertsAdvertZone[]|\Cake\Collection\CollectionInterface $advertsAdvertZones
 */
?>


<?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' => 'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page"><?php echo __('Adverts Advert Zones'); ?></li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">

    <div class="panel panel-default">

        <div class="panel-body">

            <?= $this->Form->create(null, ['method' => 'get', 'valueSources' => ['query', 'context']]) ?>
            <div class="row">

                <div class="col-md-4">
                    <?= $this->Form->control('AdvertsAdvertZones.id', ['class' => 'form-control']) ?>
                </div>

                <div class="col-md-4">
                    <?= $this->Form->control('AdvertsAdvertZones.advert_id', ['class' => 'form-control', 'empty' => 'Selecione']) ?>
                </div>

                <div class="col-md-4">
                    <?= $this->Form->control('AdvertsAdvertZones.advert_zone_id', ['class' => 'form-control', 'empty' => 'Selecione']) ?>
                </div>
            </div>
            <div class="row">

                <div class="col-md-4">
                    <?= $this->Form->control('AdvertsAdvertZones.status', ['class' => 'form-control', 'empty' => 'Selecione', 'options' => [0 => 'Inativo', 1 => 'Ativo']]) ?>
                </div>

                <div class="col-md-4">
                    <label>&nbsp;</label>
                    <button type="submit" class="btn btn-primary" style="width: 100%">Buscar</button>
                </div>
            </div>
            <?= $this->Form->end() ?>

            <?php echo $this->Form->create(null, ['url' => ['action' => 'delete']]); ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">
                            <button class="btn btn-link" type='submit'><i class="pg-trash"></i></button>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('id', __('Id')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'id') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('advert_id', __('Advert')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'advert_id') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('advert_zone_id', __('Advert Zone')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'advert_zone_id') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('unique_views_count', __('Unique Views Count')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'unique_views_count') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('views_count', __('Views Count')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'views_count') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('unique_clicks_count', __('Unique Clicks Count')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'unique_clicks_count') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('clicks_count', __('Clicks Count')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'clicks_count') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('status', __('Status')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'status') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('created', __('Created')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'created') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?php echo __('Actions'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($advertsAdvertZones as $advertsAdvertZone) { ?>
                        <tr>
                            <td>
                                <input name="Adverts Advert Zones[][id]" value="<? echo $advertsAdvertZone->id; ?>" id="advertsAdvertZone-<?php echo $advertsAdvertZone->id; ?>" type="checkbox" />
                                <label for="page-<?php echo $advertsAdvertZone->id; ?>" class="no-padding no-margin"></label>
                            </td>

                            <td><?= $this->Number->format($advertsAdvertZone->id) ?></td>

                            <td><?= $advertsAdvertZone->has('advert') ? $this->Html->link($advertsAdvertZone->advert->name, ['controller' => 'Adverts', 'action' => 'view', $advertsAdvertZone->advert->id]) : '' ?></td>

                            <td><?= $advertsAdvertZone->has('advert_zone') ? $this->Html->link($advertsAdvertZone->advert_zone->name, ['controller' => 'AdvertZones', 'action' => 'view', $advertsAdvertZone->advert_zone->id]) : '' ?></td>

                            <td><?= $this->Number->format($advertsAdvertZone->unique_views_count) ?></td>

                            <td><?= $this->Number->format($advertsAdvertZone->views_count) ?></td>

                            <td><?= $this->Number->format($advertsAdvertZone->unique_clicks_count) ?></td>

                            <td><?= $this->Number->format($advertsAdvertZone->clicks_count) ?></td>

                            <td><?= $this->Utility->__FormatStatus($advertsAdvertZone->status) ?></td>

                            <td><?= h($advertsAdvertZone->created) ?></td>

                            <td class="actions">
                                <?= $this->Html->link('<button type="button" class="btn btn-info"><i class="fa-eye fa"></i></button>', ['action' => 'view', $advertsAdvertZone->id], ['escape' => false]) ?>
                                <?= $this->Html->link('<button type="button" class="btn btn-primary"><i class="fa-pencil fa"></i></button>', ['action' => 'edit', $advertsAdvertZone->id], ['escape' => false]) ?>
                                <?= $this->Form->postLink('<button type="button" class="btn btn-danger"><i class="fa-trash fa"></i></button>', ['action' => 'delete', $advertsAdvertZone->id], ['confirm' => __('Are you sure you want to delete # {0}?', $advertsAdvertZone->id), 'escape' => false]) ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php echo $this->Form->end(); ?>

            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
            </div>

        </div>
    </div>
</div>
