<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AdvertsAdvertZone $advertsAdvertZone
 */
?>

<div class="container-fluid container-fixed-lg">
    <ol class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Adverts Advert Zones'), ['controller' => 'AdvertsAdvertZones', 'action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('View') . ' ' . __('Adverts Advert Zones'); ?></a></li>
    </ol>
</div>

<div class="container-fluid container-fixed-lg">

    <!-- BASIC INFORMATIONS -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title"><?php echo __('Basic information'); ?></div>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered">
                <tr>
                    <td>
                        <strong><?php echo __('Advert'); ?></strong>
                    </td>
                    <td>
                        <?= $advertsAdvertZone->has('advert') ? $this->Html->link($advertsAdvertZone->advert->name, ['controller' => 'Adverts', 'action' => 'view', $advertsAdvertZone->advert->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong><?php echo __('Advert Zone'); ?></strong>
                    </td>
                    <td>
                        <?= $advertsAdvertZone->has('advert_zone') ? $this->Html->link($advertsAdvertZone->advert_zone->name, ['controller' => 'AdvertZones', 'action' => 'view', $advertsAdvertZone->advert_zone->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <td><strong><?= __('Id') ?></strong></td>
                    <td><?= $this->Number->format($advertsAdvertZone->id) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Unique Views Count') ?></strong></td>
                    <td><?= $this->Number->format($advertsAdvertZone->unique_views_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Views Count') ?></strong></td>
                    <td><?= $this->Number->format($advertsAdvertZone->views_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Unique Clicks Count') ?></strong></td>
                    <td><?= $this->Number->format($advertsAdvertZone->unique_clicks_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Clicks Count') ?></strong></td>
                    <td><?= $this->Number->format($advertsAdvertZone->clicks_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Status') ?></strong></td>
                    <td><?= $this->Number->format($advertsAdvertZone->status) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Created') ?></strong></td>
                    <td><?= h($advertsAdvertZone->created) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Modified') ?></strong></td>
                    <td><?= h($advertsAdvertZone->modified) ?></td>
                </tr>

            </table>
            <div class="row"></div>
            <hr />
            <div class='row'>
                <div class="actions">
                    <?php echo $this->Html->link(__('Edit register'), array('action' => 'edit', $this->request->getAttribute('params')['pass'][0]), array('class' => 'btn btn-info')); ?>
                </div>
            </div>
        </div>
    </div>

</div>
