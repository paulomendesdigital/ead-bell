<?php
/**
 * @copyright 2019
 * @author Dayvison Silva - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Company[]|\Cake\Collection\CollectionInterface $companies
 */
?>


<?php //echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' =>'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page">Empresas</li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">
    <div class="panel panel-default">
        <div class="table-responsive">
            <?php echo $this->Form->create($this->request->getParam('controller'), ['url' => ['action' => 'delete']]); ?>
                <table cellpadding="0" cellspacing="0" class="table table-striped table-hover  order">
                    <thead>
                        <tr>
                            <th scope="col" class="hidden">
                                <button class="btn btn-link" type='submit'><i class="pg-trash"></i></button>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('id') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'id'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('code',__('Código')) ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'code'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('name') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'name'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('status') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'status'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('created',__('created')) ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'created'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('modified',__('created')) ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'modified'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?php echo __('Actions'); ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($companies as $company){ ?>
                            <tr>
                                <td class="hidden">
                                    <input name="data[Companies][][Companies][id]" value="<? echo $company->id; ?>" id="company-<?php echo $company->id; ?>" type="checkbox"/>
                                    <label for="page-<?php echo $company->id;?>" class="no-padding no-margin"></label>
                                </td>
                                <td><?= $this->Number->format($company->id) ?></td>
                                <td><?= h($company->code) ?></td>
                                <td><?= h($company->name) ?></td>
                                <td><?php echo $this->Utility->__FormatStatus( $this->Number->format($company->status) ); ?></td>
                                <td><?= $company->created->format('d/m/Y H:i'); ?></td>
                                <td><?= $company->modified->format('d/m/Y H:i'); ?></td>
                                <td class="actions">
                                    <?= $this->Html->link('<i class="fa fa-eye" aria-hidden="true"></i>', ['action' => 'view', $company->id],['class'=>'btn btn-info btn-xs text-center','escape'=>false]) ?>
                                    <?= $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i>', ['action' => 'edit', $company->id],['class'=>'btn btn-primary btn-xs text-center','escape'=>false]) ?>
                                    <?= $this->Form->postLink('<i class="fa fa-trash" aria-hidden="true"></i>', ['action' => 'delete', $company->id],['class'=>'btn btn-danger btn-xs text-center','escape'=>false], ['confirm' => __('Are you sure you want to delete # {0}?', $company->id)]) ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php echo $this->Form->end(); ?>
        </div>
        <div class="panel-footer">
            <?php echo $this->Element('Manager/pagination');?>
        </div>
    </div>
</div>