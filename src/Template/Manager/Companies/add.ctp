<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Company $company
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Empresas'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Add').' '.__('Empresa'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns hidden" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Companies'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List News'), ['controller' => 'News', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New News'), ['controller' => 'News', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Top Musics'), ['controller' => 'TopMusics', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Top Music'), ['controller' => 'TopMusics', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Webdoors'), ['controller' => 'Webdoors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Webdoor'), ['controller' => 'Webdoors', 'action' => 'add']) ?></li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="companies form large-9 medium-8 columns content">
        <?= $this->Form->create($company) ?>
            <fieldset class="col-sm-12">
                <legend><?= __('Add Company') ?></legend>
                <div class="row clearfix mt-2">
                    <div class="col-sm-6 col-xs-12">
                        <?php echo $this->Form->control('code', array('class' => 'form-control input-lg', 'label' => __('Código da Empresa')));?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('name', array('class' => 'form-control input-lg', 'label' => __('name')));?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="form-group" data-hide="modal">
                        <label class="col-sm-12" class="">Status</label>
                        <div class="col-sm-12"><?php echo $this->Form->control('status', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false, 'checked' => 'checked')); ?></div>
                    </div>
                </div>
                <div class="row clearfix mt-2 mb-2">
                    <div class="col-sm-12 col-xs-12">
                        <?= $this->Form->button(__('Add'),['class'=>'btn btn-primary']) ?>
                        <?= $this->Form->button(__('Add and continue'),['name'=>'refer','class'=>'btn btn-info']) ?>
                    </div>
                </div>
            </fieldset>
        <?= $this->Form->end() ?>
    </div>
</div>
