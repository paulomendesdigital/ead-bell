<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Company $company
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Empresas'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Ver').' '.__('Empresa'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns hidden" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Company'), ['action' => 'edit', $company->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Company'), ['action' => 'delete', $company->id], ['confirm' => __('Are you sure you want to delete # {0}?', $company->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Companies'), ['action' => 'index']) ?> </li>
        <li><? //= $this->Html->link(__('New Company'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List News'), ['controller' => 'News', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New News'), ['controller' => 'News', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Top Musics'), ['controller' => 'TopMusics', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Top Music'), ['controller' => 'TopMusics', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Webdoors'), ['controller' => 'Webdoors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Webdoor'), ['controller' => 'Webdoors', 'action' => 'add']) ?> </li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="companies view large-9 medium-8 columns content">
        <h3><?= h($company->name) ?></h3>
        <table class="vertical-table">
            <tr>
                <th scope="row"><?= __('Code') ?></th>
                <td><?= h($company->code) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($company->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($company->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Status') ?></th>
                <td><?= $this->Number->format($company->status) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($company->created->format('d/m/Y H:i')) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($company->modified->format('d/m/Y H:i')) ?></td>
            </tr>
        </table>
        <div class="related">
            <?php if (!empty($company->news)): ?>
                <hr/>
                <h4><?= __('Related News') ?></h4>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <th scope="col"><?= __('Id') ?></th>
                        <th scope="col"><?= __('Company Id') ?></th>
                        <th scope="col"><?= __('Name') ?></th>
                        <th scope="col"><?= __('Title') ?></th>
                        <th scope="col"><?= __('Description') ?></th>
                        <th scope="col"><?= __('Image Name') ?></th>
                        <th scope="col"><?= __('Image Path') ?></th>
                        <th scope="col"><?= __('Status') ?></th>
                        <th scope="col"><?= __('Created') ?></th>
                        <th scope="col"><?= __('Modified') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                    <?php foreach ($company->news as $news): ?>
                    <tr>
                        <td><?= h($news->id) ?></td>
                        <td><?= h($news->company_id) ?></td>
                        <td><?= h($news->name) ?></td>
                        <td><?= h($news->title) ?></td>
                        <td><?= h($news->description) ?></td>
                        <td><?= h($news->image_name) ?></td>
                        <td><?= h($news->image_path) ?></td>
                        <td><?= h($news->status) ?></td>
                        <td><?= h($news->created) ?></td>
                        <td><?= h($news->modified) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('Ver'), ['controller' => 'News', 'action' => 'view', $news->id]) ?>
                            <?= $this->Html->link(__('Edit'), ['controller' => 'News', 'action' => 'edit', $news->id]) ?>
                            <?= $this->Form->postLink(__('Delete'), ['controller' => 'News', 'action' => 'delete', $news->id], ['confirm' => __('Are you sure you want to delete # {0}?', $news->id)]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            <?php endif; ?>
        </div>
        <div class="related">
            <?php if (!empty($company->top_musics)): ?>
                <hr/>
                <h4><?= __('Related Top Musics') ?></h4>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <th scope="col"><?= __('Id') ?></th>
                        <th scope="col"><?= __('Company Id') ?></th>
                        <th scope="col"><?= __('Artist') ?></th>
                        <th scope="col"><?= __('Name') ?></th>
                        <th scope="col"><?= __('Order') ?></th>
                        <th scope="col"><?= __('Start') ?></th>
                        <th scope="col"><?= __('Finish') ?></th>
                        <th scope="col"><?= __('Image Name') ?></th>
                        <th scope="col"><?= __('Image Path') ?></th>
                        <th scope="col"><?= __('Status') ?></th>
                        <th scope="col"><?= __('Created') ?></th>
                        <th scope="col"><?= __('Modified') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                    <?php foreach ($company->top_musics as $topMusics): ?>
                    <tr>
                        <td><?= h($topMusics->id) ?></td>
                        <td><?= h($topMusics->company_id) ?></td>
                        <td><?= h($topMusics->artist) ?></td>
                        <td><?= h($topMusics->name) ?></td>
                        <td><?= h($topMusics->order) ?></td>
                        <td><?= h($topMusics->start) ?></td>
                        <td><?= h($topMusics->finish) ?></td>
                        <td><?= h($topMusics->image_name) ?></td>
                        <td><?= h($topMusics->image_path) ?></td>
                        <td><?= h($topMusics->status) ?></td>
                        <td><?= h($topMusics->created) ?></td>
                        <td><?= h($topMusics->modified) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('Ver'), ['controller' => 'TopMusics', 'action' => 'view', $topMusics->id]) ?>
                            <?= $this->Html->link(__('Edit'), ['controller' => 'TopMusics', 'action' => 'edit', $topMusics->id]) ?>
                            <?= $this->Form->postLink(__('Delete'), ['controller' => 'TopMusics', 'action' => 'delete', $topMusics->id], ['confirm' => __('Are you sure you want to delete # {0}?', $topMusics->id)]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            <?php endif; ?>
        </div>
        <div class="related">
            <?php if (!empty($company->users)): ?>
                <hr/>
                <h4><?= __('Related Users') ?></h4>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <th scope="col"><?= __('Id') ?></th>
                        <th scope="col"><?= __('Company Id') ?></th>
                        <th scope="col"><?= __('Group Id') ?></th>
                        <th scope="col"><?= __('Name') ?></th>
                        <th scope="col"><?= __('Email') ?></th>
                        <th scope="col"><?= __('Username') ?></th>
                        <th scope="col"><?= __('Password') ?></th>
                        <th scope="col"><?= __('Status') ?></th>
                        <th scope="col"><?= __('Created') ?></th>
                        <th scope="col"><?= __('Modified') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                    <?php foreach ($company->users as $users): ?>
                    <tr>
                        <td><?= h($users->id) ?></td>
                        <td><?= h($users->company_id) ?></td>
                        <td><?= h($users->group_id) ?></td>
                        <td><?= h($users->name) ?></td>
                        <td><?= h($users->email) ?></td>
                        <td><?= h($users->username) ?></td>
                        <td><?= h($users->password) ?></td>
                        <td><?= h($users->status) ?></td>
                        <td><?= h($users->created) ?></td>
                        <td><?= h($users->modified) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('Ver'), ['controller' => 'Users', 'action' => 'view', $users->id]) ?>
                            <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id]) ?>
                            <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id)]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            <?php endif; ?>
        </div>
        <div class="related">
            <?php if (!empty($company->webdoors)): ?>
                <hr/>
                <h4><?= __('Webdoors Cadastrados') ?></h4>
                <table class="table table-striped">
                    <tr>
                        <th scope="col"><?= __('Id') ?></th>
                        <th scope="col"><?= __('Name') ?></th>
                        <th scope="col"><?= __('Período') ?></th>
                        <th scope="col"><?= __('Texto botão') ?></th>
                        <th scope="col"><?= __('Link') ?></th>
                        <th scope="col"><?= __('Imagens') ?></th>
                        <th scope="col"><?= __('Status') ?></th>
                        <th scope="col"><?= __('Ordination') ?></th>
                        <th scope="col"><?= __('Created') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                    <?php foreach ($company->webdoors as $webdoor): ?>
                    <tr>
                        <tr>
                            <td><?= $this->Number->format($webdoor->id) ?></td>
                            <td><?= h($webdoor->name) ?></td>
                            <td nowrap="nowrap">
                                <?php if( $webdoor->start ):?>
                                    Início: <?= $webdoor->start->format('d/m/Y'); ?><br />
                                    Fim: <?= $webdoor->finish->format('d/m/Y'); ?>
                                <?php else:?>
                                    --
                                <?php endif;?>
                            </td>
                            <td><?= h($webdoor->button_label) ?></td>
                            <td><?= h($webdoor->link) ?></td>
                            <td nowrap="nowrap">
                                <?= $webdoor->image_name ? "Frente - <i class='text-success fa fa-check'></i>" : "Frente - <i class='text-danger fa fa-ban'></i>"; ?><br />
                                <?= $webdoor->image_background_name ? "Background - <i class='text-success fa fa-check'></i>" : "Background - <i class='text-danger fa fa-ban'></i>"; ?><br />
                                <?= $webdoor->image_mobile_name ? "Imagem Celular - <i class='text-success fa fa-check'></i>" : "Imagem Celular - <i class='text-danger fa fa-ban'></i>"; ?><br />
                            </td>
                            <td>
                                <?php echo $this->Utility->__FormatStatus( $this->Number->format($webdoor->status) ); ?>
                            </td>
                            <td><?= $webdoor->ordination ?></td>
                            <td><?= $webdoor->created->format('d/m/Y'); ?></td>
                            <td  class="hidden"><?= $webdoor->modified->format('d/m/Y'); ?></td>
                            <td class="actions">
                                <?= $this->Html->link('<i class="fa fa-eye" aria-hidden="true"></i>', ['action' => 'view', $webdoor->id],['class'=>'btn btn-info btn-xs text-center','escape'=>false]) ?>
                                <?= $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i>', ['action' => 'edit', $webdoor->id],['class'=>'btn btn-primary btn-xs text-center','escape'=>false]) ?>
                                <?= $this->Form->postLink('<i class="fa fa-trash" aria-hidden="true"></i>', ['action' => 'delete', $webdoor->id],['class'=>'btn btn-danger btn-xs text-center','escape'=>false], ['confirm' => __('Are you sure you want to delete # {0}?', $webdoor->id)]) ?>
                            </td>
                        </tr>
                    </tr>
                    <?php endforeach; ?>
                </table>
            <?php endif; ?>
        </div>
    </div>
</div>