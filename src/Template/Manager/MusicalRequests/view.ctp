<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MusicalRequest $musicalRequest
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Musical Request'), ['action' => 'edit', $musicalRequest->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Musical Request'), ['action' => 'delete', $musicalRequest->id], ['confirm' => __('Are you sure you want to delete # {0}?', $musicalRequest->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Musical Requests'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Musical Request'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="musicalRequests view large-9 medium-8 columns content">
    <h3><?= h($musicalRequest->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($musicalRequest->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($musicalRequest->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Artist') ?></th>
            <td><?= h($musicalRequest->artist) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Music') ?></th>
            <td><?= h($musicalRequest->music) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($musicalRequest->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($musicalRequest->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($musicalRequest->modified) ?></td>
        </tr>
    </table>
</div>
