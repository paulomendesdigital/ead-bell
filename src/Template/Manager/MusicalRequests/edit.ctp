<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MusicalRequest $musicalRequest
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $musicalRequest->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $musicalRequest->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Musical Requests'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="musicalRequests form large-9 medium-8 columns content">
    <?= $this->Form->create($musicalRequest) ?>
    <fieldset>
        <legend><?= __('Edit Musical Request') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('email');
            echo $this->Form->control('artist');
            echo $this->Form->control('music');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
