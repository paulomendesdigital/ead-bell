<?php
/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TopMusic[]|\Cake\Collection\CollectionInterface $topMusics
 */
?>

<?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' =>'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page">Top Mix</li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">
    <fieldset class="box-search">
        <?php echo $this->Form->create(null,['type'=>'get']);?>
            <?php if( isset($companies) ):?>
                <div class="col-sm-2 col-xs-6">
                    <?php echo $this->Form->control('company_id', ['options'=>$companies,'empty'=>'Filtrar por Empresa','class' => 'form-control', 'label' => __('Empresa')]);?>
                </div>
            <?php endif;?>
            <div class="col-sm-2 col-xs-6">
                <?php echo $this->Form->control('artist', ['class' => 'form-control', 'label' => __('Artista')]);?>
            </div>
            <div class="col-sm-2 col-xs-6">
                <?php echo $this->Form->control('name', ['class' => 'form-control', 'label' => __('Música')]);?>
            </div>
            <div class="col-sm-2 col-xs-6">
                <?= $this->Form->button(__('buscar'),['class'=>'btn btn-primary']) ?>
            </div>
            <div class="row clearfix mt-2 mb-2"></div>
        <?php echo $this->Form->end();?>
    </fieldset>
</div>

<div class="container-fluid container-fixed-lg">
    <div class="panel panel-default">
        <div class="table-responsive">
            <?php echo $this->Form->create($this->request->getParam('controller'), ['url' => ['action' => 'delete']]); ?>
                <table cellpadding="0" cellspacing="0" class="table table-striped table-hover  order">
                    <thead>
                        <tr>
                            <th scope="col" class="hidden">
                                <button class="btn btn-link" type='submit'><i class="pg-trash"></i></button>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('id') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'id'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col" class="hidden">
                                <?= $this->Paginator->sort('company_id',__('company_id')) ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'company_id'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('image_name','Imagem') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'image_name'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('artist','Artista') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'artist'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                                <?= $this->Paginator->sort('name',' / Música') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'name'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                                <?= $this->Paginator->sort('name',' / Link') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'link'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('audio') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'audio'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('status') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'status'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('created') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'created'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?php echo __('Actions'); ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($topMusics as $topMusic){ ?>
                            <tr>
                                <td class="hidden">
                                    <input name="data[Top Musics][][Top Musics][id]" value="<? echo $topMusic->id; ?>" id="topMusic-<?php echo $topMusic->id; ?>" type="checkbox"/>
                                    <label for="page-<?php echo $topMusic->id;?>" class="no-padding no-margin"></label>
                                </td>
                                <td><?= $this->Number->format($topMusic->id) ?></td>
                                <td class="hidden"><?= $topMusic->has('company') ? $this->Html->link($topMusic->company->name, ['controller' => 'Companies', 'action' => 'view', $topMusic->company->id]) : '' ?></td>
                                <td><?= $topMusic->image_name ? $this->Html->image("/files/TopMusics/image_name/{$topMusic->image_name}",['class'=>'img-responsive','style'=>'max-width: 100px;']) : ''; ?></td>
                                <td>
                                    <li class="list-style-none" style="min-width: 250px;"><b>Artista: </b><?= h($topMusic->artist ? $topMusic->artist : '--') ?></li>
                                    <li style="list-style: none;"><hr style="margin-top: 5px; margin-bottom: 5px;" /></li>
                                    <li class="list-style-none"><b>Música: </b><?= h($topMusic->name ? $topMusic->name : '--') ?></li>
                                    <li style="list-style: none;"><hr style="margin-top: 5px; margin-bottom: 5px;" /></li>
                                    <li class="list-style-none">
                                        <b>Link: </b>
                                        <?= h($topMusic->link ? $topMusic->link : '--') ?>
                                        <?php if(isset($topMusic->link)){ ?>
                                                <a class='btn btn-warning btn-xs text-center' href="<?= $topMusic->link ?>"target="_blank">
                                                    <i class="fa fa-link" aria-hidden="true"></i>
                                                </a>
                                        <?php } ?>
                                    </li>
                                </td>
                                <td>
                                    <?php if( $topMusic->audio ):?>
                                        <audio controls style="width: 200px;">
                                           <source src="<?php echo $this->Url->build('/files/TopMusics/audio/', true) . $topMusic->audio;?>" type="audio/mpeg">
                                        </audio>
                                    <?php else:?>
                                        --
                                    <?php endif;?>
                                </td>
                                <td> <?php echo $this->Utility->__FormatStatus( $topMusic->status); ?></td>
                                <td><?= $topMusic->created ?></td>
                                <td class="actions" nowrap="nowrap">
                                    <?= $this->Html->link('<i class="fa fa-eye" aria-hidden="true"></i>', ['action' => 'view', $topMusic->id],['class'=>'btn btn-info btn-xs text-center','escape'=>false]) ?>
                                    <?= $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i>', ['action' => 'edit', $topMusic->id],['class'=>'btn btn-primary btn-xs text-center','escape'=>false]) ?>
                                    <?= $this->Html->link('<i class="fa fa-trash" aria-hidden="true"></i>', ['action' => 'delete', $topMusic->id],['class'=>'btn btn-danger btn-xs text-center','escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $topMusic->id)]) ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php echo $this->Form->end(); ?>
        </div>
        <div class="panel-footer">
            <?php echo $this->Element('Manager/pagination');?>
        </div>
    </div>
</div>
