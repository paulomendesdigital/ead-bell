<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TopMusic $topMusic
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Top Music'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Edit').' '.__('Top Music'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li class="hidden-xs"><?= $this->Html->link('<i class="fa fa-list"></i> '.__('List Top Musics'), ['action' => 'index'],['class'=>'btn btn-small btn-info','escape'=>false]) ?></li>
        <li><?= $this->Form->postLink('<i class="fa fa-trash"></i> '.__('Delete'),['action' => 'delete', $topMusic->id],['confirm' => __('Are you sure you want to delete # {0}?', $topMusic->id), 'class'=>'btn btn-small btn-danger','escape'=>false]) ?></li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="topMusics form large-9 medium-8 columns content">
        <?= $this->Form->create($topMusic, ['type' => 'file']); ?>
        <fieldset>
            <legend><?= __('Edit Top Music') ?></legend>
            <div class="row clearfix mt-2">
                <div class="col-sm-6 col-xs-12">
                    <?php echo $this->Form->control('id');?>
                    <?php echo $this->Form->control('company_id', ['options' => $companies, 'type'=>'select','class' => 'form-control', 'label' => __('company')]);?>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <?php echo $this->Form->control('ordination', array('class' => 'form-control', 'label' => __('ordination')));?>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="col-sm-6 col-xs-12">
                    <?php echo $this->Form->control('artist', ['class' => 'form-control', 'label' => __('artist')]);?>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <?php echo $this->Form->control('name', array('class' => 'form-control', 'label' => __('Nome da Música')));?>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="col-sm-12 col-xs-12">
                    <?php echo $this->Form->control('link', ['class' => 'form-control', 'label' => __('link')]);?>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="col-sm-6 col-xs-12 hidden">
                    <fieldset style="border: 1px solid silver; padding: 15px; border-radius: 4px;">
                        <?php //echo $this->Form->control('audio', array('class' => 'form-control input-lg', 'type'=>'file', 'label' => __('Áudio de 30s')));?>
                        <?php if( !empty($topMusic->audio) ):?>
                            <audio controls style="width: 200px;">
                               <source src="<?php echo $this->Url->build('/files/TopMusics/audio/', true) . $topMusic->audio;?>" type="audio/mpeg">
                            </audio>
                            <div class="form-group row text-center hidden" data-hide="modal">
                                <label class="col-sm-12" class="">Remover Audio?</label>
                                <div class="col-sm-12"><?php echo $this->Form->control('audio_remove', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false)); ?></div>
                            </div>
                            <hr />
                        <?php endif;?>
                    </fieldset>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <fieldset style="border: 1px solid silver; padding: 15px; border-radius: 4px;">  
                        <?php echo $this->Form->control('image_name', array('class' => 'form-control  input-lg', 'type'=>'file', 'label' => "Imagem ({$image_size})"));?>
                        <?php if( !empty($topMusic->image_name) ):?>
                            <?php echo $this->Html->image("/files/TopMusics/image_name/{$topMusic->image_name}",['class'=>'img-responsive']);?>
                            <div class="form-group row text-center hidden" data-hide="modal">
                                <label class="col-sm-12" class="">Remover Imagem?</label>
                                <div class="col-sm-12"><?php echo $this->Form->control('image_name_remove', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false)); ?></div>
                            </div>
                            <hr />
                        <?php endif;?>
                    </fieldset>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="form-group" data-hide="modal">
                    <label class="col-sm-12" class="">Status</label>
                    <div class="col-sm-12"><?php echo $this->Form->control('status', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false, 'checked' => 'checked')); ?></div>
                </div>
            </div>
            <div class="row clearfix mt-2 mb-2">
                <div class="col-sm-12 col-xs-12">
                    <?= $this->Form->button(__('Edit'),['class'=>'btn btn-primary']) ?>
                        <?= $this->Form->button(__('Edit and continue'),['name'=>'refer','class'=>'btn btn-info']) ?>
                </div>
            </div>
        </fieldset>
        <?= $this->Form->end() ?>
    </div>
</div>
