<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TopMusic $topMusic
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Top Music'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Ver').' '.__('Top Music'); ?></a></li>
    </ul>
</div>
<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li class="hidden-xs"><?= $this->Html->link('<i class="fa fa-list"></i> '.__('List Top Musics'), ['action' => 'index'],['class'=>'btn btn-small btn-info','escape'=>false]) ?></li>
        <li class="hidden-xs"><?= $this->Html->link('<i class="fa fa-edit"></i> '.__('Edit Top Music'), ['action' => 'edit', $topMusic->id],['class'=>'btn btn-small btn-success','escape'=>false]) ?></li>
        <li><?= $this->Form->postLink('<i class="fa fa-trash"></i> '.__('Delete'),['action' => 'delete', $topMusic->id],['confirm' => __('Are you sure you want to delete # {0}?', $topMusic->id), 'class'=>'btn btn-small btn-danger','escape'=>false]) ?></li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="topMusics view large-9 medium-8 columns content">
        <h3><?= h($topMusic->name) ?></h3>
        <table class="vertical-table">
            <tr>
                <th scope="row"><?= __('Company') ?></th>
                <td><?= $topMusic->has('company') ? $this->Html->link($topMusic->company->name, ['controller' => 'Companies', 'action' => 'view', $topMusic->company->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Artist') ?></th>
                <td><?= h($topMusic->artist) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($topMusic->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Link') ?></th>
                <td><?= h($topMusic->link) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Audio') ?></th>
                <td>
                    <?php if( !empty($topMusic->audio) ):?>
                        <audio controls style="width: 200px;">
                           <source src="<?php echo $this->Url->build('/files/TopMusics/audio/', true) . $topMusic->audio;?>" type="audio/mpeg">
                        </audio>
                    <?php endif;?>
                </td>
            </tr>
            <tr>
                <th scope="row"><?= __('Image Name') ?></th>
                <td><?= h($topMusic->image_name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Image Path') ?></th>
                <td><?= h($topMusic->image_path) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($topMusic->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($topMusic->modified) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($topMusic->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Status') ?></th>
                <td><?= $this->Number->format($topMusic->status) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Ordination') ?></th>
                <td><?= $this->Number->format($topMusic->ordination) ?></td>
            </tr>
        </table>
    </div>
</div>