<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TopMusic $topMusic
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Top Music'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Add').' '.__('Top Music'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link('<i class="fa fa-list"></i> '.__('Listar Top Mix'), ['controller' => 'TopMusics', 'action' => 'index'],['class'=>'btn btn-small btn-info','escape'=>false]) ?></li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="topMusics form large-9 medium-8 columns content">
        <?= $this->Form->create($topMusic, ['type' => 'file']); ?>
        <fieldset class="col-sm-12">
            <legend><?= __('Add Top Music') ?></legend>
            <div class="row clearfix mt-2">
                <div class="col-sm-6 col-xs-12">
                    <?php echo $this->Form->control('company_id', ['options' => $companies, 'type'=>'select','class' => 'form-control', 'label' => __('company')]);?>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <?php echo $this->Form->control('ordination', array('class' => 'form-control', 'label' => __('ordination')));?>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="col-sm-6 col-xs-12">
                    <?php echo $this->Form->control('artist', ['class' => 'form-control', 'label' => __('artist')]);?>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <?php echo $this->Form->control('name', array('class' => 'form-control', 'label' => __('Nome da Música')));?>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="col-sm-12 col-xs-12">
                    <?php echo $this->Form->control('link', ['class' => 'form-control', 'label' => __('link')]);?>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="col-sm-6 col-xs-12">
                    <?php //echo $this->Form->control('audio', array('class' => 'form-control input-lg', 'type'=>'file', 'label' => __('Áudio de 30s')));?>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <?php echo $this->Form->control('image_name', array('class' => 'form-control input-lg', 'type'=>'file', 'label' => __('Imagem') . " ({$image_size})"));?>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="form-group" data-hide="modal">
                    <label class="col-sm-12" class="">Status</label>
                    <div class="col-sm-12"><?php echo $this->Form->control('status', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false, 'checked' => 'checked')); ?></div>
                </div>
            </div>
            <div class="row clearfix mt-2 mb-2">
                <div class="col-sm-12 col-xs-12">
                    <?= $this->Form->button(__('Add'),['class'=>'btn btn-primary']) ?>
                </div>
            </div>
        </fieldset>
        <?= $this->Form->end() ?>
    </div>
</div>