<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Winner $winner
 */
?>

<div class="container-fluid container-fixed-lg">
    <div class="winners form large-9 medium-8 columns content">
        <?= $this->Form->create($winner) ?>
        <fieldset class="col-sm-12">
            <legend><?= __('Instrução ao Vencedor') ?></legend>
            <?php
                echo $this->Form->control('id');
                echo $this->Form->control('company_id',['type'=>'hidden','value'=>$winner->company_id]);
                echo $this->Form->control('participant_id',['type'=>'hidden','value'=>$winner->participant_id]);
                echo $this->Form->control('urser_id',['type'=>'hidden','value'=>$winner->user_id]);
                echo $this->Form->control('content_id',['type'=>'hidden','value'=>$winner->content_id]);
            ?>
            <div class="row clearfix mt-2">
                <div class="col-sm-12 col-xs-12">
                    <?php echo $this->Form->control('instrution', ['type'=>'textarea','rows'=>10,'class' => 'form-control', 'label' => false]);?>
                </div>
            </div>
            <div class="row clearfix mt-2 mb-2">
                <div class="col-sm-12 col-xs-12">
                    <?= $this->Form->button(__('Edit'),['class'=>'btn btn-primary']) ?>
                </div>
            </div>
        </fieldset>
        <?= $this->Form->end() ?>
    </div>
</div>