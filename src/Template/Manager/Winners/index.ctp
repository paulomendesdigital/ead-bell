<?php
/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Winner[]|\Cake\Collection\CollectionInterface $winners
 */
?>


<?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' =>'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page">Winners</li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">

    <div class="panel panel-default">

        <div class="panel-body">

            <?php echo $this->Form->create($this->request->getParam('controller'), ['url' => ['action' => 'delete']]); ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">
                            <button class="btn btn-link" type='submit'><i class="pg-trash"></i></button>
                        </th>
                                                    <th scope="col">
                                <?= $this->Paginator->sort('id') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'id'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                                                    <th scope="col">
                                <?= $this->Paginator->sort('user_id') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'user_id'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                                                    <th scope="col">
                                <?= $this->Paginator->sort('content_id') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'content_id'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                                                    <th scope="col">
                                <?= $this->Paginator->sort('created') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'created'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                                                    <th scope="col">
                                <?= $this->Paginator->sort('modified') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'modified'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                                                <th scope="col">
                            <?php echo __('Actions'); ?>
                        </th>
                    </tr>
                </thead>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($winners as $winner){ ?>
                <tr>
                    <td>
                        <input name="data[Winners][][Winners][id]" value="<? echo $winner->id; ?>" id="winner-<?php echo $winner->id; ?>" type="checkbox"/>
                        <label for="page-<?php echo $winner->id;?>" class="no-padding no-margin"></label>
                    </td>
                                                                                                                                                                                                                                        <td><?= $this->Number->format($winner->id) ?></td>
                                                                                                                                                                                                                    <td><?= $winner->has('user') ? $this->Html->link($winner->user->name, ['controller' => 'Users', 'action' => 'view', $winner->user->id]) : '' ?></td>
                                                                                                                                                                                                                                            <td><?= $winner->has('content') ? $this->Html->link($winner->content->name, ['controller' => 'Contents', 'action' => 'view', $winner->content->id]) : '' ?></td>
                                                                                                                                                                                                                                                                                                                    <td><?= h($winner->created) ?></td>
                                                                                                                                                                                                                                                                                            <td><?= h($winner->modified) ?></td>
                                                                                                                <td class="actions">
                        <?= $this->Html->link(__('Ver'), ['action' => 'view', $winner->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $winner->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $winner->id], ['confirm' => __('Are you sure you want to delete # {0}?', $winner->id)]) ?>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <?php echo $this->Form->end(); ?>

    <?php echo $this->Element('Manager/pagination');?>

</div></div></div>
