<?php
/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Advertisement[]|\Cake\Collection\CollectionInterface $advertisements
 */
?>

<?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' =>'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page">Publicidades</li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">
    <fieldset class="box-search">
        <?php echo $this->Form->create(null,['type'=>'get']);?>
        <?php if( isset($companies) ):?>
            <div class="col-sm-2 col-xs-12">
                <?php echo $this->Form->control('company_id', ['data-toggle'=>'change_company_id', 'options'=>$companies,'empty'=>'Filtrar por Empresa','class' => 'form-control', 'label' => __('Empresa')]);?>
            </div>
        <?php endif;?>
        
        <div class="col-sm-3 col-xs-12">
            <?php echo $this->Form->control('name', ['class' => 'form-control', 'label' => __('Nome')]);?>
        </div>
        
        <div class="col-sm-2 col-xs-12">
            <?= $this->Form->button(__('buscar'),['class'=>'btn btn-primary']) ?>
        </div>
        <div class="row clearfix mt-2 mb-2">
            
        </div>
        <?php echo $this->Form->end();?>
    </fieldset>
</div>

<div class="container-fluid container-fixed-lg">
    <div class="panel panel-default">
        <div class="panel-body">
            <?php echo $this->Form->create($this->request->getParam('controller'), ['url' => ['action' => 'delete']]); ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col" class="hidden">
                            <button class="btn btn-link" type='submit'><i class="pg-trash"></i></button>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('id') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'id'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col" class="">
                            <?= $this->Paginator->sort('company_id') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'company_id'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('name') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'name'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('start','Publicar em') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'start'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('finish','Despublicar em') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'finish'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('status') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'status'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('created') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'created'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('modified') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'modified'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?php echo __('Actions'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($advertisements as $advertisement){ ?>
                        <tr>
                            <td class="hidden">
                                <input name="data[Advertisements][][Advertisements][id]" value="<? echo $advertisement->id; ?>" id="advertisement-<?php echo $advertisement->id; ?>" type="checkbox"/>
                                <label for="page-<?php echo $advertisement->id;?>" class="no-padding no-margin"></label>
                            </td>
                            <td><?= $this->Number->format($advertisement->id) ?></td>
                            <td class=""><?= $advertisement->company->name; ?></td>
                            <td><?= h($advertisement->name) ?></td>
                            <td><?= h($advertisement->start->format('d/m/Y')) ?></td>
                            <td><?= h( $advertisement->finish ? $advertisement->finish->format('d/m/Y') : '--') ?></td>
                            <td><?= $this->Utility->__FormatStatus($advertisement->status) ?></td>
                            <td><?= h($advertisement->created->format('d/m/Y')) ?></td>
                            <td><?= h($advertisement->modified->format('d/m/Y')) ?></td>
                            <td class="actions">
                                <?= $this->Html->link('<i class="fa fa-eye" aria-hidden="true"></i>', ['action' => 'view', $advertisement->id],['class'=>'btn btn-info btn-xs text-center','escape'=>false]) ?>
                                <?= $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i>', ['action' => 'edit', $advertisement->id],['class'=>'btn btn-primary btn-xs text-center','escape'=>false]) ?>
                                <?= $this->Html->link('<i class="fa fa-trash" aria-hidden="true"></i>', ['action' => 'delete', $advertisement->id],['class'=>'btn btn-danger btn-xs text-center','escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $advertisement->id)]); ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php echo $this->Form->end(); ?>

            <?php echo $this->Element('Manager/pagination');?>
        </div>
    </div>
</div>
