<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Advertisement $advertisement
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Publicidades'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Add').' '.__('Advertisement'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link('<i class="fa fa-list"></i> '.__('List Advertisements'), ['action' => 'index'],['class'=>'btn btn-small btn-info','escape'=>false]) ?></li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="advertisements form large-9 medium-8 columns content">
        <?= $this->Form->create($advertisement, ['type' => 'file']); ?>
            <fieldset>
                <legend><?= __('Add Advertisement') ?></legend>
                <div class="row clearfix mt-2">
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('company_id', ['class' => 'form-control', 'label' => __('company')]);?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('type_pub', ['options'=>$type_pubs,'class' => 'form-control', 'label' => __('Tipo de Publicidade')]);?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('position', ['options'=>$positions, 'empty'=>'Selecione', 'required'=>true, 'class' => 'form-control', 'label' => __('Posição')]);?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('start', ['value'=>date('d/m/Y'),'type'=>'text', 'class' => 'form-control', 'data-content' => "datepicker", 'label' => __('Publicar em')]);?>
                    </div>
                    <div class="col-sm-2 col-xs-12 program">
                        <?php echo $this->Form->control('finish', array('type'=>'text','class' => 'form-control', 'data-content' => "datepicker", 'label' => __('Sair de Publicação em')));?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="col-sm-12 col-xs-12">
                        <?php echo $this->Form->control('name', ['required'=>true, 'class' => 'form-control', 'label' => __('name')]);?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="col-sm-12 col-xs-12">
                        <?php echo $this->Form->control('embed', ['class' => 'form-control', 'label' => __('Código Anúncio Google')]);?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="col-sm-4 col-xs-12">
                        <?php echo $this->Form->control('HOME_SUPERBANNER_SIZE', ['id'=>'HOME_SUPERBANNER','type' => 'hidden', 'value' => $HOME_SUPERBANNER_SIZE]);?>
                        <?php echo $this->Form->control('HOME_RETANGULO_SIZE', ['id'=>'HOME_RETANGULO','type' => 'hidden', 'value' => $HOME_RETANGULO_SIZE]);?>
                        <?php echo $this->Form->control('INTERNA_SUPERBANNER_SIZE', ['id'=>'INTERNA_SUPERBANNER','type' => 'hidden', 'value' => $INTERNA_SUPERBANNER_SIZE]);?>
                        <?php echo $this->Form->control('PLAYER_CABECALHO_SIZE', ['id'=>'PLAYER_CABECALHO','type' => 'hidden', 'value' => $PLAYER_CABECALHO_SIZE]);?>
                        <?php echo $this->Form->control('PLAYER_RETANGULO_SIZE', ['id'=>'PLAYER_RETANGULO','type' => 'hidden', 'value' => $PLAYER_RETANGULO_SIZE]);?>
                        
                        <label class="label-pub" for="image">Publicidade em imagem (<?php echo $HOME_SUPERBANNER_SIZE;?>)</label>
                        <?php echo $this->Form->control('image', array('class' => 'form-control  input-lg', 'type'=>'file', 'label' => false));?>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <label class="label-pub" for="image">Publicidade em imagem p/ Celular (<?php echo $MOBILE_SIZE;?>)</label>
                        <?php echo $this->Form->control('image_mobile', array('class' => 'form-control  input-lg', 'type'=>'file', 'label' => false));?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="form-group" data-hide="modal">
                        <label class="col-sm-12" class="">Status</label>
                        <div class="col-sm-12"><?php echo $this->Form->control('status', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false, 'checked' => 'checked')); ?></div>
                    </div>
                </div>
                <div class="row clearfix mt-2 mb-2">
                    <div class="col-sm-12 col-xs-12">
                        <?= $this->Form->button(__('Add'),['class'=>'btn btn-primary']); ?>
                    </div>
                </div>
            </fieldset>
        <?= $this->Form->end() ?>
    </div>
</div>
