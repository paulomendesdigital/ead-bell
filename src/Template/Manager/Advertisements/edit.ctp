<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Advertisement $advertisement
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Publicidades'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Edit').' '.__('Advertisement'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link('<i class="fa fa-list"></i> '.__('List Advertisements'), ['action' => 'index'],['class'=>'btn btn-small btn-info','escape'=>false]) ?></li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="advertisements form large-9 medium-8 columns content">
        <?= $this->Form->create($advertisement, ['type' => 'file']); ?>
            <fieldset>
                <legend><?= __('Edit Advertisement') ?></legend>
                <div class="row clearfix mt-2">
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('id');?>
                        <?php echo $this->Form->control('company_id', ['class' => 'form-control', 'label' => __('company')]);?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('type_pub', ['options'=>$type_pubs,'class' => 'form-control', 'label' => __('Tipo de Publicidade')]);?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('position', ['options'=>$positions, 'empty'=>'Selecione', 'required'=>true, 'class' => 'form-control', 'label' => __('Posição')]);?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('start', ['value'=>$advertisement->start?$advertisement->start->format('d/m/Y'):'', 'type'=>'text', 'class' => 'form-control', 'data-content' => "datepicker", 'label' => __('Publicar em')]);?>
                    </div>
                    <div class="col-sm-2 col-xs-12 program">
                        <?php echo $this->Form->control('finish', array('value'=>$advertisement->finish ? $advertisement->finish->format('d/m/Y') : '', 'type'=>'text','class' => 'form-control', 'data-content' => "datepicker", 'label' => __('Sair de Publicação em')));?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="col-sm-12 col-xs-12">
                        <?php echo $this->Form->control('name', ['class' => 'form-control', 'label' => __('name')]);?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="col-sm-12 col-xs-12">
                        <?php echo $this->Form->control('embed', ['class' => 'form-control', 'label' => __('Embed')]);?>
                    </div>
                </div>

                <div class="row clearfix mt-2">
                    <?php echo $this->Form->control('HOME_SUPERBANNER_SIZE', ['id'=>'HOME_SUPERBANNER','type' => 'hidden', 'value' => $HOME_SUPERBANNER_SIZE]);?>
                    <?php echo $this->Form->control('HOME_RETANGULO_SIZE', ['id'=>'HOME_RETANGULO','type' => 'hidden', 'value' => $HOME_RETANGULO_SIZE]);?>
                    <?php echo $this->Form->control('INTERNA_SUPERBANNER_SIZE', ['id'=>'INTERNA_SUPERBANNER','type' => 'hidden', 'value' => $INTERNA_SUPERBANNER_SIZE]);?>
                    <?php echo $this->Form->control('PLAYER_CABECALHO_SIZE', ['id'=>'PLAYER_CABECALHO','type' => 'hidden', 'value' => $PLAYER_CABECALHO_SIZE]);?>
                    <?php echo $this->Form->control('PLAYER_RETANGULO_SIZE', ['id'=>'PLAYER_RETANGULO','type' => 'hidden', 'value' => $PLAYER_RETANGULO_SIZE]);?>
                    <div class="col-sm-4 col-xs-12">
                        <fieldset style="border: 1px solid silver; padding: 15px; border-radius: 4px;">  
                            <label class="label-pub" for="image">Publicidade em imagem (<?php echo $HOME_SUPERBANNER_SIZE;?>)</label>
                            <?php echo $this->Form->control('image', array('class' => 'form-control  input-lg', 'type'=>'file', 'label' => false));?>
                        
                            <?php if( !empty($advertisement->image) ):?>
                                <?php echo $this->Html->image("/files/Advertisements/image/{$advertisement->image}",['class'=>'img-responsive']);?>
                                <div class="form-group row text-center" data-hide="modal">
                                    <label class="col-sm-12" class="">Remover Imagem?</label>
                                    <div class="col-sm-12"><?php echo $this->Form->control('image_remove', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false)); ?></div>
                                </div>
                                <hr />
                            <?php endif;?>
                        </fieldset>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <fieldset style="border: 1px solid silver; padding: 15px; border-radius: 4px;">  
                            <label for="image">Publicidade em imagem p/ Celular (<?php echo $MOBILE_SIZE;?>)</label>
                            <?php echo $this->Form->control('image_mobile', array('class' => 'form-control  input-lg', 'type'=>'file', 'label' => false));?>
                            
                            <?php if( !empty($advertisement->image_mobile) ):?>
                                <?php echo $this->Html->image("/files/Advertisements/image_mobile/{$advertisement->image_mobile}",['class'=>'img-responsive']);?>
                                <div class="form-group row text-center" data-hide="modal">
                                    <label class="col-sm-12" class="">Remover Imagem?</label>
                                    <div class="col-sm-12"><?php echo $this->Form->control('image_mobile_remove', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false)); ?></div>
                                </div>
                                <hr />
                            <?php endif;?>
                        </fieldset>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="form-group" data-hide="modal">
                        <label class="col-sm-12" class="">Status</label>
                        <div class="col-sm-12"><?php echo $this->Form->control('status', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false, 'checked' => 'checked')); ?></div>
                    </div>
                </div>
                <div class="row clearfix mt-2 mb-2">
                    <div class="col-sm-12 col-xs-12">
                        <?= $this->Form->button(__('Edit'),['class'=>'btn btn-primary']) ?>
                        <?= $this->Form->button(__('Edit and continue'),['name'=>'refer','class'=>'btn btn-info']) ?>
                    </div>
                </div>
            </fieldset>
        <?= $this->Form->end() ?>
    </div>
</div>
