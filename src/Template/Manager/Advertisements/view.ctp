<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Advertisement $advertisement
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Publicidades'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Ver').' '.__('Publicidade'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link('<i class="fa fa-edit"></i> '.__('Edit'), ['action' => 'edit', $advertisement->id],['class'=>'btn btn-small btn-warning','escape'=>false]) ?></li>
        <li><?= $this->Form->postLink('<i class="fa fa-trash"></i> '.__('Delete'), ['action' => 'delete', $advertisement->id], ['class'=>'btn btn-small btn-danger','escape'=>false,'confirm' => __('Are you sure you want to delete # {0}?', $advertisement->id)])?> </li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="advertisements view large-9 medium-8 columns content">
        <h3><?= h($advertisement->name) ?></h3>
        <table class="vertical-table">
            <tr>
                <th scope="row"><?= __('Company') ?></th>
                <td><?= $advertisement->has('company') ? $this->Html->link($advertisement->company->name, ['controller' => 'Companies', 'action' => 'view', $advertisement->company->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($advertisement->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($advertisement->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Status') ?></th>
                <td><?= $this->Utility->__FormatStatus($advertisement->status) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Start') ?></th>
                <td><?= h($advertisement->start ? $advertisement->start->format('d/m/Y') : '') ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Finish') ?></th>
                <td><?= h($advertisement->finish ? $advertisement->finish->format('d/m/Y') : '') ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($advertisement->created->format('d/m/Y H:i:s')) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($advertisement->modified->format('d/m/Y H:i:s')) ?></td>
            </tr>
        </table>
        <div class="row">
            <h4><?= __('Embed') ?></h4>
            <?= $advertisement->embed; ?>
        </div>
    </div>
</div>
