<?php
/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $advertReports
 */
?>


<?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' =>'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page"><?php echo __('Advert Reports'); ?></li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">

    <div class="panel panel-default">

        <div class="panel-body">

            <?= $this->Form->create(null, ['method' => 'get', 'valueSources' => ['query', 'context']]) ?>
            <div class="row">
                   
                <div class="col-md-3">
                    <?= $this->Form->control('AdvertReports.id', ['class' => 'form-control']) ?>
                </div>
                   
                <div class="col-md-3">
                    <?= $this->Form->control('AdvertReports.model_id', ['class' => 'form-control']) ?>
                </div>
                   
                <div class="col-md-3">
                    <?= $this->Form->control('AdvertReports.model_name', ['class' => 'form-control']) ?>
                </div>
                   
                <div class="col-md-3">
                    <?= $this->Form->control('AdvertReports.report_date', ['class' => 'form-control']) ?>
                </div>
                   
                <div class="col-md-3">
                    <?= $this->Form->control('AdvertReports.views_count', ['class' => 'form-control']) ?>
                </div>
                   
                <div class="col-md-3">
                    <?= $this->Form->control('AdvertReports.unique_views_count', ['class' => 'form-control']) ?>
                </div>
                   
                <div class="col-md-3">
                    <?= $this->Form->control('AdvertReports.clicks_count', ['class' => 'form-control']) ?>
                </div>
                   
                <div class="col-md-3">
                    <?= $this->Form->control('AdvertReports.unique_clicks_count', ['class' => 'form-control']) ?>
                </div>
                   
                <div class="col-md-3">
                    <?= $this->Form->control('AdvertReports.ctr', ['class' => 'form-control']) ?>
                </div>
                   
                <div class="col-md-3">
                    <?= $this->Form->control('AdvertReports.unique_ctr', ['class' => 'form-control']) ?>
                </div>
                   
                <div class="col-md-3">
                    <?= $this->Form->control('AdvertReports.status', ['class' => 'form-control']) ?>
                </div>
                   
                <div class="col-md-3">
                    <?= $this->Form->control('AdvertReports.created', ['class' => 'form-control']) ?>
                </div>
                   
                <div class="col-md-3">
                    <?= $this->Form->control('AdvertReports.modified', ['class' => 'form-control']) ?>
                </div>
                                
                <div class="col-md-3">
                    <label>&nbsp;</label>
                    <button type="submit" class="btn btn-primary" style="width: 100%">Buscar</button>
                </div>
            </div>
            <?= $this->Form->end() ?>

            <?php echo $this->Form->create(null, ['url' => ['action' => 'delete']]); ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">
                            <button class="btn btn-link" type='submit'><i class="pg-trash"></i></button>
                        </th>
                       
                        <th scope="col">
                            <?= $this->Paginator->sort('id', __('Id')) ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'id'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                       
                        <th scope="col">
                            <?= $this->Paginator->sort('model_id', __('Model Id')) ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'model_id'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                       
                        <th scope="col">
                            <?= $this->Paginator->sort('model_name', __('Model Name')) ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'model_name'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                       
                        <th scope="col">
                            <?= $this->Paginator->sort('report_date', __('Report Date')) ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'report_date'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                       
                        <th scope="col">
                            <?= $this->Paginator->sort('views_count', __('Views Count')) ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'views_count'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                       
                        <th scope="col">
                            <?= $this->Paginator->sort('unique_views_count', __('Unique Views Count')) ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'unique_views_count'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                       
                        <th scope="col">
                            <?= $this->Paginator->sort('clicks_count', __('Clicks Count')) ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'clicks_count'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                       
                        <th scope="col">
                            <?= $this->Paginator->sort('unique_clicks_count', __('Unique Clicks Count')) ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'unique_clicks_count'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                       
                        <th scope="col">
                            <?= $this->Paginator->sort('ctr', __('Ctr')) ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'ctr'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                       
                        <th scope="col">
                            <?= $this->Paginator->sort('unique_ctr', __('Unique Ctr')) ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'unique_ctr'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                       
                        <th scope="col">
                            <?= $this->Paginator->sort('status', __('Status')) ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'status'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                       
                        <th scope="col">
                            <?= $this->Paginator->sort('created', __('Created')) ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'created'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                       
                        <th scope="col">
                            <?= $this->Paginator->sort('modified', __('Modified')) ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'modified'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        
                        <th scope="col">
                            <?php echo __('Actions'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($advertReports as $advertReport){ ?>
                        <tr>
                            <td>
                                <input name="Advert Reports[][id]" value="<? echo $advertReport->id; ?>" id="advertReport-<?php echo $advertReport->id; ?>" type="checkbox"/>
                                <label for="page-<?php echo $advertReport->id;?>" class="no-padding no-margin"></label>
                            </td>
                                                                              
                            <td><?= $this->Number->format($advertReport->id) ?></td>
                                                                                                          
                            <td><?= $this->Number->format($advertReport->model_id) ?></td>
                                                                                                            
                            <td><?= h($advertReport->model_name) ?></td>
                                                                                                            
                            <td><?= h($advertReport->report_date) ?></td>
                                                                                                          
                            <td><?= $this->Number->format($advertReport->views_count) ?></td>
                                                                                                          
                            <td><?= $this->Number->format($advertReport->unique_views_count) ?></td>
                                                                                                          
                            <td><?= $this->Number->format($advertReport->clicks_count) ?></td>
                                                                                                          
                            <td><?= $this->Number->format($advertReport->unique_clicks_count) ?></td>
                                                                                                          
                            <td><?= $this->Number->format($advertReport->ctr) ?></td>
                                                                                                          
                            <td><?= $this->Number->format($advertReport->unique_ctr) ?></td>
                                                                                                          
                            <td><?= $this->Number->format($advertReport->status) ?></td>
                                                                                                            
                            <td><?= h($advertReport->created) ?></td>
                                                                                                            
                            <td><?= h($advertReport->modified) ?></td>
                                                                 
                            <td class="actions">
                                <?= $this->Html->link('<button type="button" class="btn btn-info"><i class="fa-eye fa"></i></button>', ['action' => 'view', $advertReport->id], ['escape' => false]) ?>
                                <?= $this->Html->link('<button type="button" class="btn btn-primary"><i class="fa-pencil fa"></i></button>', ['action' => 'edit', $advertReport->id], ['escape' => false]) ?>
                                <?= $this->Form->postLink('<button type="button" class="btn btn-danger"><i class="fa-trash fa"></i></button>', ['action' => 'delete', $advertReport->id], ['confirm' => __('Are you sure you want to delete # {0}?', $advertReport->id), 'escape' => false]) ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php echo $this->Form->end(); ?>

            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
            </div>

        </div>
    </div>
</div>
