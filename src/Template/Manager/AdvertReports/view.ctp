<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $advertReport
 */
?>

<div class="container-fluid container-fixed-lg">
	<ol class="breadcrumb">
		<li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
		<li><?php echo $this->Html->link(__('Advert Reports'), ['controller' => 'AdvertReports', 'action' => 'index'], ['class' => '']); ?></li>
		<li><a class="active"><?php echo __('View') . ' ' . __('Advert Reports'); ?></a></li>
	</ol>
</div>

<div class="container-fluid container-fixed-lg">

	<!-- BASIC INFORMATIONS -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="panel-title"><?php echo __('Basic information'); ?></div>
		</div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
                <tr>
                    <td><strong><?= __('Model Name') ?></strong></td>
                    <td><?= h($advertReport->model_name) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Id') ?></strong></td>
                    <td><?= $this->Number->format($advertReport->id) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Model Id') ?></strong></td>
                    <td><?= $this->Number->format($advertReport->model_id) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Views Count') ?></strong></td>
                    <td><?= $this->Number->format($advertReport->views_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Unique Views Count') ?></strong></td>
                    <td><?= $this->Number->format($advertReport->unique_views_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Clicks Count') ?></strong></td>
                    <td><?= $this->Number->format($advertReport->clicks_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Unique Clicks Count') ?></strong></td>
                    <td><?= $this->Number->format($advertReport->unique_clicks_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Ctr') ?></strong></td>
                    <td><?= $this->Number->format($advertReport->ctr) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Unique Ctr') ?></strong></td>
                    <td><?= $this->Number->format($advertReport->unique_ctr) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Status') ?></strong></td>
                    <td><?= $this->Number->format($advertReport->status) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Report Date') ?></strong></td>
                    <td><?= h($advertReport->report_date) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Created') ?></strong></td>
                    <td><?= h($advertReport->created) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Modified') ?></strong></td>
                    <td><?= h($advertReport->modified) ?></td>
                </tr>
                
            </table>
                        <div class="row"></div>
            <hr/>
            <div class='row'>
                <div class="actions">
    				<?php echo $this->Html->link(__('Edit register'), array('action' => 'edit', $this->request->getAttribute('params')['pass'][0]), array('class' => 'btn btn-info')); ?>
			    </div>
            </div>
        </div>
    </div>

</div>