<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $advertReport
 */
?>
<div class="container-fluid container-fixed-lg">
	<ol class="breadcrumb">
		<li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
		<li><?php echo $this->Html->link(__('Advert Reports'), ['controller' => 'AdvertReports', 'action' => 'index'], ['class' => '']); ?></li>
		<li><a class="active"><?php echo __('Edit') . ' ' . __('Advert Reports'); ?></a></li>
	</ol>
</div>

<div class="container-fluid container-fixed-lg">

	<div class="row">
		<div class="col-lg-7 col-md-6 ">
			<div class="panel panel-transparent">
				<div class="panel-body">
                    <?php //@TODO In order to always have file upload support when model has a field named image or photo ?>
                    <?= $this->Form->create($advertReport, ['type' => 'file']) ?>
 
                    <div class='row'>
                        <div class="col-sm-12">                            
                            <?php echo $this->Form->control('model_id', ['class' => 'form-control input-lg', 'label' => __('Model Id')]); ?>
                        </div>
                    </div> 
                    <div class='row'>
                        <div class="col-sm-12">                            
                            <?php echo $this->Form->control('model_name', ['class' => 'form-control input-lg', 'label' => __('Model Name')]); ?>
                        </div>
                    </div> 
                    <div class='row'>
                        <div class="col-sm-12">                            
                            <?php echo $this->Form->control('report_date', ['class' => 'form-control input-lg', 'label' => __('Report Date')]); ?>
                        </div>
                    </div> 
                    <div class='row'>
                        <div class="col-sm-12">                            
                            <?php echo $this->Form->control('views_count', ['class' => 'form-control input-lg', 'label' => __('Views Count')]); ?>
                        </div>
                    </div> 
                    <div class='row'>
                        <div class="col-sm-12">                            
                            <?php echo $this->Form->control('unique_views_count', ['class' => 'form-control input-lg', 'label' => __('Unique Views Count')]); ?>
                        </div>
                    </div> 
                    <div class='row'>
                        <div class="col-sm-12">                            
                            <?php echo $this->Form->control('clicks_count', ['class' => 'form-control input-lg', 'label' => __('Clicks Count')]); ?>
                        </div>
                    </div> 
                    <div class='row'>
                        <div class="col-sm-12">                            
                            <?php echo $this->Form->control('unique_clicks_count', ['class' => 'form-control input-lg', 'label' => __('Unique Clicks Count')]); ?>
                        </div>
                    </div> 
                    <div class='row'>
                        <div class="col-sm-12">                            
                            <?php echo $this->Form->control('ctr', ['class' => 'form-control input-lg', 'label' => __('Ctr')]); ?>
                        </div>
                    </div> 
                    <div class='row'>
                        <div class="col-sm-12">                            
                            <?php echo $this->Form->control('unique_ctr', ['class' => 'form-control input-lg', 'label' => __('Unique Ctr')]); ?>
                        </div>
                    </div> 
                    <div class='row'>
                        <div class="col-sm-12">                        
                        <?php if(empty($statuses)){ ?>

                            <?php
                            
                            $this->Form->setTemplates([
                                'nestingLabel' => '{{hidden}}<label{{attrs}}>{{text}}</label>{{input}}'
                            ]);

                            echo $this->Form->control('status', array('label' => __('Status'), 'type' => 'checkbox', 'class' => 'switch', 'data-on' => 'success', 'data-off' => 'danger', 'data-on-label' => __('Yes'), 'data-off-label' => __('No'), 'required' => false));

                            ?>
                            <?php } else { ?>
                                <?php echo $this->Form->control('status', ['class' => 'form-control input-lg', 'label' => __('Status'), 'empty' => __('Select')]); ?>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="clearfix"></div>
					<button class="btn btn-primary" type="submit" data-hide="modal"><?php echo __('Edit'); ?></button>
					<button class="btn btn-info" type="submit" name="refer"><?php echo __('Edit'); ?> <?php echo __('and continue'); ?></button>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>

</div>