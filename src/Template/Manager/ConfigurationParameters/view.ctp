<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ConfigurationParameter $configurationParameter
 */
?>

<div class="container-fluid container-fixed-lg">
    <ol class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Configuration Parameters'), ['controller' => 'ConfigurationParameters', 'action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('View') . ' ' . __('Configuration Parameters'); ?></a></li>
    </ol>
</div>

<div class="container-fluid container-fixed-lg">

    <!-- BASIC INFORMATIONS -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title"><?php echo __('Basic information'); ?></div>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered">
                <tr>
                    <td><strong><?= __('Name') ?></strong></td>
                    <td><?= h($configurationParameter->name) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Code') ?></strong></td>
                    <td><?= h($configurationParameter->code) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Id') ?></strong></td>
                    <td><?= $this->Number->format($configurationParameter->id) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Company Id') ?></strong></td>
                    <td><?= $configurationParameter->has('company') ? $configurationParameter->company->name : null ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Status') ?></strong></td>
                    <td><?= $this->Utility->__FormatStatus($configurationParameter->status) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Created') ?></strong></td>
                    <td><?= h($configurationParameter->created) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Modified') ?></strong></td>
                    <td><?= h($configurationParameter->modified) ?></td>
                </tr>

            </table>

            <div class="text">
                <strong><?= __('Description') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($configurationParameter->description)); ?>
                </blockquote>
            </div>
            <div class="row"></div>
            <hr />
            <div class='row'>
                <div class="actions">
                    <?php echo $this->Html->link(__('Edit register'), array('action' => 'edit', $this->request->getAttribute('params')['pass'][0]), array('class' => 'btn btn-info')); ?>
                </div>
            </div>
        </div>
    </div>

</div>
