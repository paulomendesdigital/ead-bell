<?php

/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ConfigurationParameter[]|\Cake\Collection\CollectionInterface $configurationParameters
 */
?>


<?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' => 'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page"><?php echo __('Configuration Parameters'); ?></li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">

    <div class="panel panel-default">

        <div class="panel-body">

            <?= $this->Form->create(null, ['method' => 'get', 'valueSources' => ['query', 'context']]) ?>
            <div class="row">

                <div class="col-md-4">
                    <?= $this->Form->control('ConfigurationParameters.id', ['class' => 'form-control']) ?>
                </div>

                <div class="col-md-4">
                    <?= $this->Form->control('ConfigurationParameters.company_id', ['class' => 'form-control']) ?>
                </div>

                <div class="col-md-4">
                    <?= $this->Form->control('ConfigurationParameters.name LIKE', ['class' => 'form-control', 'label' => __('Name')]) ?>
                </div>

            </div>
            <div class="row">
                <div class="col-md-4">
                    <?= $this->Form->control('ConfigurationParameters.code LIKE ', ['class' => 'form-control', 'label' => __('Code')]) ?>
                </div>

                <div class="col-md-4">
                    <?= $this->Form->control('ConfigurationParameters.status', ['class' => 'form-control', 'empty' => 'Selecione', 'options' => [0 => 'Inativo', 1 => 'Ativo']]) ?>
                </div>

                <div class="col-md-4">
                    <label>&nbsp;</label>
                    <button type="submit" class="btn btn-primary" style="width: 100%">Buscar</button>
                </div>
            </div>
            <?= $this->Form->end() ?>

            <?php echo $this->Form->create(null, ['url' => ['action' => 'delete']]); ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">
                            <button class="btn btn-link" type='submit'><i class="pg-trash"></i></button>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('ConfigurationParameters.id', __('Id')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'id') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('ConfigurationParameters.company_id', __('Company')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'company_id') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('ConfigurationParameters.name', __('Name')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'name') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('ConfigurationParameters.code', __('Code')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'code') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('ConfigurationParameters.status', __('Status')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'status') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('ConfigurationParameters.created', __('Created')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'created') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?= $this->Paginator->sort('ConfigurationParameters.modified', __('Modified')) ?>
                            <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'modified') { ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                            <?php } ?>
                        </th>

                        <th scope="col">
                            <?php echo __('Actions'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($configurationParameters as $configurationParameter) { ?>
                        <tr>
                            <td>
                                <input name="Configuration Parameters[][id]" value="<? echo $configurationParameter->id; ?>" id="configurationParameter-<?php echo $configurationParameter->id; ?>" type="checkbox" />
                                <label for="page-<?php echo $configurationParameter->id; ?>" class="no-padding no-margin"></label>
                            </td>

                            <td><?= $this->Number->format($configurationParameter->id) ?></td>

                            <td><?= $configurationParameter->has('company') ? $configurationParameter->company->name : null ?></td>

                            <td><?= h($configurationParameter->name) ?></td>

                            <td><?= h($configurationParameter->code) ?></td>

                            <td><?= $this->Utility->__FormatStatus($configurationParameter->status) ?></td>

                            <td><?= h($configurationParameter->created) ?></td>

                            <td><?= h($configurationParameter->modified) ?></td>

                            <td class="actions">
                                <?= $this->Html->link('<button type="button" class="btn btn-info"><i class="fa-eye fa"></i></button>', ['action' => 'view', $configurationParameter->id], ['escape' => false]) ?>
                                <?= $this->Html->link('<button type="button" class="btn btn-primary"><i class="fa-pencil fa"></i></button>', ['action' => 'edit', $configurationParameter->id], ['escape' => false]) ?>
                                <?php // $this->Form->postLink('<button type="button" class="btn btn-danger"><i class="fa-trash fa"></i></button>', ['action' => 'delete', $configurationParameter->id], ['confirm' => __('Are you sure you want to delete # {0}?', $configurationParameter->id), 'escape' => false])
                                ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php echo $this->Form->end(); ?>

            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
            </div>

        </div>
    </div>
</div>
