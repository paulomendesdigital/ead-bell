<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Speaker $speaker
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Speaker'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Add').' '.__('Speaker'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link('<i class="fa fa-list"></i> '.__('List Speakers'), ['action' => 'index'],['class'=>'btn btn-small btn-info','escape'=>false]) ?></li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="speakers form large-9 medium-8 columns content">
        <?= $this->Form->create($speaker, ['type' => 'file']); ?>
        <fieldset>
            <legend><?= __('Add Speaker') ?></legend>
            <div class="row clearfix mt-2">
                <div class="col-sm-6 col-xs-12">
                    <?php echo $this->Form->control('company_id', ['options' => $companies, 'type'=>'select','class' => 'form-control', 'label' => __('company')]);?>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <?php echo $this->Form->control('ordination', array('class' => 'form-control', 'label' => __('ordination')));?>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="col-sm-6 col-xs-12">
                    <?php echo $this->Form->control('name', array('class' => 'form-control', 'label' => __('name')));?>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <?php echo $this->Form->control('work_schedule', array('class' => 'form-control', 'label' => __('work_schedule')));?>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="col-sm-6 col-xs-12">
                    <?php echo $this->Form->control('image', array('type'=>'file', 'class' => 'form-control', 'label' => __('image')));?>
                </div>
                <div class="col-sm-6 col-xs-12">
                    
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="col-sm-12 col-xs-12">
                    <?php echo $this->Form->control('description', array('class' => 'form-control', 'id'=>'ckfinderDescription', 'label' => __('Descrição')));?>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="col-sm-3 col-xs-12">
                    <?php echo $this->Form->control('facebook', array('class' => 'form-control', 'label' => __('facebook')));?>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <?php echo $this->Form->control('instagram', array('class' => 'form-control', 'label' => __('instagram')));?>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <?php echo $this->Form->control('twitter', array('class' => 'form-control', 'label' => __('twitter')));?>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <?php echo $this->Form->control('whatsapp', array('class' => 'form-control', 'label' => __('whatsapp')));?>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="col-sm-12 col-xs-12">
                    <?php echo $this->Form->control('video', array('class' => 'form-control', 'label' => __('Embed do Vídeo')));?>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="form-group" data-hide="modal">
                    <label class="col-sm-12" class="">Status</label>
                    <div class="col-sm-12"><?php echo $this->Form->control('status', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false, 'checked' => 'checked')); ?></div>
                </div>
            </div>
            <div class="row clearfix mt-2 mb-2">
                <div class="col-sm-12 col-xs-12">
                    <?= $this->Form->button(__('Add'),['class'=>'btn btn-primary']) ?>
                </div>
            </div>
        </fieldset>
        <?= $this->Form->end() ?>
    </div>
</div>
