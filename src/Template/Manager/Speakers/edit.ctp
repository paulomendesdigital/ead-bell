<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Speaker $speaker
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Speaker'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Edit').' '.__('Speaker'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link('<i class="fa fa-list"></i> '.__('List Speakers'), ['action' => 'index'],['class'=>'btn btn-small btn-info','escape'=>false]) ?></li>
        <li><?= $this->Form->postLink('<i class="fa fa-trash"></i> '.__('Delete'),['action' => 'delete', $speaker->id],['confirm' => __('Are you sure you want to delete # {0}?', $speaker->id), 'class'=>'btn btn-small btn-danger','escape'=>false]) ?></li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="speakers form large-9 medium-8 columns content">
        <?= $this->Form->create($speaker, ['type' => 'file']); ?>
        <fieldset>
            <legend><?= __('Edit Speaker') ?></legend>
            <div class="row clearfix mt-2">
                <div class="col-sm-6 col-xs-12">
                    <?php echo $this->Form->control('id');?>
                    <?php echo $this->Form->control('company_id', ['options' => $companies, 'type'=>'select','class' => 'form-control', 'label' => __('company')]);?>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <?php echo $this->Form->control('ordination', array('class' => 'form-control', 'label' => __('ordination')));?>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="col-sm-6 col-xs-12">
                    <?php echo $this->Form->control('name', array('class' => 'form-control', 'label' => __('name')));?>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <?php echo $this->Form->control('work_schedule', array('class' => 'form-control', 'label' => __('work_schedule')));?>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="col-sm-6 col-xs-12">
                    <fieldset style="border: 1px solid silver; padding: 15px; border-radius: 4px;">  
                        <?php echo $this->Form->control('image', array('class' => 'form-control  input-lg', 'type'=>'file', 'label' => "Imagem ({$image_size})"));?>
                        <?php if( !empty($speaker->image) ):?>
                            <?php echo $this->Html->image("/files/Speakers/image/{$speaker->image}",['class'=>'img-responsive']);?>
                            <div class="form-group row text-center hidden" data-hide="modal">
                                <label class="col-sm-12" class="">Remover Imagem?</label>
                                <div class="col-sm-12"><?php echo $this->Form->control('image_remove', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false)); ?></div>
                            </div>
                            <hr />
                        <?php endif;?>
                    </fieldset>
                </div>
                <div class="col-sm-6 col-xs-12">
                    
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="col-sm-12 col-xs-12">
                    <?php echo $this->Form->control('description', array('class' => 'form-control', 'id'=>'ckfinderDescription', 'label' => __('Descrição')));?>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="col-sm-3 col-xs-12">
                    <?php echo $this->Form->control('facebook', array('class' => 'form-control', 'label' => __('facebook')));?>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <?php echo $this->Form->control('instagram', array('class' => 'form-control', 'label' => __('instagram')));?>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <?php echo $this->Form->control('twitter', array('class' => 'form-control', 'label' => __('twitter')));?>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <?php echo $this->Form->control('whatsapp', array('class' => 'form-control', 'label' => __('whatsapp')));?>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="col-sm-12 col-xs-12">
                    <?php echo $this->Form->control('video', array('class' => 'form-control', 'label' => __('Embed do Vídeo')));?>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="form-group" data-hide="modal">
                    <label class="col-sm-12" class="">Status</label>
                    <div class="col-sm-12"><?php echo $this->Form->control('status', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false, 'checked' => 'checked')); ?></div>
                </div>
            </div>
            <div class="row clearfix mt-2 mb-2">
                <div class="col-sm-12 col-xs-12">
                    <?= $this->Form->button(__('Edit'),['class'=>'btn btn-primary']) ?>
                    <?= $this->Form->button(__('Edit and continue'),['name'=>'refer','class'=>'btn btn-info']) ?>
                </div>
            </div>
        </fieldset>
        <?= $this->Form->end() ?>
    </div>
</div>
