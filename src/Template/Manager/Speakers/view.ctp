<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Speaker $speaker
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Speaker'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Ver').' '.__('Speaker'); ?></a></li>
    </ul>
</div>
<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li class="hidden-xs"><?= $this->Html->link('<i class="fa fa-list"></i> '.__('List Speakers'), ['action' => 'index'],['class'=>'btn btn-small btn-info','escape'=>false]) ?></li>
        <li class="hidden-xs"><?= $this->Html->link('<i class="fa fa-edit"></i> '.__('Edit Speaker'), ['action' => 'edit', $speaker->id],['class'=>'btn btn-small btn-success','escape'=>false]) ?></li>
        <li><?= $this->Form->postLink('<i class="fa fa-trash"></i> '.__('Delete'),['action' => 'delete', $speaker->id],['confirm' => __('Are you sure you want to delete # {0}?', $speaker->id), 'class'=>'btn btn-small btn-danger','escape'=>false]) ?></li>
    </ul>
</nav>
<div class="container-fluid container-fixed-lg">
    <div class="speakers view large-9 medium-8 columns content">
        <h3><?= h($speaker->name) ?></h3>
        <table class="vertical-table">
            <tr>
                <th scope="row"><?= __('Company') ?></th>
                <td><?= $speaker->has('company') ? $this->Html->link($speaker->company->name, ['controller' => 'Companies', 'action' => 'view', $speaker->company->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($speaker->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Image') ?></th>
                <td><?= h($speaker->image) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Facebook') ?></th>
                <td><?= h($speaker->facebook) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Instagram') ?></th>
                <td><?= h($speaker->instagram) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Twitter') ?></th>
                <td><?= h($speaker->twitter) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Whatsapp') ?></th>
                <td><?= h($speaker->whatsapp) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Work Schedule') ?></th>
                <td><?= h($speaker->work_schedule) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($speaker->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('ordination') ?></th>
                <td><?= $this->Number->format($speaker->ordination) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Status') ?></th>
                <td><?= $this->Utility->__FormatStatus($speaker->status) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($speaker->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($speaker->modified) ?></td>
            </tr>
        </table>
        <div class="row">
            <h4><?= __('Description') ?></h4>
            <?= $this->Text->autoParagraph(h($speaker->description)); ?>
        </div>
        <div class="row">
            <h4><?= __('Video') ?></h4>
            <?= $speaker->video; ?>
        </div>
    </div>
</div>
