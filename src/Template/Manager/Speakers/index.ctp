<?php
/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Speaker[]|\Cake\Collection\CollectionInterface $speakers
 */
?>


<?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' =>'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page">Locutores</li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">
    <fieldset class="box-search">
        <?php echo $this->Form->create(null,['type'=>'get']);?>
        <?php if( isset($companies) ):?>
            <div class="col-sm-2 col-xs-12">
                <?php echo $this->Form->control('company_id', ['data-toggle'=>'change_company_id', 'options'=>$companies,'empty'=>'Filtrar por Empresa','class' => 'form-control', 'label' => __('Empresa')]);?>
            </div>
        <?php endif;?>
        
        <div class="col-sm-3 col-xs-12">
            <?php echo $this->Form->control('name', ['class' => 'form-control', 'label' => __('Nome')]);?>
        </div>
        
        <div class="col-sm-2 col-xs-12">
            <?= $this->Form->button(__('buscar'),['class'=>'btn btn-primary']) ?>
        </div>
        <div class="row clearfix mt-2 mb-2">
            
        </div>
        <?php echo $this->Form->end();?>
    </fieldset>
</div>

<div class="container-fluid container-fixed-lg">
    <div class="panel panel-default">
        <div class="table-responsive">
            <?php echo $this->Form->create($this->request->getParam('controller'), ['url' => ['action' => 'delete']]); ?>
                <table cellpadding="0" cellspacing="0" class="table table-striped table-hover  order">
                        <thead>
                        <tr>
                            <th scope="col" class="hidden">
                                <button class="btn btn-link" type='submit'><i class="pg-trash"></i></button>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('id') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'id'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col" class="">
                                <?= $this->Paginator->sort('company_id') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'company_id'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('image',__('image')) ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'image'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('name') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'name'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('work_schedule',__('work_schedule')) ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'work_schedule'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('status') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'status'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('created') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'created'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?php echo __('Actions'); ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($speakers as $speaker){ ?>
                            <tr>
                                <td><?= $this->Number->format($speaker->id) ?></td>
                                <td class=""><?= $speaker->company->name; ?></td>
                                <td>
                                    <?php if( $speaker->image ): ?>
                                        <?php echo $this->Html->image("/files/Speakers/image/{$speaker->image}",['class'=>'img-responsive','style'=>'max-width: 100px;']);?>
                                    <?php else: ?>
                                    <?php endif; ?>
                                </td>
                                <td nowrap="nowrap">
                                    <li class='list-style-none'><?= h($speaker->name) ?></li>
                                    <?= $speaker->facebook ? "<li> {$speaker->facebook}</li>" : ''; ?>
                                    <?= $speaker->instagram ? "<li><i class=\"fa fa-instagram\"></i> {$speaker->instagram}</li>" : ''; ?>
                                    <?= $speaker->twitter ? "<li><i class=\"fa fa-twitter\"></i> {$speaker->twitter}</li>" : ''; ?>
                                    <?= $speaker->whatsapp ? "<li><i class=\"fa fa-whatsapp\"></i> {$speaker->whatsapp}</li>" : ''; ?>
                                </td>
                                <td><?= h($speaker->work_schedule) ?></td>
                                <td><?= $this->Utility->__FormatStatus($speaker->status) ?></td>
                                <td nowrap="nowrap">
                                    <li class="list-style-none"><b>Criado em: </b><?= h($speaker->created->format('d/m/Y')) ?></li>
                                    <li class="list-style-none"><b>Modificado em:</b> <?= h($speaker->modified->format('d/m/Y')) ?></li>
                                </td>
                                <td class="actions" nowrap="nowrap">
                                    <?= $this->Html->link('<i class="fa fa-eye" aria-hidden="true"></i>', ['action' => 'view', $speaker->id],['class'=>'btn btn-info btn-xs text-center','escape'=>false]) ?>
                                    <?= $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i>', ['action' => 'edit', $speaker->id],['class'=>'btn btn-primary btn-xs text-center','escape'=>false]) ?>
                                    <?= $this->Html->link('<i class="fa fa-trash" aria-hidden="true"></i>', ['action' => 'delete', $speaker->id],['class'=>'btn btn-danger btn-xs text-center','escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $speaker->id)]) ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php echo $this->Form->end(); ?>
        </div>
        <div class="panel-footer">
            <?php echo $this->Element('Manager/pagination');?>
        </div>
    </div>
</div>
