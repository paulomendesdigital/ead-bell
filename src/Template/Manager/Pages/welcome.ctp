<?php 
/**
 * @copyright Copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 * Page Index
 *
*/
use Cake\Core\Configure;
?>
<div class="container-fluid container-fixed-lg padding-25 sm-padding-10">
	<div class="page-header">
	    <div class="page-title">
	        <h3>Boas Vindas!</h3>
	    </div>
	</div>
	<div class="jumbotron">
		<div class="col-md-12 boas-vindas">
			<h1>Olá <?php echo $this->request->getSession()->read('Auth.User.name');?> !</h1>
			<p>Você está em um ambiente administrativo desenvolvido por <?php echo Configure::read('Developer.Author');?>. </p>
		</div>
	</div>
</div>