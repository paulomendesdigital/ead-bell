<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AdvertCampaign $advertCampaign
 */
?>

<div class="container-fluid container-fixed-lg">
    <ol class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Advert Campaigns'), ['controller' => 'AdvertCampaigns', 'action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?= __('Statistics') ?></a></li>
    </ol>
</div>

<div class="container-fluid container-fixed-lg">

    <!-- BASIC INFORMATIONS -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title"><?php echo __('Basic information'); ?></div>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered">
                <tr>
                    <td>
                        <strong><?php echo __('Advertiser'); ?></strong>
                    </td>
                    <td>
                        <?= $advertCampaign->has('advertiser') ? $this->Html->link($advertCampaign->advertiser->name, ['controller' => 'Advertisers', 'action' => 'view', $advertCampaign->advertiser->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong><?php echo __('Company'); ?></strong>
                    </td>
                    <td>
                        <?= $advertCampaign->has('company') ? $this->Html->link($advertCampaign->company->name, ['controller' => 'Companies', 'action' => 'view', $advertCampaign->company->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <td><strong><?= __('Name') ?></strong></td>
                    <td><?= h($advertCampaign->name) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Id') ?></strong></td>
                    <td><?= $this->Number->format($advertCampaign->id) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Weight') ?></strong></td>
                    <td><?= $this->Number->format($advertCampaign->weight) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Max Views') ?></strong></td>
                    <td><?= $this->Number->format($advertCampaign->max_views) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Max Unique Views') ?></strong></td>
                    <td><?= $this->Number->format($advertCampaign->max_unique_views) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Max Clicks') ?></strong></td>
                    <td><?= $this->Number->format($advertCampaign->max_clicks) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Max Unique Clicks') ?></strong></td>
                    <td><?= $this->Number->format($advertCampaign->max_unique_clicks) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Unique Views Count') ?></strong></td>
                    <td><?= $this->Number->format($advertCampaign->unique_views_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Views Count') ?></strong></td>
                    <td><?= $this->Number->format($advertCampaign->views_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Unique Clicks Count') ?></strong></td>
                    <td><?= $this->Number->format($advertCampaign->unique_clicks_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Clicks Count') ?></strong></td>
                    <td><?= $this->Number->format($advertCampaign->clicks_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Status') ?></strong></td>
                    <td><?= $this->Utility->__FormatStatus($advertCampaign->status) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Start Date') ?></strong></td>
                    <td><?= h($advertCampaign->start_date) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Finish Date') ?></strong></td>
                    <td><?= h($advertCampaign->finish_date) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Created') ?></strong></td>
                    <td><?= h($advertCampaign->created) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Modified') ?></strong></td>
                    <td><?= h($advertCampaign->modified) ?></td>
                </tr>

            </table>

            <div class='visible-print' style='margin-top: 140px'></div>

            <div class="related">
                <h4><?= __('Statistics') ?></h4>
                <?php if (!empty($advertCampaign->advert_reports)) : ?>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <th><?= __('Date') ?></th>
                                <th><?= __('Unique Views Count') ?></th>
                                <th><?= __('Views Count') ?></th>
                                <th><?= __('Unique Clicks Count') ?></th>
                                <th><?= __('Clicks Count') ?></th>
                                <th>CTR Único</th>
                                <th>CTR</th>
                                <th>Status da Estatística</th>
                            </tr>
                            <?php foreach ($advertCampaign->advert_reports as $advertReport) : ?>
                                <tr>
                                    <td><?= h($advertReport->report_date->format('d/m/Y')) ?></td>
                                    <td><?= h($advertReport->unique_views_count) ?></td>
                                    <td><?= h($advertReport->views_count) ?></td>
                                    <td><?= h($advertReport->unique_clicks_count) ?></td>
                                    <td><?= h($advertReport->clicks_count) ?></td>
                                    <td><?= number_format($advertReport->unique_ctr, 3, ',', '.') ?>%</td>
                                    <td><?= number_format($advertReport->ctr, 3, ',', '.') ?>%</td>
                                    <td><?= $this->AdvertReports->formatStatus($advertReport->status) ?></td>
                                </tr>
                            <?php endforeach; ?>
                            <tr class="success">
                                <td>Total</td>
                                <td><?= $advertReportsTotal['unique_views_count'] ?></td>
                                <td><?= $advertReportsTotal['views_count'] ?></td>
                                <td><?= $advertReportsTotal['unique_clicks_count'] ?></td>
                                <td><?= $advertReportsTotal['clicks_count'] ?></td>
                                <td><?= number_format($advertReportsTotal['unique_ctr'], 3, ',', '.') ?>%</td>
                                <td colspan="2"><?= number_format($advertReportsTotal['ctr'], 3, ',', '.') ?>%</td>
                            </tr>
                        </table>
                    </div>
                <?php endif; ?>


            </div>
            <div class="row"></div>
            <hr />
            <div class='row hidden-print'>
                <div class="actions">
                    <?php echo $this->Html->link(__('Edit register'), array('action' => 'edit', $this->request->getAttribute('params')['pass'][0]), array('class' => 'btn btn-info')); ?>
                    <?php echo $this->Html->link('Imprimir', 'javascript:window.print()', array('class' => 'btn btn-primary')); ?>
                </div>
            </div>
        </div>
    </div>

</div>
