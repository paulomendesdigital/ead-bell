<?php

/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AdvertCampaign[]|\Cake\Collection\CollectionInterface $advertCampaigns
 */
?>

<?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' => 'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page"><?php echo __('Advert Campaigns'); ?></li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">
    <fieldset class="box-search">
        <?php echo $this->Form->create(null,['type'=>'get']);?>
            <div class="col-md-3 col-xs-2">
                <?= $this->Form->control('AdvertCampaigns.id', ['class' => 'form-control']) ?>
            </div>
            <div class="col-md-3 col-xs-5">
                <?= $this->Form->control('AdvertCampaigns.advertiser_id', ['class' => 'form-control', 'empty' => 'Selecione']) ?>
            </div>
            <div class="col-md-3 col-xs-5">
                <?= $this->Form->control('AdvertCampaigns.company_id', ['class' => 'form-control', 'empty' => 'Selecione']) ?>
            </div>
            <div class="col-md-3 col-xs-12">
                <?= $this->Form->control('AdvertCampaigns.name LIKE', ['class' => 'form-control', 'label' => __('Name')]) ?>
            </div>
            <div class="col-md-3 col-xs-6">
                <?= $this->Form->control('AdvertCampaigns.start_date', ['class' => 'form-control', 'data-mask-input' => 'date', 'label' => 'Data de Início']) ?>
            </div>
            <div class="col-md-3 col-xs-6">
                <?= $this->Form->control('AdvertCampaigns.finish_date', ['class' => 'form-control', 'data-mask-input' => 'date', 'label' => 'Data de Término']) ?>
            </div>
            <div class="col-md-3 col-xs-6">
                <?= $this->Form->control('AdvertCampaigns.status', ['class' => 'form-control', 'options' => [0 => 'Inativo', 1 => 'Ativo'], 'empty' => 'Selecione']) ?>
            </div>
            
            <div class="col-sm-2 col-xs-6">
                <?= $this->Form->button(__('buscar'),['class'=>'btn btn-primary']) ?>
            </div>
            <div class="row clearfix mt-2 mb-2"></div>
        <?php echo $this->Form->end();?>
    </fieldset>
</div>

<div class="container-fluid container-fixed-lg">
    <div class="panel panel-default">
        <div class="table-responsive">
            <?php echo $this->Form->create($this->request->getParam('controller'), ['url' => ['action' => 'delete']]); ?>
                <table cellpadding="0" cellspacing="0" class="table table-striped table-hover  order">
                    <thead>
                        <tr>
                            <th scope="col" class="hidden">
                                <button class="btn btn-link" type='submit'><i class="pg-trash"></i></button>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('id', __('Id')) ?>
                                <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'id') { ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('advertiser_id', __('Advertiser')) ?>
                                <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'advertiser_id') { ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('company_id', __('Company')) ?>
                                <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'company_id') { ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('name', __('Name')) ?>
                                <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'name') { ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('start_date', __('Início')) ?>
                                <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'start_date') { ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('finish_date', __('Fim')) ?>
                                <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'finish_date') { ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('weight', __('Weight')) ?>
                                <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'weight') { ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col" nowrap="nowrap">
                                <?= $this->Paginator->sort('clicks_count', __('Qtde Cliques')) ?>
                                <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'clicks_count') { ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col" nowrap="nowrap">
                                <?= $this->Paginator->sort('views_count', __('Qtde Views')) ?>
                                <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'views_count') { ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('status', __('Status')) ?>
                                <?php if ($this->request->getQuery() && $this->request->getQuery('sort') == 'status') { ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down'; ?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?php echo __('Actions'); ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($advertCampaigns as $advertCampaign) { ?>
                            <tr>
                                <td class="hidden">
                                    <input name="Advert Campaigns[][id]" value="<? echo $advertCampaign->id; ?>" id="advertCampaign-<?php echo $advertCampaign->id; ?>" type="checkbox" />
                                    <label for="page-<?php echo $advertCampaign->id; ?>" class="no-padding no-margin"></label>
                                </td>
                                <td><?= $this->Number->format($advertCampaign->id) ?></td>
                                <td><?= $advertCampaign->has('advertiser') ? $this->Html->link($advertCampaign->advertiser->name, ['controller' => 'Advertisers', 'action' => 'view', $advertCampaign->advertiser->id]) : '' ?></td>
                                <td><?= $advertCampaign->has('company') ? h($advertCampaign->company->name) : '' ?></td>
                                <td><?= $this->Html->link($advertCampaign->name, ['controller' => 'AdvertCampaigns', 'action' => 'view', $advertCampaign->id]) ?></td>
                                <td nowrap="nowrap"><?= h($advertCampaign->start_date->format('d/m/Y H:i')) ?></td>
                                <td nowrap="nowrap"><?= h($advertCampaign->finish_date->format('d/m/Y H:i')) ?></td>
                                <td><?= $this->Number->format($advertCampaign->weight) ?></td>
                                <td><?= $this->Number->format($advertCampaign->clicks_count) ?></td>
                                <td><?= $this->Number->format($advertCampaign->views_count) ?></td>
                                <td><?= $this->Utility->__FormatStatus($advertCampaign->status) ?></td>
                                <td class="actions" nowrap="nowrap">
                                    <?= $this->Html->link('<button type="button" class="btn btn-info"><i class="fa-eye fa"></i></button>', ['action' => 'view', $advertCampaign->id], ['escape' => false]) ?>
                                    <?= $this->Html->link('<button type="button" class="btn btn-warning"><i class="fa-signal fa"></i></button>', ['action' => 'advertStatistics', $advertCampaign->id], ['escape' => false]) ?>
                                    <?= $this->Html->link('<button type="button" class="btn btn-primary"><i class="fa-pencil fa"></i></button>', ['action' => 'edit', $advertCampaign->id], ['escape' => false]) ?>
                                    <?= $this->Form->postLink('<button type="button" class="btn btn-danger"><i class="fa-trash fa"></i></button>', ['action' => 'delete', $advertCampaign->id], ['confirm' => __('Are you sure you want to delete # {0}?', $advertCampaign->id), 'escape' => false]) ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php echo $this->Form->end(); ?>
        </div>
        <div class="panel-footer">
            <?php echo $this->Element('Manager/pagination');?>
        </div>
    </div>
</div>