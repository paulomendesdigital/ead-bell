<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AdvertCampaign $advertCampaign
 */
?>
<div class="container-fluid container-fixed-lg">
    <ol class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Advert Campaigns'), ['controller' => 'AdvertCampaigns', 'action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Edit') . ' ' . __('Advert Campaigns'); ?></a></li>
    </ol>
</div>

<div class="container-fluid container-fixed-lg">

    <div class="row">
        <div class="col-lg-7 col-md-6 ">
            <div class="panel panel-transparent">
                <div class="panel-body">
                    <?php //@TODO In order to always have file upload support when model has a field named image or photo
                    ?>
                    <?php
                    echo $this->Form->create($advertCampaign, ['type' => 'file', 'class' => '']);
                    echo $this->Element('Manager/adverts/advert-campaigns');
                    echo $this->Form->end();
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>
