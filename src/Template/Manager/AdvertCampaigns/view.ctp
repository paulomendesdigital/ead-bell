<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AdvertCampaign $advertCampaign
 */
?>

<div class="container-fluid container-fixed-lg">
    <ol class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Advert Campaigns'), ['controller' => 'AdvertCampaigns', 'action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('View') . ' ' . __('Advert Campaigns'); ?></a></li>
    </ol>
</div>

<div class="container-fluid container-fixed-lg">

    <!-- BASIC INFORMATIONS -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title"><?php echo __('Basic information'); ?></div>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered">
                <tr>
                    <td>
                        <strong><?php echo __('Advertiser'); ?></strong>
                    </td>
                    <td>
                        <?= $advertCampaign->has('advertiser') ? $this->Html->link($advertCampaign->advertiser->name, ['controller' => 'Advertisers', 'action' => 'view', $advertCampaign->advertiser->id]) : '' ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong><?php echo __('Company'); ?></strong>
                    </td>
                    <td>
                        <?= $advertCampaign->has('company') ? h($advertCampaign->company->name): '' ?>
                    </td>
                </tr>
                <tr>
                    <td><strong><?= __('Name') ?></strong></td>
                    <td><?= h($advertCampaign->name) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Id') ?></strong></td>
                    <td><?= $this->Number->format($advertCampaign->id) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Weight') ?></strong></td>
                    <td><?= $this->Number->format($advertCampaign->weight) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Max Views') ?></strong></td>
                    <td><?= $this->Number->format($advertCampaign->max_views) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Max Unique Views') ?></strong></td>
                    <td><?= $this->Number->format($advertCampaign->max_unique_views) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Max Clicks') ?></strong></td>
                    <td><?= $this->Number->format($advertCampaign->max_clicks) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Max Unique Clicks') ?></strong></td>
                    <td><?= $this->Number->format($advertCampaign->max_unique_clicks) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Unique Views Count') ?></strong></td>
                    <td><?= $this->Number->format($advertCampaign->unique_views_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Views Count') ?></strong></td>
                    <td><?= $this->Number->format($advertCampaign->views_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Unique Clicks Count') ?></strong></td>
                    <td><?= $this->Number->format($advertCampaign->unique_clicks_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Clicks Count') ?></strong></td>
                    <td><?= $this->Number->format($advertCampaign->clicks_count) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Status') ?></strong></td>
                    <td><?= $this->Utility->__FormatStatus($advertCampaign->status) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Start Date') ?></strong></td>
                    <td><?= h($advertCampaign->start_date) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Finish Date') ?></strong></td>
                    <td><?= h($advertCampaign->finish_date) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Created') ?></strong></td>
                    <td><?= h($advertCampaign->created) ?></td>
                </tr>
                <tr>
                    <td><strong><?= __('Modified') ?></strong></td>
                    <td><?= h($advertCampaign->modified) ?></td>
                </tr>

            </table>

            <div class='visible-print' style='margin-top: 120px'></div>

            <div class="related">
                <h4><?= __('Related Adverts') ?></h4>
                <?php if (!empty($advertCampaign->adverts)) : ?>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th><?= __('Id') ?></th>
                                <th><?= __('Name') ?></th>
                                <th><?= __('Url') ?></th>
                                <th><?= __('Unique Views Count') ?></th>
                                <th><?= __('Views Count') ?></th>
                                <th><?= __('Unique Clicks Count') ?></th>
                                <th><?= __('Clicks Count') ?></th>
                                <th><?= __('Status') ?></th>
                                <th class='hidden-print'><?= __('Created') ?></th>
                                <th class='hidden-print'><?= __('Modified') ?></th>
                                <th class="actions hidden-print"><?= __('Actions') ?></th>
                            </tr>
                            <?php foreach ($advertCampaign->adverts as $adverts) : ?>
                                <tr>
                                    <td><?= h($adverts->id) ?></td>
                                    <td><?= h($adverts->name) ?></td>
                                    <td><?= h($adverts->url) ?></td>
                                    <td><?= h($adverts->unique_views_count) ?></td>
                                    <td><?= h($adverts->views_count) ?></td>
                                    <td><?= h($adverts->unique_clicks_count) ?></td>
                                    <td><?= h($adverts->clicks_count) ?></td>
                                    <td><?= $this->Utility->__FormatStatus($adverts->status) ?></td>
                                    <td class='hidden-print'><?= h($adverts->created) ?></td>
                                    <td class='hidden-print'><?= h($adverts->modified) ?></td>
                                    <td class="actions hidden-print">
                                        <?= $this->Html->link(__('View'), ['controller' => 'Adverts', 'action' => 'view', $adverts->id]) ?>
                                        <?= $this->Html->link(__('Edit'), ['controller' => 'Adverts', 'action' => 'edit', $adverts->id]) ?>
                                        <?= $this->Form->postLink(__('Delete'), ['controller' => 'Adverts', 'action' => 'delete', $adverts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $adverts->id)]) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                <?php endif; ?>
            </div>
            <div class="row"></div>
            <hr />
            <div class='row hidden-print'>
                <div class="actions">
                    <?php echo $this->Html->link(__('Edit register'), array('action' => 'edit', $this->request->getAttribute('params')['pass'][0]), array('class' => 'btn btn-info')); ?>
                    <?php echo $this->Html->link('Imprimir', 'javascript:window.print()', array('class' => 'btn btn-primary')); ?>
                </div>
            </div>
        </div>
    </div>

</div>
