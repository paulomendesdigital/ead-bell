<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<?php if( !isset($clientGroupId) ):?>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb">
            <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
            <li><?php echo $this->Html->link(__('Usuários'), ['action' => 'index'], ['class' => '']); ?></li>
            <li><a class="active"><?php echo __('Edit').' '.__('Usuário'); ?></a></li>
        </ul>
    </div>
    <nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
        <ul class="side-nav">
            <li><?= $this->Html->link('<i class="fa fa-list"></i> '.__('Listar Usuários'), ['controller' => 'users', 'action' => 'index'],['class'=>'btn btn-small btn-info','escape'=>false]) ?></li>
            <li><?= $this->Form->postLink(
                    '<i class="fa fa-trash"></i> '.__('Delete'),
                    ['action' => 'delete', $user->id],
                    [
                        'class'=>'btn btn-small btn-danger','escape'=>false,
                        'confirm' => __('Are you sure you want to delete # {0}?', $user->id)
                    ]
                )
            ?></li>
        </ul>
    </nav>
<?php else:?>
    <div class="container-fluid container-fixed-lg">
        <ul class="breadcrumb">
            <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
            <li><?php echo $this->Html->link(__('Ouvintes'), ['action' => 'listeners'], ['class' => '']); ?></li>
            <li><a class="active"><?php echo __('Edit').' '.__('Ouvinte'); ?></a></li>
        </ul>
    </div>
    <nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
        <ul class="side-nav">
            <li><?= $this->Html->link('<i class="fa fa-list"></i> '.__('Listar Ouvintes'), ['controller' => 'users', 'action' => 'listeners'],['class'=>'btn btn-small btn-info','escape'=>false]) ?></li>
            <li><?= $this->Form->postLink(
                    '<i class="fa fa-trash"></i> '.__('Delete'),
                    ['action' => 'delete', $user->id],
                    [
                        'class'=>'btn btn-small btn-danger','escape'=>false,
                        'confirm' => __('Are you sure you want to delete # {0}?', $user->id)
                    ]
                )
            ?></li>
        </ul>
    </nav>
<?php endif;?>

<div class="container-fluid container-fixed-lg">
    <div class="users form large-9 medium-8 columns content">
        <?= $this->Form->create($user) ?>
        <fieldset>
            <legend>Editar <?= $title ?></legend>
            <div class="col-md-6 col-xs-12">
                <div class="row clearfix mt-2">
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('id');?>
                        <div class="hidden">
                            <?php echo $this->Form->control('group_id', ['options' => $groups, 'type'=>'select','class' => 'form-control', 'label' => __('Grupo')]);?>
                        </div>
                        <?php if( isset($clientGroupId) ):?>
                            <?php echo $this->Form->control('company_id', ['options' => $companies, 'type'=>'select','class' => 'form-control', 'label' => __('Empresa')]);?>
                        <?php endif;?>
                    </div>
                    <div class="col-sm-10 col-xs-12">
                        <?php echo $this->Form->control('name', array('class' => 'form-control', 'label' => __('name')));?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="col-sm-6 col-xs-12">
                        <?php echo $this->Form->control('email', array('class' => 'form-control', 'label' => __('Email')));?>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <?php echo $this->Form->control('schooling', array('options' => $schoolings, 'empty'=>'Selecione', 'class' => 'form-control', 'label' => __('Escolaridade')));?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="col-sm-6 col-xs-12">
                        <?php echo $this->Form->control('cpf', array('data-mask-input'=>'cpf','class' => 'form-control data-input-mask-cpf', 'label' => __('Cpf')));?>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <?php echo $this->Form->control('rg', array('class' => 'form-control', 'label' => __('RG')));?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="col-sm-6 col-xs-12">
                        <?php echo $this->Form->control('phone', array('data-mask-input'=>'cellphone','class' => 'form-control', 'label' => __('Telefone')));?>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <?php echo $this->Form->control('birth', array('value'=>$birth,'data-mask-input'=>'date','type'=>'text','class' => 'form-control', 'label' => __('Data Nascimento')));?>
                    </div>
                </div>
                
                <div class="row clearfix mt-2">
                    <div class="col-md-3 form-group" data-hide="modal">
                        <label class="col-sm-12" class="">Status</label>
                        <div class="col-sm-12"><?php echo $this->Form->control('status', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false, 'checked' => $user->status ? 'checked' : '')); ?></div>
                    </div>
                    <div class="col-md-3 form-group" data-hide="modal">
                        <label class="col-sm-12" class="">Receber Newsletter?</label>
                        <div class="col-sm-12"><?php echo $this->Form->control('news', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false, 'checked' => $user->news ? 'checked' : '')); ?></div>
                    </div>
                </div>
                <div class="row clearfix mt-2 mb-2">
                    <div class="col-sm-12 col-xs-12">
                        <?= $this->Form->button(__('Edit'),['class'=>'btn btn-primary']) ?>
                        <?= $this->Form->button(__('Edit and continue'),['name'=>'refer','class'=>'btn btn-info']) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">

                <div class="row clearfix mt-2">
                    <div class="col-sm-6 col-xs-12">
                        <?php echo $this->Form->control('cep', array('data-mask-input'=>'zipcode','class' => 'form-control', 'label' => __('Cep')));?>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <?php echo $this->Form->control('address', array('data-toggle'=>'returnAddress','type'=>'text','class' => 'form-control', 'label' => __('Rua')));?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="col-sm-3 col-xs-12">
                        <?php echo $this->Form->control('number', array('class' => 'form-control', 'label' => __('Número')));?>
                    </div>
                    <div class="col-sm-9 col-xs-12">
                        <?php echo $this->Form->control('complement', array('type'=>'text','class' => 'form-control', 'label' => __('Complemento')));?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="col-sm-4 col-xs-12">
                        <?php echo $this->Form->control('neighborhood', array('data-toggle'=>'returnNeighborhood','class' => 'form-control', 'label' => __('Bairro')));?>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <?php echo $this->Form->control('city', array('data-toggle'=>'returnCity','class' => 'form-control', 'label' => __('Cidade')));?>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <?php echo $this->Form->control('state', array('data-toggle'=>'returnState','class' => 'form-control', 'label' => __('Estado')));?>
                    </div>
                </div>

                <div class="row clearfix mt-2">
                    <div class="col-sm-12 col-xs-12">
                        <?php echo $this->Form->control('username', array('class' => 'form-control', 'label' => __('Login')));?>
                    </div>
                </div>
                
                <div class="row clearfix mt-2">
                    <div class="col-sm-12 col-xs-12">
                        <label>Senha</label>
                        <div class="form-group form-group-default input-group disabled" data-toggle="UserPassword">
                            <div class="col-sm-10">
                                <?php echo $this->Form->input('password', ['value'=>'','type'=>'password','class' => 'form-control', 'div' => false, 'label' => false, 'disabled' => 'disabled','style'=>'height: 40px']);?>
                            </div>
                            <span style="height: 35px;" class="input-group-addon link" data-toggle="unlock" data-element="UserPassword"><i class="fa fa-lock"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
        <?= $this->Form->end() ?>
    </div>
</div>
