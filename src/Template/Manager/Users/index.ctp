<?php
/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<style>li{list-style: none;}</style>

<?php if( isset($clientGroupId) ):?>
    <?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add',$clientGroupId], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' =>'tooltip', 'data-placement' => 'left']); ?>
<?php else:?>
    <?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' =>'tooltip', 'data-placement' => 'left']); ?>
<?php endif;?>

<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page"><?php echo $title;?></li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">
    <fieldset class="box-search">
        <?php echo $this->Form->create(null,['type'=>'get']);?>
        <?php if( isset($companies) ):?>
            <div class="col-sm-2 col-xs-12">
                <?php echo $this->Form->control('company_id', ['options'=>$companies,'empty'=>'Filtrar por Empresa','class' => 'form-control', 'label' => __('Empresa')]);?>
            </div>
        <?php endif;?>
        <div class="col-sm-3 col-xs-12">
            <?php echo $this->Form->control('name', ['class' => 'form-control', 'label' => __('Nome')]);?>
        </div>
        <div class="col-sm-3 col-xs-12">
            <?php echo $this->Form->control('email', ['type'=>'text','class' => 'form-control', 'label' => __('Email')]);?>
        </div>
        <div class="col-sm-2 col-xs-12">
            <?php echo $this->Form->control('cpf', ['data-mask-input'=>'cpf','class' => 'form-control', 'label' => __('Cpf')]);?>
        </div>
        <div class="col-sm-2 col-xs-12">
            <?= $this->Form->button(__('buscar'),['class'=>'btn btn-primary']) ?>
        </div>
        <div class="row clearfix mt-2 mb-2">
            
        </div>
        <?php echo $this->Form->end();?>
    </fieldset>
</div>

<div class="container-fluid container-fixed-lg">
    <div class="panel panel-default">
        <div class="table-responsive">
            <?php echo $this->Form->create($this->request->getParam('controller'), ['url' => ['action' => 'delete']]); ?>
                <table cellpadding="0" cellspacing="0" class="table table-striped table-hover  order">
                    <thead>
                    <tr>
                        <th scope="col">
                            <?= $this->Paginator->sort('company_id','Empresa') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'company_id'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('username','Login') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'username'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('name','Nome') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'name'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?= $this->Paginator->sort('address','Endereço') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'address'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        
                        <th scope="col">
                            <?= $this->Paginator->sort('created') ?>
                            <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'created'){ ?>
                                <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                            <?php } ?>
                        </th>
                        <th scope="col">
                            <?php echo __('Actions'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $user){ if($user->id == 1) continue;?>
                        <tr>
                            <td nowrap="nowrap">
                                <?= $user->company->name;?>
                            </td>
                            <td nowrap="nowrap">
                                <li><b>Id:</b> <?= $user->id;?></li>
                                <li><b>Grupo:</b> <?= $user->group->name;?></li>
                                <li><b>Login:</b> <?= $user->username;?></li>
                                <li><b>Status:</b> <?= $this->Utility->__FormatStatus($user->status) ?></li>
                            </td>
                            <td nowrap="nowrap">
                                <li><b>Nome:</b> <?= $user->name;?></li>
                                <li><b>Email:</b> <?= $user->email;?></li>
                                <li><b>Telefone:</b> <?= $user->phone;?></li>
                            </td>
                            <td>
                                <li><b>Rua:</b> <?= h($user->address) ?>, <?= h($user->number) ?> - <?= h($user->complement) ?></li>
                                <li><b>Bairro:</b> <?= h($user->neighborhood) ?></li>
                                <li><b>Cidade:</b> <?= h($user->city) ?></li>
                                <li><b>Estado:</b> <?= h($user->state) ?></li>
                                <li><b>Cep:</b> <?= h($user->cep) ?></li>
                            </td>
                            <td nowrap="nowrap">
                                <li><b>Criado em: </b><?= h($user->created->format('d/m/Y')) ?></li>
                                <li><b>Modificado em:</b> <?= h($user->modified->format('d/m/Y')) ?></li>
                            </td>
                            <td class="actions">
                                <?= $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i>', ['action' => 'edit', $user->id],['class'=>'btn btn-primary btn-xs text-center','escape'=>false]) ?>
                                <?php
                                if( $user->id <> 1 ){

                                echo $this->Html->link('<i class="fa fa-trash" aria-hidden="true"></i>', ['action' => 'delete', $user->id],['class'=>'btn btn-danger btn-xs text-center','escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $user->id)]);

                                //echo $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]);
                                }
                                ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php echo $this->Form->end(); ?>
        </div>
        <div class="panel-footer">
            <?php echo $this->Element('Manager/pagination');?>
        </div>
    </div>
</div>
