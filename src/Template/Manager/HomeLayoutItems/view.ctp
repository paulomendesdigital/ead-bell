<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\HomeLayoutItem $homeLayoutItem
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Home Layout Item'), ['action' => 'edit', $homeLayoutItem->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Home Layout Item'), ['action' => 'delete', $homeLayoutItem->id], ['confirm' => __('Are you sure you want to delete # {0}?', $homeLayoutItem->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Home Layout Items'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Home Layout Item'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Home Layouts'), ['controller' => 'HomeLayouts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Home Layout'), ['controller' => 'HomeLayouts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Contents'), ['controller' => 'Contents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Content'), ['controller' => 'Contents', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="homeLayoutItems view large-9 medium-8 columns content">
    <h3><?= h($homeLayoutItem->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Home Layout') ?></th>
            <td><?= $homeLayoutItem->has('home_layout') ? $this->Html->link($homeLayoutItem->home_layout->name, ['controller' => 'HomeLayouts', 'action' => 'view', $homeLayoutItem->home_layout->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Content') ?></th>
            <td><?= $homeLayoutItem->has('content') ? $this->Html->link($homeLayoutItem->content->name, ['controller' => 'Contents', 'action' => 'view', $homeLayoutItem->content->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($homeLayoutItem->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Size') ?></th>
            <td><?= $this->Number->format($homeLayoutItem->size) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Row') ?></th>
            <td><?= $this->Number->format($homeLayoutItem->row) ?></td>
        </tr>
    </table>
</div>
