<?php
/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\HomeLayoutItem[]|\Cake\Collection\CollectionInterface $homeLayoutItems
 */
?>


<?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' =>'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page">Home Layout Items</li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">

    <div class="panel panel-default">

        <div class="panel-body">

            <?php echo $this->Form->create($this->request->getParam('controller'), ['url' => ['action' => 'delete']]); ?>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">
                                <button class="btn btn-link" type='submit'><i class="pg-trash"></i></button>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('id') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'id'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('home_layout_id') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'home_layout_id'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('content_id') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'content_id'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('size') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'size'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('row') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'row'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?php echo __('Actions'); ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($homeLayoutItems as $homeLayoutItem){ ?>
                            <tr>
                                <td>
                                    <input name="data[Home Layout Items][][Home Layout Items][id]" value="<? echo $homeLayoutItem->id; ?>" id="homeLayoutItem-<?php echo $homeLayoutItem->id; ?>" type="checkbox"/>
                                    <label for="page-<?php echo $homeLayoutItem->id;?>" class="no-padding no-margin"></label>
                                </td>                                                                                                 <td class="actions">
                                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $homeLayoutItem->id]) ?>
                                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $homeLayoutItem->id]) ?>
                                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $homeLayoutItem->id], ['confirm' => __('Are you sure you want to delete # {0}?', $homeLayoutItem->id)]) ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php echo $this->Form->end(); ?>   
        </div>
        <div class="panel-footer">
            <?php echo $this->Element('Manager/pagination');?>
        </div>
    </div>
</div>
