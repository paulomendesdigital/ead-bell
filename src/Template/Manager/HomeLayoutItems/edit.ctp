<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\HomeLayoutItem $homeLayoutItem
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $homeLayoutItem->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $homeLayoutItem->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Home Layout Items'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Home Layouts'), ['controller' => 'HomeLayouts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Home Layout'), ['controller' => 'HomeLayouts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Contents'), ['controller' => 'Contents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Content'), ['controller' => 'Contents', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="homeLayoutItems form large-9 medium-8 columns content">
    <?= $this->Form->create($homeLayoutItem) ?>
    <fieldset>
        <legend><?= __('Edit Home Layout Item') ?></legend>
        <?php
            echo $this->Form->control('home_layout_id', ['options' => $homeLayouts]);
            echo $this->Form->control('content_id', ['options' => $contents]);
            echo $this->Form->control('size');
            echo $this->Form->control('row');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
