<?php
/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Content[]|\Cake\Collection\CollectionInterface $contents
 */
?>
<input type="hidden" name="local" id="local" value="index_content">

<?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' =>'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page">Publicações</li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">
    <fieldset class="box-search">
        <?php echo $this->Form->create(null,['type'=>'get']);?>
        <?php if( isset($companies) ):?>
            <div class="col-sm-2 col-xs-12">
                <?php echo $this->Form->control('company_id', ['data-toggle'=>'change_company_id', 'options'=>$companies,'empty'=>'Filtrar por Empresa','class' => 'form-control', 'label' => __('Empresa')]);?>
            </div>
        <?php endif;?>
        <?php if( isset($contentCategories) ):?>
            <div class="col-sm-2 col-xs-12">
                <?php echo $this->Form->control('content_category_id', ['data-toggle'=>'content-category-id', 'options'=>$contentCategories,'empty'=>'Filtrar por Categoria','class' => 'form-control', 'label' => __('Categorias')]);?>
            </div>
        <?php endif;?>

        <?php if( isset($destinations) ):?>
            <div class="col-sm-3 col-xs-12">
                <?php echo $this->Form->control('destination', ['data-toggle'=>'destination', 'options'=>$destinations,'empty'=>'Filtrar por Conteúdos exclusivos','class' => 'form-control', 'label' => __('Conteúdos Exclusivos')]);?>
            </div>
        <?php endif;?>
        
        <div class="col-sm-3 col-xs-12">
            <?php echo $this->Form->control('name', ['class' => 'form-control', 'label' => __('Nome')]);?>
        </div>
        
        <div class="col-sm-2 col-xs-12">
            <?= $this->Form->button(__('buscar'),['class'=>'btn btn-primary']) ?>
        </div>
        <div class="row clearfix mt-2 mb-2"></div>
        <?php echo $this->Form->end();?>
    </fieldset>
</div>

<div class="container-fluid container-fixed-lg">
    <div class="panel panel-default">
        <div class="table-responsive">
            <?php echo $this->Form->create($this->request->getParam('controller'), ['url' => ['action' => 'delete']]); ?>
                <table cellpadding="0" cellspacing="0" class="table table-striped table-hover  order">
                    <thead>
                        <tr>
                            <th scope="col" class="hidden">
                                <button class="btn btn-link" type='submit'><i class="pg-trash"></i></button>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('id') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'id'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col" class="">
                                <?= $this->Paginator->sort('company_id') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'company_id'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('content_category_id','Categorias') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'content_category_id'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('name') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'name'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col" class='hidden'>
                                <?= $this->Paginator->sort('button_label','Texto do botão') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'button_label'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('start','Publicação') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'start'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('image_small_name','Imagens') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'image_small_name'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('status') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'status'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?= $this->Paginator->sort('created') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'created'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col" class="hidden">
                                <?= $this->Paginator->sort('modified') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'modified'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                            <th scope="col">
                                <?php echo __('Actions'); ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($contents as $content){ ?>
                            <tr>
                                <td class="hidden">
                                    <input name="data[Contents][][Contents][id]" value="<? echo $content->id; ?>" id="content-<?php echo $content->id; ?>" type="checkbox"/>
                                    <label for="page-<?php echo $content->id;?>" class="no-padding no-margin"></label>
                                </td>
                                <td><?= $this->Number->format($content->id) ?></td>
                                <td class=""><?= $content->company->name;?></td>
                                <td><?= $content->has('content_category') ? $this->Html->link($content->content_category->name, ['controller' => 'ContentCategories', 'action' => 'view', $content->content_category->id]) : '' ?></td>
                                <td>
                                    <li class="list-style-none"><b>Nome: </b><?= h($content->name ? $content->name : '--') ?></li>
                                    <li style="list-style: none;"><hr style="margin-top: 5px; margin-bottom: 5px;" /></li>
                                    <li class="list-style-none"><b>Título: </b><?= h($content->title ? $content->title : '--') ?></li>
                                    <li style="list-style: none;"><hr style="margin-top: 5px; margin-bottom: 5px;" /></li>
                                    <li class="list-style-none"><b>Subtitulo: </b><?= h($content->subtitle ? $content->subtitle : '--') ?></li>
                                    <li style="list-style: none;"><hr style="margin-top: 5px; margin-bottom: 5px;" /></li>
                                    <li class="list-style-none">
                                        <b>Link:</b> <?= $this->Utility->__GetLinkPromotion($content); ?> 
                                        <a class='btn btn-warning btn-xs text-center' href="<?= $this->Utility->__GetLinkPromotion($content) ?>"target="_blank">
                                            <i class="fa fa-link" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </td>
                                <td class='hidden'><?= h($content->button_label) ?></td>
                                <td nowrap="nowrap">
                                    <li class="list-style-none"><b>De: </b><?= $content->start ? $content->start->format('d/m/Y') : '--' ?></li>
                                    <li class="list-style-none"><b>Até: </b><?= $content->finish ? $content->finish->format('d/m/Y') : '--' ?></li>
                                    <?php if( $content->finish_promotion ):?>
                                        <li class="list-style-none">Válida até: <?php echo $content->finish_promotion->format('d/m/Y');?></li>
                                    <?php endif;?>
                                </td>
                                <td nowrap="nowrap">
                                    <li style="list-style: none;"><?= $content->image_medium_name ? "Destaque Home Quadrado - <i class='text-success fa fa-check'></i>" : "Imagem média - <i class='text-danger fa fa-ban'></i>"; ?></li>
                                    <li style="list-style: none;"><?= $content->image_large_name ? "Destaque Home Retangular - <i class='text-success fa fa-check'></i>" : "Imagem grande - <i class='text-danger fa fa-ban'></i>"; ?></li>
                                    <li style="list-style: none;"><?= $content->image_internal ? "Página Interna - <i class='text-success fa fa-check'></i>" : "Imagem Interna - <i class='text-danger fa fa-ban'></i>"; ?></li>
                                </td>
                                <td><?php echo $this->Utility->__FormatStatus( $this->Number->format($content->status) ); ?></td>
                                <td><?= h($content->created->format('d/m/Y H:i')) ?></td>
                                <td class="hidden"><?= h($content->modified->format('d/m/Y')) ?></td>
                                <td class="actions" nowrap="nowrap">
                                    <?php if( isset($content->participants) and !empty($content->participants) ):?>
                                        <?= $this->Html->link('<i class="fa fa-users" aria-hidden="true"></i>', ['controller'=>'participants','action' => 'index', $content->id],['class'=>'btn btn-success btn-xs text-center','title'=>'Participantes','escape'=>false]) ?>
                                    <?php endif;?>
                                    <?= $this->Html->link('<i class="fa fa-eye" aria-hidden="true"></i>', ['action' => 'view', $content->id],['class'=>'btn btn-info btn-xs text-center','escape'=>false]) ?>
                                    <?= $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i>', ['action' => 'edit', $content->id],['class'=>'btn btn-primary btn-xs text-center','escape'=>false]) ?>
                                    <?= $this->Html->link('<i class="fa fa-trash" aria-hidden="true"></i>', ['action' => 'delete', $content->id],['class'=>'btn btn-danger btn-xs text-center','escape'=>false, 'confirm' => __('Are you sure you want to delete # {0}?', $content->id)]); ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php echo $this->Form->end(); ?>
        </div>
        <div class="panel-footer">
            <?php echo $this->Element('Manager/pagination');?>
        </div>
    </div>
</div>

