<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Content $content
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Conteúdos'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Ver') . ' ' . __('Conteúdo'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link('<i class="fa fa-edit"></i> ' . __('Edit'), ['action' => 'edit', $content->id], ['class' => 'btn btn-small btn-warning', 'escape' => false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-copy"></i> ' . 'Clonar', ['action' => 'clone', $content->id], ['class' => 'btn btn-small btn-primary', 'escape' => false]) ?></li>
        <li><?= $this->Form->postLink('<i class="fa fa-trash"></i> ' . __('Delete'), ['action' => 'delete', $content->id], ['class' => 'btn btn-small btn-danger', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $content->id)]) ?> </li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="contents view large-9 medium-8 columns content">
        <h3><?= h($content->name) ?></h3>
        <table class="vertical-table">
            <tr>
                <th scope="row"><?= __('Content Category') ?></th>
                <td> <?= $content->has('content_category') ? $this->Html->link($content->content_category->name, ['controller' => 'ContentCategories', 'action' => 'view', $content->content_category->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($content->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('title') ?></th>
                <td><?= h($content->title) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('subtitle') ?></th>
                <td><?= h($content->subtitle) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('button Label') ?></th>
                <td><?= h($content->button_label) ?></td>
            </tr>
            <tr class="hidden">
                <th scope="row"><?= __('Imagem') ?></th>
                <td><?= h($content->image_small_name) ?></td>
            </tr>
            <tr class="hidden">
                <th scope="row"><?= __('Image Small Path') ?></th>
                <td><?= h($content->image_small_path) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Imagem Média') ?></th>
                <td><?= h($content->image_medium_name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Caminho Imagem Média') ?></th>
                <td><?= h($content->image_medium_path) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Imagem Grande') ?></th>
                <td><?= h($content->image_large_name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Caminho Imagem Grande') ?></th>
                <td><?= h($content->image_large_path) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($content->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Company Id') ?></th>
                <td><?= $this->Number->format($content->company_id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Status') ?></th>
                <td><?= $this->Number->format($content->status) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($content->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($content->modified) ?></td>
            </tr>
        </table>
        <div class="row">
            <div class="col-md-12">
                <h4><?= __('Description') ?></h4>
                <?= $content->description; ?>
            </div>
        </div>
    </div>
</div>