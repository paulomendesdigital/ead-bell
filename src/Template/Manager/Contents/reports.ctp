<?php
/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Content[]|\Cake\Collection\CollectionInterface $contents
 */
?>

<?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add_promotion'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' =>'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page">Relatório de Promoções</li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">
    <fieldset class="box-search">
        <?php echo $this->Form->create(null,['type'=>'get']);?>
        <?php if( isset($companies) ):?>
            <div class="col-sm-2 col-xs-12">
                <?php echo $this->Form->control('company_id', ['options'=>$companies,'empty'=>'Filtrar por Empresa','class' => 'form-control', 'label' => __('Empresa')]);?>
            </div>
        <?php endif;?>
        <?php if( isset($promotion_types) ):?>
            <div class="col-sm-2 col-xs-12">
                <?php echo $this->Form->control('type_promotion', ['id'=>'search-type-promotion','options'=>$promotion_types,'empty'=>'Filtrar por Tipo','class' => 'form-control', 'label' => __('Tipos')]);?>
            </div>
        <?php endif;?>

        <?php if( isset($destinations) ):?>
            <div class="col-sm-3 col-xs-12">
                <?php echo $this->Form->control('destination', ['data-toggle'=>'destination', 'options'=>$destinations,'empty'=>'Filtrar por Conteúdos exclusivos','class' => 'form-control', 'label' => __('Conteúdos Exclusivos')]);?>
            </div>
        <?php endif;?>
        
        <div class="col-sm-3 col-xs-12">
            <?php echo $this->Form->control('name', ['class' => 'form-control', 'label' => __('Nome')]);?>
        </div>
        
        <div class="col-sm-2 col-xs-12">
            <?= $this->Form->button(__('buscar'),['class'=>'btn btn-primary']) ?>
        </div>
        <div class="row clearfix mt-2 mb-2">
            
        </div>
        <?php echo $this->Form->end();?>
    </fieldset>
</div>

<div class="container-fluid container-fixed-lg">
    <div class="panel panel-default">
        <div class="table-responsive">
            <?php echo $this->Form->create($this->request->getParam('controller'), ['url' => ['action' => 'delete']]); ?>
             
            <?php echo $this->Form->end(); ?>
            <div id="app">
                <promotions-reports 
                prop-promotions='<?= base64_encode(json_encode($promotions)) ?>' 
                prop-companies='<?= htmlspecialchars(json_encode($companies)) ?>' 
                >
                    Carregando...
                </promotions-reports>
            </div>
        </div>
        <div class="panel-footer">
            <?php echo $this->Element('Manager/pagination');?>
        </div>
    </div>
</div>

<script src="/js/vue/dist/promotions_reports.js"></script>