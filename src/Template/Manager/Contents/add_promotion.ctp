<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Content $content
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Promoções'), ['action' => 'promotions'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Add') . ' ' . __('Promoção'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link('<i class="fa fa-list"></i> ' . __('Listar Promoções'), ['action' => 'promotions'], ['class' => 'btn btn-small btn-info', 'escape' => false]) ?></li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="contents form large-9 medium-8 columns content">
        <?= $this->Form->create($content, ['type' => 'file', 'class' => 'form-promotion']); ?>
        <fieldset>
            <legend><?= __('Criar Promoção') ?></legend>

            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <div class="row clearfix mt-2">
                        <div class="col-sm-3 col-xs-12">
                            <?php //Tipos de promoção
                            ?>
                            <?php echo $this->Form->control('local', ['type' => 'hidden', 'value' => 'add_content']); ?>
                            <?php echo $this->Form->control('promocaoId', ['type' => 'hidden', 'value' => $promocaoId]); ?>
                            <?php echo $this->Form->control('podcastId', ['type' => 'hidden', 'value' => $podcastId]); ?>
                            <?php echo $this->Form->control('perguntaId', ['type' => 'hidden', 'value' => $perguntaId]); ?>
                            <?php echo $this->Form->control('quizId', ['type' => 'hidden', 'value' => $quizId]); ?>

                            <?php //Tipos de Pergunta
                            ?>
                            <?php echo $this->Form->control('questionTypeTextId', ['type' => 'hidden', 'value' => $questionTypeTextId]); ?>
                            <?php echo $this->Form->control('questionTypeRadioButtonId', ['type' => 'hidden', 'value' => $questionTypeRadioButtonId]); ?>
                            <?php echo $this->Form->control('questionTypeCheckboxId', ['type' => 'hidden', 'value' => $questionTypeCheckboxId]); ?>

                            <?php echo $this->Form->control('company_id', ['data-toggle' => 'change_company_id', 'class' => 'form-control', 'label' => __('company')]); ?>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            <?php echo $this->Form->control('content_category_id', array('required' => true, 'class' => 'form-control', 'label' => __('Categoria'))); ?>
                        </div>
                        <div class="col-sm-3 col-xs-12 program">
                            <?php echo $this->Form->control('program_id', array('data-toggle' => 'program-id', 'class' => 'form-control', 'empty' => 'Selecione', 'label' => __('Programas'))); ?>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            <?php echo $this->Form->control('destination', array('class' => 'form-control', 'empty' => 'Selecione', 'options' => $destinations, 'label' => __('Conteúdo exclusivo'), 'required' => true)); ?>
                        </div>
                    </div>
                    <div class="row clearfix mt-2">
                        <div class="col-sm-4 col-xs-12">
                            <?php echo $this->Form->control('name', ['class' => 'form-control input-lg', 'label' => __('Título Página')]); ?>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <?php echo $this->Form->control('title', array('class' => 'form-control input-lg', 'label' => __('Título Destaque'))); ?>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <?php echo $this->Form->control('subtitle', array('class' => 'form-control input-lg', 'label' => __('subtitle'))); ?>
                        </div>
                    </div>
                    <div class="row clearfix mt-2">
                        <div class="col-sm-12 col-xs-12">
                            <?php echo $this->Form->control('description', ['class' => 'form-control input-lg', 'id' => 'ckfinderDescriptionContent', 'label' => __('Descrição')]); ?>
                        </div>
                    </div>
                    <div class="row clearfix mt-2">
                        <div class="col-sm-12 col-xs-12">
                            <?php echo $this->Form->control('tags', ['type' => 'text', 'class' => 'form-control input-lg', 'label' => __('Tags - separadas por ;') . " (até {$limitTags} tags)"]); ?>
                        </div>
                    </div>
                    <div class="row clearfix mt-2 hidden">
                        <div class="col-sm-12 col-xs-12 audio">
                            <?php //echo $this->Form->control('audio', array('class' => 'form-control', 'label' => __('Código do Áudio (Podcast)')));
                            ?>
                        </div>
                    </div>
                    <div class="row clearfix mt-2 regulation">
                        <div class="col-sm-12 col-xs-12">
                            <?php echo $this->Form->control('regulation', ['class' => 'form-control input-lg', 'id' => 'ckfinderRegulation', 'label' => __('Regulamento da Promoção')]); ?>
                        </div>
                    </div>
                    <div class="row clearfix mt-2 instrutions">
                        <div class="col-sm-12 col-xs-12">
                            <?php echo $this->Form->control('instrutions', ['class' => 'form-control input-lg', 'id' => 'ckfinderInstrutionsWinners', 'label' => __('Instruções aos Ganhadores')]); ?>
                        </div>
                    </div>
                    <div class="row clearfix mt-2">
                        <div class="col-sm-4 col-xs-12 hidden">
                            <?php //echo $this->Form->control('image_small_name', array('class' => 'form-control  input-lg', 'type'=>'file', 'label' => "Imagem pequena ({$image_small_size})"));
                            ?>
                        </div>

                        <div class="col-sm-4 col-xs-12">
                            <?php echo $this->Form->control('image_medium_name', array('class' => 'form-control  input-lg', 'type' => 'file', 'label' => "Destaque Home Quadrado ({$image_medium_size})")); ?>
                        </div>

                        <div class="col-sm-4 col-xs-12">
                            <?php echo $this->Form->control('image_large_name', array('class' => 'form-control  input-lg', 'type' => 'file', 'label' => "Destaque Home Retangular ({$image_large_size})")); ?>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <?php echo $this->Form->control('image_internal', array('class' => 'form-control  input-lg', 'type' => 'file', 'label' => "Página Interna ({$image_internal_size})")); ?>
                        </div>
                    </div>
                    <div class="row clearfix mt-2">
                        <div class="form-group" data-hide="modal">
                            <label class="col-sm-12" class="">Status</label>
                            <div class="col-sm-12"><?php echo $this->Form->control('status', array('type' => 'checkbox', 'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false, 'checked' => 'checked')); ?></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-sm-12 col-xs-12">
                    <div class="row clearfix promotion padding-20 bg-master-light">
                        CONFIGURAÇÃO DA PROMOÇÃO
                    </div>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="col-sm-2 col-xs-12">
                    <?php $labelStart = "Exibir no site a partir de:"; ?>
                    <?php if ($content->start) : ?>
                        <?php echo $this->Form->control('start', ['value' => $content->start->format('d/m/Y H:i'), 'type' => 'text', 'class' => 'form-control input-lg', 'data-content' => "datetimepicker", 'label' => $labelStart]); ?>
                    <?php else : ?>
                        <?php echo $this->Form->control('start', ['value' => date('d/m/Y H:i'), 'type' => 'text', 'class' => 'form-control input-lg', 'data-content' => "datetimepicker", 'label' => $labelStart]); ?>
                    <?php endif; ?>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <?php $labelFinish = "Sair do site em:"; ?>
                    <?php if ($content->finish) : ?>
                        <?php echo $this->Form->control('finish', ['value' => $content->finish->format('d/m/Y H:i'), 'type' => 'text', 'class' => 'form-control input-lg', 'data-content' => "datetimepicker", 'label' => $labelFinish]); ?>
                    <?php else : ?>
                        <?php echo $this->Form->control('finish', ['type' => 'text', 'class' => 'form-control input-lg', 'data-content' => "datetimepicker", 'label' => $labelFinish]); ?>
                    <?php endif; ?>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <?php $labelFinishPromotion = "Participar da promoção até:"; ?>
                    <?php if ($content->finish_promotion) : ?>
                        <?php echo $this->Form->control('finish_promotion', ['value' => $content->finish_promotion->format('d/m/Y H:i'), 'type' => 'text', 'class' => 'form-control input-lg', 'data-content' => "datetimepicker", 'label' => $labelFinishPromotion]); ?>
                    <?php else : ?>
                        <?php echo $this->Form->control('finish_promotion', ['type' => 'text', 'class' => 'form-control input-lg', 'data-content' => "datetimepicker", 'label' => $labelFinishPromotion]); ?>
                    <?php endif; ?>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <?php echo $this->Form->control('limitwinners', array('class' => 'form-control input-lg', 'required' => true, 'label' => __('Limite de Ganhadores'))); ?>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <?php echo $this->Form->control('type_promotion', array('class' => 'form-control input-lg', 'empty' => 'Selecione', 'options' => $promotion_types, 'label' => __('Tipo de Promoção'))); ?>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <div class="form-group" data-hide="modal">
                        <label class="col-sm-12" class="">Habilitar Sorteio Automático</label>
                        <div class="col-sm-12"><?php echo $this->Form->control('auto_draw', array('type' => 'checkbox', 'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false)); ?></div>
                    </div>
                </div>
            </div>

            <div class="row questions hidden" data-toggle="questions-wrapper">
                <div class="col-xs-12">
                    <legend class="padding-20 bg-master-light"><?= __('Perguntas da Promoção') ?> <button type='button' data-toggle="add-question" class='btn btn-primary' disabled><i class="fa fa-plus"></i></button></legend>
                </div>
                <?php echo $this->Element('Manager/promotions/base-questions-list'); ?>
            </div>
            <div class="row clearfix mt-2 mb-2">
                <div class="col-sm-12 col-xs-12">
                    <?= $this->Form->button(__('Add'), ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        </fieldset>
        <?= $this->Form->end() ?>
    </div>
</div>

<?php echo $this->Element('Manager/promotions/question-type-options'); ?>

<?php echo $this->Html->css('manager/promotions'); ?>

<?= $this->Html->script('manager/contents') ?>
