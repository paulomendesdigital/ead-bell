<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Content $content
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Promoções'), ['action' => 'promotions'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Edit') . ' ' . __('Promoção'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link('<i class="fa fa-list"></i> ' . __('Listar Promoções'), ['action' => 'promotions'], ['class' => 'btn btn-small btn-info', 'escape' => false]) ?></li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="contents form large-9 medium-8 columns content">
        <?= $this->Form->create($content, ['type' => 'file']); ?>
        <fieldset>
            <legend><?= __('Edit') . ' ' . __('Promoção') ?></legend>
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <div class="row clearfix mt-2">
                        <div class="col-sm-3 col-xs-12">
                            <?php echo $this->Form->control('id'); ?>
                            <?php echo $this->Form->control('local', ['type' => 'hidden', 'value' => 'add_content']); ?>
                            <?php echo $this->Form->control('promocaoId', ['type' => 'hidden', 'value' => $promocaoId]); ?>
                            <?php echo $this->Form->control('podcastId', ['type' => 'hidden', 'value' => $podcastId]); ?>
                            <?php echo $this->Form->control('perguntaId', ['type' => 'hidden', 'value' => $perguntaId]); ?>
                            <?php echo $this->Form->control('quizId', ['type' => 'hidden', 'value' => $quizId]); ?>

                            <?php //Tipos de Pergunta
                            ?>
                            <?php echo $this->Form->control('questionTypeTextId', ['type' => 'hidden', 'value' => $questionTypeTextId]); ?>
                            <?php echo $this->Form->control('questionTypeRadioButtonId', ['type' => 'hidden', 'value' => $questionTypeRadioButtonId]); ?>
                            <?php echo $this->Form->control('questionTypeCheckboxId', ['type' => 'hidden', 'value' => $questionTypeCheckboxId]); ?>

                            <?php echo $this->Form->control('company_id', ['data-toggle' => 'change_company_id', 'class' => 'form-control', 'label' => __('company')]); ?>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            <?php echo $this->Form->control('content_category_id', array('class' => 'form-control', 'label' => __('Categoria'))); ?>
                        </div>
                        <div class="col-sm-3 col-xs-12 programs">
                            <?php echo $this->Form->control('program_id', array('data-toggle' => 'program-id', 'class' => 'form-control', 'empty' => 'Selecione', 'label' => __('Programas'))); ?>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            <?php echo $this->Form->control('destination', array('class' => 'form-control', 'empty' => 'Selecione', 'options' => $destinations, 'label' => __('Conteúdo exclusivo'), 'required' => true)); ?>
                        </div>
                    </div>
                    <div class="row clearfix mt-2">
                        <div class="col-sm-4 col-xs-12">
                            <?php echo $this->Form->control('name', ['class' => 'form-control input-lg', 'label' => __('Título Página')]); ?>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <?php echo $this->Form->control('title', array('class' => 'form-control input-lg', 'label' => __('Título Destaque'))); ?>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <?php echo $this->Form->control('subtitle', array('class' => 'form-control input-lg', 'label' => __('subtitle'))); ?>
                        </div>
                    </div>
                    <div class="row clearfix mt-2">
                        <div class="col-sm-12 col-xs-12">
                            <?php echo $this->Form->control('description', ['class' => 'form-control input-lg', 'id' => 'ckfinderDescriptionContent', 'label' => __('Descrição')]); ?>
                        </div>
                    </div>
                    <div class="row clearfix mt-2">
                        <div class="col-sm-12 col-xs-12">
                            <?php echo $this->Form->control('tags', ['type' => 'text', 'class' => 'form-control input-lg', 'label' => __('Tags - separadas por ;') . " (até {$limitTags} tags)"]); ?>
                        </div>
                    </div>
                    <div class="row clearfix mt-2 hidden">
                        <div class="col-sm-12 col-xs-12 audio">
                            <?php //echo $this->Form->control('audio', array('class' => 'form-control', 'label' => __('Código do Áudio (Podcast)')));
                            ?>
                        </div>
                    </div>
                    <div class="row clearfix mt-2 regulation">
                        <div class="col-sm-12 col-xs-12">
                            <?php echo $this->Form->control('regulation', ['class' => 'form-control input-lg', 'id' => 'ckfinderRegulation', 'label' => __('Regulamento da Promoção')]); ?>
                        </div>
                    </div>
                    <div class="row clearfix mt-2 instrutions_winners">
                        <div class="col-sm-12 col-xs-12">
                            <?php echo $this->Form->control('instrutions', ['class' => 'form-control input-lg', 'id' => 'ckfinderInstrutionsWinners', 'label' => __('Instruções aos Ganhadores')]); ?>
                        </div>
                    </div>

                    <div class="row clearfix mt-2">
                        <div class="col-sm-4 col-xs-12 hidden">
                            <?php echo $this->Form->control('image_small_name', array('class' => 'form-control  input-lg', 'type' => 'file', 'label' => "Imagem pequena ({$image_small_size})")); ?>
                        </div>

                        <div class="col-sm-4 col-xs-12">
                            <fieldset style="border: 1px solid silver; padding: 15px; border-radius: 4px;">
                                <?php echo $this->Form->control('image_medium_name', array('class' => 'form-control  input-lg', 'type' => 'file', 'label' => "Destaque Home Quadrado ({$image_medium_size})")); ?>
                                <?php if (!empty($content->image_medium_name)) : ?>
                                    <?php echo $this->Html->image("/files/Contents/image_medium_name/{$content->image_medium_name}", ['class' => 'img-responsive']); ?>
                                    <div class="form-group row text-center" data-hide="modal">
                                        <label class="col-sm-12" class="">Remover Imagem?</label>
                                        <div class="col-sm-12"><?php echo $this->Form->control('image_medium_name_remove', array('type' => 'checkbox', 'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false)); ?></div>
                                    </div>
                                    <hr />
                                <?php endif; ?>
                            </fieldset>
                        </div>

                        <div class="col-sm-4 col-xs-12">
                            <fieldset style="border: 1px solid silver; padding: 15px; border-radius: 4px;">
                                <?php echo $this->Form->control('image_large_name', array('class' => 'form-control  input-lg', 'type' => 'file', 'label' => "Destaque Home Retangular ({$image_large_size})")); ?>
                                <?php if (!empty($content->image_large_name)) : ?>
                                    <?php echo $this->Html->image("/files/Contents/image_large_name/{$content->image_large_name}", ['class' => 'img-responsive']); ?>
                                    <div class="form-group row text-center" data-hide="modal">
                                        <label class="col-sm-12" class="">Remover Imagem?</label>
                                        <div class="col-sm-12"><?php echo $this->Form->control('image_large_name_remove', array('type' => 'checkbox', 'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false)); ?></div>
                                    </div>
                                    <hr />
                                <?php endif; ?>
                            </fieldset>
                        </div>

                        <div class="col-sm-4 col-xs-12">
                            <fieldset style="border: 1px solid silver; padding: 15px; border-radius: 4px;">
                                <?php echo $this->Form->control('image_internal', array('class' => 'form-control  input-lg', 'type' => 'file', 'label' => "Página Interna ({$image_internal_size})")); ?>
                                <?php if (!empty($content->image_internal)) : ?>
                                    <?php echo $this->Html->image("/files/Contents/image_internal/{$content->image_internal}", ['class' => 'img-responsive']); ?>
                                    <div class="form-group row text-center" data-hide="modal">
                                        <label class="col-sm-12" class="">Remover Imagem?</label>
                                        <div class="col-sm-12"><?php echo $this->Form->control('image_internal_remove', array('type' => 'checkbox', 'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false)); ?></div>
                                    </div>
                                    <hr />
                                <?php endif; ?>
                            </fieldset>
                        </div>
                    </div>
                    <div class="row clearfix mt-2">
                        <div class="form-group" data-hide="modal">
                            <label class="col-sm-12" class="">Status</label>
                            <div class="col-sm-12"><?php echo $this->Form->control('status', array('type' => 'checkbox', 'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false, 'checked' => 'checked')); ?></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-sm-12 col-xs-12">
                    <div class="row clearfix promotion padding-20 bg-master-light">
                        CONFIGURAÇÃO DA PROMOÇÃO
                    </div>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="col-sm-2 col-xs-12">
                    <?php $labelStart = "Exibir no site a partir de:"; ?>
                    <?php if ($content->start) : ?>
                        <?php echo $this->Form->control('start', ['value' => $content->start->format('d/m/Y H:i'), 'type' => 'text', 'class' => 'form-control input-lg', 'data-content' => "datetimepicker", 'label' => $labelStart]); ?>
                    <?php else : ?>
                        <?php echo $this->Form->control('start', ['type' => 'text', 'class' => 'form-control input-lg', 'data-content' => "datetimepicker", 'label' => $labelStart]); ?>
                    <?php endif; ?>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <?php $labelFinish = "Sair do site em:"; ?>
                    <?php if ($content->finish) : ?>
                        <?php echo $this->Form->control('finish', ['value' => $content->finish->format('d/m/Y H:i'), 'type' => 'text', 'class' => 'form-control input-lg', 'data-content' => "datetimepicker", 'label' => $labelFinish]); ?>
                    <?php else : ?>
                        <?php echo $this->Form->control('finish', ['type' => 'text', 'class' => 'form-control input-lg', 'data-content' => "datetimepicker", 'label' => $labelFinish]); ?>
                    <?php endif; ?>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <?php $labelFinishPromotion = "Encerrar participação em: <i class='fa fa-info-circle' data-toggle='tooltip' data-placement='top' title='Se a promoção for do tipo sorteio, o mesmo será realizado nesta data'></i>"; ?>
                    <?php if ($content->finish_promotion) : ?>
                        <?php echo $this->Form->control('finish_promotion', ['value' => $content->finish_promotion->format('d/m/Y H:i'), 'type' => 'text', 'class' => 'form-control input-lg', 'data-content' => "datetimepicker", 'label' => $labelFinishPromotion, 'escape' => false]); ?>
                    <?php else : ?>
                        <?php echo $this->Form->control('finish_promotion', ['type' => 'text', 'class' => 'form-control input-lg', 'data-content' => "datetimepicker", 'label' => $labelFinishPromotion, 'escape' => false]); ?>
                    <?php endif; ?>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <?php echo $this->Form->control('limitwinners', array('class' => 'form-control input-lg', 'required' => true, 'label' => __('Limite de Ganhadores'))); ?>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <?php echo $this->Form->control('type_promotion', array('class' => 'form-control input-lg', 'empty' => 'Selecione', 'options' => $promotion_types, 'label' => __('Tipo de Promoção'))); ?>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <div class="form-group" data-hide="modal">
                        <label class="col-sm-12" class="">Habilitar Sorteio Automático</label>
                        <div class="col-sm-12"><?php echo $this->Form->control('auto_draw', array('type' => 'checkbox', 'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false)); ?></div>
                    </div>
                </div>
            </div>

            <div class=" mt-2 row questions <?php echo empty($content->questions) ? 'hidden' : ''; ?>" data-toggle="questions-wrapper">
                <div class="col-xs-12">
                    <legend class="padding-20 bg-master-light"><?= __('Perguntas da Promoção') ?> <button type='button' data-toggle="add-question" class='btn btn-primary' disabled><i class="fa fa-plus"></i></button></legend>
                </div>
                <?php if (!empty($content->questions)) { ?>
                    <div class="col-xs-12 questions-list" data-toggle="questions-list">
                        <?php foreach ($content->questions as $key_question => $question) { ?>
                            <div class="panel panel-default question" data-toggle='question-wrapper' data-question-index="<?php echo $key_question; ?>">
                                <div class="panel-body">
                                    <button type='button' data-toggle="remove-question" class='btn btn-danger'><i class="fa fa-trash"></i></button>
                                    <div class="row clearfix">
                                        <div class="col-xs-8">
                                            <?php echo $this->Form->control("questions.{$key_question}.id"); ?>
                                            <?php echo $this->Form->control("questions.{$key_question}.question", ['class' => 'form-control', 'label' => 'Pergunta', 'data-toggle' => 'question', 'required', 'type' => 'text']); ?>
                                        </div>
                                        <div class="col-xs-4">
                                            <?php echo $this->Form->control("questions.{$key_question}.type", ['class' => 'form-control', 'label' => 'Tipo de Pergunta', 'data-toggle' => 'question-type', 'required', 'options' => ($content->type_promotion != $quizId ? $question_types : [$questionTypeRadioButtonId => 'Radio Button']), 'empty' => 'Selecione']); ?>
                                        </div>
                                        <div class='col-xs-12 alternatives <?php echo empty($question->alternatives) ? 'hidden' : ''; ?>' data-toggle='alternatives-wrapper'>
                                            <br />
                                            <label class="padding-20 bg-master-light">Alternativas <button type='button' data-toggle="add-alternative" class='btn btn-primary'><i class="fa fa-plus"></i></button></label>
                                            <?php if (!empty($question->alternatives)) { ?>
                                                <div class="alternatives-list" data-toggle='alternatives-list'>
                                                    <?php foreach ($question->alternatives as $key_alternative => $alternative) { ?>
                                                        <div class="row alternative mt-1" data-alternative-index="<?php echo $key_alternative; ?>" data-toggle='alternative-wrapper'>
                                                            <div class="col-xs-10">
                                                                <?php echo $this->Form->control("questions.{$key_question}.alternatives.{$key_alternative}.id"); ?>
                                                                <?php echo $this->Form->control("questions.{$key_question}.alternatives.{$key_alternative}.alternative", ['class' => 'form-control', 'label' => 'Alternativa', 'required', 'data-toggle' => "alternative", 'type' => 'text', 'maxlength' => '255']); ?>
                                                            </div>
                                                            <div class="col-xs-2">
                                                                <div class="row">
                                                                    <div class="col-xs-9">
                                                                        <?php echo $this->Form->control("questions.{$key_question}.alternatives.{$key_alternative}.is_correct", ['class' => 'form-control', 'label' => 'Correta?', 'disabled' => ($content->type_promotion == $quizId ? false : true), 'required', 'data-toggle' => "alternative-is-correct", 'options' => ['0' => 'Não', '1' => 'Sim'], 'empty' => 'Selecione']); ?>
                                                                    </div>
                                                                    <div class="col-xs-3">
                                                                        <label style='color: transparent'>foo</label>
                                                                        <button type='button' data-toggle="remove-alternative" class='btn btn-danger'><i class="fa fa-trash"></i></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            <?php } else { ?>
                                                <?php echo $this->Element('Manager/promotions/base-alternatives-list'); ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } else { ?>
                    <?php echo $this->Element('Manager/promotions/base-questions-list'); ?>
                <?php } ?>
            </div>
            <div class="row clearfix mt-2 mb-2">
                <div class="col-sm-12 col-xs-12">
                    <?= $this->Form->button(__('Edit'), ['class' => 'btn btn-primary']) ?>
                    <?= $this->Form->button(__('Edit and continue'), ['name' => 'refer', 'class' => 'btn btn-info']) ?>
                </div>
            </div>
        </fieldset>
        <?= $this->Form->end() ?>
    </div>
</div>

<?php echo $this->Element('Manager/promotions/question-type-options'); ?>

<?php echo $this->Html->css('manager/promotions'); ?>

<?= $this->Html->script('manager/contents') ?>
