<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Content $content
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Contents'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Edit') . ' ' . __('Content'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link('<i class="fa fa-list"></i> ' . __('List Contents'), ['action' => 'index'], ['class' => 'btn btn-small btn-info', 'escape' => false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-list"></i> ' . __('Listar Categorias'), ['controller' => 'ContentCategories', 'action' => 'index'], ['class' => 'btn btn-small btn-info', 'escape' => false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-plus"></i> ' . __('Nova Categoria'), ['controller' => 'ContentCategories', 'action' => 'add'], ['class' => 'btn btn-small btn-success', 'escape' => false]) ?></li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="contents form large-9 medium-8 columns content">
        <?= $this->Form->create($content, ['type' => 'file']); ?>
        <fieldset>
            <legend><?= __('Edit') . ' ' . __('Content') ?></legend>
            <div class="col-sm-12 col-xs-12">
                <div class="row clearfix mt-2">
                    <div class="col-sm-3 col-xs-12">
                        <?php echo $this->Form->control('id'); ?>
                        <?php echo $this->Form->control('local', ['type' => 'hidden', 'value' => 'edit_content']); ?>
                        <?php echo $this->Form->control('promocaoId', ['type' => 'hidden', 'value' => $promocaoId]); ?>
                        <?php echo $this->Form->control('podcastId', ['type' => 'hidden', 'value' => $podcastId]); ?>
                        <?php echo $this->Form->control('perguntaId', ['type' => 'hidden', 'value' => $perguntaId]); ?>
                        <?php echo $this->Form->control('company_id', ['data-toggle' => 'change_company_id', 'class' => 'form-control', 'label' => __('company')]); ?>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <?php echo $this->Form->control('content_category_id', array('data-toggle' => 'content-category-id', 'class' => 'form-control', 'label' => __('Categoria'))); ?>
                    </div>
                    <div class="col-sm-3 col-xs-12 programs">
                        <?php echo $this->Form->control('program_id', array('data-toggle' => 'program-id', 'class' => 'form-control', 'empty' => 'Selecione', 'label' => __('Programas'))); ?>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <?php echo $this->Form->control('destination', array('class' => 'form-control', 'empty' => 'Selecione', 'options' => $destinations, 'label' => __('Conteúdo exclusivo'), 'required' => true)); ?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="col-sm-4 col-xs-12">
                        <?php echo $this->Form->control('name', ['class' => 'form-control input-lg', 'label' => __('Título Página')]); ?>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <?php echo $this->Form->control('title', array('class' => 'form-control input-lg', 'label' => __('Título Destaque'))); ?>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <?php echo $this->Form->control('subtitle', array('class' => 'form-control input-lg', 'label' => __('subtitle'))); ?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="col-sm-12 col-xs-12">
                        <?php echo $this->Form->control('description', ['class' => 'form-control input-lg', 'id' => 'ckfinderDescriptionContent', 'label' => __('Descrição')]); ?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="col-sm-12 col-xs-12">
                        <?php echo $this->Form->control('tags', ['class' => 'form-control input-lg', 'label' => __('Tags - separadas por ;') . " (até {$limitTags} tags)"]); ?>
                    </div>
                </div>
                <div class="row clearfix mt-2 hidden">
                    <div class="col-sm-12 col-xs-12 audio">
                        <?php //echo $this->Form->control('audio', array('class' => 'form-control', 'label' => __('Código do Áudio (Podcast)')));
                        ?>
                    </div>
                </div>
                <div class="row clearfix mt-2 regulation">
                    <div class="col-sm-12 col-xs-12">
                        <?php echo $this->Form->control('regulation', ['class' => 'form-control input-lg', 'id' => 'ckfinderRegulation', 'label' => __('Regulamento da Promoção')]); ?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="col-sm-3 col-xs-12">
                        <?php $labelStart = "Exibir no site a partir de:"; ?>
                        <?php if ($content->start) : ?>
                            <?php echo $this->Form->control('start', ['value' => $content->start->format('d/m/Y H:i'), 'type' => 'text', 'class' => 'form-control input-lg', 'data-content' => "datetimepicker", 'label' => $labelStart]); ?>
                        <?php else : ?>
                            <?php echo $this->Form->control('start', ['type' => 'text', 'class' => 'form-control input-lg', 'data-content' => "datetimepicker", 'label' => $labelStart]); ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <?php $labelFinish = "Sair do site em:"; ?>
                        <?php if ($content->finish) : ?>
                            <?php echo $this->Form->control('finish', ['value' => $content->finish->format('d/m/Y H:i'), 'type' => 'text', 'class' => 'form-control input-lg', 'data-content' => "datetimepicker", 'label' => $labelFinish]); ?>
                        <?php else : ?>
                            <?php echo $this->Form->control('finish', ['type' => 'text', 'class' => 'form-control input-lg', 'data-content' => "datetimepicker", 'label' => $labelFinish]); ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <?php echo $this->Form->control('author', ['type' => 'text', 'class' => 'form-control input-lg', 'label' => __('Autor')]); ?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="col-sm-4 col-xs-12 hidden">
                        <?php echo $this->Form->control('image_small_name', array('class' => 'form-control  input-lg', 'type' => 'file', 'label' => "Imagem pequena ({$image_small_size})")); ?>
                    </div>

                    <div class="col-sm-4 col-xs-12">
                        <fieldset style="border: 1px solid silver; padding: 15px; border-radius: 4px;">
                            <?php echo $this->Form->control('image_medium_name', array('class' => 'form-control  input-lg', 'type' => 'file', 'label' => "Imagem Média ({$image_medium_size})")); ?>
                            <?php if (!empty($content->image_medium_name)) : ?>
                                <?php echo $this->Html->image("/files/Contents/image_medium_name/{$content->image_medium_name}", ['class' => 'img-responsive']); ?>
                                <div class="form-group row text-center" data-hide="modal">
                                    <label class="col-sm-12" class="">Remover Imagem?</label>
                                    <div class="col-sm-12"><?php echo $this->Form->control('image_medium_name_remove', array('type' => 'checkbox', 'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false)); ?></div>
                                </div>
                                <hr />
                            <?php endif; ?>
                        </fieldset>
                    </div>

                    <div class="col-sm-4 col-xs-12">
                        <fieldset style="border: 1px solid silver; padding: 15px; border-radius: 4px;">
                            <?php echo $this->Form->control('image_large_name', array('class' => 'form-control  input-lg', 'type' => 'file', 'label' => "Imagem Grande ({$image_large_size}) ou (489x351) para Podcast")); ?>
                            <?php if (!empty($content->image_large_name)) : ?>
                                <?php echo $this->Html->image("/files/Contents/image_large_name/{$content->image_large_name}", ['class' => 'img-responsive']); ?>
                                <div class="form-group row text-center" data-hide="modal">
                                    <label class="col-sm-12" class="">Remover Imagem?</label>
                                    <div class="col-sm-12"><?php echo $this->Form->control('image_large_name_remove', array('type' => 'checkbox', 'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false)); ?></div>
                                </div>
                                <hr />
                            <?php endif; ?>
                        </fieldset>
                    </div>

                    <div class="col-sm-4 col-xs-12">
                        <fieldset style="border: 1px solid silver; padding: 15px; border-radius: 4px;">
                            <?php echo $this->Form->control('image_internal', array('class' => 'form-control  input-lg', 'type' => 'file', 'label' => "Imagem Interna ({$image_internal_size})")); ?>
                            <?php if (!empty($content->image_internal)) : ?>
                                <?php echo $this->Html->image("/files/Contents/image_internal/{$content->image_internal}", ['class' => 'img-responsive']); ?>
                                <div class="form-group row text-center" data-hide="modal">
                                    <label class="col-sm-12" class="">Remover Imagem?</label>
                                    <div class="col-sm-12"><?php echo $this->Form->control('image_internal_remove', array('type' => 'checkbox', 'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false)); ?></div>
                                </div>
                                <hr />
                            <?php endif; ?>
                        </fieldset>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="form-group" data-hide="modal">
                        <label class="col-sm-12" class="">Status</label>
                        <div class="col-sm-12"><?php echo $this->Form->control('status', array('type' => 'checkbox', 'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false, 'checked' => 'checked')); ?></div>
                    </div>
                    <div class="form-group" data-hide="modal">
                        <label class="col-sm-12" class="">Destaque</label>
                        <div class="col-sm-12"><?php echo $this->Form->control('fixed', array('type' => 'checkbox', 'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false, 'unchecked' => 'checked')); ?></div>
                    </div>
                </div>
            </div>

            <div class="col-sm-3 col-xs-12 hidden">
                <div class="row clearfix mt-2 promotion">
                    <div class="col-sm-12 col-xs-12">
                        <?php if ($content->finish_promotion) : ?>
                            <?php echo $this->Form->control('finish_promotion', ['value' => $content->finish_promotion->format('d/m/Y'), 'type' => 'text', 'class' => 'form-control input-lg', 'data-content' => "datepicker", 'label' => __('Promoção válida até:')]); ?>
                        <?php else : ?>
                            <?php echo $this->Form->control('finish_promotion', ['type' => 'text', 'class' => 'form-control input-lg', 'data-content' => "datepicker", 'label' => __('Promoção válida até:')]); ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-sm-12 mt-2 col-xs-12">
                        <?php echo $this->Form->control('type_promotion', array('class' => 'form-control', 'empty' => 'Selecione', 'options' => $promotion_types, 'label' => __('Tipo de Promoção'))); ?>
                    </div>
                </div>
                <div class="row clearfix mt-2 question">
                    <?php echo $this->Form->control('questions.0.id', ['data-toggle' => 'question', 'disabled' => ($content->type_promotion != $perguntaId)]); ?>
                    <?php echo $this->Form->control('questions.0.question', ['class' => 'form-control', 'label' => 'Pergunta da Promoção', 'disabled' => ($content->type_promotion != $perguntaId), 'data-toggle' => 'question', 'required', 'type' => 'text']); ?>
                </div>
            </div>

            <div class="row clearfix mt-2 mb-2">
                <div class="col-sm-12 col-xs-12">
                    <?= $this->Form->button(__('Edit'), ['class' => 'btn btn-primary']) ?>
                    <?= $this->Form->button(__('Edit and continue'), ['name' => 'refer', 'class' => 'btn btn-info']) ?>
                </div>
            </div>
        </fieldset>
        <?= $this->Form->end() ?>
    </div>
</div>

<?= $this->Html->script('manager/contents') ?>
