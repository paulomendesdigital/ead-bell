<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Content $content
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Promoções'), ['action' => 'promotions'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Ver') . ' ' . __('Promoção'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link('<i class="fa fa-edit"></i> ' . __('Edit'), ['action' => 'editPromotion', $content->id], ['class' => 'btn btn-small btn-warning', 'escape' => false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-copy"></i> ' . 'Clonar', ['action' => 'clone', $content->id], ['class' => 'btn btn-small btn-primary', 'escape' => false]) ?></li>
        <li><?= $this->Form->postLink('<i class="fa fa-trash"></i> ' . __('Delete'), ['action' => 'delete', $content->id], ['class' => 'btn btn-small btn-danger', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $content->id)]) ?> </li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="contents view large-9 medium-8 columns content">
        <h3>#<?php echo $this->Number->format($content->id); ?> - <?php echo $content->company->name; ?> - <?= h($content->name) ?></h3>
        <table class="vertical-table">
            <tr>
                <th scope="row"><?= __('Categoria') ?></th>
                <td> <?= $content->has('content_category') ? $content->content_category->name : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($content->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('title') ?></th>
                <td><?= h($content->title) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('subtitle') ?></th>
                <td><?= h($content->subtitle) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Tags') ?></th>
                <td><?= h($content->tags) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('button Label') ?></th>
                <td><?= h($content->button_label) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Status') ?></th>
                <td><?= $this->Utility->__FormatStatus($content->status) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($content->created->format('d/m/Y H:i')) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($content->modified->format('d/m/Y H:i')) ?></td>
            </tr>
        </table>
        <div class="row">
            <div class="col-md-12">
                <hr />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h4><?= __('Description') ?></h4>
                <?= $content->description; ?>
            </div>
        </div>
        <?php if ($content->regulation) : ?>
            <div class="row">
                <div class="col-md-12">
                    <hr />
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4><?= __('Regulamento') ?></h4>
                    <?= $content->regulation; ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if ($content->instrutions) : ?>
            <div class="row">
                <div class="col-md-12">
                    <hr />
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h4><?= __('Instruções so Ganhadores') ?></h4>
                    <?= $content->instrutions; ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-md-12">
                <hr />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h4><?= __('Destaque Home Quadrado') ?></h4>
                <?php if (!empty($content->image_medium_name)) : ?>
                    <?php echo $this->Html->image("/files/Contents/image_medium_name/{$content->image_medium_name}", ['class' => 'img-responsive']); ?>
                <?php endif; ?>
            </div>
            <div class="col-md-4">
                <h4><?= __('Destaque Home Retangular') ?></h4>
                <?php if (!empty($content->image_large_name)) : ?>
                    <?php echo $this->Html->image("/files/Contents/image_large_name/{$content->image_large_name}", ['class' => 'img-responsive']); ?>
                <?php endif; ?>
            </div>
            <div class="col-md-4">
                <h4><?= __('Página Interna') ?></h4>
                <?php if (!empty($content->image_internal)) : ?>
                    <?php echo $this->Html->image("/files/Contents/image_internal/{$content->image_internal}", ['class' => 'img-responsive']); ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <hr />
            </div>
        </div>
    </div>
</div>