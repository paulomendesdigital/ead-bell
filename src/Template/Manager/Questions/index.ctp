<?php
/**
 * @copyright 2019
 * @author Ricardo Aranha - www.grupogrow.com.br
 *
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Question[]|\Cake\Collection\CollectionInterface $questions
 */
?>


<?php echo $this->Html->link('<i class="fa fa-plus fa-lg"></i>', ['action' => 'add'], ['escape' => false, 'class' => 'builder-new btn-info thumbnail-wrapper d48 circular', 'title' => __('Add new register'), 'data-toggle' =>'tooltip', 'data-placement' => 'left']); ?>
<div class="container-fluid container-fixed-lg">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'index'], ['class' => '']); ?></li>
            <li class="breadcrumb-item active" aria-current="page">Questions</li>
        </ol>
    </nav>
</div>

<div class="container-fluid container-fixed-lg">

    <div class="panel panel-default">

        <div class="panel-body">

            <?php echo $this->Form->create($this->request->getParam('controller'), ['url' => ['action' => 'delete']]); ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">
                            <button class="btn btn-link" type='submit'><i class="pg-trash"></i></button>
                        </th>
                                                    <th scope="col">
                                <?= $this->Paginator->sort('id') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'id'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                                                    <th scope="col">
                                <?= $this->Paginator->sort('content_id') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'content_id'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                                                    <th scope="col">
                                <?= $this->Paginator->sort('status') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'status'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                                                    <th scope="col">
                                <?= $this->Paginator->sort('created') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'created'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                                                    <th scope="col">
                                <?= $this->Paginator->sort('modified') ?>
                                <?php if($this->request->getQuery() && $this->request->getQuery('sort') == 'modified'){ ?>
                                    <i class='fas fa-caret-<?php echo $this->request->getQuery('direction') == ' asc' ? ' up' : ' down' ;?>'></i>
                                <?php } ?>
                            </th>
                                                <th scope="col">
                            <?php echo __('Actions'); ?>
                        </th>
                    </tr>
                </thead>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($questions as $question){ ?>
                <tr>
                    <td>
                        <input name="data[Questions][][Questions][id]" value="<? echo $question->id; ?>" id="question-<?php echo $question->id; ?>" type="checkbox"/>
                        <label for="page-<?php echo $question->id;?>" class="no-padding no-margin"></label>
                    </td>
                                                                                                                                                                                                                                        <td><?= $this->Number->format($question->id) ?></td>
                                                                                                                                                                                                                    <td><?= $question->has('content') ? $this->Html->link($question->content->name, ['controller' => 'Contents', 'action' => 'view', $question->content->id]) : '' ?></td>
                                                                                                                                                                                                                                                                                                                    <td><?= $this->Number->format($question->status) ?></td>
                                                                                                                                                                                                                                                                                            <td><?= h($question->created) ?></td>
                                                                                                                                                                                                                                                                                            <td><?= h($question->modified) ?></td>
                                                                                                                <td class="actions">
                        <?= $this->Html->link(__('Ver'), ['action' => 'view', $question->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $question->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $question->id], ['confirm' => __('Are you sure you want to delete # {0}?', $question->id)]) ?>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <?php echo $this->Form->end(); ?>

    <?php echo $this->Element('Manager/pagination');?>

</div></div></div>
