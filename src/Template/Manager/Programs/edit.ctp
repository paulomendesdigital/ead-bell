<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Program $program
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Programs'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Edit').' '.__('programs'); ?></a></li>
    </ul>
</div>

<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li class="hidden-xs"><?= $this->Html->link('<i class="fa fa-list"></i> '.__('List Companies'), ['controller' => 'Companies', 'action' => 'index'],['class'=>'btn btn-small btn-info','escape'=>false]) ?></li>
        <li><?= $this->Form->postLink('<i class="fa fa-trash"></i> '.__('Delete'),['action' => 'delete', $program->id],['confirm' => __('Are you sure you want to delete # {0}?', $program->id), 'class'=>'btn btn-small btn-danger','escape'=>false]) ?></li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="programs form large-9 medium-8 columns content">
        <?= $this->Form->create($program, ['type' => 'file']); ?>
            <fieldset class="col-sm-12">
                <legend><?= __('Edit Program') ?></legend>
                <div class="row clearfix mt-2">
                    <div class="col-sm-3 col-xs-12">
                        <?php echo $this->Form->control('id');?>
                        <?php echo $this->Form->control('company_id', ['options' => $companies, 'type'=>'select','class' => 'form-control', 'label' => __('company')]);?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('program_type', ['options' => $program_types, 'type'=>'select','class' => 'form-control', 'label' => __('Tipo de Conteúdo Mix')]);?>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <?php echo $this->Form->control('ordination', array('class' => 'form-control', 'label' => __('ordination')));?>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group status" data-hide="modal">
                            <label class="col-sm-12" class="">Marcar como Destaque</label>
                            <div class="col-sm-12"><?php echo $this->Form->control('spotlight', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false)); ?></div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="col-sm-6">
                        <?php echo $this->Form->control('name', array('class' => 'form-control input-lg', 'label' => __('name')));?>
                    </div>
                    <div class="col-sm-6">
                        <?php echo $this->Form->control('sutbtitle', array('class' => 'form-control input-lg', 'label' => __('sutbtitle')));?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="col-sm-12">
                        <?php echo $this->Form->control('info_hour', array('class' => 'form-control input-lg', 'label' => __('Informação do Horário')));?>
                    </div>
                </div>
                <div class="row clearfix mt-2">
                    <div class="col-sm-12 col-xs-12">
                        <?php echo $this->Form->control('description', ['class' => 'form-control input-lg', 'id'=>'ckfinderDescription', 'label' => __('Descrição')]);?>
                    </div>
                </div>

                <div class="row clearfix mt-2">
                    <div class="col-sm-4 col-xs-12">
                        <fieldset style="border: 1px solid silver; padding: 15px; border-radius: 4px;">  
                            <?php echo $this->Form->control('image', array('class' => 'form-control  input-lg', 'type'=>'file', 'label' => "Imagem ({$image_xvga})"));?>
                            <?php if( !empty($program->image) ):?>
                                <?php echo $this->Html->image("/files/Programs/image/{$program->image}",['class'=>'img-responsive']);?>
                                <div class="form-group row text-center hidden" data-hide="modal">
                                    <label class="col-sm-12" class="">Remover Imagem?</label>
                                    <div class="col-sm-12"><?php echo $this->Form->control('image_remove', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false)); ?></div>
                                </div>
                                <hr />
                            <?php endif;?>
                        </fieldset>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <fieldset style="border: 1px solid silver; padding: 15px; border-radius: 4px;">  
                            <?php echo $this->Form->control('image_home', array('class' => 'form-control  input-lg', 'type'=>'file', 'label' => "Imagem Home ({$image_home_xvga})"));?>
                            <?php if( !empty($program->image_home) ):?>
                                <?php echo $this->Html->image("/files/Programs/image_home/{$program->image_home}",['class'=>'img-responsive']);?>
                                <div class="form-group row text-center hidden" data-hide="modal">
                                    <label class="col-sm-12" class="">Remover Imagem?</label>
                                    <div class="col-sm-12"><?php echo $this->Form->control('image_home_remove', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false)); ?></div>
                                </div>
                                <hr />
                            <?php endif;?>
                        </fieldset>
                    </div>
                </div>

                <div class="row clearfix mt-2">
                    <div class="form-group" data-hide="modal">
                        <label class="col-sm-12" class="">Status</label>
                        <div class="col-sm-12"><?php echo $this->Form->control('status', array('type' => 'checkbox' ,'class' => 'form-control', 'label' => false, 'data-init-plugin' => 'switchery', 'div' => false, 'checked' => 'checked')); ?></div>
                    </div>
                </div>
                <div class="row clearfix mt-2 mb-2">
                    <div class="col-sm-12 col-xs-12">
                        <?= $this->Form->button(__('Edit'),['class'=>'btn btn-primary']) ?>
                        <?= $this->Form->button(__('Edit and continue'),['name'=>'refer','class'=>'btn btn-info']) ?>
                    </div>
                </div>
            </fieldset>
        <?= $this->Form->end() ?>
    </div>
</div>
