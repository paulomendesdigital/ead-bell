<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Program $program
 */
?>
<div class="container-fluid container-fixed-lg">
    <ul class="breadcrumb">
        <li><?php echo $this->Html->link(__('Home'), ['controller' => 'pages', 'action' => 'welcome'], ['class' => '']); ?></li>
        <li><?php echo $this->Html->link(__('Conteúdos'), ['action' => 'index'], ['class' => '']); ?></li>
        <li><a class="active"><?php echo __('Ver').' '.__('Conteúdo'); ?></a></li>
    </ul>
</div>
<nav class="large-3 medium-4 columns top-content" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link('<i class="fa fa-edit"></i> '.__('Edit'), ['action' => 'edit', $program->id],['class'=>'btn btn-small btn-warning','escape'=>false]) ?></li>
        <li><?= $this->Form->postLink('<i class="fa fa-trash"></i> '.__('Delete'), ['action' => 'delete', $program->id], ['class'=>'btn btn-small btn-danger','escape'=>false,'confirm' => __('Are you sure you want to delete # {0}?', $program->id)])?> </li>
    </ul>
</nav>

<div class="container-fluid container-fixed-lg">
    <div class="programs view large-9 medium-8 columns content">
        <h3><?= h($program->name) ?></h3>
        <table class="vertical-table">
            <tr>
                <th scope="row"><?= __('Company') ?></th>
                <td><?= $program->has('company') ? $this->Html->link($program->company->name, ['controller' => 'Companies', 'action' => 'view', $program->company->id]) : '' ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Name') ?></th>
                <td><?= h($program->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Image') ?></th>
                <td><?= h($program->image) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Image Path') ?></th>
                <td><?= h($program->image_path) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($program->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Status') ?></th>
                <td><?= $this->Utility->__FormatStatus($program->status) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= $program->created->format('d/m/Y H:i') ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= $program->modified->format('d/m/Y H:i') ?></td>
            </tr>
        </table>
        <div class="row">
            <h4><?= __('Description') ?></h4>
            <?= $this->Text->autoParagraph($program->description); ?>
        </div>
        <div class="row col-12">
            <?php echo $this->Html->image("/files/Programs/image/{$program->image}",['class'=>'img-responsive']);?>
        </div>
    </div>
</div>
