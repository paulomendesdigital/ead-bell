<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
	<head>
	    <title><?= $this->fetch('title') ?></title>
	</head>
	<body>
		<table align="center" style="width: 600px;" cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<th><?php echo $this->Html->image('https://api.dialbrasil.com.br/img/mix-header-email.jpg',['style'=>'width: 600px;']);?></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
	    				<?= $this->fetch('content') ?>
					</td>
				</tr>
			</tbody>
			<tfoot>
				<tr><?php echo $this->Html->image('https://api.dialbrasil.com.br/img/mix-footer-email.jpg',['style'=>'width: 600px;']);?></tr>
			</tfoot>
		</table>
	</body>
</html>
