<?php 
$params = $this->request->query;
$data_embed = json_decode(file_get_contents("https://publish.twitter.com/oembed?url={$params['url']}"), true);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body style="margin: 0 !important; padding: 0 !important;">

<?php 

if(!empty($data_embed)){
echo $data_embed['html'];

?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
<script> 
    function isRendered(){
        if($('.twitter-tweet').find('#twitter-widget-0')[0] !== undefined){
            var height_twitter = $('.twitter-tweet').find('#twitter-widget-0').height();
            //console.log(height_twitter);
            if(height_twitter > 0){
                height_twitter += 30;
                //console.log(window.parent.document.getElementById('twitter-<?=$params['time']?>'));
                window.parent.document.getElementById('twitter-<?=$params['time']?>').style.height = height_twitter+'px';
            }
        }else{
            //console.log('no');
        } 
    }
    isRendered(); 

    $('#twitter-widget-0').resize(function(e){
        //console.log(e);
    });
    
    var observer = setInterval(isRendered, 1000);

</script>

<?php } ?>
</body>
</html>