<?php

/**
 * Helper to create form fields.
 *
 * @filesource
 * @copyright  Copyright 2019, Ricardo Aranha grupogrow.com.br
 * @author     Ricardo Aranha <rikarudoaranha@gmail.com>
 */

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\Helper\HtmlHelper;
use Cake\Core\Configure;
use Cake\Utility\Text;
//App::uses('HtmlHelper', 'View/Helper');

/*
 * Actions
 * __FormatDate
 * __FormatDatePicker
 * __Normalize
 * __LimitText
 * __SumDate
 * __FormatStatus
 * __FormatIs
 * __ArrayRandom
 * __FormatPrice
 * __LimitDeliveryRadius
 * __Slug
 * __Normalize
 */

class UtilityHelper extends HtmlHelper { 
    
    public $helpers = array('Html');

    /*
     * Função responsável por tratar uma data,
     * formatando ela de diferentes formas.
     * 
     * Exemplos:
     * Normal (00/00/0000)
     * DayWeekMonth (Dia (00.Mês))
     * Week (Dia)
     * NormalWithTime (00/00/0000 às 00:00:00)
     * WithoutExtensiveYear (Dia de Mês)
     * WithExtensiveYear (Dia, 00 de Mês de 0000)
     * ExtensiveYear (00 de Mês de 0000)
     * ExtensiveMonth (Mês)
     * Date (Data 00/00/0000)
     * SimpleDate (Data 00/00)
     * Hour (Hora 00h 00)
     */
    public function __FormatDate($USDate, $Format="NormalWithTime")
    {
        if(!$USDate):
            return false;
        endif;
        
        if($USDate != '0000-00-00' && $USDate != '0000-00-00 00:00:00' && (strstr($USDate, '-'))):
            if( strlen($USDate) == 19 ){
                $USDate = substr($USDate, 0, 16);
            }  
            if(strstr( $USDate, ' ' )):
                list($date, $time) = explode(' ', $USDate);
            else:
                $date = $USDate;
            endif;
            list($year, $month, $day) = explode('-', $date);
            
            $Months = array(
                '01'=>'Janeiro',
                '02'=>'Fevereiro',
                '03'=>'Março',
                '04'=>'Abril',
                '05'=>'Maio',
                '06'=>'Junho',
                '07'=>'Julho',
                '08'=>'Agosto',
                '09'=>'Setembro',
                '10'=>'Outubro',
                '11'=>'Novembro',
                '12'=>'Dezembro'
            );
            
            $Week = array(
                '0'=>'Domingo',
                '1'=>'Segunda-feira',
                '2'=>'Terça-feira',
                '3'=>'Quarta-feira',
                '4'=>'Quinta-feira',
                '5'=>'Sexta-feira',
                '6'=>'Sábado'
            );

            $tstamp = strtotime($USDate);
            $tstamp = date("w", $tstamp);
            
            if ($Format == "Normal"):
                return "$day/$month/$year";

            elseif($Format == "DayWeekMonth"):
                return "$Week[$tstamp] ($day.$month)";

            elseif($Format == "Week"):
                return "$Week[$tstamp]";

            elseif($Format == "NormalWithTime"):
                return "$day/$month/$year às $time";
            
            elseif($Format == "WithoutExtensiveYear"):
                return "$day de $Months[$month]";

            elseif($Format == "WithExtensiveYear"):
                return "$Week[$tstamp], $day de $Months[$month] de $year";

            elseif($Format == "ExtensiveYear"):
                return "$day de $Months[$month] de $year";

            elseif($Format == "ExtensiveYearTime"):
                return "$day de $Months[$month] de $year às $time";

            elseif($Format == "MonthYear"):
                return "$Months[$month] de $year";

            elseif($Format == "ExtensiveMonth"):
                return "$Months[$month]";

            elseif($Format == "Date"):
                return "$day/$month/$year";

             elseif($Format == "SimpleDate"):
                return "$day/$month";

            elseif($Format == "Hour"):
                return "$time";

            elseif($Format == "SimpleHour"):
                $t = explode(':', $time);
                return "$t[0]h$t[1]";

            else:
                return "$day/$month/$year às $time";
            endif;
            
            
        endif;

        return $USDate;
    }

    public function __FormatDatePicker($data)
    {
        return date('d/m/Y', strtotime($data));
    }

    /*
     * Função responsável por tratar uma string,
     * limitando a quantidade de caracteres sem cortar a palavra.
     */
    public function __LimitText($string, $length = 60, $center = false, $append = null)
    {
        // Set the default append string
        if ($append === null)
            $append = ($center === true) ? ' ... ' : '...';

        // Get some measurements
        $len_string = strlen($string);
        $len_append = strlen($append);

        // If the string is longer than the maximum length, we need to chop it
        if ($len_string > $length) {
            // Check if we want to chop it in half
            if ($center === true) {
                // Get the lengths of each segment
                $len_start = $length / 2;
                $len_end = $len_start - $len_append;

                // Get each segment
                $seg_start = substr($string, 0, $len_start);
                $seg_end = substr($string, $len_string - $len_end, $len_end);

                // Stick them together
                $string = $seg_start.$append.$seg_end;
            } else {
                // Otherwise, just chop the end off
                $string = substr($string, 0, $length - $len_append).$append;
            }
        }

        return $string;
    }
    
    /*
     * Função responsável por somar dias,
     * passando uma data atual + x dias para uma data futura.
     * 
     * Exemplo:
     * __sumDate('2013-01-01', 10);
     * Seu retorno será 10 dias da data informada.
     */
    public function __SumDate( $datein , $count = 1 )
    {
        $datexplode = explode('-',$datein);
        $dias = +$count;
        $dia = $datexplode[2];
        $mes = $datexplode[1];
        $ano = $datexplode[0];
        $datein = mktime(24*$dias, 0, 0, $mes, $dia, $ano);
        $datein = date('Y-m-d',$datein);

        return $datein;
    }
    
    /*
     * Função responsável por tratar status,
     * aplicando layout bootstrap e click para atualização de status ajax.
     * 
     * Ao passar o valor $remote, ele irá aplicar ao click.
     */
    public function __FormatStatus($data,$remote=null)
    {

        // Inactive
        if($data == 0):
            if($remote):
                $data = '<a onclick="'.$remote.'"><span class="label label-danger">Inativo</span></a>';
            else:
                $data = '<span class="label label-danger">Inativo</span>';
            endif;

        // Active
        elseif($data == 1):
            if($remote):
                $data = '<a onclick="'.$remote.'"><span class="label label-success">Ativo</span></a>';
            else:
                $data = '<span class="label label-success">Ativo</span>';
            endif;

        // Removido
        elseif($data == 3):
            if($remote):
                $data = '<a onclick="'.$remote.'"><span class="label label-default">Removido</span></a>';
            else:
                $data = '<span class="label label-success">Removido</span>';
            endif;
            
        endif;

        return $data;

    }

    /*
     * Função responsável por tratar status de uma oportunidade,
     * aplicando layout bootstrap e click para atualização de status ajax.
     * 
     * Ao passar o valor $remote, ele irá aplicar ao click.
     */
    public function __FormatStatusProject($data,$remote=null)
    {

        // sucesso
        if($data == 0):
            if($remote):
                $data = '<a onclick="'.$remote.'"><span class="label bg-danger-light">Sem sucesso</span></a>';
            else:
                $data = '<span class="label bg-danger-light">Sem sucesso</span>';
            endif;

        // Oportunidade
        elseif($data == 1):
            if($remote):
                $data = '<a onclick="'.$remote.'"><span class="label bg-info-light">Oportunidade</span></a>';
            else:
                $data = '<span class="label bg-info-light">Oportunidade</span>';
            endif;

        // Proposta
        elseif($data == 2):
            if($remote):
                $data = '<a onclick="'.$remote.'"><span class="label bg-warning">Proposta</span></a>';
            else:
                $data = '<span class="label bg-warning">Proposta</span>';
            endif;

        // Projeto
        elseif($data == 3):
            if($remote):
                $data = '<a onclick="'.$remote.'"><span class="label bg-success">Projeto</span></a>';
            else:
                $data = '<span class="label bg-success">Projeto</span>';
            endif;
            
        endif;

        return $data;

    }
    
    public function __FormatIs($data)
    {

        // Inactive
        if($data == 0):
            $data = '<span class="label label-danger">Não</span>';

        // Active
        elseif($data == 1):
            $data = '<span class="label label-success">Sim</span>';
        endif;

        return $data;

    }

    public function __FormatLikeDislike($value=false)
    {
        switch ($value) {
            case 'LIKE': return '<i class="fa fa-heart text-success" aria-hidden="true"></i>';
            case 'OK': return '<i class="fa fa-thumbs-up text-primary" aria-hidden="true"></i>';
            case 'DISLIKE': return '<i class="fa fa-thumbs-down text-danger" aria-hidden="true"></i>';
            default: return '';
        }
    }

    public function __FormatIsEmpty($data){

        return empty($data) ? '<span class="label label-danger">Não</span>' : '<span class="label label-success">Sim</span>';
    }

    public function avatarUser($user_id = null, $avatar = null, $class = null, $id = null, $width = 'auto'){
        if($avatar):
            return $this->Html->image('../files/user/avatar/'.$user_id.'/thumb_'.$avatar, ['class' => $class, 'id' => $id, 'width' => $width]);
        else:
            return $this->Html->image('avatar_small2x.jpg', ['class' => $class, 'id' => $id, 'width' => $width]);
        endif;
    }

    public function __LinkUserPermission ($user_id, $user_permissions, $validate, $title, $url = null, $options = array(), $confirmMessage = false){

        if($user_id){
            if($user_permissions){
                if(isset($validate['controller']) && isset($validate['action'])){
                    $controller = Inflector::camelize($validate['controller']);
                    $action = $validate['action'];
                    $string = $controller.'/'.$action;
                    if(array_key_exists($string, $user_permissions)){
                        if($user_permissions[$string]){
                            return parent::link($title, $url, $options, $confirmMessage);
                        }
                    }
                }
            }
            
        }

        return false;
    }

    public function __isUserPermission ($user_id, $user_permissions, $validate){
        if($user_id){
            if($user_permissions){
                if(isset($validate['controller']) && isset($validate['action'])){
                    $controller = Inflector::camelize($validate['controller']);
                    $action = $validate['action'];
                    $string = $this->params['prefix'] == 'manager' ? $controller.'/'."{$this->params['prefix']}_{$action}" : $controller.'/'.$action;
                    if(array_key_exists($string, $user_permissions)){
                        if($user_permissions[$string]){
                            return true;
                        }
                    }
                }
            }
            
        }

        return false;
    }

    public function __getAclLink($link_name, $user_id, $user_permissions, $validate, $options = false ){
        if( $this->__isUserPermission ($user_id, $user_permissions, $validate) ){
            return $this->Html->link($this->__LimitText($link_name), $validate, $options);
        }else{
            return false;
        }
    }

    public function __FirstLastName($name){
        $ex_name = explode(' ',$name);
        $count_ex_name = count($ex_name);

        if($count_ex_name>1){
            return $ex_name[0].' '.$ex_name[$count_ex_name-1];
        }else{
            return $ex_name[0];
        }
    }

    public function __getBgColor($index,$bg_colors){
        if (($index % 2) == 0){
            return "style='background-color: {$bg_colors[0]}'";
        }else{
            return "style='background-color: {$bg_colors[1]}'";
        }
    }

    function __Slug($string){
        return Text::slug( strtolower( $this->__Normalize($string) ), '-' );
    }

    /*
     * Função responsável por tratar uma string,
     * retirando os espaços e caracteres especiais.
     */
    public function __Normalize($string)
    {
        $a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $b = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        $string = utf8_decode($string);
        $string = strtr($string, utf8_decode($a), $b);
        $string = str_replace("-","",$string);
        $string = str_replace("&","e",$string);
        $string = str_replace(" ","-",$string);
        $string = str_replace(".","",$string);
        $string = str_replace("!","",$string);
        $string = str_replace("?","",$string);
        $string = str_replace(":","",$string);
        $string = str_replace(";","",$string);
        $string = str_replace(";","",$string);
        $string = str_replace(",","",$string);
        $string = str_replace("'","",$string);
        $string = str_replace("\"","",$string);
        $string = str_replace("/","",$string);
        $string = str_replace("|","",$string);
        $string = str_replace("--","-",$string);
        $string = str_replace("---","-",$string);
        $string = str_replace("----","-",$string);
        $string = strtolower($string);

        return utf8_encode($string);
    }

    public function __getCategorySlug($category=false, $activeCategory=false){
        if( $activeCategory ){
            return $activeCategory['Category']['slug'];
        }
        else{
            if( isset($category[0]['slug']) ){
                return $category[0]['slug'];
            }
            if( isset($category['slug']) ){
                return $category['slug'];
            }
            if( isset($category['Category']['slug']) ){
                return $category['Category']['slug'];
            }
            else{
                return $category[0]['Category']['slug'];
            }
        }
    }

    public function __FormatPrice($data, $format=null, $cent=null){
        if($data==0):
            $data = '0.00';
        endif;

        /**
        * Formatação de Real.
        * Padrão ex: 10,00
        * Personalizado ex: R$ 10,00
        *
        * Para utilizar o personalizado envie formatMoeda($valor, 'cifrao')
        */
        $data_string = str_replace(',','.',$data);
        if($data_string):
            if($format=='cifrao'){
                $data = 'R$ '.number_format($data_string,2,',','.');
            }else{
                $data = number_format($data_string,2,',','.');
            }

            if($cent):
                if($format=='cifrao'){
                    $data = 'R$ '.number_format($data_string,0,',','.');
                }else{
                    $data = number_format($data_string,0,',','.');
                }
            endif;

            return $data;
        else:
            return false;
        endif;
    }

    public function __ShowLinkTags($tags=false){
        $return = [];
        foreach ($tags as $tag) {
            $return[] = "<a href='/blog/busca/q:{$tag}'>{$tag}</a>";
        }
        return join(', ', $return);
    }

    public function __FormatPaymentStatus($status){
        
        switch ($status) {
            case 'OP': return '<span title="Aguardando Pagto" class="bg-warning padding-5 text-white order-status">'.$status.'</span>';
            case 'PE': return '<span title="Pendente" class="bg-info padding-5 text-white order-status">'.$status.'</span>';
            case 'CO': return '<span title="Confirmado" class="bg-success padding-5 text-white order-status">'.$status.'</span>';
            case 'CA': return '<span title="Cancelado" class="bg-danger padding-5 text-white order-status">'.$status.'</span>';
            default: return '<span title="Pendente" class="bg-info padding-5 text-white order-status">'.$status.'</span>';
        }
    }

    public function __GetLinkPromotion($data){
        $sites = [
            1 => 'https://mixriofm.com.br', 
            2 => 'https://sulamericaparadiso.com.br', 
        ];
        $page = strtolower($data->content_category->code);
        $slug = $this->__Slug($data->name);
        return "{$sites[$data->company_id]}/{$page}/{$data->id}/{$slug}";
        //return Configure::read('Corporate.Website') . "/site/".strtolower($data->content_category->code)."/{$data->id}/".$this->__Slug($data->name);
    }

    public function __GenerateManagerLinkWebdoor($data){
        $sites = [
            1 => 'https://mixriofm.com.br', 
            2 => 'https://sulamericaparadiso.com.br', 
        ];
        $url = $data->link;

        if( strstr($url, 'http') ){
            return $url;
        }
        elseif( strstr($url, 'www') ){
            return $url;
        }
        else{
            return "{$sites[$data->company_id]}/{$url}";
        }
    }
}