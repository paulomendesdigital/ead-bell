<?php

namespace App\View\Helper;

use Cake\ORM\TableRegistry;
use Cake\View\Helper;
use Cake\View\View;

/**
 * AdvertReports helper
 * @property \App\Model\Table\AdvertReportsTable $table
 */
class AdvertReportsHelper extends Helper
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    private $table;

    public function __construct()
    {
        $this->table = TableRegistry::getTableLocator()->get('AdvertReports');
    }

    public function formatStatus($statusId)
    {
        return $this->table->getStatusesList()[$statusId];
    }
}
