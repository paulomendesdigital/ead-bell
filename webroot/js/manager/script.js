$(document).ready(function ($) {

    if( $('[data-toggle="change_company_id"]').val() ){
        var local = $('#local').val();
        var company_id = $('[data-toggle="change_company_id"]').val();
        if( company_id ){
            if( local == 'add_content' ){
                //loadContentCategories( company_id );
                loadPrograms( company_id );
            }
            if( local == 'index_content' ){
                //loadContentCategories( company_id );
            }
        }
    }
    
    if( $('#position option:selected').val() ){
        var position = $('#position option:selected').val();
        var image_size_value = $('#' + position).val();
        $('.label-pub').html('Publicidade em imagem ('+ image_size_value +')');
    }

    //ações qdo carrega a página
    if( $('#content-category-id option:selected').val() == $('#podcastid').val() ){
        //enabledProgram();
        //enabledAudio();
    }
    else{
        //disabledProgram();
        //disabledAudio();
    }

    if( $('#content-category-id option:selected').val() == $('#promocaoid').val() ){
        enabledPromotion();
        enabledRegulation();
    }
    else{
        disabledPromotion();
        disabledRegulation();
    }

    $('[data-toggle="change_company_id"]').on('change', function(){
        
        var company_id = $(this).val();
        var local = $('#local').val();
        
        if( company_id ){
            if( local == 'add_content' || local == 'edit_content' ){
                //loadContentCategories( company_id );
                loadPrograms( company_id );
            }
            if( local == 'index_content' ){
                //loadContentCategories( company_id );
            }
        }
    });

    $('#position').on('change', function(){
        var position = $('#position option:selected').val();
        var image_size_value = $('#' + position).val();
        $('.label-pub').html('Publicidade em imagem ('+ image_size_value +')');
    });

    //ações qdo selecionar categorias
    $('#content-category-id').on('change', function(){
        if( $('#content-category-id option:selected').val() == $('#podcastid').val() ){
            //enabledProgram();
            //enabledAudio();            
        }
        else{
            //disabledProgram();
            //disabledAudio();
        }

        if( $('#content-category-id option:selected').val() == $('#promocaoid').val() ){
            enabledPromotion();
            enabledRegulation();      
            if($(this).val() == $('#perguntaid').val()){
                enableQuestion();
            }
        }
        else{
            disabledPromotion();
            disabledRegulation();
            disableQuestion();
        }
    });

    $('#type-promotion').on('change', function(){
        if($(this).val() == $('#perguntaid').val()){
            $('[data-toggle="questions-wrapper"]').removeClass('hidden');   
            enableQuestionAddButton();
            enableQuestion();
            enableQuestionType();
            questionQuestionTypeOptions();
            $('[data-toggle="alternative-is-correct"]').prop('disabled', true);
        } else
            if($(this).val() == $('#quizid').val()) {
                $('[data-toggle="questions-wrapper"]').removeClass('hidden');
                enableQuestionAddButton();
                enableQuestion();                
                enableQuestionType();
                quizQuestionTypeOptions();
            } else {
                $('[data-toggle="questions-wrapper"]').addClass('hidden');
                $('[data-toggle="questions-wrapper"]').find('input,select').prop('disabled', true);;
                disableQuestionAddButton();
                disableQuestion();
                disableQuestionType();
            }
    });

    $('[data-element="UserPassword"]').on('click', function () {
        if( $('#password').attr('disabled') == 'disabled' ){
            $('#password').removeAttr('disabled');
        }
        else{
            $('#password').attr('disabled','disabled');
        }
    });

    $('.contents').on('change', '[data-toggle="question-type"]', function(){
        let question_wrapper = $(this).closest('[data-toggle="question-wrapper"]');
        if($(this).val() == $('#questiontypetextid').val() || !$(this).val()){
            $(question_wrapper).find('[data-toggle="alternatives-wrapper"]').addClass('hidden');
            $(question_wrapper).find('[data-toggle="alternatives-wrapper"]').find('input,select').prop('disabled', true);
        }else{
            $(question_wrapper).find('[data-toggle="alternatives-wrapper"]').removeClass('hidden');
            let field_elements_to_enable = $(question_wrapper).find('[data-toggle="alternatives-wrapper"]').find('input,select');
            if($('#type-promotion').val() == $('#perguntaid').val()){
                field_elements_to_enable = $(field_elements_to_enable).not('[data-toggle="alternative-is-correct"]').prop('disabled', false);
            }
            $(field_elements_to_enable).prop('disabled', false);
        }
    });

    $('[data-toggle="add-question"]').on('click', function(){
        let last_question = $('[data-toggle="question-wrapper"]').last();
        let last_question_index = $(last_question).data('question-index');
        let new_question = $(last_question).clone();
        let new_question_index = parseInt(last_question_index) + 1;
        $(new_question).data('question-index', new_question_index);
        $(new_question).attr('data-question-index', new_question_index);

        $(new_question).find('input, select, label').each(function(){
            if($(this).prop('tagName').toUpperCase() === 'LABEL'){
                let label_for = $(this).prop('for');
                label_for = label_for.replace('questions-'+last_question_index+'-', 'questions-'+new_question_index+'-');
                $(this).prop('for',label_for);
            } else {
                let input_name = $(this).prop('name');
                input_name = input_name.replace('questions['+last_question_index+']','questions['+new_question_index+']');
                $(this).prop('name',input_name);
                
                let input_id = $(this).prop('id');
                input_id = input_id.replace('questions-'+last_question_index+'-','questions-'+new_question_index+'-');
                $(this).prop('id',input_id);

                $(this).val('');
            }
        });

        while($(new_question).find('[data-toggle="alternatives-list"]').find('[data-toggle="alternative-wrapper"]').length > 1){
            $(new_question).find('[data-toggle="alternatives-list"]').find('[data-toggle="alternative-wrapper"]').last().remove();
        }

        $(new_question).find('[data-toggle="alternatives-wrapper"]').addClass('hidden');

        $('[data-toggle="questions-list"]').append(new_question);
    });

    $('.contents').on('click', '[data-toggle="add-alternative"]', function(){
        let question_wrapper = $(this).closest('[data-toggle="question-wrapper"]');
        let last_alternative = $(question_wrapper).find('[data-toggle="alternative-wrapper"]').last();
        let last_alternative_index = $(last_alternative).data('alternative-index');
        let new_alternative = $(last_alternative).clone();
        let new_alternative_index = parseInt(last_alternative_index) + 1;
        $(new_alternative).data('alternative-index', new_alternative_index);
        $(new_alternative).attr('data-alternative-index', new_alternative_index);

        $(new_alternative).find('input, select, label').each(function(){
            if($(this).prop('tagName').toUpperCase() === 'LABEL'){
                let label_for = $(this).prop('for');
                label_for = label_for.replace('-alternatives-'+last_alternative_index+'-', '-alternatives-'+new_alternative_index+'-');
                $(this).prop('for',label_for);
            } else {
                let input_name = $(this).prop('name');
                input_name = input_name.replace('[alternatives]['+last_alternative_index+']','[alternatives]['+new_alternative_index+']');
                $(this).prop('name',input_name);
                
                let input_id = $(this).prop('id');
                input_id = input_id.replace('-alternatives-'+last_alternative_index+'-','-alternatives-'+new_alternative_index+'-');
                $(this).prop('id',input_id);

                $(this).val('');
            }
        });

        $(question_wrapper).find('[data-toggle="alternatives-list"]').append(new_alternative);
    });

    $('.contents').on('click','[data-toggle="remove-alternative"]', function(){
        $(this).closest('[data-toggle="alternative-wrapper"]').remove();
    });

    $('.contents').on('click', '[data-toggle="remove-question"]', function(){
        $(this).closest('[data-toggle="question-wrapper"]').remove();
    });

    $('.contents').find('form').on('submit', function(){
        let valid = true;        
        if($('#type-promotion').val() == $('#quizid').val()){
            for(let i = 0; i < $('[data-toggle="question-wrapper"]').length; i++){
                let has_correct = false;
                let question_wrapper = $('[data-toggle="question-wrapper"]')[i];                
                for(let j = 0; j < $(question_wrapper).find('[data-toggle="alternative-wrapper"]').length; j++){
                    let alternative_wrapper = $(question_wrapper).find('[data-toggle="alternative-wrapper"]')[j];
                    let is_correct_alternative = ($(alternative_wrapper).find('[data-toggle="alternative-is-correct"]').val() == 1) ? true : false;
                    if(!has_correct && is_correct_alternative){
                        has_correct = true;
                    } else 
                        if(has_correct && is_correct_alternative){
                            valid = false;
                            alert("Pergunta " + (i + 1) + " possui duas ou mais respostas corretas!");
                            break;
                        }
                }
                if(!valid){
                    break;
                }
                if(!has_correct){
                    valid = false;
                    alert("Pergunta " + (i + 1) + " não possui uma resposta correta!");
                    break;
                }
            }
        }
        return valid;
    });

});

function enabledPromotion(){
    $('#type-promotion').removeAttr('disabled');
    $('#type-promotion').attr('required','required');

    $('#finish-promotion').removeAttr('disabled');
    $('#finish-promotion').attr('required','required');
}

function disabledPromotion(){
    $('#type-promotion').attr('disabled','disabled');
    $('#type-promotion').removeAttr('required');

    $('#finish-promotion').attr('disabled','disabled');
    $('#finish-promotion').removeAttr('required');
}

function enabledRegulation(){
    $('.regulation').removeClass('hidden');
}

function disabledRegulation(){
    $('.regulation').addClass('hidden');
}

function enabledProgram(){
    $('#program-id').removeAttr('disabled');
    $('#program-id').attr('required','required');
}

function disabledProgram(){
    $('#program-id').attr('disabled','disabled');
    $('#program-id').removeAttr('required');
}

function enabledAudio(){
    $('#audio').removeAttr('disabled');
    $('#audio').attr('required','required');
}

function disabledAudio(){
    $('#audio').attr('disabled','disabled');
    $('#audio').removeAttr('required');
}

function enableQuestion(){
    $('[data-toggle="question"]').prop('disabled',false);
}

function disableQuestion(){
    $('[data-toggle="question"]').prop('disabled',true);
}

function enableQuestionType(){
    $('[data-toggle="question-type"]').prop('disabled', false);
}

function disableQuestionType(){
    $('[data-toggle="question-type"]').prop('disabled', true);
}

function enableQuestionAddButton(){
    $('[data-toggle="add-question"]').prop('disabled', false);
}

function disableQuestionAddButton(){
    $('[data-toggle="add-question"]').prop('disabled', true);
}

function questionQuestionTypeOptions(){
    $('[data-toggle="question-type"]').html($('[data-toggle="question-type-all"]').html());
    $('[data-toggle="question-type"]').val('');
    $('[data-toggle="question-type"]').trigger('change');
}

function quizQuestionTypeOptions() {
    $('[data-toggle="question-type"]').html($('[data-toggle="question-type-quiz"]').html());
    $('[data-toggle="question-type"]').val('');
    $('[data-toggle="question-type"]').trigger('change');
}

function loadContentCategories(company_id){
    $('[data-toggle="content-category-id"]').html('<option value>Carregando...</option>');
    $('[data-toggle="content-category-id"]').val(null);
    
    $.get('/manager/content-categories/ajax-get-list/'+company_id)
    .then(function(result){
        $('[data-toggle="content-category-id"]').html(null);
        $('[data-toggle="content-category-id"]').append('<option value>Selecione</option>');
        if(result.data){
            $.each(result.data, function (key, item) {
                $('[data-toggle="content-category-id"]').append('<option value="'+key+'">'+item+'</option>');
            });
        }
    });
}

function loadPrograms(company_id){
    $('[data-toggle="program-id"]').html('<option value>Carregando...</option>');
    $('[data-toggle="program-id"]').val(null);
    
    $.get('/manager/programs/ajax-get-list/'+company_id)
    .then(function(result){
        $('[data-toggle="program-id"]').html(null);
        $('[data-toggle="program-id"]').append('<option value>Selecione</option>');
        if(result.data){
            $.each(result.data, function (key, item) {
                $('[data-toggle="program-id"]').append('<option value="'+key+'">'+item+'</option>');
            });
        }
    });
}