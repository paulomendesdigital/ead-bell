document.addEventListener('DOMContentLoaded', function () {
    $('#ckfinderDescriptionContent').each(function () {
        let editor =
            CKEDITOR.replace('ckfinderDescriptionContent', {
                toolbar: 'Full',
                width: '100%',
                height: '350',
                filebrowserBrowseUrl: '/js/manager/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl: '/js/manager/ckfinder/ckfinder.html?type=Images',
                filebrowserUploadUrl: '/manager/contents/uploadCkeditorFiles',
                filebrowserImageUploadUrl: '/js/manager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl: '/js/manager/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
            });
    });
});
