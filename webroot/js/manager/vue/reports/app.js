import Vue from 'vue'

Vue.config.productionTip = false

Vue.component('promotions-reports', require('./components/Reports.vue').default)

new Vue({
  el: '#app'
})