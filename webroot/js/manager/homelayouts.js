var teste = '';
var content_index = 0;
if(typeof content_index_view !== 'undefined' && content_index_view){
    content_index = content_index_view;
}

function setContentSizeOptions(content_wrapper) {
    let select_element = $(content_wrapper).find('[data-toggle="size"]');
    if ($(select_element).length > 0) {
        let size = $(select_element).val();
        let max_size = $(content_wrapper).data('max-size');
        //Caso não possua o tamanho máximo
        if (!max_size) {
            max_size = 2;
        }
        $(select_element).find('option').remove();
        for (let i = 1; i <= max_size; i++) {
            $(select_element).append('<option value="' + i + '">' + i + '</option>');
        }
        $(select_element).val(size);
    }
}

function setContentMaxSize(max_size, content_wrapper) {
    if (!max_size) {
        $(content_wrapper).data('max-size', '');
    } else {
        $(content_wrapper).data('max-size', max_size);
    }
    setContentSizeOptions(content_wrapper);
}

function handleContentSizes(row) {
    let size_total = 0;
    let sizes = [];
    let i = 0;
    $(row).find('.col-content').each(function () {
        let size = 0;
        let select_element = $(this).find('[data-toggle="size"]');
        if ($(select_element).length > 0) {
            size = parseInt($(select_element).val());
        }
        size_total = size_total + size;
        sizes[i++] = size;
    });
    if ($(row).data('row') == 1) {
        let selector = '.col-content.col-sm-4 + .col-content.col-sm-8 + .col-content,';
        selector = selector + '.col-content.col-sm-8 + .col-content.col-sm-4 + .col-content';
        $(row).find(selector).removeClass('has-content').find('.content-layout-wrapper').remove();
        if (sizes[0] == 2) {
            setContentMaxSize(1, $(row).find('.col-content')[1]);
        } else
            if (sizes[1] == 2) {
                setContentMaxSize(1, $(row).find('.col-content')[0]);
            } else
                //Caso a coluna 3 exista
                if (sizes[2] == 1) {
                    setContentMaxSize(1, $(row).find('.col-content')[0]);
                    setContentMaxSize(1, $(row).find('.col-content')[1]);
                    setContentMaxSize(1, $(row).find('.col-content')[2]);
                } else {
                    setContentMaxSize(false, $(row).find('.col-content')[0]);
                    setContentMaxSize(false, $(row).find('.col-content')[1]);
                    setContentMaxSize(false, $(row).find('.col-content')[2]);
                }
    } else {
        if(sizes[1]){
            setContentMaxSize(1, $(row).find('.col-content')[0]);
            setContentMaxSize(1, $(row).find('.col-content')[1]);
        } else {
            setContentMaxSize(false, $(row).find('.col-content')[0]);
        }      
    }
}

$('[data-toggle="add-content"]').on('click', function () {
    let content_wrapper = $('.form-structures').find('.content-layout-wrapper').clone();
    let row = $(this).closest('.row-contents');
    let col = $(this).closest('.col-content');
    $(content_wrapper).find('input, select').each(function () {
        let name = $(this).prop('name').replace('{{index}}', content_index);
        let id = $(this).prop('id').replace('{{index}}', content_index);
        $(this).prop('name', name);
        $(this).prop('id', id);
    });        
    $(content_wrapper).find('[data-toggle="ordination"]').val($(col).data('col'));
    $(content_wrapper).find('[data-toggle="row"]').val($(row).data('row'));    
    $(this).parent().addClass('has-content').append(content_wrapper);
    content_index++;
    
    $(row).find('.col-content.last-has-content').removeClass('last-has-content');
    $(col).addClass('last-has-content');
    if($(row).data('row') == 1){
        if($(col).data('col') < 3){
            let next_col = $(row).find('.col-content')[$(col).data('col')];
            $(next_col).find('[data-toggle="add-content"]').prop('disabled', false);
        }
    } else {
        if($(col).data('col') < 2){
            let next_col = $(row).find('.col-content')[$(col).data('col')];
            $(next_col).find('[data-toggle="add-content"]').prop('disabled', false);
        }
    }
    handleContentSizes(row);
});

$('.homeLayouts.form').on('click', '[data-toggle="delete-content"]', function () {  
    let col_content = $(this).closest('.col-content');
    let row = $(col_content).closest('.row-contents');
    $(col_content).removeClass('has-content');
    $(this).closest('.content-layout-wrapper').remove();   
    $(row).find('.has-content').last().addClass('last-has-content');
    handleContentSizes(row);
});

$('.homeLayouts.form').on('change', '[data-toggle="size"]', function () {
    let size = $(this).val();
    if (size == 1) {
        $(this).closest('.content-layout-wrapper').parent().removeClass('col-sm-8').addClass('col-sm-4');
    } else {
        $(this).closest('.content-layout-wrapper').parent().removeClass('col-sm-4').addClass('col-sm-8');
    }
    handleContentSizes($(this).closest('.row-contents'));
});

$('[data-toggle="company_id"]').on('change', function(){
    if($(this).val()){
        $('[data-toggle="content_id"]').html(null);
        $('[data-toggle="content_id"]').val(null);
        $.get('/api/contents?company_id='+$(this).val()+'&content_category_code=DESTAQUE')
            .then(function(result){
                $('[data-toggle="content_id"]').append('<option value>Selecione o Conteúdo</option>');
                if(result.data){
                    for(let i in result.data){
                        $('[data-toggle="content_id"]').append('<option value="'+result.data[i]['id']+'">'+result.data[i]['name']+'</option>');
                    }
                }
            });
    }
});