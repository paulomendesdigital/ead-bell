document.addEventListener('DOMContentLoaded', function(){
    document.querySelector('[data-role="advert-image-input"]').addEventListener('change', function(ev){
        const img = new Image();
        img.addEventListener('load', () => {
            document.querySelectorAll('[data-role="select-advert-zones"] option').forEach(function(option){
                option.classList.remove('highlight');
            });
            document.querySelectorAll('[data-role="select-advert-zones"] [data-width="'+img.width+'"][data-height="'+img.height+'"]').forEach(function(option){
                option.classList.add('highlight');
            });
            window.URL.revokeObjectURL(img.src); // Free some memory
        });
        img.src = window.URL.createObjectURL(ev.target.files[0]);
    });
});
