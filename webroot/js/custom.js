$(document).ready(function ($) {

  /// efeito campos formulario
  function verificInputs(){
  $('#myModal .form-group .form-control').each(function(){
    if($(this).val().length<1){ $(this).parent().children('label').removeClass('focada'); }else{$(this).parent().children('label').addClass('focada');}
  });
}verificInputs();
  
  $('#myModal .form-group .form-control').focus(function(){
    //console.log('');
    $(this).parent().children('label').addClass('focada');
  });

  $('#myModal .form-group .form-control').blur(function(){
    if($(this).val().length<1){ $(this).parent().children('label').removeClass('focada'); }else{}

    //console.log($(this).val().length);

  });

  $('#cadastro-tel').mask('(00) 00000-0000');
  $('#cadastro-nasc').mask('00/00/0000');
  $('#cadastro-doc').mask('000.000.000-00');

	$('.openPopup').on('click',function(){

        var dataURL = $(this).attr('data-href');
        var modal = $(this).attr('data-modal');
        $('.modal-body').load(dataURL,function(){
            $('#'+modal).modal({show:true,backdrop:"static"});
        });
    });

  $(".wysihtml5").each(function(){$(this).wysihtml5();});
  //$('.wysihtml5').wysihtml5();

  //Abrir modal dinâmica
  $('body').on('click', '[data-toggle="modal"]', function(){
  	var title = $(this).attr('data-content-modal-title');
  	var url = $(this).attr('data-content-modal-url');
  	var form_id = $(this).attr('data-form');
  	$('[data-content="modal-title"]').html(title);

  	$.ajax({
       type: 'GET',
       url: url,
       success: function (data) {
        let form = $(data).find( form_id )[0].outerHTML;
       	$('[data-content="modal-content-fluid"]').html(form);
        $('[data-content="modal-content-fluid"] [data-hide="modal"]').hide();
        $('[data-content="modal-content-fluid"]').find('.checkbox').removeClass('checkbox');
        $('[data-content="modal-content-fluid"]').find('label').css({'display': 'block'});
        $('[data-content="modal-content-fluid"]').find('[name="status"][type="hidden"]').remove();
        $('[data-content="modal-content-fluid"]').find('[name="_method"]').remove();
       }
    });

  });

    

  $(document).on('submit', '.modal-content form', function(){
    let data = $(this).serializeArray();
    let url = $(this).attr('action');
    let set_input = $(this).attr('data-set-input');
    //console.log(data);

    $.ajax({
      type: 'POST',
      url: url,
      data: data,
      dataType: 'json',
      success: function (result) {
        console.log(result.data);

        let option = `<option value="${result.data.id}">${result.data.name}</option>`;
        $(set_input).append(option);
        $(set_input).val(result.data.id);

        $('#modalDinamic').modal('hide');
        
      }
   });

    return false;
  });

  //Forma que o menu aparece para o usuario
  $('body').on('click', '[data-toggle-pin="sidebar"]', function(){
  	console.log('Cehgou');
  	$.ajax({
       type: 'GET',
       url: '/users/sidebar',
       success: function (data) {
       }
    });

  });
  $('body').on('click', 'span[data-toggle="unlock"]', function(){
		var element = $(this).data('element');
		$('#'+element).removeAttr('disabled');
		$('div[data-toggle="UserPassword"]').removeClass('disabled');

		$(this).html('<i class="fa fa-unlock-alt"></i>');
		$(this).attr('data-toggle', 'lock');
		$('#'+element).focus();
	});
	$('body').on('click', 'span[data-toggle="lock"]', function(){
		var element = $(this).data('element');
		$('#'+element).attr('disabled', 'disabled');
		$('div[data-toggle="UserPassword"]').addClass('disabled');

		$(this).html('<i class="fa fa-lock"></i>');
		$(this).attr('data-toggle', 'unlock');
	});

	//-- MASKINPUT
		$('[data-mask-input="date"]').mask('00/00/0000');
		$('[data-mask-input="datetime"]').mask('00/00/0000 00:00');
		$('[data-mask-input="cpf"]').mask('000.000.000-00');
    $('[data-mask-input="zipcode"]').mask('00.000-000');
    $('[data-mask-input="cep"]').mask('00.000-000');
		$('[data-mask-input="expiration"').mask('00/0000');
		
		var CellPhone = function (val) {
			return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
		},
		spOptions = {
			onKeyPress: function(val, e, field, options) {
				field.mask(CellPhone.apply({}, arguments), options);
			}
		};

		$('[data-mask-input="phone"]').mask(CellPhone, spOptions);
		$('[data-mask-input="cellphone"]').mask(CellPhone, spOptions);
	//-- END MASKINPUT


	$('input[data-mask-input="zipcode"]').change(function() {
        // Se o campo CEP não estiver vazio
        if ($.trim($(this).val()) != "") {
            specificWay = $(this).parent().parent().parent().parent();
            //$('#load').show();
            $.getScript("http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep=" + $(this).val(), function() {
                if (resultadoCEP["resultado"]) {
                    // troca o valor dos elementos
                    specificWay.find('input[data-toggle="returnAddress"]').val(unescape(resultadoCEP["tipo_logradouro"]) + " " + unescape(resultadoCEP["logradouro"]));
                    specificWay.find('input[data-toggle="returnNeighborhood"]').val(unescape(resultadoCEP["bairro"]));
                    specificWay.find('input[data-toggle="returnCity"]').val(unescape(resultadoCEP["cidade"]));
                    specificWay.find('select[data-toggle="returnState"]').val(unescape(resultadoCEP["uf"]));
                    specificWay.find('input[data-toggle="returnNumber"]').val("");
                    specificWay.find('input[data-toggle="returnComplement"]').val("");

                    specificWay.find('input[data-toggle="returnAddress"]').removeAttr('disabled');
                    specificWay.find('input[data-toggle="returnNeighborhood"]').removeAttr('disabled');
                    specificWay.find('input[data-toggle="returnCity"]').removeAttr('disabled');
                    specificWay.find('select[data-toggle="returnState"]').removeAttr('disabled');
                    specificWay.find('input[data-toggle="returnNumber"]').removeAttr('disabled');
                    specificWay.find('input[data-toggle="returnComplement"]').removeAttr('disabled');
                    specificWay.find('input[data-toggle="returnReference"]').removeAttr('disabled');

                    specificWay.find('input[data-toggle="returnNumber"]').focus();

                    //$('#load').hide();

                } else {
                    specificWay.find('input[data-toggle="returnAddress"]').removeAttr('disabled');
                    specificWay.find('input[data-toggle="returnNeighborhood"]').removeAttr('disabled');
                    specificWay.find('input[data-toggle="returnCity"]').removeAttr('disabled');
                    specificWay.find('select[data-toggle="returnState"]').removeAttr('disabled');
                    specificWay.find('input[data-toggle="returnNumber"]').removeAttr('disabled');
                    specificWay.find('input[data-toggle="returnComplement"]').removeAttr('disabled');
                    specificWay.find('input[data-toggle="returnReference"]').removeAttr('disabled');

                    specificWay.find('input[data-toggle="returnAddress"]').focus();

                    //$('#load').hide();
                }
            });
        }
    });


	//-- DATAPICKER
		//$('input[data-content="datepicker"]').bootstrapMaterialDatePicker({ weekStart : 0, time: false, locale: 'pt-br', lang: 'pt-br', format: 'DD/MM/YYYY'});

		//$('input[data-content="datepicker"]').bootstrapMaterialDatePicker('setDate', moment());

		//$('input[data-content="datepicker-start"]').bootstrapMaterialDatePicker({ weekStart : 0, time: false, locale: 'pt-br', lang: 'pt-br', format: 'DD/MM/YYYY'});
		//$('input[data-content="datepicker-start"]').bootstrapMaterialDatePicker('setDate', moment());
		//$('input[data-content="datepicker-finish"]').bootstrapMaterialDatePicker({ weekStart : 0, time: false, locale: 'pt-br', lang: 'pt-br', format: 'DD/MM/YYYY'});
		//$('input[data-content="datepicker-finish"]').bootstrapMaterialDatePicker('setDate', moment());
		
		
		$('body').on('focus', 'input[data-content="datepicker"]', function(){
			$(this).bootstrapMaterialDatePicker({ weekStart : 0, time: false, locale: 'pt-br', lang: 'pt-br', format: 'DD/MM/YYYY'});
			//$(this).bootstrapMaterialDatePicker('setDate', moment());
    });
    
    $('body').on('focus', 'input[data-content="datetimepicker"]', function(){
			$(this).bootstrapMaterialDatePicker({ weekStart : 0, time: true, locale: 'pt-br', lang: 'pt-br', format: 'DD/MM/YYYY HH:mm'});
			//$(this).bootstrapMaterialDatePicker('setDate', moment());
		});

		$('body').on('focus', 'input[data-content="datepicker-finish"]', function(){
			$(this).bootstrapMaterialDatePicker({ weekStart : 0, time: false, locale: 'pt-br', lang: 'pt-br', format: 'DD/MM/YYYY'});
		});

		$('body').on('focus', 'input[data-content="datepicker-start"]', function(){
			$('input[data-content="datepicker-finish"]').bootstrapMaterialDatePicker({ weekStart : 0, time: false, locale: 'pt-br', lang: 'pt-br', format: 'DD/MM/YYYY'});
			var id = $(this).data('id');
			$(this).bootstrapMaterialDatePicker({ weekStart : 0, time: false, locale: 'pt-br', lang: 'pt-br', format: 'DD/MM/YYYY'}).on('change', function(e, date){
				$('input[data-content="datepicker-finish"]').bootstrapMaterialDatePicker('setDate', date);
				$('input[data-content="datepicker-finish"]').bootstrapMaterialDatePicker('setMinDate', date);
			});
		});

		/* Portuguese initialisation for the jQuery UI date picker plugin. */

    $('#EditPassword').click(function() {
        $('input[name="data[User][password]"]').removeAttr('disabled');
    });

	//-- END DATAPICKER
});

function readURL(input, element) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#'+element).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function CreateDefaultEditor(id) {
  var myBaseUrl = '/';
  return CKEDITOR.replace( id, {
    height: 300,
    toolbarGroups : [
      { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
      { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
      { name: 'forms' },
      '/',
      { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
      { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
      { name: 'links' },
      { name: 'insert' },
      '/',
      { name: 'styles' },
      { name: 'colors' },
      { name: 'tools' },
      { name: 'others' }
    ],

    // Configure your file manager integration. This example uses CKFinder 3 for PHP. ../ para local
    filebrowserBrowseUrl: myBaseUrl + 'files/ckeditor',
    filebrowserImageBrowseUrl: myBaseUrl + 'files/ckeditor',

    filebrowserUploadUrl: myBaseUrl + 'files/ckeditor',
    filebrowserImageUploadUrl: myBaseUrl + 'files/ckeditor'
  });
}
