/*
* Embed Media Dialog based on http://www.fluidbyte.net/embed-youtube-vimeo-etc-into-ckeditor
*
* Plugin name:      mediaembed
* Menu button name: MediaEmbed
*
* Youtube Editor Icon
* http://paulrobertlloyd.com/
*
* @author Fabian Vogelsteller [frozeman.de]
* @version 0.5
*/
( function() {
    CKEDITOR.plugins.add( 'mediaembed',
    {
        icons: 'mediaembed', // %REMOVE_LINE_CORE%
        hidpi: true, // %REMOVE_LINE_CORE%
        init: function( editor )
        {
           var me = this;
           CKEDITOR.dialog.add( 'MediaEmbedDialog', function (instance)
           {
              return {
                 title : 'Incorporar',
                 minWidth : 550,
                 minHeight : 200,
                 contents :
                       [
                          {
                             id : 'iframe',
                             expand : true,
                             elements :[{
                                id : 'redeSocial',
                                type : 'select',
                                label : 'Escolha a rede social:',
                                items: [ [ 'Youtube' ], [ 'Twitter' ], [ 'Instagram' ], [ 'Spotify' ], [ 'Deezer' ] ],
                                'default': 'Youtube',
                                setup: function(element){
                                },
                                commit: function(element){
                                }
                              },
                              {
                                id : 'urlEmbed',
                                type : 'text',
                                label : 'Cole URL da Publicação:',
                                autofocus:'autofocus',
                                setup: function(element){
                                },
                                commit: function(element){
                                }
                              },
                              /*{
                                id : 'widthEmbed',
                                type : 'tel',
                                label : 'largura em px', 
                                setup: function(element){
                                },
                                commit: function(element){
                                }
                              }*/]
                          }
                       ],
                  onOk: function() {
                        var redeSocial = this.getContentElement('iframe', 'redeSocial').getValue();
                        var div = instance.document.createElement('div');
                        var url = encodeURI(this.getContentElement('iframe', 'urlEmbed').getValue());
                        var iframe;

                        if(redeSocial == 'Youtube'){

                           
                          var urlEmbed = url.replace('watch?v=', 'embed/');  
                          var urlarray = urlEmbed.split('&');

                          iframe = `
                          <div style="position: relative; width: 100%; max-width: 600px; margin-bottom:0px; margin-left:auto; margin-right:auto; margin-top:0px; overflow: hidden;">
                          <div style="padding-top: 56%;">
                          <div style="width: 100%; height: 100%; position: absolute; top: 0%; left: 0%;" >
                            <iframe width="100%" height="100%" src="${urlarray[0]}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                          </div>
                          </div>
                          </div> 
                          `;
                        }
                        
                        if(redeSocial == 'Twitter'){

                          var time = Date.now();

                          iframe = `
                          <div style="margin: 0 auto;" >
                          <iframe style="width: 100%; max-width: 500px; height:0;" id="twitter-${time}" 
                          src="api.dialbrasil.com.br/pages/twitter?time=${time}&url=${url}" 
                          frameborder="0"></iframe>
                          </div>
                          `;
                         } 
                        
                        if(redeSocial == 'Instagram'){

                          url += '/embed/captioned';
                          var urlEmbed = url.replace("//embed", "/embed");
                        
                          iframe = `<div style="margin-bottom:0px; margin-left:auto; margin-right:auto; margin-top:0px; max-width:400px; width:100%">
                          <iframe allowtransparency="true" scrolling="no" src="${urlEmbed}" width="100%" height="600" frameborder="0"></iframe> 
                          </div>
                          `;

                         } 

                         if(redeSocial == 'Spotify'){
 
                          var urlEmbed = url.replace("embed/", "");
                              urlEmbed = url.replace("spotify.com/", "spotify.com/embed/");
                        
                          iframe = `

                          <div style="margin-bottom:0px; margin-left:auto; margin-right:auto; margin-top:0px; max-width:400px; width:100%">
                          <iframe src="${urlEmbed}" width="100%" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                          </div>

                          `;
                         } 

                         if(redeSocial == 'Deezer'){

                          let urlEmbed = url.replace("https://www.deezer.com/br/", "");
                              urlEmbed = urlEmbed.split('/');
                        
                          iframe = `

                          <div style="margin-bottom:0px; margin-left:auto; margin-right:auto; margin-top:0px; max-width:400px; width:100%">
                          <iframe id="dzplayer" dztype="dzplayer" src="http://developers.deezer.com/br/plugins/player?playlist=true&width=100%&height=240&autoplay=false&type=${urlEmbed[0]}&id=${urlEmbed[1]}" scrolling="no" frameborder="0" style="border:none; overflow:hidden;" width="100%" height="240" allowTransparency="true"></iframe>
                          <iframe scrolling="no" frameborder="0" allowTransparency="true" src="https://www.deezer.com/plugins/player?format=classic&autoplay=true&playlist=false&width=600&height=350&color=007FEB&layout=dark&size=medium&type=${urlEmbed[0]}&id=${urlEmbed[1]}&app_id=1" width="100%" height="350"></iframe>
                          </div>
                          
                          `;

                         }

                        div.setHtml(iframe);
                        instance.insertElement(div);
                  }
              };
           } );

            editor.addCommand( 'MediaEmbed', new CKEDITOR.dialogCommand( 'MediaEmbedDialog',
                { allowedContent: 'iframe[*]' }
            ) );

            editor.ui.addButton( 'MediaEmbed',
            {
                label: 'Embed Youtube, Twitter, Instagram, Spotify ou Deezer',
                command: 'MediaEmbed',
                toolbar: 'mediaembed'
            } );
        }
    } );
} )();
