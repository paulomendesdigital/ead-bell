/*
* Embed Media Dialog based on http://www.fluidbyte.net/embed-youtube-vimeo-etc-into-ckeditor
*
* Plugin name:      mediaembed
* Menu button name: MediaEmbed
*
* Youtube Editor Icon
* http://paulrobertlloyd.com/
*
* @author Fabian Vogelsteller [frozeman.de]
* @version 0.5
*/
( function() {
    CKEDITOR.plugins.add( 'mediaembed',
    {
        icons: 'mediaembed', // %REMOVE_LINE_CORE%
        hidpi: true, // %REMOVE_LINE_CORE%
        init: function( editor )
        {
           var me = this;
           CKEDITOR.dialog.add( 'MediaEmbedDialog', function (instance)
           {
              return {
                 title : 'Incorporar',
                 minWidth : 550,
                 minHeight : 200,
                 contents :
                       [
                          {
                             id : 'iframe',
                             expand : true,
                             elements :[{
                                id : 'redeSocial',
                                type : 'select',
                                label : 'Escolha a rede social:',
                                items: [ [ 'Youtube' ], [ 'Twitter' ], [ 'Instagram' ] ],
                                'default': 'Youtube',
                                setup: function(element){
                                },
                                commit: function(element){
                                }
                              },
                              {
                                id : 'urlEmbed',
                                type : 'text',
                                label : 'Cole URL da Publicação:',
                                autofocus:'autofocus',
                                setup: function(element){
                                },
                                commit: function(element){
                                }
                              }]
                          }
                       ],
                  onOk: function() {
                        var redeSocial = this.getContentElement('iframe', 'redeSocial').getValue();
                        var div = instance.document.createElement('div');
                        var url = encodeURI(this.getContentElement('iframe', 'urlEmbed').getValue());
                        var iframe;

                        if(redeSocial == 'Youtube'){

                           
                          var urlEmbed = url.replace('watch?v=', 'embed/');  
                          var urlarray = urlEmbed.split('&');

                          iframe = `
                          <div style="position: relative; width: 100%; max-width: 600px; margin-bottom:0px; margin-left:auto; margin-right:auto; margin-top:0px; overflow: hidden;">
                          <div style="padding-top: 56%;">
                          <div style="width: 100%; height: 100%; position: absolute; top: 0%; left: 0%;" >
                            <iframe width="100%" height="100%" src="${urlarray[0]}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                          </div>
                          </div>
                          </div> 
                          `;
                        }
                        
                        if(redeSocial == 'Twitter'){

                         iframe = `
                         <div style="width: 100%; max-width: 555px; margin-bottom:0px; margin-left:auto; margin-right:auto; margin-top:0px; ">
                         <iframe class="dial-iframe-twitter" border=0 frameborder=0 height=350 width=100% src="https://twitframe.com/show?url=${url}"></iframe> 
                         </div>
                         `;
                         } 
                        
                        if(redeSocial == 'Instagram'){

                          url += '/embed/captioned';
                          var urlEmbed = url.replace("//embed", "/embed");
                        
                          iframe = `<div style="margin-bottom:0px; margin-left:auto; margin-right:auto; margin-top:0px; max-width:400px; width:100%">
                          <iframe allowtransparency="true" scrolling="no" src="${urlEmbed}" width="100%" height="600" frameborder="0"></iframe> 
                          </div>
                          `;
                         } 

                        div.setHtml(iframe);
                        instance.insertElement(div);
                  }
              };
           } );

            editor.addCommand( 'MediaEmbed', new CKEDITOR.dialogCommand( 'MediaEmbedDialog',
                { allowedContent: 'iframe[*]' }
            ) );

            editor.ui.addButton( 'MediaEmbed',
            {
                label: 'Embed Youtube, Twitter ou Instagram',
                command: 'MediaEmbed',
                toolbar: 'mediaembed'
            } );
        }
    } );
} )();
