<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\AccessOriginsComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\AccessOriginsComponent Test Case
 */
class AccessOriginsComponentTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Controller\Component\AccessOriginsComponent
     */
    public $AccessOrigins;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->AccessOrigins = new AccessOriginsComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AccessOrigins);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
