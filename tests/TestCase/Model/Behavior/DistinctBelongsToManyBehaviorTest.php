<?php
namespace App\Test\TestCase\Model\Behavior;

use App\Model\Behavior\DistinctBelongsToManyBehavior;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Behavior\DistinctBelongsToManyBehavior Test Case
 */
class DistinctBelongsToManyBehaviorTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Behavior\DistinctBelongsToManyBehavior
     */
    public $DistinctBelongsToMany;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->DistinctBelongsToMany = new DistinctBelongsToManyBehavior();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DistinctBelongsToMany);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
