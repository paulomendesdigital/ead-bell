<?php
namespace App\Test\TestCase\Model\Behavior;

use App\Model\Behavior\UuidBehavior;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Behavior\UuidBehavior Test Case
 */
class UuidBehaviorTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Behavior\UuidBehavior
     */
    public $Uuid;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Uuid = new UuidBehavior();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Uuid);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
