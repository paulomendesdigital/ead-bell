<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AdvertViewsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AdvertViewsTable Test Case
 */
class AdvertViewsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AdvertViewsTable
     */
    public $AdvertViews;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.AdvertViews',
        'app.AdvertZones',
        'app.AdvertCampaigns',
        'app.Adverts',
        'app.AdvertsAdvertZones'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AdvertViews') ? [] : ['className' => AdvertViewsTable::class];
        $this->AdvertViews = TableRegistry::getTableLocator()->get('AdvertViews', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AdvertViews);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
