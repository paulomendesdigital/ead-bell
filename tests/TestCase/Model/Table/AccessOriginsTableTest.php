<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AccessOriginsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AccessOriginsTable Test Case
 */
class AccessOriginsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AccessOriginsTable
     */
    public $AccessOrigins;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.AccessOrigins',
        'app.Participants',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AccessOrigins') ? [] : ['className' => AccessOriginsTable::class];
        $this->AccessOrigins = TableRegistry::getTableLocator()->get('AccessOrigins', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AccessOrigins);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
