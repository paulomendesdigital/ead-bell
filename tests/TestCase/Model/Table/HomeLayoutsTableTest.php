<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HomeLayoutsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HomeLayoutsTable Test Case
 */
class HomeLayoutsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\HomeLayoutsTable
     */
    public $HomeLayouts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.HomeLayouts',
        'app.Companies',
        'app.HomeLayoutItems'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('HomeLayouts') ? [] : ['className' => HomeLayoutsTable::class];
        $this->HomeLayouts = TableRegistry::getTableLocator()->get('HomeLayouts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->HomeLayouts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
