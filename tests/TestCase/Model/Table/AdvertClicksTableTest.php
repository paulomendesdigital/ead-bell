<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AdvertClicksTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AdvertClicksTable Test Case
 */
class AdvertClicksTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AdvertClicksTable
     */
    public $AdvertClicks;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.AdvertClicks',
        'app.AdvertZones',
        'app.AdvertCampaigns',
        'app.Adverts',
        'app.AdvertsAdvertZones',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AdvertClicks') ? [] : ['className' => AdvertClicksTable::class];
        $this->AdvertClicks = TableRegistry::getTableLocator()->get('AdvertClicks', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AdvertClicks);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
