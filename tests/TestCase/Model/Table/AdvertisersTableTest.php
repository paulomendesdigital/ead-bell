<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AdvertisersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AdvertisersTable Test Case
 */
class AdvertisersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AdvertisersTable
     */
    public $Advertisers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Advertisers',
        'app.AdvertCampaigns'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Advertisers') ? [] : ['className' => AdvertisersTable::class];
        $this->Advertisers = TableRegistry::getTableLocator()->get('Advertisers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Advertisers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
