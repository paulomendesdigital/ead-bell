<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserProvidersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserProvidersTable Test Case
 */
class UserProvidersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UserProvidersTable
     */
    public $UserProviders;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UserProviders',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UserProviders') ? [] : ['className' => UserProvidersTable::class];
        $this->UserProviders = TableRegistry::getTableLocator()->get('UserProviders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserProviders);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
