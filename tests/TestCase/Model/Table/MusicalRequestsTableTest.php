<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MusicalRequestsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MusicalRequestsTable Test Case
 */
class MusicalRequestsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\MusicalRequestsTable
     */
    public $MusicalRequests;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.MusicalRequests'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MusicalRequests') ? [] : ['className' => MusicalRequestsTable::class];
        $this->MusicalRequests = TableRegistry::getTableLocator()->get('MusicalRequests', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MusicalRequests);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
