<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LikeMusicsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LikeMusicsTable Test Case
 */
class LikeMusicsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\LikeMusicsTable
     */
    public $LikeMusics;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.LikeMusics',
        'app.Companies',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('LikeMusics') ? [] : ['className' => LikeMusicsTable::class];
        $this->LikeMusics = TableRegistry::getTableLocator()->get('LikeMusics', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LikeMusics);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
