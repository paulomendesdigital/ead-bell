<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AdvertZonesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AdvertZonesTable Test Case
 */
class AdvertZonesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AdvertZonesTable
     */
    public $AdvertZones;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.AdvertZones',
        'app.Companies',
        'app.AdvertViews',
        'app.Adverts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AdvertZones') ? [] : ['className' => AdvertZonesTable::class];
        $this->AdvertZones = TableRegistry::getTableLocator()->get('AdvertZones', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AdvertZones);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
