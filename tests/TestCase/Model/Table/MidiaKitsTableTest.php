<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MidiaKitsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MidiaKitsTable Test Case
 */
class MidiaKitsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\MidiaKitsTable
     */
    public $MidiaKits;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.MidiaKits',
        'app.Companies'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('MidiaKits') ? [] : ['className' => MidiaKitsTable::class];
        $this->MidiaKits = TableRegistry::getTableLocator()->get('MidiaKits', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MidiaKits);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
