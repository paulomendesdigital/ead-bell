<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HomeLayoutItemsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HomeLayoutItemsTable Test Case
 */
class HomeLayoutItemsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\HomeLayoutItemsTable
     */
    public $HomeLayoutItems;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.HomeLayoutItems',
        'app.HomeLayouts',
        'app.Contents'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('HomeLayoutItems') ? [] : ['className' => HomeLayoutItemsTable::class];
        $this->HomeLayoutItems = TableRegistry::getTableLocator()->get('HomeLayoutItems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->HomeLayoutItems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
