<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AdvertsAdvertZonesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AdvertsAdvertZonesTable Test Case
 */
class AdvertsAdvertZonesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AdvertsAdvertZonesTable
     */
    public $AdvertsAdvertZones;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.AdvertsAdvertZones',
        'app.Adverts',
        'app.AdvertZones'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AdvertsAdvertZones') ? [] : ['className' => AdvertsAdvertZonesTable::class];
        $this->AdvertsAdvertZones = TableRegistry::getTableLocator()->get('AdvertsAdvertZones', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AdvertsAdvertZones);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
