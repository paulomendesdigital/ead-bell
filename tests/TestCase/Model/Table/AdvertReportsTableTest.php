<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AdvertReportsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AdvertReportsTable Test Case
 */
class AdvertReportsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AdvertReportsTable
     */
    public $AdvertReports;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.AdvertReports',
        'app.Models',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AdvertReports') ? [] : ['className' => AdvertReportsTable::class];
        $this->AdvertReports = TableRegistry::getTableLocator()->get('AdvertReports', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AdvertReports);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
