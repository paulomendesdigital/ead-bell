<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WebdoorsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WebdoorsTable Test Case
 */
class WebdoorsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\WebdoorsTable
     */
    public $Webdoors;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Webdoors',
        'app.Companies'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Webdoors') ? [] : ['className' => WebdoorsTable::class];
        $this->Webdoors = TableRegistry::getTableLocator()->get('Webdoors', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Webdoors);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
