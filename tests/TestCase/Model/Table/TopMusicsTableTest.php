<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TopMusicsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TopMusicsTable Test Case
 */
class TopMusicsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\TopMusicsTable
     */
    public $TopMusics;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.TopMusics',
        'app.Companies'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TopMusics') ? [] : ['className' => TopMusicsTable::class];
        $this->TopMusics = TableRegistry::getTableLocator()->get('TopMusics', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TopMusics);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
