<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConfigurationParametersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConfigurationParametersTable Test Case
 */
class ConfigurationParametersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ConfigurationParametersTable
     */
    public $ConfigurationParameters;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ConfigurationParameters',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ConfigurationParameters') ? [] : ['className' => ConfigurationParametersTable::class];
        $this->ConfigurationParameters = TableRegistry::getTableLocator()->get('ConfigurationParameters', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ConfigurationParameters);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
