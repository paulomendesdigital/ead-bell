<?php
namespace App\Test\TestCase\View\Helper;

use App\View\Helper\AdvertReportsHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\AdvertReportsHelper Test Case
 */
class AdvertReportsHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\AdvertReportsHelper
     */
    public $AdvertReports;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->AdvertReports = new AdvertReportsHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AdvertReports);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
