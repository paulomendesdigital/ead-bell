<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AdvertClicksFixture
 */
class AdvertClicksFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'advert_zone_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'advert_campaign_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'advert_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'advert_advert_zone_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'sessionid' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'advert_clicks_FK' => ['type' => 'index', 'columns' => ['advert_id'], 'length' => []],
            'advert_clicks_FK_1' => ['type' => 'index', 'columns' => ['advert_zone_id'], 'length' => []],
            'advert_clicks_FK_2' => ['type' => 'index', 'columns' => ['advert_campaign_id'], 'length' => []],
            'advert_clicks_FK_3' => ['type' => 'index', 'columns' => ['advert_advert_zone_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'advert_clicks_FK' => ['type' => 'foreign', 'columns' => ['advert_id'], 'references' => ['adverts', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'advert_clicks_FK_1' => ['type' => 'foreign', 'columns' => ['advert_zone_id'], 'references' => ['advert_zones', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'advert_clicks_FK_2' => ['type' => 'foreign', 'columns' => ['advert_campaign_id'], 'references' => ['advert_campaigns', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'advert_clicks_FK_3' => ['type' => 'foreign', 'columns' => ['advert_advert_zone_id'], 'references' => ['adverts_advert_zones', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'advert_zone_id' => 1,
                'advert_campaign_id' => 1,
                'advert_id' => 1,
                'advert_advert_zone_id' => 1,
                'sessionid' => 'Lorem ipsum dolor sit amet',
                'created' => '2020-11-16 10:18:22',
                'modified' => '2020-11-16 10:18:22',
            ],
        ];
        parent::init();
    }
}
